//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "Ntuple.h"

ClassImp(Ntuple)

////////////////////////////////////////////////////////////////////////////////////
Ntuple::Ntuple(const int aNstreams){ 
////////////////////////////////////////////////////////////////////////////////////

  // Ntuple conventions
  // ex N=4
  // stream index (s): s=0, s=1, s=2, s=3
  // types (N+1) t = 0, 1, 2, 3 or 4
  // number of combinations of type t: Ncomb[0]=1, Ncomb[1]=4, Ncomb[2]=6, Ncomb[3]=4, Ncomb[4]=1
  
  // number of streams
  if(aNstreams<=0 || aNstreams>=NETMAX){
    cerr<<"Ntuple::Ntuple: the number of streams is incorrect --> set it to 0"<<endl;
    Nstreams=0;
  }
  else
    Nstreams=aNstreams;

  // number of combinations per type
  for(int t=0; t<Nstreams+1; t++)
    Ncomb[t]=TMath::Factorial(Nstreams)/TMath::Factorial(t)/TMath::Factorial(Nstreams-t);
    
  // create 2^N tuples
  ntuples=(int)pow(2.0,(double)Nstreams);
  tuple=new Tuple* [ntuples];
  int ni; vector <int> sindex;
  for(int n=0; n<ntuples; n++){
    sindex.clear();
    for(int s=0; s<Nstreams; s++){
      ni=(int)pow(2.0,(double)s);
      if((n&ni)==ni) sindex.push_back(s);
    }
    tuple[n] = new Tuple(sindex);
  }
  sindex.clear();
  
}

////////////////////////////////////////////////////////////////////////////////////
Ntuple::~Ntuple(void){
////////////////////////////////////////////////////////////////////////////////////
  for(int n=0; n<ntuples; n++) delete tuple[n];
  delete tuple;
}

////////////////////////////////////////////////////////////////////////////////////
bool Ntuple::IsStreamInside(const int aStreamIndex, const int aTupleIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aTupleIndex<0||aTupleIndex>=ntuples){
    cerr<<"Ntuple::IsStreamInside: the tuple index "<<aTupleIndex<<" does not exist"<<endl;
    return false;
  }
  return tuple[aTupleIndex]->IsStreamInside(aStreamIndex);
}

////////////////////////////////////////////////////////////////////////////////////
vector <int> Ntuple::GetEvents(const int aTupleIndex, const int aStreamIndex){
////////////////////////////////////////////////////////////////////////////////////
  vector <int> vnull;
  if(aTupleIndex<0||aTupleIndex>=ntuples){
    cerr<<"Ntuple::GetEvents: the tuple index "<<aTupleIndex<<" does not exist"<<endl;
    return vnull;
  }
  if(!tuple[aTupleIndex]->IsStreamInside(aStreamIndex)){
    cerr<<"Ntuple::GetEvents: the stream index "<<aStreamIndex<<" does not exist"<<endl;
    return vnull;
  }

  return tuple[aTupleIndex]->GetEventsFromStream(aStreamIndex);
}

////////////////////////////////////////////////////////////////////////////////////
bool Ntuple::AddPair(const int aStreamIndex0, const int aStreamIndex1, const int aEventIndex0, const int aEventIndex1){
////////////////////////////////////////////////////////////////////////////////////
  // check streams
  if( (aStreamIndex0 < 0) || (aStreamIndex1 < 0) || (aStreamIndex1 >= Nstreams) || (aStreamIndex1 <= aStreamIndex0) ){
    cerr<<"Ntuple::AddPair: the pair ("<<aStreamIndex0<<","<<aStreamIndex1<<") does not exist"<<endl;
    return false;
  }
  
  // net value
  int net = (int)pow(2.0,(double)aStreamIndex0) + (int)pow(2.0,(double)aStreamIndex1);
 
  // add pair to 2-tuple
  vector<int> ev;
  ev.push_back(aEventIndex0);
  ev.push_back(aEventIndex1);
  if(!tuple[net]->AddTuple(ev)) return false;
  ev.clear();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Ntuple::MakeNtuple(const int aNmax, const bool aUniq){
////////////////////////////////////////////////////////////////////////////////////
  if(aNmax>Nstreams){
    cerr<<"Ntuple::MakeNtuple: the aNmax value is incorrect"<<endl;
    return false;
  }
  
  // local variables
  vector <int> insidenet, newtuple;
  int *ncombmax, *combmin, *comb;
  int ntest;
  int ie, js, je;
  bool gotonextperm;

  for(int t=3; t<=aNmax; t++){// loop over higher-type Ntuple (types 3->Nmax)
    for(int n=0; n<ntuples; n++){
      if(GetType(n)!=t) continue; // select only type = t

      // reset tuple
      tuple[n]->ResetEvents();

      // search for (t-1) networks contained in network n
      insidenet.clear();
      for(int n2=0; n2<ntuples; n2++){
	if(GetType(n2)!=t-1) continue; // select only type = t-1
	if((n2&n)==n2) insidenet.push_back(n2);
      }

      // make sure that events exist for all subnets
      ntest=0;
      for(int c=0; c<t; c++){
	if(!tuple[insidenet[c]]->GetNevents()){
	  ntest=1;
	  break;
	}
      }
      if(ntest) continue;
       
      // init combination vectors
      comb = new int [t];
      combmin = new int [t];
      ncombmax = new int [t];
      for(int c=0; c<t; c++){
	comb[c]=0;
	combmin[c]=0;
	ncombmax[c]=tuple[insidenet[c]]->GetNevents();
      }
      comb[t-1]=-1;
       
      while(NextPerm(t,comb,ncombmax,combmin)){
	
	// use the first subnet as reference to optimize the last stream
	if(tuple[insidenet[0]]->GetEvent(1,comb[0])>tuple[insidenet[t-1]]->GetEvent(0,comb[t-1]))
	  continue; // before -> keep going
	if(tuple[insidenet[0]]->GetEvent(1,comb[0])<tuple[insidenet[t-1]]->GetEvent(0,comb[t-1])){
	  comb[t-1]=ncombmax[t-1]; // after -> finish the last column
	  continue;
	}

	// find optimal start for last subnet
	if(comb[0]){
	  if(tuple[insidenet[0]]->GetEvent(0,comb[0])>tuple[insidenet[0]]->GetEvent(0,comb[0]-1)){
	    for(int e=combmin[t-1]; e<ncombmax[t-1]; e++){
	      if(tuple[insidenet[t-1]]->GetEvent(0,e)==tuple[insidenet[0]]->GetEvent(1,comb[0])){
		combmin[t-1]=e;
		break;
	      }
	    }
	  }
	}

	gotonextperm=false;
	
	// use the first subnet as reference to optimize the starting point
	for(int tt=1; tt<t-1; tt++){
	  if(tuple[insidenet[0]]->GetEvent(0,comb[0])>tuple[insidenet[tt]]->GetEvent(0,comb[tt])){
	    combmin[tt]=comb[tt]+1;
	    comb[t-1]=0;// the last stream should not be implemented
	    gotonextperm=true;
	  }
	}
	
	// use the first subnet as reference to optimize the ending point
	for(int tt=1; tt<t-1; tt++){
	  if(tuple[insidenet[0]]->GetEvent(0,comb[0])<tuple[insidenet[tt]]->GetEvent(0,comb[tt])){
	    gotonextperm=true;
	    break;
	  }
	}
	
	// don't waste time on this perm
	if(gotonextperm) continue;

	ntest=0;
	for(int is=0; is<t-1; is++){// t=insidenet.size() always!
	  for(int j=0; j<t; j++){
	    if(j==t-1-is) continue;
	    if(is==t-j-2) ie=is+2;
	    else ie=is+1;
	    if(ie>=t) continue;
	    if(j>t-1-is) js=j-1;
	    else js=j;
	    if(j>t-1-ie) je=j-1;
	    else je=j;
	    if(tuple[insidenet[is]]->GetEvent(js,comb[is])==tuple[insidenet[ie]]->GetEvent(je,comb[ie])) ntest++;
	  }
	}
	if(ntest==t*(t-2)){// we found a t-tuple!
	  newtuple.clear();
	  for(int s=0; s<t; s++){
	    if(s<t-1) newtuple.push_back(tuple[insidenet[0]]->GetEvent(s,comb[0]));
	    else newtuple.push_back(tuple[insidenet[1]]->GetEvent(s-1,comb[1]));
	  }
	  tuple[n]->AddTuple(newtuple);
	}
      }
      delete comb;
      delete combmin;
      delete ncombmax;
    }
  }

  // cleaning
  insidenet.clear();
  newtuple.clear();

  // remove redundancy?
  if(aUniq) return MakeUniq(aNmax);
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Ntuple::MakeUniq(const int aNmax){
////////////////////////////////////////////////////////////////////////////////////

  // local variables
  vector <int> insidestreams;
  int ntest;
  int ep1start;

  for(int t=2; t<aNmax; t++){
    for(int n=0; n<ntuples; n++){// loop over networks of type t
      if(GetType(n)!=t) continue; // select only type = t
      
      // get stream numbers in network n
      insidestreams.clear();
      for(int s=0; s<Nstreams; s++)
	if(tuple[n]->IsStreamInside(s)) insidestreams.push_back(s);
      
      // look at (n+1) nets
      for(int np1=0; np1<ntuples; np1++){
	if(GetType(np1)!=t+1) continue;
	if((n&np1)==n){// n is a subnet of n+1
	  ep1start=0;
	  
	  // loop over events in net n
	  for(int e=0; e<tuple[n]->GetNevents(); e++){

	    // compare with events in net n+1
	    for(int ep1=ep1start; ep1<tuple[np1]->GetNevents(); ep1++){
	      // after -> stop
	      if(tuple[np1]->GetEventFromStream(insidestreams[0],ep1)>tuple[n]->GetEventFromStream(insidestreams[0],e)) break;
	      ntest=0;
	      for(int s=0; s<t; s++)
		if(tuple[n]->GetEventFromStream(insidestreams[s],e)==tuple[np1]->GetEventFromStream(insidestreams[s],ep1)) ntest++;
	      if(ntest==t){
		tuple[n]->RemoveEvent(e);
		e--;// don't move!
		ep1start=ep1;
		break;
	      }
	    }   
	  }

	}
      }

    }
  }

  insidestreams.clear();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Ntuple::PrintEvents(void){
////////////////////////////////////////////////////////////////////////////////////

  for(int t=Nstreams; t>=0; t--){// decreasing types
    for(int n=0; n<ntuples; n++){// loop over networks of type t
      if(GetType(n)==t){ // select only type = t
	cout<<"\n********* "<<GetNevents(n)<<" events for tuple #"<<n<<" ("<<tuple[n]->GetName()<<") *********"<<endl;
	tuple[n]->PrintEvents();
      }
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
Tuple::Tuple(vector <int> aStreamIndex){ 
////////////////////////////////////////////////////////////////////////////////////
  StreamIndex=aStreamIndex;
  tname="NULL";
  if(StreamIndex.size()){

    ostringstream tname_tmp;
    for(int s=0; s<(int)StreamIndex.size(); s++) tname_tmp<<StreamIndex[s];
    tname=tname_tmp.str();

    int p=0;
    for(int s=0; s<=StreamIndex[StreamIndex.size()-1]; s++){
      if(s==StreamIndex[p]){ IndexFromStream.push_back(p); p++; }
      else IndexFromStream.push_back(-1);
    }
  }
}
  
////////////////////////////////////////////////////////////////////////////////////
Tuple::~Tuple(void){
////////////////////////////////////////////////////////////////////////////////////
  for(int s=0; s<NETMAX; s++) Event[s].clear();
  StreamIndex.clear();
  IndexFromStream.clear();
}

