/**
 * @file 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Coinc2__
#define __Coinc2__

#include "GwollumPlot.h"
#include "ReadTriggers.h"
#include "MakeTriggers.h"
#include "TriggerSelect.h"

using namespace std;

/**
 * @brief Set two trigger sets in coincidence.
 * @details The coincidence algorithm includes a time and a frequency coincidence scheme.
 * @author Florent Robinet
 */
class Coinc2: public GwollumPlot {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Coinc2 class.
   * @details Sets parameters to default:
   * - coincidence time distance = 0.0 s (see SetCoincDt())
   * - coincidence frequency distance = 0.0 Hz (see SetCoincDf())
   * - coincidence frequency asymmetry = 1.0 (see SetCoincAf())
   * - no time offset applied to the clusters (see SetTriggerSamples())
   *
   * Two input triggger samples must be given. They are indexed 0 and 1. Triggers must be clustered before using this class! Triggers must not be changed when using the Coinc2 class methods. Use SetTriggerSamples() to define the input trigger samples.
   * @param[in] aVerbose verbosity level.
   */
  Coinc2(const int aVerbose=0);

  /**
   * @brief Constructor of the Coinc2 class.
   * @details Sets parameters to default:
   * - coincidence time distance = 0.0s (see SetCoincDt())
   * - coincidence frequency distance = 0.0Hz (see SetCoincDf())
   * - coincidence frequency asymmetry = 1.0 (see SetCoincAf())
   * - no time offset applied to the cluster (see SetTriggerSamples())
   *
   * Two input triggger samples must be given. They are indexed 0 and 1. Triggers must be clustered before using this class! Triggers must not be changed when using the Coinc2 class methods. If needed, use SetTriggerSamples() to change the input trigger samples defined in this constructor.
   * @param[in] aTriggerSample0 first input trigger sample
   * @param[in] aTriggerSample1 second input trigger sample
   * @param[in] aVerbose verbosity level
   */
  Coinc2(ReadTriggers *aTriggerSample0, ReadTriggers *aTriggerSample1, const int aVerbose=0);

  /**
   * @brief Destructor of the Coinc2 class.
   */
  virtual ~Coinc2(void);
  /**
     @}
  */

  /**
   * @brief Sets trigger samples.
   * @details This function is used to define the input trigger samples. This function is also to be used after any change performed in the trigger sample (with ReadTriggers methods). Finally, this function can be used to apply a time offset to the clusters.
   * - coinc segments are defined as the intersection of both trigger samples (using time offsets and user time segements).
   * - previous coinc events are deleted from memory
   * - clusters are tagged: 1 if the peak time is inside a coinc segment, -1 otherwise (the cluster is discarded by the methods of this class).
   * - coinc monitors are reset
   *
   * @warning Input triggers must be clustered first!
   *
   * @warning By default, if clusters are tagged with -1 value before calling this function, they will not be considered at all, even if inside a coinc segment. This behavior can be canceled with the aIgnoreMinusOnes argument.
   * @param[in] aTriggerSample0 pointer to the first trigger object
   * @param[in] aTriggerSample1 pointer to the second trigger object
   * @param[in] aTimeOffset0 time offset to apply to the clusters of the first sample
   * @param[in] aTimeOffset1 time offset to apply to the clusters of the second sample
   * @param[in] aValidSegments list of segments where to consider clusters (before time offset!)
   * @param[in] aIgnoreMinusOnes if this parameter is set to true, clusters tagged with -1 are ignored
   */
  bool SetTriggerSamples(ReadTriggers *aTriggerSample0, ReadTriggers *aTriggerSample1, const double aTimeOffset0=0.0, const double aTimeOffset1=0.0, Segments *aValidSegments=NULL, const bool aIgnoreMinusOnes=true);

  /**
   * @brief Runs the reference coincidence algorithm.
   * @details Returns the number of coinc events. Returns -1 if this function fails.
   *
   * 2 clusters are said to coincide if 3 conditions are verified:
   * - the time distance between the cluster time limits is smaller than the coinc parameter set with SetCoincDt() (note that the time offset defined with SetTriggerSamples() is applied).
   * - the frequency distance between the cluster frequency limits is smaller than the coinc parameter set with SetCoincDf().
   * - the frequency asymmetry between the cluster peak frequency is smaller or equal to the coinc parameter set with SetCoincAf().
   *
   * Coinc events can be tagged (see SetCoincTag()), by default, all coincs are tagged to true.
   */
  int MakeCoinc(void);

  /**
   * @brief Runs the 0-type coincidence algorithm.
   * @details Returns the number of coinc events. Returns -1 if this function fails.
   *
   * For this coincidence algorithm, a time window is defined around the peak time of cluster (0). By default the time window is symmetric around the peak time and the window width is given by the time parameter set with SetCoincDt(). However, if the window size exceeds the size of cluster (0), the window is ajusted back to the cluster time edges.
   *
   * 2 clusters (0) and (1) are said to coincide if the cluster peak time of (1) is inside the time window defined with (0) (note that the time offset defined with SetTriggerSamples() is applied).
   *
   * @note there is no frequency coincidence.
   *
   * Coinc events can be tagged (see SetCoincTag()), by default, all coincs are tagged to true.
   */
  int MakeCoinc0(void);

  /**
   * @brief Prints the list of coinc events.
   */
  void PrintCoinc(void);

  /**
   * @brief Prints the list of clusters which do not participate to a coinc event.
   * @details Clusters which do not participate to a coinc event are printed in the standard output (time/frequency/SNR).
   * @warning Only active clusters are printed.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   */
  void PrintNoCoinc(const int aSampleIndex);

  /**
   * @brief Writes coinc events on disk.
   * @details Two trigger structures (See MakeTriggers) are saved in two separate root directories (clusters_0 and clusters_1). The trigger structures contain parameters for clusters involved in a coinc event.
   * Only coinc with a tag set to true are saved, see SetCoincTag().
   * Additionally, a tree named coinc2param is saved. It contains a single entry with the parameters used for the coincidence (time and frequency windows, time offsets etc.).
   * @param[in] aOutDir output directory (must exist)
   * @param[in] aOutFileName output file name (do not provide an extension)
   */
  bool WriteCoinc(const string aOutDir, const string aOutFileName);

  /**
   * @brief Sets the maximum time distance to define a coinc.
   * @param[in] aDt maximum time distance value [s]
   */
  inline void SetCoincDt(const double aDt){ coinc_dt=aDt; return; };

  /**
   * @brief Sets the maximum frequency distance to define a coinc.
   * @param[in] aDf maximum frequency distance value [Hz]
   */
  inline void SetCoincDf(const double aDf){ coinc_df=aDf; return; };

  /**
   * @brief Sets the maximum frequency asymmetry to define a coinc.
   * @param[in] aDf maximum frequency asymmetry value [between 0 and 1]
   */
  inline void SetCoincAf(const double aAf){ coinc_af=aAf; return; };

  /**
   * @brief Returns the current number of coinc events.
   */
  inline int GetNCoinc(void){ return (int)CoC[0].size(); };

  /**
   * @brief Returns the cluster index involved in a given coinc event.
   * @warning The validity of indexes is not checked.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   * @param[in] aCoincIndex coinc index
   */
  inline int GetClusterIndex(const int aSampleIndex, const int aCoincIndex){ 
    return CoC[aSampleIndex][aCoincIndex];
  };

  /**
   * @brief Sets a new tag to a given coinc.
   * @warning The validity of indexes is not checked.
   * @param[in] aCoincIndex coinc index
   * @param[in] aNewTag new tag value
   */
  inline void SetCoincTag(const int aCoincIndex, const bool aNewTag){ 
    CoTag[aCoincIndex]=aNewTag;
  };

  /**
   * @brief Sets a new tag for all coincs.
   * @param[in] aNewTag new tag value
   */
  inline void SetCoincTag(const bool aNewTag){ 
    for(int c=0; c<(int)CoC[0].size(); c++) CoTag[c]=aNewTag;
  };

  /**
   * @brief Gets the tag value of a given coinc.
   * @warning The validity of indexes is not checked.
   * @param[in] aCoincIndex coinc index
   */
  inline bool GetCoincTag(const int aCoincIndex){ return CoTag[aCoincIndex]; };

  /**
   * @brief Gets the number of active clusters.
   * @warning The validity of indexes is not checked.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   */
  inline int GetNActiveClusters(const int aSampleIndex){ 
    return nactive[aSampleIndex];
  };

  /**
   * @brief Returns the time offset of a trigger sample.
   * @warning The validity of indexes is not checked.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   */
  inline double GetTimeOffset(const int aSampleIndex){ 
    return toffset[aSampleIndex];
  };

  /**
   * @brief Returns the time offset of a trigger sample.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   */
  inline double GetCoincDt(void){ return coinc_dt; };

  /**
   * @brief Returns the live time of coinc segments.
   */
  inline double GetCoincLiveTime(void){ return CoSeg->GetLiveTime(); };

  /**
   * @brief Sets a minimum frequency for a given cluster sample.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   * @param[in] aFreqMin minimum frequency [Hz]
   */
  inline bool SetFrequencyMin(const int aSampleIndex, const double aFreqMin){ 
    if(aSampleIndex < 0 || aSampleIndex > 1) return false;
    freqmin[aSampleIndex]=aFreqMin;
    return true;
  };

  /**
   * @brief Sets a maximum frequency for a given cluster sample.
   * @param[in] aSampleIndex trigger sample index: 0 or 1
   * @param[in] aFreqMax maximum frequency [Hz]
   */
  inline bool SetFrequencyMax(const int aSampleIndex, const double aFreqMax){ 
    if(aSampleIndex < 0 || aSampleIndex > 1) return false;
    freqmax[aSampleIndex]=aFreqMax;
    return true;
  };

  /**
   * @brief Produce plots for coinc events.
   * @details List of plots:
   * - Fraction of coincident events as function of SNR and frequency
   * - SNR vs SNR for coinc events
   * - Frequency vs frequency for coinc events
   * - SNR vs time for clusters of trigger sample 0
   * - SNR vs time for clusters of trigger sample 1
   * - Frequency vs time for clusters of trigger sample 0
   * - Frequency vs time for clusters of trigger sample 1
   *
   * @warning Only valid coinc events are plotted.
   */
  bool MakeComparators(void);

  /**
   * @brief Prints plots for coinc events.
   * @details After calling MakeComparators(), the plots are printed in png files.
   * @param[in] aFileName Output file name. Do not provide the file extension, it will be automatically added. For example: `"/path/to/directory/filename"`.
   */
  void PrintComparators(const string aFileName);

 protected:

  // GENERAL
  int verbosity;              ///< verbosity level

  // INPUT
  ReadTriggers *triggers[2];  ///< trigger samples
  double toffset[2];          ///< time offset
  int nactive[2];             ///< number of active clusters
  double freqmin[2];          /// frequency min selection
  double freqmax[2];          /// frequency max selection

  // COINC
  vector <int> CoC[2];        ///< coinc cluster index
  bool *CoTag;                ///< coinc tag
  Segments *CoSeg;            ///< coinc segments
  TTree *CoTree;              ///< coinc tree

  // PARAMETERS
  double coinc_dt;            ///< coinc time distance
  double coinc_df;            ///< coinc frequency distance
  double coinc_af;            ///< coinc frequency asymmetry

  // COMPARATORS
  TH1D *hc_snrfrac[2];        ///< coinc fraction vs SNR
  TH1D *hc_freqfrac[2];       ///< coinc fraction vs frequency
  TGraph *gc_snrtime[4];      ///< SNR vs time
  TGraph *gc_freqtime[4];     ///< freq vs time
  TGraph *gc_snrsnr;          ///< SNR vs SNR
  TGraph *gc_freqfreq;        ///< Frequency vs frequency


 private:

  /**
   * @brief Select clusters in coinc segments.
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   * @param[in] aIgnoreMinusOnes Flag to also select clusters tagged with -1.
   */
  void SelectClusters(const int aSampleIndex, const bool aIgnoreMinusOnes=true);
  
  ClassDef(Coinc2,0)  
};

#endif


