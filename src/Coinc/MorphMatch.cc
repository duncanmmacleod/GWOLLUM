//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "MorphMatch.h"

ClassImp(MorphMatch)

////////////////////////////////////////////////////////////////////////////////////
MorphMatch::MorphMatch(EventMap *aTriggerSample0, EventMap *aTriggerSample1, const int aVerbose): Coinc2(aTriggerSample0, aTriggerSample1, aVerbose) {
////////////////////////////////////////////////////////////////////////////////////

  // attach branches
  CoTree->Branch("MorphMatch",      &CoMatch,      "MorphMatch/D");
  CoTree->Branch("MorphMatchSumw2", &CoMatchSumw2, "MorphMatchSumw2/D");
  CoTree->Branch("MorphMatchShift", &CoMatchShift, "MorphMatchShift/D");
  
  // get map triggers
  // all the checks are made by Coinc2
  triggers[0]=aTriggerSample0;
  triggers[1]=aTriggerSample1;
}

////////////////////////////////////////////////////////////////////////////////////
MorphMatch::~MorphMatch(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>1) cout<<"MorphMatch::~MorphMatch"<<endl;
}

////////////////////////////////////////////////////////////////////////////////////
int MorphMatch::ComputeMorphMatch(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!GetNCoinc()) return 0;

  // check map parameters
  if(triggers[0]->GetMapTimeRange(0)!=triggers[1]->GetMapTimeRange(0)){
    cerr<<"MorphMatch::ComputeMorphMatch: Maps do not have the same time range"<<endl;
    return -1;
  }
  if(triggers[0]->GetMapTimeResolution(0)!=triggers[1]->GetMapTimeResolution(0)){
    cerr<<"MorphMatch::ComputeMorphMatch: Maps do not have the same time resolution"<<endl;
    return -1;
  }
  if(triggers[0]->GetMapFrequencyMin(0)!=triggers[1]->GetMapFrequencyMin(0)){
    cerr<<"MorphMatch::ComputeMorphMatch: Maps do not have the same frequency range"<<endl;
    return -1;
  }
  if(triggers[0]->GetMapFrequencyMax(0)!=triggers[1]->GetMapFrequencyMax(0)){
    cerr<<"MorphMatch::ComputeMorphMatch: Maps do not have the same frequency range"<<endl;
    return -1;
  }
  if(triggers[0]->GetMapFrequencyResolution(0)!=triggers[1]->GetMapFrequencyResolution(0)){
    cerr<<"MorphMatch::ComputeMorphMatch: Maps do not have the same frequency resolution"<<endl;
    return -1;
  }

  double tcenter;

  double res=(double)triggers[0]->GetMapTimeResolution(0)*(double)triggers[0]->GetMapFrequencyResolution(0);

  // optimize speed
  triggers[0]->SetTriggerBranchStatus("time",false);
  triggers[1]->SetTriggerBranchStatus("time",false);
  triggers[0]->SetTriggerBranchStatus("frequency",false);
  triggers[1]->SetTriggerBranchStatus("frequency",false);
  triggers[0]->SetTriggerBranchStatus("phase",false);
  triggers[1]->SetTriggerBranchStatus("phase",false);
  triggers[0]->SetTriggerBranchStatus("tstart_ms",false);
  triggers[1]->SetTriggerBranchStatus("tstart_ms",false);
  
  // loop over coinc
  for(int c=0; c<(int)CoC[0].size(); c++){
    if(!GetCoincTag(c)) continue;

    // use peak times to define map centers
    tcenter = (triggers[0]->GetClusterTime(CoC[0][c])+toffset[0]+triggers[1]->GetClusterTime(CoC[1][c])+toffset[1])/2.0;

    // make maps
    if(!triggers[0]->BuildClusterMap(0,CoC[0][c],tcenter-toffset[0])) continue;
    if(!triggers[1]->BuildClusterMap(0,CoC[1][c],tcenter-toffset[1])) continue;

    // get match
    CoMatch = triggers[0]->GetMapMatch(CoMatchShift,CoMatchSumw2,0,triggers[1],0,coinc_dt);

    // resolution invariance
    CoMatchSumw2/=res;

    // fill coinc tree
    CoTree->Fill();
  }
  
  triggers[0]->SetTriggerBranchStatus("time",true);
  triggers[1]->SetTriggerBranchStatus("time",true);
  triggers[0]->SetTriggerBranchStatus("frequency",true);
  triggers[1]->SetTriggerBranchStatus("frequency",true);
  triggers[0]->SetTriggerBranchStatus("phase",true);
  triggers[1]->SetTriggerBranchStatus("phase",true);
  triggers[0]->SetTriggerBranchStatus("tstart_ms",true);
  triggers[1]->SetTriggerBranchStatus("tstart_ms",true);
  return CoTree->GetEntries();
}

