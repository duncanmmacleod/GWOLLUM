/**
 * @file 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Coinc2.h"

ClassImp(Coinc2)

////////////////////////////////////////////////////////////////////////////////////
Coinc2::Coinc2(const int aVerbose): GwollumPlot("coinc2","STANDARD"){
////////////////////////////////////////////////////////////////////////////////////

  // inputs
  verbosity=aVerbose;

  // init
  coinc_dt   = 0.0;
  coinc_df   = 0.0;
  coinc_af   = 1.0;
  CoTree     = new TTree("coinc","coinc");
  CoSeg      = new Segments();
  CoTag      = new bool [0];
   
  for(int i=0; i<2; i++){
    triggers[i]=NULL;
    toffset[i]=0.0;
    nactive[i]=0.0;
    freqmin[i] = -1.0;
    freqmax[i] = 1e20;
  }
  hc_snrfrac[0] = new TH1D();
  hc_snrfrac[0]->SetName("snrfrac0");
  hc_snrfrac[1] = new TH1D();
  hc_snrfrac[1]->SetName("snrfrac1");
  hc_freqfrac[0] = new TH1D();
  hc_freqfrac[0]->SetName("freqfrac0");
  hc_freqfrac[1] = new TH1D();
  hc_freqfrac[1]->SetName("freqfrac1");
  stringstream ss;
  for(int i=0; i<4; i++){
    ss<<i;
    gc_snrtime[i] = new TGraph();
    gc_snrtime[i]->SetName(("snrtime"+ss.str()).c_str());
    gc_freqtime[i] = new TGraph();
    gc_freqtime[i]->SetName(("freqtime"+ss.str()).c_str());
  }
  gc_snrsnr = new TGraph();
  gc_snrsnr->SetName("snrsnr");
  gc_freqfreq = new TGraph();
  gc_freqfreq->SetName("freqfreq");

}

////////////////////////////////////////////////////////////////////////////////////
Coinc2::Coinc2(ReadTriggers *aTriggerSample0, ReadTriggers *aTriggerSample1, const int aVerbose): GwollumPlot("coinc2","STANDARD"){
////////////////////////////////////////////////////////////////////////////////////

  // inputs
  verbosity=aVerbose;

  // init
  coinc_dt   = 0.0;
  coinc_df   = 0.0;
  coinc_af   = 1.0;
  CoTree     = new TTree("coinc","coinc");
  CoSeg      = new Segments();
  CoTag      = new bool [0];
  
  for(int i=0; i<2; i++){
    triggers[i]=NULL;
    toffset[i]=0.0;
    nactive[i]=0.0;
    freqmin[i] = -1.0;
    freqmax[i] = 1e20;
  }
  hc_snrfrac[0] = new TH1D();
  hc_snrfrac[0]->SetName("snrfrac0");
  hc_snrfrac[1] = new TH1D();
  hc_snrfrac[1]->SetName("snrfrac1");
  hc_freqfrac[0] = new TH1D();
  hc_freqfrac[0]->SetName("freqfrac0");
  hc_freqfrac[1] = new TH1D();
  hc_freqfrac[1]->SetName("freqfrac1");
  stringstream ss;
  for(int i=0; i<4; i++){
    ss<<i;
    gc_snrtime[i] = new TGraph();
    gc_snrtime[i]->SetName(("snrtime"+ss.str()).c_str());
    gc_freqtime[i] = new TGraph();
    gc_freqtime[i]->SetName(("freqtime"+ss.str()).c_str());
    ss.clear(); ss.str("");
  }
  gc_snrsnr = new TGraph();
  gc_snrsnr->SetName("snrsnr");
  gc_freqfreq = new TGraph();
  gc_freqfreq->SetName("freqfreq");

  // set triggers
  SetTriggerSamples(aTriggerSample0, aTriggerSample1);
}

////////////////////////////////////////////////////////////////////////////////////
Coinc2::~Coinc2(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>1) cout<<"Coinc2::~Coinc2"<<endl;
  delete CoTree;
  delete CoSeg;
  delete CoTag;
  for(int i=0; i<2; i++){
    CoC[i].clear();
  }
  delete hc_snrfrac[0];
  delete hc_snrfrac[1];
  delete hc_freqfrac[0];
  delete hc_freqfrac[1];
  for(int i=0; i<4; i++){
    delete gc_snrtime[i];
    delete gc_freqtime[i];
  }
  delete gc_snrsnr;
  delete gc_freqfreq;
}

////////////////////////////////////////////////////////////////////////////////////
bool Coinc2::SetTriggerSamples(ReadTriggers *aTriggerSample0, ReadTriggers *aTriggerSample1, const double aTimeOffset0, const double aTimeOffset1, Segments *aValidSegments, const bool aIgnoreMinusOnes){
////////////////////////////////////////////////////////////////////////////////////
  if(aTriggerSample0==NULL || !aTriggerSample0->GetStatus()){
    cerr<<"Coinc2::SetTriggerSamples: the trigger sample 0 is corrupted"<<endl;
    return false;
  }
  if(aTriggerSample1==NULL || !aTriggerSample1->GetStatus()){
    cerr<<"Coinc2::SetTriggerSamples: the trigger sample 1 is corrupted"<<endl;
    return false;
  }
  if(aValidSegments!=NULL&&!aValidSegments->GetStatus()){
    cerr<<"Coinc2::SetTriggerSamples: the input Segments object are corrupted"<<endl;
    return false;
  }
  if(verbosity>1) cout<<"Coinc2::SetTriggerSamples: sets new input trigger samples"<<endl;

  // link trigger objects
  triggers[0]=aTriggerSample0;
  triggers[1]=aTriggerSample1;
  toffset[0]=aTimeOffset0;
  toffset[1]=aTimeOffset1;

  // reset coinc segments
  delete CoSeg;

  // ************ MAKE COINC SEGMENTS ************
  // STEP1: take the first trigger sample segments
  CoSeg = new Segments(triggers[0]->GetSegments()->GetStarts(),triggers[0]->GetSegments()->GetEnds());

  // STEP2: restrict to valid segments (if any)
  if(aValidSegments!=NULL) CoSeg->Intersect(aValidSegments);

  // STEP3: apply time offset
  CoSeg->ApplyPadding(toffset[0],toffset[0]);

  // STEP4: take the second trigger sample segments
  Segments *tmp = new Segments(triggers[1]->GetSegments()->GetStarts(),triggers[1]->GetSegments()->GetEnds());

  // STEP5: restrict to valid segments (if any)
  if(aValidSegments!=NULL) tmp->Intersect(aValidSegments);

  // STEP6: apply time offset
  tmp->ApplyPadding(toffset[1],toffset[1]);
  
  // STEP6: intersect both segments
  CoSeg->Intersect(tmp);
  delete tmp;
  if(verbosity>1) cout<<"Coinc2::SetTriggerSamples: coincident live-time = "<<CoSeg->GetLiveTime()<<" s"<<endl;
  //***********************************************

  // reset coincs
  CoTree->Reset();
  delete CoTag;
  CoTag = new bool [0];
  CoC[0].clear();
  CoC[1].clear();
  
  // tag clusters based on coinc segments --> active clusters
  SelectClusters(0,aIgnoreMinusOnes);
  SelectClusters(1,aIgnoreMinusOnes);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
int Coinc2::MakeCoinc(void){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL){
    cerr<<"Coinc2::MakeCoinc: both trigger samples must be defined"<<endl;
    return -1;
  }

  // reset coinc
  CoTree->Reset();
  for(int i=0; i<2; i++) CoC[i].clear();
  
  // locals
  double ts0, te0, ts1, te1;
  double f0, fs0, fe0, f1, fs1, fe1;
  int c1start=0;
  if(verbosity) cout<<"Coinc2::MakeCoinc: run the reference coincidence algo..."<<endl;

  // loop over trigger sample #0
  for(int c0=0; c0<triggers[0]->GetNClusters(); c0++){
    if(triggers[0]->GetClusterTag(c0)==-1) continue;
    triggers[0]->SetClusterTag(c0,1);
    ts0=triggers[0]->GetClusterTimeStart(c0)+toffset[0];
    te0=triggers[0]->GetClusterTimeEnd(c0)+toffset[0];
      
    // loop over trigger sample #1
    for(int c1=c1start; c1<triggers[1]->GetNClusters(); c1++){
      if(c1) c1start=c1-1;// assumes clusters with no time overlap
      if(triggers[1]->GetClusterTag(c1)==-1) continue;
      triggers[1]->SetClusterTag(c1,1);
 
      // time coincidence
      te1=triggers[1]->GetClusterTimeEnd(c1)+toffset[1];
      if(te1<=ts0-coinc_dt) continue;// before
      ts1=triggers[1]->GetClusterTimeStart(c1)+toffset[1];
      if(ts1>=te0-coinc_dt) break;  // after

      // frequency coincidence
      fs0=triggers[0]->GetClusterFrequencyStart(c0);
      fe0=triggers[0]->GetClusterFrequencyEnd(c0);
      fs1=triggers[1]->GetClusterFrequencyStart(c1);
      fe1=triggers[1]->GetClusterFrequencyEnd(c1);
      if(fs0>fe1+coinc_df) continue;
      if(fs1>fe0+coinc_df) continue;

      // frequency asymmetry coincidence
      f0=triggers[0]->GetClusterFrequency(c0);
      f1=triggers[1]->GetClusterFrequency(c1);
      if(fabs(f1-f0)/(f1+f0)>coinc_af) continue;

      // save coinc
      CoC[0].push_back(c0);
      CoC[1].push_back(c1);
    }
  }
  
  // init coinc tag
  delete CoTag;
  CoTag = new bool [(int)CoC[0].size()];
  for(int c=0; c<(int)CoC[0].size(); c++) CoTag[c]=true;
      
  if(verbosity>1) cout<<"Coinc2::MakeCoinc: "<<CoC[0].size()<<" coinc events found"<<endl;
  return (int)CoC[0].size();
}

////////////////////////////////////////////////////////////////////////////////////
int Coinc2::MakeCoinc0(void){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL){
    cerr<<"Coinc2::MakeCoinc0: both trigger samples must be defined"<<endl;
    return -1;
  }

  // reset coinc
  CoTree->Reset();
  for(int i=0; i<2; i++) CoC[i].clear();
  
  // locals
  double ts0, te0, t1;
  int c1start=0;
  if(verbosity) cout<<"Coinc2::MakeCoinc0: run the 0-type coincidence algo..."<<endl;

  // loop over trigger sample #0
  for(int c0=0; c0<triggers[0]->GetNClusters(); c0++){
    if(triggers[0]->GetClusterTag(c0)==-1) continue;
    triggers[0]->SetClusterTag(c0,1);

    // coinc window = coinc_dt
    ts0=TMath::Max(triggers[0]->GetClusterTime(c0)+toffset[0]-coinc_dt/2.0,triggers[0]->GetClusterTimeStart(c0)+toffset[0]);
    te0=TMath::Min(triggers[0]->GetClusterTime(c0)+toffset[0]+coinc_dt/2.0,triggers[0]->GetClusterTimeEnd(c0)+toffset[0]);
      
    // loop over trigger sample #1
    for(int c1=c1start; c1<triggers[1]->GetNClusters(); c1++){
      if(c1) c1start=c1-1;// assumes clusters with no time overlap
      if(triggers[1]->GetClusterTag(c1)==-1) continue;
      triggers[1]->SetClusterTag(c1,1);
 
      // time coincidence
      t1=triggers[1]->GetClusterTime(c1)+toffset[1];
      if(t1<ts0) continue;// before
      if(t1>te0) break;  // after

      // NO frequency coincidence
      
      // save coinc
      CoC[0].push_back(c0);
      CoC[1].push_back(c1);
    }
  }
  
  // init coinc tag
  delete CoTag;
  CoTag = new bool [(int)CoC[0].size()];
  for(int c=0; c<(int)CoC[0].size(); c++) CoTag[c]=true;
      
  if(verbosity>1) cout<<"Coinc2::MakeCoinc0: "<<CoC[0].size()<<" coinc events found"<<endl;
  return (int)CoC[0].size();
}

////////////////////////////////////////////////////////////////////////////////////
bool Coinc2::WriteCoinc(const string aOutDir, const string aOutFileName){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL){
    cerr<<"Coinc2::WriteCoinc: both trigger samples must be defined"<<endl;
    return false;
  }

  // coinc clusters
  MakeTriggers *CoCl[2];
  CoCl[0] = new MakeTriggers(triggers[0]->GetName(),verbosity);
  CoCl[1] = new MakeTriggers(triggers[1]->GetName(),verbosity);
  CoCl[0]->SetMprocessname("coinc2");
  CoCl[1]->SetMprocessname("coinc2");
  if(!CoCl[0]->GetStatus()||!CoCl[1]->GetStatus()){
    delete CoCl[0];
    delete CoCl[1];
    return false;
  }

  if(verbosity) cout<<"Coinc2::WriteCoinc: fill coinc event structures..."<<endl;
  for(int i=0; i<2; i++){
    for(int c=0; c<(int)CoC[0].size(); c++){
      if(!CoTag[c]) continue;// non-valid coinc
      CoCl[i]->AddTrigger(triggers[i]->GetClusterTime(CoC[i][c]),triggers[i]->GetClusterFrequency(CoC[i][c]),triggers[i]->GetClusterSNR(CoC[i][c]),triggers[i]->GetClusterQ(CoC[i][c]),triggers[i]->GetClusterTimeStart(CoC[i][c]),triggers[i]->GetClusterTimeEnd(CoC[i][c]),triggers[i]->GetClusterFrequencyStart(CoC[i][c]),triggers[i]->GetClusterFrequencyEnd(CoC[i][c]),triggers[i]->GetClusterAmplitude(CoC[i][c]),triggers[i]->GetClusterPhase(CoC[i][c]));
    }
   
    // set coinc segments
    CoCl[i]->Append(CoSeg);
  }

  // write coinc events
  if(verbosity) cout<<"Coinc2::WriteCoinc: write coinc event structures..."<<endl;
  int ncoinc = CoCl[0]->GetNTrig();
  CoCl[0]->Write(aOutDir, "root", aOutFileName,"clusters_0","RECREATE");
  CoCl[1]->Write(aOutDir, "root", aOutFileName,"clusters_1","UPDATE");
  delete CoCl[0];
  delete CoCl[1];

  // save coinc parameters
  TTree *CoParam = new TTree("coinc2param","coinc2param");
  CoParam->Branch("dt", &coinc_dt, "dt/D");
  CoParam->Branch("df", &coinc_df, "df/D");
  CoParam->Branch("af", &coinc_af, "af/D");
  CoParam->Branch("toffset_0", &toffset[0], "toffset_0/D");
  CoParam->Branch("toffset_1", &toffset[1], "toffset_1/D");
  CoParam->Fill();

  // save coinc parameters tree
  if(verbosity) cout<<"Coinc2::WriteCoinc: write coinc2param tree..."<<endl;
  TFile fout((aOutDir+"/"+aOutFileName+".root").c_str(),"UPDATE");
  if(fout.IsZombie()){ delete CoTree; return false; }
  fout.cd();
  CoParam->Write();
  if(CoTree->GetEntries()==ncoinc) CoTree->Write();
  fout.Close();
 
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintCoinc(void){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL) return;

  // loop over coinc
  for(int c=0; c<(int)CoC[0].size(); c++){
    cout<<"*********************************************************************"<<endl;
    cout<<"coinc_"<<c<<endl;
    cout<<"  "<<triggers[0]->GetStreamName()<<": t="<<triggers[0]->GetClusterTime(CoC[0][c])<<" s, f="<<triggers[0]->GetClusterFrequency(CoC[0][c])<<" Hz, snr="<<triggers[0]->GetClusterSNR(CoC[0][c])<<endl;;
    cout<<"  "<<triggers[1]->GetStreamName()<<": t="<<triggers[1]->GetClusterTime(CoC[1][c])<<" s, f="<<triggers[1]->GetClusterFrequency(CoC[1][c])<<", snr="<<triggers[1]->GetClusterSNR(CoC[1][c])<<endl;;
  }
  cout<<"*********************************************************************"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintNoCoinc(const int aSampleIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(aSampleIndex < 0 || aSampleIndex > 1) return;
  if(triggers[0]==NULL || triggers[1]==NULL) return;

  // loop over trigger sample
  int costart=0;
  bool found=false;
  for(int c=0; c<triggers[aSampleIndex]->GetNClusters(); c++){
    if(triggers[aSampleIndex]->GetClusterTag(c)==-1) continue;
    found=false;
    
    // search for a coinc
    for(int co=costart; co<(int)CoC[aSampleIndex].size(); co++){
      if(CoC[aSampleIndex][co]>c) break;
      if(c==CoC[aSampleIndex][co]){
        found=true;
        break;
      }
      costart=co;
    }
    
    // do not print
    if(found) continue;
    
    // print cluster
    cout<<triggers[aSampleIndex]->GetStreamName()<<": t="<<triggers[aSampleIndex]->GetClusterTime(c)<<" s, f="<<triggers[aSampleIndex]->GetClusterFrequency(c)<<" Hz, snr="<<triggers[aSampleIndex]->GetClusterSNR(c)<<endl;
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::SelectClusters(const int aSampleIndex, const bool aIgnoreMinusOnes){
////////////////////////////////////////////////////////////////////////////////////

  nactive[aSampleIndex]=0;

  // frequency selection is active
  if(freqmin[aSampleIndex]>0||freqmax[aSampleIndex]<1e20){
    for(int c=0; c<triggers[aSampleIndex]->GetNClusters(); c++){
      if(aIgnoreMinusOnes&&triggers[aSampleIndex]->GetClusterTag(c)==-1) continue;
      if(CoSeg->IsInsideSegment(triggers[aSampleIndex]->GetClusterTime(c)+toffset[aSampleIndex])
	 && triggers[aSampleIndex]->GetClusterFrequency(c)>=freqmin[aSampleIndex]
	 && triggers[aSampleIndex]->GetClusterFrequency(c)<freqmax[aSampleIndex]){
	triggers[aSampleIndex]->SetClusterTag(c,1);
	nactive[aSampleIndex]++;
      }
      else triggers[aSampleIndex]->SetClusterTag(c,-1);// discarded
    }
  }
  // frequency selection is NOT active
  else{
    for(int c=0; c<triggers[aSampleIndex]->GetNClusters(); c++){
      if(aIgnoreMinusOnes&&triggers[aSampleIndex]->GetClusterTag(c)==-1) continue;
      if(CoSeg->IsInsideSegment(triggers[aSampleIndex]->GetClusterTime(c)+toffset[aSampleIndex])){ triggers[aSampleIndex]->SetClusterTag(c,1); nactive[aSampleIndex]++; }
      else triggers[aSampleIndex]->SetClusterTag(c,-1);// discarded
    }
  }
    
  if(verbosity)
    cout<<"Coinc2::SelectClusters: trigger sample #"<<aSampleIndex<<" = "<<triggers[aSampleIndex]->GetStreamName()<<", time offset = "<<toffset[aSampleIndex]<<", "<<nactive[aSampleIndex]<<" active clusters"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Coinc2::MakeComparators(void){
////////////////////////////////////////////////////////////////////////////////////
  if(triggers[0]==NULL || triggers[1]==NULL){
    cerr<<"Coinc2::MakeComparators: both trigger samples must be defined"<<endl;
    return false;
  }

  if(verbosity) cout<<"Coinc2::MakeComparators: fill comparators..."<<endl;

  // init comparators
  stringstream ss;
  for(int i=0; i<4; i++){
    ss<<i;
    delete gc_snrtime[i];
    gc_snrtime[i] = new TGraph();
    gc_snrtime[i]->SetName(("snrtime"+ss.str()).c_str());
    gc_freqtime[i] = new TGraph();
    gc_freqtime[i]->SetName(("freqtime"+ss.str()).c_str());
    ss.clear(); ss.str("");
  }
  delete gc_snrsnr;
  gc_snrsnr = new TGraph();
  gc_snrsnr->SetName("coinc_snrsnr");
  delete gc_freqfreq;
  gc_freqfreq = new TGraph();
  gc_freqfreq->SetName("coinc_freqfreq");

  // histograms
  TriggerSelect *TS;
  TS = new TriggerSelect(triggers[0], 0);
  TS->SetSNRResolution(20);
  TS->SetFrequencyResolution(20);
  delete hc_snrfrac[0];
  hc_snrfrac[0] = TS->GetTH1D("SNR");
  hc_snrfrac[0]->SetName("hc_snrfrac0");
  TH1D *hsnr0 = TS->GetTH1D("SNR");
  hsnr0->SetName("hsnr0");
  delete hc_freqfrac[0];
  hc_freqfrac[0] = TS->GetTH1D("Frequency");
  hc_freqfrac[0]->SetName("hc_freqfrac0");
  TH1D *hfreq0 = TS->GetTH1D("Frequency");
  hfreq0->SetName("hfreq0");
  delete TS;
  TS = new TriggerSelect(triggers[1], 0);
  TS->SetSNRResolution(20);
  TS->SetFrequencyResolution(20);
  delete hc_snrfrac[1];
  hc_snrfrac[1] = TS->GetTH1D("SNR");
  hc_snrfrac[1]->SetName("hc_snrfrac1");
  TH1D *hsnr1 = TS->GetTH1D("SNR");
  hsnr1->SetName("hsnr1");
  delete hc_freqfrac[1];
  hc_freqfrac[1] = TS->GetTH1D("Frequency");
  hc_freqfrac[1]->SetName("hc_freqfrac1");
  TH1D *hfreq1 = TS->GetTH1D("Frequency");
  hfreq1->SetName("hfreq1");
  delete TS;
  
  // fill trigger 0 plots
  int p=0;
  int tstart=-1;
  for(int c0=0; c0<triggers[0]->GetNClusters(); c0++){
    if(triggers[0]->GetClusterTag(c0)==-1) continue;
    if(tstart<0) tstart=triggers[0]->GetClusterTime(c0);
    gc_snrtime[0]->SetPoint(p, triggers[0]->GetClusterTime(c0)-tstart, triggers[0]->GetClusterSNR(c0));
    gc_freqtime[0]->SetPoint(p, triggers[0]->GetClusterTime(c0)-tstart, triggers[0]->GetClusterFrequency(c0));
    hsnr0->Fill(triggers[0]->GetClusterSNR(c0));
    hfreq0->Fill(triggers[0]->GetClusterFrequency(c0));
    p++;
  }

  // fill trigger 1 plots
  p=0;
  for(int c1=0; c1<triggers[1]->GetNClusters(); c1++){
    if(triggers[1]->GetClusterTag(c1)==-1) continue;
    gc_snrtime[1]->SetPoint(p, triggers[1]->GetClusterTime(c1)-tstart, triggers[1]->GetClusterSNR(c1));
    gc_freqtime[1]->SetPoint(p, triggers[1]->GetClusterTime(c1)-tstart, triggers[1]->GetClusterFrequency(c1));
    hsnr1->Fill(triggers[1]->GetClusterSNR(c1));
    hfreq1->Fill(triggers[1]->GetClusterFrequency(c1));
    p++;
  }

  // fill coinc comparators
  p=0;
  for(int co=0; co<(int)CoC[0].size(); co++){
    if(!CoTag[co]) continue;// non-valid coinc

    gc_snrsnr->SetPoint(p, triggers[0]->GetClusterSNR(CoC[0][co]), triggers[1]->GetClusterSNR(CoC[1][co]));
    gc_freqfreq->SetPoint(p, triggers[0]->GetClusterFrequency(CoC[0][co]), triggers[1]->GetClusterFrequency(CoC[1][co]));
    gc_snrtime[2]->SetPoint(p, triggers[0]->GetClusterTime(CoC[0][co])-tstart, triggers[0]->GetClusterSNR(CoC[0][co]));
    gc_freqtime[2]->SetPoint(p, triggers[0]->GetClusterTime(CoC[0][co])-tstart, triggers[0]->GetClusterFrequency(CoC[0][co]));
    gc_snrtime[3]->SetPoint(p, triggers[1]->GetClusterTime(CoC[1][co])-tstart, triggers[1]->GetClusterSNR(CoC[1][co]));
    gc_freqtime[3]->SetPoint(p, triggers[1]->GetClusterTime(CoC[1][co])-tstart, triggers[1]->GetClusterFrequency(CoC[1][co]));
    hc_snrfrac[0]->Fill(triggers[0]->GetClusterSNR(CoC[0][co]));
    hc_snrfrac[1]->Fill(triggers[1]->GetClusterSNR(CoC[1][co]));
    hc_freqfrac[0]->Fill(triggers[0]->GetClusterFrequency(CoC[0][co]));
    hc_freqfrac[1]->Fill(triggers[1]->GetClusterFrequency(CoC[1][co]));
    p++;
  }

  hc_snrfrac[0]->Divide(hsnr0);
  hc_snrfrac[1]->Divide(hsnr1);
  hc_freqfrac[0]->Divide(hfreq0);
  hc_freqfrac[1]->Divide(hfreq1);
  delete hsnr0;
  delete hsnr1;
  delete hfreq0;
  delete hfreq1;

  // cosmetics
  ss<<tstart;
  hc_snrfrac[0]->SetTitle((triggers[0]->GetName()+" Fraction of coincident events").c_str());
  hc_snrfrac[0]->GetXaxis()->SetTitle("SNR");
  hc_snrfrac[0]->GetYaxis()->SetTitle("Fraction of coincident events");
  hc_snrfrac[1]->SetTitle((triggers[1]->GetName()+" Fraction of coincident events").c_str());
  hc_snrfrac[1]->GetXaxis()->SetTitle("SNR");
  hc_snrfrac[1]->GetZaxis()->SetTitle("Fraction of coincident events");
  hc_freqfrac[0]->SetTitle((triggers[0]->GetName()+" Fraction of coincident events").c_str());
  hc_freqfrac[0]->GetXaxis()->SetTitle("Frequency [Hz]");
  hc_freqfrac[0]->GetYaxis()->SetTitle("Fraction of coincident events");
  hc_freqfrac[1]->SetTitle((triggers[1]->GetName()+" Fraction of coincident events").c_str());
  hc_freqfrac[1]->GetXaxis()->SetTitle("Frequency [Hz]");
  hc_freqfrac[1]->GetZaxis()->SetTitle("Fraction of coincident events");
  gc_snrtime[0]->SetTitle(triggers[0]->GetName().c_str());
  gc_snrtime[0]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_snrtime[0]->GetHistogram()->GetYaxis()->SetTitle("SNR");
  gc_snrtime[0]->SetMarkerStyle(4);
  gc_snrtime[1]->SetTitle(triggers[1]->GetName().c_str());
  gc_snrtime[1]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_snrtime[1]->GetHistogram()->GetYaxis()->SetTitle("SNR");
  gc_snrtime[1]->SetMarkerStyle(4);
  gc_snrtime[2]->SetMarkerStyle(2);
  gc_snrtime[3]->SetMarkerStyle(2);
  gc_snrtime[2]->SetMarkerColor(2);
  gc_snrtime[3]->SetMarkerColor(2);
  gc_freqtime[0]->SetTitle(triggers[0]->GetName().c_str());
  gc_freqtime[0]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_freqtime[0]->GetHistogram()->GetYaxis()->SetTitle("Frequency [Hz]");
  gc_freqtime[0]->SetMarkerStyle(4);
  gc_freqtime[1]->SetTitle(triggers[1]->GetName().c_str());
  gc_freqtime[1]->GetHistogram()->GetXaxis()->SetTitle(("Time after "+ss.str()+" [s]").c_str());
  gc_freqtime[1]->GetHistogram()->GetYaxis()->SetTitle("Frequency [Hz]");
  gc_freqtime[1]->SetMarkerStyle(4);
  gc_freqtime[2]->SetMarkerStyle(2);
  gc_freqtime[3]->SetMarkerStyle(2);
  gc_freqtime[2]->SetMarkerColor(2);
  gc_freqtime[3]->SetMarkerColor(2);
  gc_snrsnr->SetTitle(("Coinc events: "+triggers[0]->GetName()+" / "+triggers[1]->GetName()).c_str());
  gc_snrsnr->GetHistogram()->GetXaxis()->SetTitle((triggers[0]->GetName()+" SNR").c_str());
  gc_snrsnr->GetHistogram()->GetYaxis()->SetTitle((triggers[1]->GetName()+" SNR").c_str());
  gc_snrsnr->SetMarkerStyle(20);
  gc_snrsnr->SetMarkerColor(2);
  gc_freqfreq->SetTitle(("Coinc events: "+triggers[0]->GetName()+" / "+triggers[1]->GetName()).c_str());
  gc_freqfreq->GetHistogram()->GetXaxis()->SetTitle((triggers[0]->GetName()+" frequency [Hz]").c_str());
  gc_freqfreq->GetHistogram()->GetYaxis()->SetTitle((triggers[1]->GetName()+" frequency [Hz]").c_str());
  gc_freqfreq->SetMarkerStyle(20);
  gc_freqfreq->SetMarkerColor(2);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Coinc2::PrintComparators(const string aFileName){
////////////////////////////////////////////////////////////////////////////////////
  
  if(verbosity) cout<<"Coinc2::PrintComparators: print comparators..."<<endl;
  
  // print comparators
  Draw(hc_snrfrac[0]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_snrfrac[0]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_snrfrac0.png");

  Draw(hc_snrfrac[1]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_snrfrac[1]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_snrfrac1.png");
  
  Draw(hc_freqfrac[0]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_freqfrac[0]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_freqfrac0.png");

  Draw(hc_freqfrac[1]);
  SetLogx(1);
  SetLogy(0);
  SetGridx(1);
  SetGridy(1);
  hc_freqfrac[1]->GetXaxis()->SetMoreLogLabels();
  Print(aFileName+"_freqfrac1.png");
  
  if(gc_snrsnr->GetN()>0){
    Draw(gc_snrsnr,"AP");
    SetLogx(1);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrsnr->GetHistogram()->GetXaxis()->SetMoreLogLabels();
    gc_snrsnr->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    Print(aFileName+"_snrsnr.png");
  }
  
  if(gc_freqfreq->GetN()>0){
    Draw(gc_freqfreq,"AP");
    SetLogx(1);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqfreq->GetHistogram()->GetXaxis()->SetMoreLogLabels();
    gc_freqfreq->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    Print(aFileName+"_freqfreq.png");
  }
  
  if(gc_snrtime[0]->GetN()>0){
    Draw(gc_snrtime[0],"AP");
    if(gc_snrtime[2]->GetN()>0) Draw(gc_snrtime[2],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrtime[0]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_snrtime[0]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_snrtime[0],"Triggers","P");
    AddLegendEntry(gc_snrtime[2],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_snrtime0.png");
    ResetLegend();
  }

  if(gc_snrtime[1]->GetN()>0){
    Draw(gc_snrtime[1],"AP");
    if(gc_snrtime[3]->GetN()>0) Draw(gc_snrtime[3],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_snrtime[1]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_snrtime[1]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_snrtime[1],"Triggers","P");
    AddLegendEntry(gc_snrtime[3],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_snrtime1.png");
    ResetLegend();
  }

  if(gc_freqtime[0]->GetN()>0){
    Draw(gc_freqtime[0],"AP");
    if(gc_freqtime[2]->GetN()>0) Draw(gc_freqtime[2],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqtime[0]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_freqtime[0]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_freqtime[0],"Triggers","P");
    AddLegendEntry(gc_freqtime[2],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_freqtime0.png");
    ResetLegend();
  }

  if(gc_freqtime[1]->GetN()>0){
    Draw(gc_freqtime[1],"AP");
    if(gc_freqtime[3]->GetN()>0) Draw(gc_freqtime[3],"Psame");
    SetLogx(0);
    SetLogy(1);
    SetGridx(1);
    SetGridy(1);
    gc_freqtime[1]->GetHistogram()->GetYaxis()->SetMoreLogLabels();
    gc_freqtime[1]->GetHistogram()->GetXaxis()->SetNoExponent();
    AddLegendEntry(gc_freqtime[1],"Triggers","P");
    AddLegendEntry(gc_freqtime[3],"Coinc","P");
    DrawLegend();
    Print(aFileName+"_freqtime1.png");
    ResetLegend();
  }

  return;
}
