//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __Ntuple__
#define __Ntuple__

#include "CUtils.h"
#include "TMath.h"
using namespace std;

#define NETMAX 10

/**
 * Build tuples.
 * This class was designed to build tuples of events defined by an index.
 * It is recommended not to use this class as is. It is better to use the Ntuple class.
 * \author    Florent Robinet
 */
class Tuple {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the Tuple class.
   * A tuple is defined by a number of streams (up to 10). Each stream must be indexed by an integer. These indexes do not have to be contiguous but they must be sorted by increasing values.
   * @param aStreamIndex vector of stream indexes composing the tuple.
   */
  Tuple(vector <int> aStreamIndex);

  /**
   * Destructor of the Tuple class.
   */
  virtual ~Tuple(void);
  /**
     @}
  */


  /**
   * Tests whether a stream is part of the tuple.
   * Returns true if the answer is yes.
   * @param aStreamIndex stream index to test
   */
  inline bool IsStreamInside(const int aStreamIndex){
    for(int s=0; s<(int)StreamIndex.size(); s++) if(aStreamIndex==StreamIndex[s]) return true;
    return false;
  };

  /**
   * Returns the Tuple name.
   * The Tuple name is defined by the sequence of stream indexes.
   */
  inline string GetName(void){ return tname; };

  /**
   * Tests whether 2 tuples are identical.
   * Returns true if the answer is yes.
   * 2 tuples are said to be identical if:
   * - the number of streams is the same
   * - the number of events is the same
   * - the event indexes are the same
   * @param Tuple external tuple to test
   */
  inline bool IsTheSame(Tuple *aTestTuple){
    if((int)StreamIndex.size()!=aTestTuple->GetNstreams()) return false;
    if((int)Event[0].size()!=aTestTuple->GetNevents()) return false;
    //FIXME: the stream numbers are not compared!!
    for(int s=0; s<(int)StreamIndex.size(); s++)
      for(int e=0; e<(int)Event[0].size(); e++)
	if(Event[s][e]!=aTestTuple->GetEvent(s,e)) return false;
    return false;
  };

  /**
   * Adds an event to the tuple.
   * The event is defined by a set of event indexes. There should be as many event indexes as the number of streams defining the tuple. Event indexes should be provided by increasing stream index.
   * @param aEvent event indexes to be added to the tuple.
   */
  inline bool AddTuple(vector <int> aEvent){
    for(int s=0; s<(int)StreamIndex.size(); s++) Event[s].push_back(aEvent[s]);
    return true;
  };

  /**
   * Resets all events in the tuple.
   */  
  inline bool ResetEvents(void){
    for(int s=0; s<(int)StreamIndex.size(); s++) Event[s].clear();
    return true;
  };

  /**
   * Remove an event from the tuple.
   * @param aPosition event position in the list
   */
  inline bool RemoveEvent(const int aPosition){
    for(int s=0; s<(int)StreamIndex.size(); s++) Event[s].erase (Event[s].begin()+aPosition);
    return true;
  };

  /**
   * Returns the number of streams defining the tuple.
   */
  inline int GetNstreams(void){ return (int)StreamIndex.size(); };

  /**
   * Returns the current number of events in the tuple.
   */
  inline int GetNevents(void){ return (int)Event[0].size(); };

  /**
   * Returns the event index of stream number 'aStreamNumber'.
   * @param aStreamNumber stream number in the tuple
   * @param aPosition event position in the list
   */ 
  inline int GetEvent(const int aStreamNumber, const int aPosition){
    return Event[aStreamNumber][aPosition];
  };

  /**
   * Returns the event index of stream index 'aStreamIndex'.
   * @param aStreamIndex stream index
   * @param aPosition event position in the list
   */ 
  inline int GetEventFromStream(const int aStreamIndex, const int aPosition){
    return Event[IndexFromStream[aStreamIndex]][aPosition];
  };
  
  /**
   * Returns the full event list of stream number 'aStreamNumber'.
   * @param aStreamNumber stream number in the tuple
   */ 
  inline vector <int> GetEvents(const int aStreamNumber){
    return Event[aStreamNumber];
  };

  /**
   * Returns the full event list of stream index 'aStreamIndex'.
   * @param aStreamIndex stream index
   */ 
  inline vector <int> GetEventsFromStream(const int aStreamIndex){ 
    return Event[IndexFromStream[aStreamIndex]]; 
  };

  /**
   * Prints current event list.
   */ 
  inline void PrintEvents(void){
    for(int e=0; e<(int)Event[0].size(); e++){
      for(int s=0; s<(int)StreamIndex.size(); s++) cout<<Event[s][e]<<" ";
      cout<<endl;
    }
    return;
  };

 private:

  string tname;                   ///< tuple name
  vector <int> StreamIndex;       ///< vector stream indexes
  vector <int> IndexFromStream;   ///< stream index -> number
  vector <int> Event[NETMAX];     ///< event list for each stream

};


/**
 * Combine tuples (Tuple).
 * This class was designed to combine N-Tuples of events.
 * \author    Florent Robinet
 */
class Ntuple {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the Ntuple class.
   * A Ntuple object is defined by N streams (up to 10). 2^N Tuple objects are created corresponding to every combinations of streams. Tuple objects are indexed following the <a href="../../Main/convention.html#network">GWOLLUM convention for networks</a> where streams are used as detectors.
   * @param aNstreams number of input streams
   */
  Ntuple(const int aNstreams);

  /**
   * Destructor of the Ntuple class.
   */
  virtual ~Ntuple(void);
  /**
     @}
  */

  /**
   * Tests whether a stream is part of a Tuple composing the Ntuple.
   * Returns true if the answer is yes.
   * @param aStreamIndex stream index to test
   * @param aTupleIndex Tuple index to search for
   */
  bool IsStreamInside(const int aStreamNumber, const int aTupleIndex);
 
  /**
   * Adds a pair of events to a given 2-Tuple.
   * A pair of events, defined by the indexes 'aEventIndex0' and 'aEventIndex1', is added to the 2-Tuple defined by the stream indexes 'aStreamIndex0' and 'aStreamIndex1'. Stream index 'aStreamIndex0' should be smaller than 'aStreamIndex1'.
   *
   * IMPORTANT: For a given Tuple, events should be added by increasing event indexes: first condition: aEventIndex0(this pair) >= aEventIndex0(previous pair), second condition: aEventIndex1(this pair) >= aEventIndex1(previous pair)
   * @param aStreamNumber0 first stream index
   * @param aStreamNumber1 second stream index
   * @param aEventIndex0 event index for first stream index
   * @param aEventIndex1 event index for second stream index
   */
  bool AddPair(const int aStreamNumber0, const int aStreamNumber1, const int aEventIndex0, const int aEventIndex1);

  /**
   * Combines 2-Tuple objects to fill N-Tuple objects up to aNmax.
   * 2-Tuple objects are combined to fill N-Tuple objects from N=3 up to N=aNmax. It uses events stored in 2-Tuple objects up to now. Higher-types Tuple objects are first reset before being filled. N-Tuple objects are filled recursively: the N-Tuple object is filled with events which are contained by all the (N-1)-Tuple objects composing the N-Tuple object. For example, the 3-tuple defined by the stream indexes 0-2-3 is filled by an event contained in all the 2-tuples 0-2, 0-3 and 2-3.
   *
   * If aUniq is set to true, every time a N-Tuple event is found, all the events in the (N-1)-Tuple objects are removed.
   * @param aNmax
   * @param aUniq
   */
  bool MakeNtuple(const int aNmax, const bool aUniq=false);

  /**
   * Returns a vector of events.
   * This applies to Tuple with index 'aTupleIndex' and stream with index 'aStreamIndex'.
   * Returns an empty vector if 'aTupleIndex' or 'aStreamIndex' does not exist.
   * @param aTupleIndex Tuple index
   * @param aStreamIndex Stream index
   */
  vector <int> GetEvents(const int aTupleIndex, const int aStreamIndex);

  /**
   * Resets all Tuple objects.
   * Every events saved so far are deleted.
   */
  inline bool ResetEvents(void){
    for(int n=0; n<ntuples; n++) if(!tuple[n]->ResetEvents()) return false;
    return true;
  };

  /**
   * Returns the number of Tuple objects for this NTuple object.
   */
  inline int GetNtuples(void){ return (int)pow(2.0,(double)Nstreams); };

  /**
   * Returns the type of Tuple indexed by 'aTupleIndex'.
   * Returns -1 if aTupleIndex does not exist.
   * @param aTupleIndex Tuple index
   */
  inline int GetTupleType(const int aTupleIndex){ return GetType(aTupleIndex); };

  /**
   * Returns the number events stored in the Tuple indexed by 'aTupleIndex'.
   * Returns -1 if aTupleIndex does not exist.
   * @param aTupleIndex Tuple index
   */
  inline int GetNevents(const int aTupleIndex){
    if(aTupleIndex<0||aTupleIndex>=ntuples){
      cerr<<"Ntuple::GetNevents: the tuple number "<<aTupleIndex<<" does not exist"<<endl;
      return -1;
    }

    return tuple[aTupleIndex]->GetNevents();
  };

  /**
   * Prints current event list, tuple by tuple.
   */ 
  void PrintEvents(void);
  
 private:

  int Nstreams;            ///< number of input streams
  
  // TUPLES
  int Ncomb[NETMAX+1];     ///< number of stream combinations per type
  int ntuples;             ///< total number of tuples = 2^N
  Tuple **tuple;           ///< tuples

  // INTERNALS
  bool MakeUniq(const int aNmax);///< remove redundancy between tuples

  // returns the type for a given Tuple
  inline int GetType(const int aTupleIndex){
    if(aTupleIndex<0) return -1;
    if(aTupleIndex>=ntuples) return -1;
    int count = 0;
    int n=aTupleIndex;
    while(n){
      n &= (n-1) ;
      count++;
    }
    return count;
  };
  
  // increment permutation
  inline bool NextPerm(const int aN, int *aComb, int *aNcombmax, int *aCombMin){
    aComb[aN-1]++;
    for(int i=aN-1; i>=0; i--){
      if(aComb[i]>=aNcombmax[i]){
	if(i==0) return false;
	aComb[i]=aCombMin[i];
	aComb[i-1]++;
      }
      if(aComb[i]<aCombMin[i]) aComb[i]=aCombMin[i];
    }
    return true;
  };
  
  ClassDef(Ntuple,0)  
};

#endif


