//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __MorphMatch__
#define __MorphMatch__

#include "EventMap.h"
#include "Coinc2.h"

using namespace std;

/**
 * Compute morphology match of coinc events.
 * \author    Florent Robinet
 */
class MorphMatch: public Coinc2 {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the MorphMatch class.
   * See parent class: Coinc2::Coinc2(). Here, EventMap objects are used as input in order to compute the morphology match.
   * Input maps with index=0 are used. It is the user responsibility to set the map parameters. The morphmatch alghorithm only works if maps have the same resolution in time and frequency and if the ranges (in time and frequency) are the same.
   * FIXME: describe the MorphMatch algorithm.
   * @param aTriggerSample0 first input trigger sample
   * @param aTriggerSample1 second input trigger sample
   * @param aVerbose verbosity level
   */
  MorphMatch(EventMap *aTriggerSample0, EventMap *aTriggerSample1, const int aVerbose=0);

  /**
   * Destructor of the MorphMatch class.
   */
  virtual ~MorphMatch(void);
  /**
     @}
  */
 
  /**
   * Runs the morphmatch algorithm.
   * After the coincidence being performed (see Coinc2::MakeCoinc()), this function computes the morphology match parameters for each coinc event tagged to true.
   * The map parameters must be properly set: see MorphMatch::MorphMatch().
   * Returns the number of coinc events with a mophology match. Returns -1 if this function fails.
   */
  int ComputeMorphMatch(void);///< computes morphology match


 protected:

  EventMap *triggers[2];       ///< overloads trigger sample

  double CoMatch;              ///< coinc match
  double CoMatchSumw2;         ///< coinc match stat
  double CoMatchShift;         ///< coinc match time shift

 private:


  ClassDef(MorphMatch,0)
};

#endif


