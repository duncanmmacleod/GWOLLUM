/**
 * @file 
 * @brief See Spectrum.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Spectrum.h"

ClassImp(Spectrum)

////////////////////////////////////////////////////////////////////////////////////
static int compare_double(const void *p1, const void *p2){
////////////////////////////////////////////////////////////////////////////////////
  double x1 = *(const double *)p1;
  double x2 = *(const double *)p2;
  return (x1 > x2) - (x1 < x2);
}

static double* GetHannWindow(const unsigned int aSize);

////////////////////////////////////////////////////////////////////////////////////
Spectrum::Spectrum(const unsigned int aSpectrumSize,
                   const unsigned int aDataDuration,
                   const unsigned int aDataSamplingFrequency,
                   const unsigned int aVerbosity){ 
////////////////////////////////////////////////////////////////////////////////////

  // save parameters
  fVerbosity            = aVerbosity;
  unsigned int datasize = aDataDuration*aDataSamplingFrequency;

  // FFT
  unsigned int fft_size = TMath::Max(2*aSpectrumSize,(unsigned int)2);
  fft_psd = new fft(fft_size, "FFTW_MEASURE", "r2c");
 
  // nominal number of subsegments
  nSubSegments[0] = datasize/fft_psd->GetSize_t();
  nSubSegments[1] = (datasize-fft_psd->GetSize_t()/2)/fft_psd->GetSize_t();
  if(nSubSegments[0]+nSubSegments[1] == 0){
    cerr<<"Spectrum::Spectrum: the data duration ("<<datasize<<" points) is too short to estimate the PSD with this resolution ("<<fft_psd->GetSize_f()<<" points)"<<endl;
    nSubSegments[0] = 0;
    nSubSegments[1] = 0;
  }
  
  // random integer
  unsigned int rnd = gRandom->Integer(10000000);
  stringstream ss;
  double frequency;

  // periodogram buffer
  for(unsigned int t=0; t<2; t++){
    periodogram[t] = new TGraph* [nSubSegments[t]];
    for(unsigned int s=0; s<nSubSegments[t]; s++){
      periodogram[t][s] = new TGraph(fft_psd->GetSize_f());
      ss<<"periodogram_"<<t<<"_"<<s<<"_"<<rnd;
      periodogram[t][s]->SetName((ss.str()).c_str());
      ss.clear(); ss.str("");
      periodogram[t][s]->SetTitle("Periodogram");
      periodogram[t][s]->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
      for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
        frequency = (double)i*(double)aDataSamplingFrequency/(double)fft_psd->GetSize_t();
        periodogram[t][s]->SetPoint(i, frequency, -1.0);
      }
    }
  }

  // normalized hann window
  // to enforce unity RMS
  HannWindow = GetTukeyWindow(fft_psd->GetSize_t(), 1.0);
  double norm=0.0;
  for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
    norm += HannWindow[i]*HannWindow[i];
  norm = TMath::Sqrt(norm/(double)fft_psd->GetSize_t());
  for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
    HannWindow[i] /= norm;


  // PSD graphs
  ss<<rnd;
  PSD = new TGraph (fft_psd->GetSize_f());// one-sided
  logPSD = new TGraph (fft_psd->GetSize_f());// one-sided
  PSD->SetName(("PSD"+ss.str()).c_str());
  logPSD->SetName(("logPSD"+ss.str()).c_str());
  PSD->SetBit(TGraph::kIsSortedX);
  logPSD->SetBit(TGraph::kIsSortedX);
  PSD->SetTitle("Power spectrum density");
  PSD->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");

  // set PSD to 0
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    frequency = (double)i*(double)aDataSamplingFrequency/(double)fft_psd->GetSize_t();
    PSD->SetPoint(i, frequency, 0.0);
    logPSD->SetPoint(i, frequency, 0.0);
  }

  // init all
  Reset();

  // print info
  if(fVerbosity>1){
    cout<<"************* Spectrum::Spectrum *************"<<endl;
    cout<<"Input data sampling frequency = "<<aDataSamplingFrequency<<" Hz"<<endl;
    cout<<"PSD size                      = "<<fft_psd->GetSize_f()<<" points"<<endl;
    cout<<"PSD resolution                = "<<(double)aDataSamplingFrequency/(double)fft_psd->GetSize_t()<<" Hz"<<endl;
    cout<<"Number of even samples        = "<<nSubSegments[0]<<endl;
    cout<<"Number of odd samples         = "<<nSubSegments[1]<<endl;
    cout<<"**********************************************"<<endl;
  }
}

////////////////////////////////////////////////////////////////////////////////////
Spectrum::~Spectrum(void){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity>1) cout<<"Spectrum::~Spectrum"<<endl;
  delete HannWindow;
  delete logPSD;
  delete PSD;
  delete fft_psd;
  for(unsigned int t=0; t<2; t++){
    for(unsigned int s=0; s<nSubSegments[t]; s++) delete periodogram[t][s];
    delete periodogram[t];
  }
}

////////////////////////////////////////////////////////////////////////////////////
void Spectrum::Reset(void){
////////////////////////////////////////////////////////////////////////////////////

  // reset buffer
  for(unsigned int t=0; t<2; t++){
    write_index[t]=0;
    for(unsigned int s=0; s<nSubSegments[t]; s++)
      periodogram[t][s]->GetY()[0]=-1.0;
  }

  // reset PSD
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    PSD->GetY()[i] = 0.0;
    logPSD->GetY()[i] = 0.0;
  }
}

////////////////////////////////////////////////////////////////////////////////////
double Spectrum::GetPower(const double aFrequency){
////////////////////////////////////////////////////////////////////////////////////

  // return DC power
  if(aFrequency<=0) return PSD->Eval(0.0);

  double psd = PSD->Eval(aFrequency);

  // no power
  if(psd<=0.0) return 0.0;

  return TMath::Exp(logPSD->Eval(aFrequency));
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::AddData(const unsigned int aDataSize,
                       const double *aData,
                       const unsigned int aDataStart){
////////////////////////////////////////////////////////////////////////////////////

  // number of new PSD subsegments
  unsigned int n_sub[2];
  n_sub[0] = aDataSize/fft_psd->GetSize_t();
  n_sub[1] = (aDataSize-fft_psd->GetSize_t()/2)/fft_psd->GetSize_t();
  if(n_sub[0]==0){
    cerr<<"Spectrum::Spectrum: the data duration ("<<aDataSize<<" points) is too short to estimate the PSD with this resolution ("<<fft_psd->GetSize_f()<<" points)"<<endl;
    return false;
  }

  // data sampling frequency
  double sampling = fft_psd->GetSize_t() * PSD->GetX()[1];
  
  // loop over even and odd sub-segments
  unsigned int index_start;
  for(unsigned int t=0; t<2; t++){

    if(!nSubSegments[t]) continue; // not to divide by 0 (%nSubSegments[t])

    for(unsigned int s=0; s<n_sub[t]; s++){

      index_start = aDataStart + (2*s+t)*fft_psd->GetSize_t()/2;

      // fill subsegment data vector
      for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
        fft_psd->SetRe_t(i,aData[index_start+i]*HannWindow[i]);

      // fft-forward it
      fft_psd->Forward();

      // get power for this sub-segment
      // normalize periodogram to give correct units:
      // 2/Tspec = 2f_s/N (periodogram)
      // /f_s / f_s (FFT squared)
      for(unsigned int i=0; i<fft_psd->GetSize_f(); i++)
	periodogram[t][write_index[t]]->GetY()[i] = 2.0*fft_psd->GetNorm2_f(i)/(double)fft_psd->GetSize_t()/sampling;

      // increment write index
      write_index[t]=(write_index[t]+1)%nSubSegments[t];
    }
  }

  // compute PSD
  if(fVerbosity) cout<<"Spectrum::AddData: compute PSD"<<endl;
  ComputeMedianMean();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::LoadData(const unsigned int aDataSize,
                        const double *aData,
                        const unsigned int aDataStart){
////////////////////////////////////////////////////////////////////////////////////
  Reset();
  return AddData(aDataSize, aData, aDataStart);
}

////////////////////////////////////////////////////////////////////////////////////
void Spectrum::ComputeMedianMean(void){
////////////////////////////////////////////////////////////////////////////////////

  // count number of active buffers
  unsigned int size_odd=0, size_evn=0;
  for(unsigned int s=0; s<nSubSegments[0]; s++) if(periodogram[0][s]->GetY()[0]>=0.0) size_evn++;
  for(unsigned int s=0; s<nSubSegments[1]; s++) if(periodogram[1][s]->GetY()[0]>=0.0) size_odd++;

  // buffer is empty
  if(size_evn+size_odd==0) return;
  
  // warning: non-nominal PSD estimation
  if(fVerbosity>0){
    if(size_evn!=nSubSegments[0] || size_odd!=nSubSegments[1])
      cout<<"Spectrum::ComputeMedianMean: the PSD estimation is not nominal: "<<size_evn<<"/"<<size_odd<<" subPSDs are used instead of "<<nSubSegments[0]<<"/"<<nSubSegments[1]<<endl;
  }
  
  // allocate memory
  double *sorted_odd = new double [size_odd];
  double *sorted_evn = new double [size_evn];
  double median_odd=0, median_evn=0;
  unsigned int sa;
  
  // loop over frequency bins
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){

    // loop over even buffers
    sa=0;
    for(unsigned int s=0; s<nSubSegments[0]; s++){
      if(periodogram[0][s]->GetY()[0]>=0){
	sorted_evn[sa]=periodogram[0][s]->GetY()[i];
	sa++;
      }
    }

    // loop over odd buffers
    sa=0;
    for(unsigned int s=0; s<nSubSegments[1]; s++){
      if(periodogram[1][s]->GetY()[0]>=0){
	sorted_odd[sa]=periodogram[1][s]->GetY()[i];
	sa++;
      }
    }

    // sort PSD values
    qsort(sorted_odd, size_odd, sizeof(*sorted_odd), compare_double);
    qsort(sorted_evn, size_evn, sizeof(*sorted_evn), compare_double);

    // take median and correct median bias
    if(size_odd)
      median_odd=(sorted_odd[(size_odd-1)/2]+sorted_odd[size_odd/2])/2.0;
    else
      median_odd=0.0;
    if(size_evn)
      median_evn=(sorted_evn[(size_evn-1)/2]+sorted_evn[size_evn/2])/2.0;
    else
      median_evn=0.0;
    median_odd/=medianbiasfactor(size_odd);
    median_evn/=medianbiasfactor(size_evn);
    
    // average odd and even medians
    PSD->GetY()[i] = ((double)size_odd*median_odd+(double)size_evn*median_evn) / (double)(size_odd+size_evn);
    if(PSD->GetY()[i]>0) logPSD->GetY()[i]=TMath::Log(PSD->GetY()[i]);
    else logPSD->GetY()[i]=0.0;
  }
  
  // clean memory
  delete sorted_odd;
  delete sorted_evn;
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetPSD(const double aFrequencyMin, const double aFrequencyMax){
////////////////////////////////////////////////////////////////////////////////////
  TGraph *GPSD=new TGraph();
  GPSD->SetName("PSD");
  GPSD->SetTitle("Power spectrum density");
  unsigned int p=0;
  for(unsigned int i=0; i<PSD->GetN(); i++){
    if(PSD->GetX()[i]<aFrequencyMin) continue;
    if(PSD->GetX()[i]>aFrequencyMax) break;
    GPSD->SetPoint(p, PSD->GetX()[i], PSD->GetY()[i]);
    p++;
  }
  return GPSD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetASD(void){
////////////////////////////////////////////////////////////////////////////////////
  TGraph *GASD=new TGraph(PSD->GetN());
  GASD->SetName("ASD");
  GASD->SetTitle("Amplitude spectrum density");
  for(unsigned int i=0; i<PSD->GetN(); i++)
    GASD->SetPoint(i, PSD->GetX()[i], TMath::Sqrt(PSD->GetY()[i])); 
  return GASD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetASD(const double aFrequencyMin, const double aFrequencyMax){
////////////////////////////////////////////////////////////////////////////////////
  TGraph *GASD=new TGraph();
  GASD->SetName("ASD");
  GASD->SetTitle("Amplitude spectrum density");
  unsigned int p=0;
  for(unsigned int i=0; i<PSD->GetN(); i++){
    if(PSD->GetX()[i]<aFrequencyMin) continue;
    if(PSD->GetX()[i]>aFrequencyMax) break;
    GASD->SetPoint(p, PSD->GetX()[i], TMath::Sqrt(PSD->GetY()[i]));
    p++;
  }
  return GASD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetAmplitudePeriodogram(const unsigned int aParity, const unsigned int aIndex){
////////////////////////////////////////////////////////////////////////////////////
  TGraph *G = GetPeriodogram(aParity, aIndex);
  for(unsigned int i=0; i<G->GetN(); i++)
    G->GetY()[i]=TMath::Sqrt(G->GetY()[i]);
  return G;
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::WritePeriodogram(const string aOutFileName){
////////////////////////////////////////////////////////////////////////////////////

  // output file
  TFile *fout = new TFile(aOutFileName.c_str(),"RECREATE");
  if(fout->IsZombie()) return false;
  fout->cd();
  
  // write periodograms
  stringstream ss;
  for(unsigned int t=0; t<2; t++){
    for(unsigned int s=0; s<nSubSegments[t]; s++){
      if(periodogram[t][s]->GetY()[0]>=0){
        ss<<"periodogram_"<<t<<"_"<<s;
        periodogram[t][s]->Write(ss.str().c_str());
        ss.clear(); ss.str("");
      }
    }
  }

  // write PSD
  PSD->Write("PSD");

  fout->Close();
  return true;
}

