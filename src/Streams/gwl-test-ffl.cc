/**
 * @file 
 * @brief Program to test the ffl class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"

using namespace std;

/**
 * @brief Test program.
 */
int main (int argc, char* argv[]){

  // getdata
  string gwf = (string)getenv("GWOLLUM_DATA");
  gwf+="/V1-hrec-vsr2.gwf";

  // generate FFL file
  ofstream of("./gwl-test-ffl.ffl");
  if(of.fail()) return 1;
  of<<"/it/does/not/matter/frame1.gwf  1200000000 100 0 0"<<endl;
  of<<"/it/does/not/matter/frame2.gwf  1200000300 50 0 0"<<endl;
  of<<"/it/does/not/matter/frame3.gwf  1200000200 100 0 0"<<endl;
  of<<gwf<<" 941380000 10  0 0"<<endl;
  of.close();
  


  //*********************************************************************
  // ffl(const string aFrameFileList, const string aPlotStyle, const int aVerbosity)
  //*********************************************************************
  cout<<"\nffl(const string aFrameFileList, const string aPlotStyle, const int aVerbosity)"<<endl;
  ffl *F = new ffl("./gwl-test-ffl.ffl");
  cout<<"\tGetInputFfl()"<<endl;
  if(F->GetInputFfl().compare("./gwl-test-ffl.ffl")) return 2;
  cout<<"\tConvert2Ffl()"<<endl;
  if(F->Convert2Ffl().compare("./gwl-test-ffl.ffl")) return 2;

  //*********************************************************************
  // LoadFrameFile(const unsigned int aGps)
  //*********************************************************************
  cout<<"\nLoadFrameFile(const unsigned int aGps)"<<endl;
  F->LoadFrameFile(941380000);
  cout<<"\tGetSegments()"<<endl;
  if(F->GetSegments()->GetN()!=3) return 3;
  if(F->GetSegments()->GetStart(0)!=941380000) return 3;
  if(F->GetSegments()->GetStart(1)!=1200000000) return 3;
  if(F->GetSegments()->GetStart(2)!=1200000200) return 3;
  if(F->GetSegments()->GetEnd(0)!=941380010) return 3;
  if(F->GetSegments()->GetEnd(1)!=1200000100) return 3;
  if(F->GetSegments()->GetEnd(2)!=1200000350) return 3;
 
  //*********************************************************************
  // GetChannelList(const string aFilter)
  //*********************************************************************
  cout<<"\nGetChannelList(const string aFilter)"<<endl;
  vector <string> channels = F->GetChannelList("");
  if(channels.size()!=10) return 4;
  channels = F->GetChannelList("V1:Hrec_*");
  if(channels.size()!=7) return 4;
  channels = F->GetChannelList("V1:Hrec_Flag_LineRmvl V1:h_16384Hz");
  if(channels.size()!=2) return 4;
  channels = F->GetChannelList("V1:h_?????Hz");
  if(channels.size()!=2) return 4;
  channels = F->GetChannelList("V1:h_?????Hz V1:Hrec_Flag_*");
  if(channels.size()!=9) return 4;
  channels = F->GetChannelList("*");
  if(channels.size()!=10) return 4;

  //*********************************************************************
  // GetChannelSampling(const string aChannelName)
  //*********************************************************************
  cout<<"\nGetChannelSampling(const string aChannelName)"<<endl;
  if(F->GetChannelSampling("V1:h_16384Hz")!=16384) return 5;
  if(F->GetChannelSampling("V1:h_20000Hz")!=20000) return 5;
  if(F->GetChannelSampling("V1:Hrec_Flag_Channel")!=1) return 5;

  //*********************************************************************
  // IsChannel(const string aChannelName)
  //*********************************************************************
  cout<<"\nIsChannel(const string aChannelName)"<<endl;
  if(F->IsChannel("V1:h_16384Hz")==false) return 6;
  if(F->IsChannel("V1:h_16384H")==true) return 6;
  if(F->IsChannel("")==true) return 6;

  //*********************************************************************
  // GetData()
  //*********************************************************************
  cout<<"\nGetData() (FFL)"<<endl;
  double *data;
  unsigned dsize;
  data=F->GetData(dsize, "V1:DOES-NOT-EXIST", 941380000, 941380010);
  if(dsize!=0) return 7;
  if(data!=NULL) return 7;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380020);
  if(dsize!=0) return 7;
  if(data!=NULL) return 7;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380010);
  if(dsize!=16384*10) return 7;
  delete data;
  data=F->GetData(dsize, "V1:h_20000Hz", 941380000, 941380010);
  if(dsize!=20000*10) return 7;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380010);
  if(dsize!=1*10) return 7;
  if(data==NULL) return 7;
  if(data[0]!=1) return 7;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380001);
  if(dsize!=1*1) return 7;
  if(data==NULL) return 7;
  if(data[0]!=1) return 7;
  delete data;

  delete F;

  //*********************************************************************
  // GetData() (LCF)
  //*********************************************************************
  cout<<"\nGetData() (LCF)"<<endl;

  // generate LCF file
  ofstream of2("./gwl-test-ffl.lcf");
  if(of.fail()) return 8;
  of2<<"V1 VSR2 1200000000 100 file://localhost/it/does/not/matter/frame1.gwf"<<endl;
  of2<<"V1 VSR2 1200000300 50 file://localhost/it/does/not/matter/frame2.gwf"<<endl;
  of2<<"V1 VSR2 1200000200 100 file://localhost/it/does/not/matter/frame3.gwf"<<endl;
  of2<<"V1 VSR2 941380000 10 file://localhost"<<gwf<<endl;
  of2.close();

  F = new ffl("./gwl-test-ffl.lcf");
  cout<<"\tGetInputFfl()"<<endl;
  if(F->GetInputFfl().compare("./gwl-test-ffl.lcf")) return 8;

  F->LoadFrameFile(941380000);
  cout<<"\tGetSegments()"<<endl;
  if(F->GetSegments()->GetN()!=3) return 8;
  if(F->GetSegments()->GetStart(0)!=941380000) return 8;
  if(F->GetSegments()->GetStart(1)!=1200000000) return 8;
  if(F->GetSegments()->GetStart(2)!=1200000200) return 8;
  if(F->GetSegments()->GetEnd(0)!=941380010) return 8;
  if(F->GetSegments()->GetEnd(1)!=1200000100) return 8;
  if(F->GetSegments()->GetEnd(2)!=1200000350) return 8;

  cout<<"\tGetData()"<<endl;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380010);
  if(dsize!=16384*10) return 8;
  delete data;
  data=F->GetData(dsize, "V1:h_20000Hz", 941380000, 941380010);
  if(dsize!=20000*10) return 8;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380010);
  if(dsize!=1*10) return 8;
  if(data==NULL) return 8;
  if(data[0]!=1) return 8;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380001);
  if(dsize!=1*1) return 8;
  if(data==NULL) return 8;
  if(data[0]!=1) return 8;
  delete data;

  system("rm -f ./gwl-test-ffl.lcf ./gwl-test-ffl.ffl ./ffconvert.*.ffl");

  delete F;
  return 0;
}

