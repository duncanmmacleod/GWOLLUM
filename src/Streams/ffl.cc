/**
 * @file 
 * @brief See ffl.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"

ClassImp(ffl)


////////////////////////////////////////////////////////////////////////////////////
ffl::ffl(const string aFrameFileList, const string aPlotStyle, const int aVerbosity){
////////////////////////////////////////////////////////////////////////////////////

  // init
  status_OK=true;
  fInFrameFile=aFrameFileList;
  fVerbosity=aVerbosity;
  frfile=NULL;

  // should be an existing text file
  if(!IsTextFile(fInFrameFile)){
    cerr<<"ffl::ffl: file "<<aFrameFileList<<" cannot be found/read"<<endl;
    status_OK=false;
  }

  // detect format: FFL or LCF?
  if(fVerbosity) cout<<"ffl::ffl: detect file format"<<endl;
  if(status_OK){
    ifstream infile(fInFrameFile);
    string firstline="";
    if(infile.good()){
      getline(infile, firstline);
      infile.close();
    }
    else{
      cerr<<"ffl::ffl: the input frame file is corrupted"<<endl;
      fFormat="none";
      status_OK=false;
    }
    vector<string> words = SplitString(firstline);
    if(words.size()!=5){
      cerr<<"ffl::ffl: the input frame file list has an unknown format"<<endl;
      fFormat="none";
      status_OK=false;
    }
    else{
      if(atoi(words[1].c_str())==0) fFormat="lcf";
      else fFormat="ffl";
      if(fVerbosity>1) cout<<"ffl::ffl: detected file format = "<<fFormat<<endl;
    }
  }

  // random id
  srand (time(NULL));
  int randint = rand();
  stringstream tmpstream;
  tmpstream<<randint;
  srandint=tmpstream.str();

  // working frame file
  DefineTmpDir(".");

  seg = new Segments();
  ntrials = 1;
  nsleepsec = 0;

  // plotting
  GP = new GwollumPlot ("ffl", aPlotStyle);
  G = new TGraph(0);
  G->SetName(("ffl_graph_"+srandint).c_str());
 
}

////////////////////////////////////////////////////////////////////////////////////
ffl::~ffl(){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity>1) cout<<"ffl::~ffl"<<endl;
  Reset();
  delete seg;
  delete G;
  delete GP;
}


////////////////////////////////////////////////////////////////////////////////////
void ffl::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity) cout<<"ffl::Reset: reset ffl"<<endl;
  channels.clear();
  sampling.clear();
  seg->Reset();
}

////////////////////////////////////////////////////////////////////////////////////
void ffl::SetPlotName(const string aName){
////////////////////////////////////////////////////////////////////////////////////
  string p = GP->GetCurrentStyle();
  delete GP;
  GP = new GwollumPlot (aName, p);
}
  
////////////////////////////////////////////////////////////////////////////////////
void ffl::DefineTmpDir(const string aTmpDirPath){
////////////////////////////////////////////////////////////////////////////////////
  if(fFormat.compare("ffl")&&IsDirectory(aTmpDirPath))
    fWoFrameFile=aTmpDirPath+"/ffconvert."+srandint+".ffl";
  else
    fWoFrameFile=fInFrameFile;
  if(fVerbosity>1) cout<<"ffl::ffl: working ffl file = "<<fWoFrameFile<<endl;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
string ffl::Convert2Ffl(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::Convert2Ffl: The ffl object is corrupted"<<endl;
    return "none";
  }
  if(!fFormat.compare("ffl")){
    return fWoFrameFile;
  }

  // load LCF file
  if(fVerbosity) cout<<"ffl::Convert2Ffl: FFL conversion"<<endl;
  ReadAscii *R = new ReadAscii(fInFrameFile,"s;s;d;d;s");

  vector<double> start_tmp;
  vector<double> dur_tmp;
  vector<string> ffile_tmp;

  // extract data
  if((!R->GetCol(ffile_tmp,4)) || (!R->GetCol(start_tmp,2)) || (!R->GetCol(dur_tmp,3))){
    cerr<<"ffl::Loadffl: cannot extract data"<<endl;
    delete R;
    return "none";
  }
  delete R;

  // converted file
  string fff1, fff2;
  ofstream outfile(fWoFrameFile.c_str());
  for(unsigned int l=0; l<start_tmp.size(); l++){
    fff1.clear();
    fff2.clear();
    if(!(ffile_tmp[l].substr(0,5)).compare("file:")) fff1=ffile_tmp[l].substr(5);// remove "file:"
    else fff1=ffile_tmp[l];
    if(!(fff1.substr(0,11)).compare("//localhost")) fff2=fff1.substr(11);// remove "//localhost"
    else fff2=fff1;
    outfile<<fff2<<" "<<setprecision(15)<<start_tmp[l]<<" "<<dur_tmp[l]<<" 0 0"<<endl;
  }
  outfile.close();
  
  start_tmp.clear();
  dur_tmp.clear();
  ffile_tmp.clear();

  return fWoFrameFile;
}

////////////////////////////////////////////////////////////////////////////////////
bool ffl::ExtractChannels(const unsigned int aGps){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::ExtractChannels: The ffl object is corrupted"<<endl;
    return false;
  }
  if(aGps>0&&!seg->IsInsideSegment(aGps)){
    cerr<<"ffl::ExtractChannels: the requested GPS time is not covered by the ffl"<<endl;
    return false;
  }
  if(frfile==NULL){
    cerr<<"ffl::ExtractChannels: frfile is not defined"<<endl;
    return false;
  }

  // get channel list
  char* clist = NULL;
  for(unsigned int n=0; n<ntrials; n++){
    clist = FrFileIGetChannelList(frfile, aGps);
    if(clist!=NULL) break;
    sleep(nsleepsec);
  }
  if(clist==NULL){
    cerr<<"ffl::ExtractChannels: no channel returned"<<endl;
    return false;
  }
  string slist = (string)clist;
  delete clist;
  channels.clear();
  sampling.clear();

  // split into lines
  vector <string> lines = SplitString(slist, '\n');
  
  // read each line
  vector <string> words;
  double tmp;
  for(unsigned int l=0; l<lines.size(); l++){
    words=SplitString(lines[l], ' ');

    // there should be 3 words: TYPE - NAME - SAMPLING
    if(words.size()<3){
      words.clear();
      continue;
    }

    // search for ADC/PROC/SIM
    if((!words[0].compare("ADC")) ||
       (!words[0].compare("PROC")) ||
       (!words[0].compare("SIM"))){
      tmp=atof(words[2].c_str());
      if(tmp&&tmp==floor(tmp)){
	channels.push_back(words[1]);
	sampling.push_back((unsigned int)tmp);
      }
    }

    // search for SER
    else if(!words[0].compare("SER")){
      tmp=atof(words[2].c_str());
      if(tmp&&tmp==floor(tmp)){
	for(unsigned int w=3; w<words.size(); w+=2){
	  channels.push_back(words[1]+"_"+words[w]);
	  sampling.push_back((unsigned int)tmp);
	}
      }
    }
    
    words.clear();
  }

  if(fVerbosity>2){
    cout<<"ffl::ExtractChannels: extracted information = "<<endl;
    for(unsigned int l=0; l<channels.size(); l++) cout<<channels[l]<<" "<<sampling[l]<<endl;
  }
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool ffl::LoadFrameFile(const unsigned int aGps){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::Loadffl: The ffl object is corrupted"<<endl;
    return false;
  }

  // if LCF -> convert first
  if(!Convert2Ffl().compare("none")){
    cerr<<"ffl::Loadffl: the conversion to FFL failed"<<endl;
    return false;
  }

  // reset ffl
  Reset();

  // load file
  if(fVerbosity) cout<<"ffl::LoadFrameFile: load FFL file"<<endl;
  ReadAscii *R = new ReadAscii(fWoFrameFile, "s;d;d;s;s");

  // extract information
  vector<double> start;
  vector<double> stop;
  vector<double> dur;
  if(fVerbosity) cout<<"ffl::LoadFrameFile: extract frame information"<<endl;
  if((!R->GetCol(start,1)) || (!R->GetCol(dur,2))){
    cerr<<"ffl::Loadffl: cannot extract data"<<endl;
    delete R;
    return false;
  }

  // sort segments
  stop.push_back(start[0]+dur[0]);
  double tmp1, tmp2;
  int ll;
  for(unsigned int l=1; l<start.size(); l++){
    
    // test value
    tmp1=start[l];
    ll=(int)l-1;
    stop.push_back(start[l]+dur[l]);

    while(tmp1<start[ll]){
      // back the list
      tmp2=start[ll];
      start[ll]=start[ll+1];
      start[ll+1]=tmp2;
      tmp2=stop[ll];
      stop[ll]=stop[ll+1];
      stop[ll+1]=tmp2;
      ll--;
    }
  }
  dur.clear();

  if(fVerbosity) cout<<"ffl::LoadFrameFile: check frame information"<<endl;
    
  // make segments
  delete seg;
  seg = new Segments(start,stop);
  start.clear();
  stop.clear();
  if(!seg->GetStatus()) return false;

  // make Fr file
  for(unsigned int n=0; n<ntrials; n++){
    frfile = FrFileINew((char*)fWoFrameFile.c_str());
    if(frfile!=NULL) break;
    sleep(nsleepsec);
  }
  
  // check frfile
  if(frfile==NULL){
    cerr<<"ffl::Loadffl: FFL file "<<fWoFrameFile<<" cannot be loaded"<<endl;
    return false;
  }
 
  // get list of channels (first frame file!)
  if(!ExtractChannels(aGps)){
    cerr<<"ffl::Loadffl: cannot extract channel information"<<endl;
    return false;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int ffl::GetChannelSampling(const string aChannelName, unsigned int &aChannelIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::GetChannelSampling: The ffl object is corrupted"<<endl;
    return 0;
  }
  
  for(aChannelIndex=0; aChannelIndex<channels.size(); aChannelIndex++){
    if(!aChannelName.compare(channels[aChannelIndex]))
      return sampling[aChannelIndex];
  }

  return 0;
}

////////////////////////////////////////////////////////////////////////////////////
vector <string> ffl::GetChannelList(const string aFilter){
////////////////////////////////////////////////////////////////////////////////////

  // no filter
  if(!aFilter.compare("")) return channels;

  vector <string> subchannels;

  // get sub-strings
  vector <string> substring = SplitString(aFilter, ' ');

  // loop over channels
  for(unsigned int c=0; c<channels.size(); c++){

    // check pattern
    for(unsigned int s=0; s<substring.size(); s++){
      if(!fnmatch(substring[s].c_str(), channels[c].c_str(), FNM_NOESCAPE)){
	subchannels.push_back(channels[c]);
	break;
      }
    }
  }

  return subchannels;
}

////////////////////////////////////////////////////////////////////////////////////
bool ffl::IsChannel(const string aChannelName){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int l=0; l<channels.size(); l++){
    if(!aChannelName.compare(channels[l])) return true;
  }
  return false;
}

////////////////////////////////////////////////////////////////////////////////////
double* ffl::GetData(unsigned int &aSize, const string aChannelName,
                     const double aGpsStart, const double aGpsEnd,
                     const double aMissing){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::GetData: the ffl object is corrupted"<<endl;
    aSize=0;
    return NULL;
  }
  if(seg->GetLiveTime(aGpsStart, aGpsEnd) != aGpsEnd-aGpsStart){
    cerr<<"ffl::GetData: the requested GPS range is not entirely covered by the ffl"<<endl;
    aSize=0;
    return NULL;
  }
  if(GetChannelSampling(aChannelName)==0){
    cerr<<"ffl::GetData: channel "<<aChannelName<<" was not found in the data"<<endl;
    aSize=0;
    return NULL;
  }
  if(frfile==NULL){
    cerr<<"ffl::GetData: frfile is not defined"<<endl;
    aSize=0;
    return NULL;
  }

  // output vector
  double *outvector;
  FrVect *chanvect = NULL; // main vector

  // fill Frame vector
  if(fVerbosity>1)
    cout<<"ffl::GetData: read data for "<<aChannelName<<" between "<<aGpsStart<<" and "<<aGpsEnd<<endl;
  for(unsigned int n=0; n<ntrials; n++){
    chanvect = FrFileIGetVectDN(frfile, (char*)(aChannelName.c_str()),
                                aGpsStart, aGpsEnd-aGpsStart);
    if(chanvect!=NULL) break;
    sleep(nsleepsec);
  }
  
  // test output
  if(chanvect==NULL||!chanvect->nData){
    cerr<<"ffl::GetData: data could not be read"<<endl;
    if(chanvect!=NULL) FrVectFree(chanvect);
    aSize=0;
    return NULL;
  }

  // get size
  aSize=chanvect->nData;
  if(fVerbosity>1) cout<<"ffl::GetData: "<<aSize<<" samples"<<endl;

  // copy output somewhere else
  outvector = new double [aSize];
  for(unsigned int i=0; i<aSize; i++){

    // test for nan (corrupted data)
    if(chanvect->dataD[i]==chanvect->dataD[i]) outvector[i]=chanvect->dataD[i];
    else{
      cerr<<"ffl::GetData: corrupted data"<<endl;
      FrVectFree(chanvect);
      delete outvector;
      aSize=0;
      return NULL;
    }
  }

  // missing data
  if(chanvect->next!=NULL){

    // return
    if(aMissing==999.0){
      cerr<<"ffl::GetData: missing data"<<endl;
      FrVectFree(chanvect);
      delete outvector;
      aSize=0;
      return NULL;
    }
 
    // flag missing samples
    for(unsigned int i=0; i<aSize; i++){
      if(chanvect->next->dataU[i]==0) outvector[i]=aMissing;
    }
  }
  FrVectFree(chanvect);

  return outvector;
}

////////////////////////////////////////////////////////////////////////////////////
bool ffl::PrintData(const string aChannelName, const double aGpsStart, const double aGpsEnd, string aFileName){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::PrintData: the ffl object is corrupted"<<endl;
    return false;
  }
  
  // get data
  unsigned int dvsize;
  double *dv = GetData(dvsize, aChannelName, aGpsStart, aGpsEnd);
  if(dvsize==0) return false;

  // fill outputfile
  ofstream outputfile(aFileName.c_str());
  if(outputfile.fail()){
    cerr<<"ffl::PrintData cannot open output file" <<endl;
    delete dv;
    return false;
  }

  for(unsigned int i=0; i<dvsize; i++) 
    outputfile<<setprecision(16)<<aGpsStart+((double)i)*(aGpsEnd-aGpsStart)/(double)dvsize<<" "<<setprecision(6)<<dv[i]<<endl;
  delete dv;

  outputfile.close();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* ffl::PlotData(const string aChannelName, const double aGpsStart, const double aGpsEnd){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"ffl::PlotData: the ffl object is corrupted"<<endl;
    return NULL;
  }
  
  // get data
  unsigned int dvsize;
  double *dv = GetData(dvsize, aChannelName, aGpsStart, aGpsEnd);
  if(dvsize==0) return NULL;

  // fill graph
  delete G;
  G = new TGraph(dvsize);
  G->SetName(("ffl_graph_"+srandint).c_str());
  for(int i=0; i<dvsize; i++) G->SetPoint(i,aGpsStart+(double)i*(aGpsEnd-aGpsStart)/(double)dvsize,dv[i]);
  delete dv;

  // plot
  G->GetHistogram()->GetXaxis()->SetNoExponent();
  G->GetHistogram()->GetXaxis()->SetNdivisions(5,5,0);
  G->GetHistogram()->GetXaxis()->SetTitle("Time [s]");
  G->GetHistogram()->SetTitle((aChannelName+" time series").c_str());
  G->GetHistogram()->GetYaxis()->SetTitle("Amplitude");
  GP->Draw(G,"APL");
  GP->SetGridx();  GP->SetGridy();

  return G;
}
