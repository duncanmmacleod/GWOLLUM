/**
 * @file 
 * @brief Program to compute the detector response to a gravitational wave.
 * @snippet this gwl-print-detector-response-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Streams.h"

using namespace std;

/**
 * @brief Usage funcion.
 */
int usage(void){
    //! [gwl-print-detector-response-usage]
    cerr<<"This program computes the detector response to a gravitational wave"<<endl;
    cerr<<"with the following parameters:"<<endl;
    cerr<<"ra  = source right ascension [rad]"<<endl;
    cerr<<"dec = source declination [rad]"<<endl;
    cerr<<"psi = source polarization angle [rad]"<<endl;
    cerr<<"gps = GW geocentric GPS time"<<endl;
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<"gwl-print-detector-response [det prefix] [ra] [dec] [psi] [gps]"<<endl; 
    cerr<<endl;
    cerr<<"  [det prefix] : detector prefix (\"V1\", \"H1\"...)"<<endl; 
    //! [gwl-print-detector-response-usage]
    return 1;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  // check the output directory
  if(argc!=6) return usage();

  // get det prefix
  string detprefix=(string)argv[1];
  string streamname=detprefix+":whatever";

  // source
  double ra = atof(argv[2]);
  double dec = atof(argv[3]);
  double psi = atof(argv[4]);
  double geo_gps = atof(argv[5]);

  // stream
  Streams *S = new Streams(streamname, 1);

  // det time
  double det_gps = S->GetLocalTime(ra,dec,geo_gps);

  // det response
  double fplus, fcross;
  S->GetDetectorAMResponse(fplus, fcross, ra, dec, psi, GreenwichMeanSiderealTime(det_gps));

  cout<<endl;
  cout<<DET_NAME[S->GetDetectorIndex()]<<endl;
  cout<<"Local time = "<<setprecision(15)<<det_gps<<endl;
  cout<<"GMST       = "<<setprecision(15)<<GreenwichMeanSiderealTime(det_gps)<<endl;
  cout<<"F+         = "<<fplus<<endl;
  cout<<"Fx         = "<<fcross<<endl;
  cout<<endl;

  delete S;
  return 0;
}
