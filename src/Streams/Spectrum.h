/**
 * @file 
 * @brief Noise power spectral density.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Spectrum__
#define __Spectrum__

#include <TMath.h>
#include <TRandom.h>
#include <TH1.h>
#include <TGraph.h>
#include <TFile.h>
#include "FFT.h"

using namespace std;

/**
 * @brief Compute the noise power spectral density.
 * @details This class is designed to estimate the one-sided noise power spectral density of a given stretch of data (PSD).
 * It uses the median-mean method described in <a href="http://arxiv.org/abs/gr-qc/0509116">gr-qc/0509116</a>.
 * This method can be represented by the diagram below:
 * \anchor spectrum_spectrum
 * \image html Spectrum/spectrum.svg "Data segments to estimate the noise PSD."
 *
 * The input data is divided into \f$N_e\f$ "even" subsegments and \f$N_o\f$ "odd" subsegments (Spectrum::Spectrum()) of duration \f$T_s\f$.
 * Odd and even subsegments overlap by 50%.
 * The number of subsegments, \f$N_e\f$ and \f$N_o\f$, is given by the duration of the input data \f$T\f$ and the requested frequency resolution for the PSD.
 * The input data is sampled at a frequency \f$f_s\f$;
 * therefore the total number of data points is \f$N = T\times f_s\f$.
 * The PSD frequency resolution \f$\delta f\f$ is defined by the number of points, \f$n = f_s/(2\delta f)\f$, to cover frequencies from 0 Hz to the Nyquist frequency, \f$f_s/2\f$.
 * We have:
 * \f[
 \begin{cases}
 N_e = \lfloor{ \frac{T}{T_s} }\rfloor = \lfloor{ \frac{N}{2n} }\rfloor = \lfloor{ \frac{N\delta f}{f_s} } \rfloor \\
 N_o = \lfloor{ \frac{T-T_s/2}{T_s} }\rfloor = \lfloor{ \frac{N-n}{2n} }\rfloor
 \end{cases}
 * \f]
 * @note Some input data may be left unused at the end of the data segment (in red on the diagram above).
 * @warning The spectrum frequency resolution \f$n\f$ and the input data length \f$T\f$ must be chosen to have at least one subsegment.
 *
 * Each subsegment \f$d_i(t)\f$ is hann-windowed with \f$w(t)\f$ and Fourier transformed: \f$\widetilde{(wd_i)}(f)\f$.
 * Then, periodograms \f$P_i(f)\f$ are evaluated for each subsegment:
 * \f[
 P_i(f) = \frac{2}{T_s}\left| \widetilde{(wd_i)}(f)\right|^2
 * \f]
 * A median PSD is computed using periodograms for odd and even subsegments separately and a median-to-mean correction factor is applied (medianbiasfactor()).
 * The 2 PSDs are finally averaged into one.
 *
 * In fact, a dynamical PSD estimation is implemented here.
 * The number of even and odd sub-segments is defined and fixed in the constructor of this class (Spectrum::Spectrum()).
 * This defines a circular buffer of periodograms.
 * Then, segments of data are provided with the AddData() method.
 * This segment of data is used to compute periodograms, fill the circular buffer, and update the PSD estimate.
 * \author    Florent Robinet
 */
class Spectrum{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Spectrum class.
   * @details It defines the data sizes and initiates the FFT plan (FFTW_MEASURE).
   * The spectrum resolution is defined by the number of points used to compute the data spectrum: \f$n\f$.
   * A nominal duration over which the PSD is estimated must be given: \f$T\f$.
   * A circular buffer of sub-segments is defined and is used to apply the median-mean method.
   * @param[in] aSpectrumSize Number of points in the spectrum (one sided): \f$n\f$.
   * @param[in] aDataDuration Nominal data duration to estimate the PSD [s]: \f$T\f$.
   * @param[in] aDataSamplingFrequency Input data sampling frequency [Hz]: \f$f_s\f$.
   * @param[in] aVerbosity Verbosity level.
   */
  Spectrum(const unsigned int aSpectrumSize,
	   const unsigned int aDataDuration, 
	   const unsigned int aDataSamplingFrequency,
	   const unsigned int aVerbosity=0);

  /**
   * @brief Destructor of the Spectrum class.
   */
  virtual ~Spectrum(void);
  /**
     @}
  */

  /**
   * Resets the current PSD and the periodogram circular buffer.
   * The periodogram buffer is flushed. The current PSD is set to 0.
   */
  void Reset(void);
  
  /**
   * @brief Loads a data vector and updates the current PSD.
   * @details The data 'aData' is used to update the current PSD.
   * The user must specify the size of the input data.
   * If the size of the input data is smaller than the size specified in the constructor, the circular buffer is updated with this new data set.
   * The oldest periodograms in the buffer are removed.
   * If the size of the input data is larger than the size specified in the constructor, only the end of the input data is considered.
   *
   * @note Optionally, the user can provide a start index. The data vector is scanned from this index.
   *
   * The input data is broken into odd and even subsegments as described in this class introduction.
   * The periodograms are computed and saved in the circular buffer. Finally, a new PSD is estimated.
   * @returns true if the input data is correctly used for the PSD estimate. false if the input data is of too short duration to compute the PSD.
   * @param[in] aDataSize Input vector size (number of points) starting at aDataStart.
   * @param[in] aData Input time series.
   * @param[in] aDataStart First data index to consider for the PSD estimation.
   */
  bool AddData(const unsigned int aDataSize,
               const double *aData,
               const unsigned int aDataStart=0);

  /**
   * Loads a data vector and computes a new PSD.
   * This function is the same as AddData() but the PSD circular buffer is flushed before loading the new data vector.
   * @returns true if the input data is correctly used for the PSD estimate. false if the input data is of too short duration to compute the PSD.
   * @param[in] aDataSize Input vector size (number of samples) starting at aDataStart
   * @param[in] aData Input time series.
   * @param[in] aDataStart First data index to consider for the PSD estimation.
   */
  bool LoadData(const unsigned int aDataSize,
                const double *aData,
                const unsigned int aDataStart=0);

  /**
   * @brief Returns the current PSD value at a given frequency.
   * @details The power value is computed by interpolating the PSD if the requested frequency is inside the PSD frequency range (0 to Nyquist).
   * If it is outside this range, the PSD is extrapolated.
   *
   * @note If the requested frequency is negative or zero, the DC power value is returned.
   *   
   * @param[in] aFrequency Frequency value [Hz].
   */
  double GetPower(const double aFrequency);

  /**
   * @brief Returns the current PSD value at a given frequency index.
   * @param aFrequencyIndex Frequency index.
   * @pre The frequency index must be a valid index: from 0 to the value returned by GetSpectrumSize().
   */
  inline double GetPower(const int aFrequencyIndex){
    return PSD->GetY()[aFrequencyIndex];
  };

  /**
   * @brief Returns the current ASD value at a given frequency.
   * @details The ASD value is the square root of the PSD returned with GetPower().
   * @param[in] aFrequency Frequency value [Hz].
   */
  inline double GetAmplitude(const double aFrequency){
    return TMath::Sqrt(GetPower(aFrequency));
  };

  /**
   * @brief Returns a copy of the current PSD as a TGraph.
   * @warning The returned TGraph must be deleted by the user.
   */
  inline TGraph* GetPSD(void){
    return (TGraph*)PSD->Clone("PSD");
  };

  /**
   * @brief Returns a copy of the current PSD as a TGraph between 2 frequencies.
   * @warning The returned TGraph must be deleted by the user.
   * @param[in] aFrequencyMin Minimum frequency value [Hz].
   * @param[in] aFrequencyMax Maximum frequency value [Hz].
   */
  TGraph* GetPSD(const double aFrequencyMin, const double aFrequencyMax);

  /**
   * @brief Returns a copy of the current ASD as a TGraph.
   * @warning The returned TGraph must be deleted by the user.
   */
  TGraph* GetASD(void);

  /**
   * @brief Returns a copy of the current ASD as a TGraph between 2 frequencies.
   * @warning The returned TGraph must be deleted by the user.
   * @param[in] aFrequencyMin Minimum frequency value [Hz].
   * @param[in] aFrequencyMax Maximum frequency value [Hz].
   */
  TGraph* GetASD(const double aFrequencyMin, const double aFrequencyMax);

  /**
   * @brief Returns a copy of a given periodogram.
   * @warning The returned TGraph must be deleted by the user.
   * @param[in] aParity 0 for even subsegments, 1 for odd subsegments.
   * @param[in] aIndex Subsegment index.
   */
  inline TGraph* GetPeriodogram(const unsigned int aParity, const unsigned int aIndex){
    return (TGraph*)periodogram[aParity%2][aIndex]->Clone("periodogram");
  };

  /**
   * @brief Returns a copy of a given (amplitude) periodogram.
   * @warning The returned TGraph must be deleted by the user.
   * @param[in] aParity 0 for even subsegments, 1 for odd subsegments.
   * @param[in] aIndex Subsegment index.
   */
  TGraph* GetAmplitudePeriodogram(const unsigned int aParity, const unsigned int aIndex);

  /**
   * @brief Writes periodograms and the resulting PSD in a ROOT file.
   * @param[in] aOutFileName ROOT output file name (use a ".root" extension).
   * @returns false if the file can not be opened, and true otherwise.
   */
  bool WritePeriodogram(const string aOutFileName);

  /**
   * @brief Returns the number of points in the spectrum: \f$n\f$.
   */
  inline unsigned int GetSpectrumSize(void){ return fft_psd->GetSize_f(); };

  /**
   * @brief Returns the frequency of a given spectrum frequency index [Hz].
   * @param[in] aIndex Frequency index (0=DC).
   */
  inline double GetSpectrumFrequency(const unsigned int aIndex){ return PSD->GetX()[aIndex]; };

  /**
   * @brief Returns the frequency resolution of the spectrum [Hz]: \f$\delta f\f$.
   */
  inline double GetSpectrumResolution(void){ return PSD->GetX()[1]; };

  /**
   * @brief Returns the Nyquist frequency of the spectrum [Hz].
   */
  inline unsigned int GetSpectrumNyquist(void){
    return (unsigned int)(GetSpectrumResolution()*(double)PSD->GetN()); };

  /**
   * @brief Returns the maximum number of sub-segments in the buffer.
   * @param[in] aParity 0 for even subsegments, 1 for odd subsegments
   */
  inline unsigned int GetNSubSegmentsMax(const unsigned int aParity){
    return nSubSegments[aParity%2];
  };

  /**
   * @brief Returns the effective circular buffer length [s] used to compute the PSD.
   */
  inline unsigned int GetDataBufferLength(void){ return (double)nSubSegments[0]/GetSpectrumResolution(); };

 private:

  unsigned int fVerbosity;     ///< verbosity level.

  // derived parameters
  unsigned int nSubSegments[2];///< number of even/odd sub-subsegments.

  // containers
  double *HannWindow;          ///< hann window vector.
  TGraph *PSD;                 ///< PSD(f).
  TGraph *logPSD;              ///< log(PSD)(f).
  TGraph **periodogram[2];     ///< buffer for periodograms.
  unsigned int write_index[2]; ///< write index in the buffer.

  fft *fft_psd;                ///< FFT plan to compute the PSD

  void ComputeMedianMean(void);///< Computes median-mean PSD.
  
  ClassDef(Spectrum,0)  
};

#endif


