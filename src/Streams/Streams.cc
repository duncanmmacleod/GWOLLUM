/**
 * @file 
 * @brief See Streams.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Streams.h"

ClassImp(Streams)

/////////////////////////////////////////////////////////////////////////
Streams::Streams(const string aName, const unsigned int aVerbose): Sample(aVerbose) { 
/////////////////////////////////////////////////////////////////////////

  // init
  verbose=aVerbose;
  
  // set name
  SetName(aName);
}

/////////////////////////////////////////////////////////////////////////
Streams::~Streams(void){
/////////////////////////////////////////////////////////////////////////
  if(verbose>1) cout<<"Streams::~Streams"<<endl;
}

/////////////////////////////////////////////////////////////////////////
void Streams::SetName(const string aName){
/////////////////////////////////////////////////////////////////////////
 
  Name=aName;

  if(Name.length()==0)
    Name="00:none";
  else if(aName.length()<4)
    Name="00:"+Name;
  else;
  
  // 00 prefix
  if((Name.substr(2,1)).compare(":")) Name="00:"+Name;

  // prefix/suffix
  NamePrefix=Name.substr(0,2);
  NameSuffix=Name.substr(3,Name.length()-3);
  Name=NamePrefix+":"+NameSuffix;
  NameSuffixUnderScore=ReplaceAll(NameSuffix,"-","_");
  
  // make a detector
  MakeDetector();

  return;
}

/////////////////////////////////////////////////////////////////////////
void Streams::GetDetectorAMResponse(double &aFplus, double &aFcross,
                               const double aRa, const double aDec,
                               const double aPsi, const double aGmst){
/////////////////////////////////////////////////////////////////////////

  double X[3];
  double Y[3];

  // Greenwich hour angle of source (radians)
  const double gha = aGmst - aRa;

  // pre-compute trig functions
  const double cosgha = cos(gha);
  const double singha = sin(gha);
  const double cosdec = cos(aDec);
  const double sindec = sin(aDec);
  const double cospsi = cos(aPsi);
  const double sinpsi = sin(aPsi);

  // Note that dec = pi/2 - theta, and gha =
  // -phi where theta and phi are the standard spherical coordinates
  // used in that paper
  X[0] = -cospsi * singha - sinpsi * cosgha * sindec;
  X[1] = -cospsi * cosgha + sinpsi * singha * sindec;
  X[2] =  sinpsi * cosdec;
  Y[0] =  sinpsi * singha - cospsi * cosgha * sindec;
  Y[1] =  sinpsi * cosgha + cospsi * singha * sindec;
  Y[2] =  cospsi * cosdec;

  aFplus = aFcross = 0.0;
  double DX, DY;
  for(unsigned int i = 0; i < 3; i++){
    DX = Response[i][0] * X[0] + Response[i][1] * X[1] + Response[i][2] * X[2];
    DY = Response[i][0] * Y[0] + Response[i][1] * Y[1] + Response[i][2] * Y[2];
    aFplus  += X[i] * DX - Y[i] * DY;
    aFcross += X[i] * DY + Y[i] * DX;
  }

  return;
}

/////////////////////////////////////////////////////////////////////////
void Streams::MakeDetector(void){
/////////////////////////////////////////////////////////////////////////

  // get det index based on the prefix
  DetIndex=::GetDetectorIndex(NamePrefix);
  if(verbose) cout<<"Streams::MakeDetector: this stream is associated to detector: "<<DET_NAME[DetIndex]<<" ("<<DET_PREFIX[DetIndex]<<")"<<endl;

  // compute detector response
  ComputeDetectorResponse();

  // detector location
  DetLoc[0] = DET_VERTEX_LOCATION_X_SI[DetIndex];
  DetLoc[1] = DET_VERTEX_LOCATION_Y_SI[DetIndex];
  DetLoc[2] = DET_VERTEX_LOCATION_Z_SI[DetIndex];

  return;
}

/////////////////////////////////////////////////////////////////////////
void Streams::ComputeDetectorResponse(void){
/////////////////////////////////////////////////////////////////////////
  Response[0][0]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[0][1]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[0][2]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);
  Response[1][0]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[1][1]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[1][2]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);
  Response[2][0]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[2][1]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[2][2]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);

  return;
}
