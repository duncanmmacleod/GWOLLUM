/**
 * @file 
 * @brief Program to print the channels included in a FFL (or LCF) file.
 * @snippet this gwl-print-channels-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"

using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  // check the arguments
  if(argc<2){
     //! [gwl-print-channels-usage]
    cerr<<"This program prints the channels included in a FFL (or LCF) file."<<endl;
    cerr<<"The channel sampling frequency (Hz) is also printed."<<endl;
    cerr<<""<<endl;
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [ffl file] ([GPS time])"<<endl; 
    cerr<<""<<endl;
    //! [gwl-print-channels-usage]
    return 1;
  }

  // FFL object
  ffl *FFL = new ffl((string)argv[1]);

  // activate multiple loading
  FFL->SetTrials(5,2);

  if(argc==2) return !(FFL->PrintChannels(0));
  else return !(FFL->PrintChannels((unsigned int)atoi(argv[2])));
     
  return 0;
}

