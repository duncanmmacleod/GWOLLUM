/**
 * @file 
 * @brief See Sample.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Sample.h"

ClassImp(Sample)


////////////////////////////////////////////////////////////////////////////////////
Sample::Sample(const unsigned int aVerbosity){
////////////////////////////////////////////////////////////////////////////////////

  // parameters
  fNativeFrequency   = 0;
  fWorkingFrequency  = 0;
  fHighPassFrequency = 0.0; // no highpassing
  fTukeyFraction     = 0.0; // no tukey window
  fRemoveDC          = false; // no DC removal
  fVerbosity         = aVerbosity;

  // tukey window
  fTukeySize=0;
  fTukey = new double [fTukeySize];

  // nulling filters
  for(unsigned int i=0; i<NORDER; i++)// downsampling
    iirFilter[i]=NULL;
  
  for(unsigned int i=0; i<NORDERHP; i++)// highpass
    iirFilterHP[i]=NULL;
  
}

////////////////////////////////////////////////////////////////////////////////////
Sample::~Sample(){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity>1) cout<<"Sample::~Sample"<<endl;
  for(unsigned int i=0; i<NORDER; i++)
    if(iirFilter[i]!=NULL) delete iirFilter[i];
  for(unsigned int i=0; i<NORDERHP; i++)
    if(iirFilterHP[i]!=NULL) delete iirFilterHP[i];
  delete fTukey;
}

////////////////////////////////////////////////////////////////////////////////////
bool Sample::Transform(const unsigned int aInSize, double *aInData, const unsigned int aOutSize, double *aOutData){
////////////////////////////////////////////////////////////////////////////////////
  if(fNativeFrequency==0){
    return false;
  }

  // check size consistency
  unsigned int induration  = aInSize/fNativeFrequency;
  unsigned int outduration = aOutSize/fWorkingFrequency;
  if((double)induration!=(double)aInSize/(double)fNativeFrequency){
    cerr<<"Sample::Transform: the input vector must have a integer duration"<<endl;
    return false;
  }
  if((double)outduration!=(double)aOutSize/(double)fWorkingFrequency){
    cerr<<"Sample::Transform: the output vector must have a integer duration"<<endl;
    return false;
  }
  if(outduration!=induration){
    cerr<<"Sample::Transform: the input and output vectors must have the same duration"<<endl;
    return false;
  }

  // requested DC removal
  if(fRemoveDC){
    if(fVerbosity) cout<<"Sample::Transform: remove DC component..."<<endl;
    RemoveDC(aInSize, aInData);
  }

  // requested highpass filtering
  if(fHighPassFrequency>0.0){
    if(fVerbosity) cout<<"Sample::Transform: highpass input data..."<<endl;
    HighPass(aInSize, aInData);
  }

  // requested windowing
  if((fTukeyFraction>0.0) && (fTukeyFraction<=1.0)){
    if(fVerbosity) cout<<"Sample::Transform: window data..."<<endl;
    if(aInSize!=fTukeySize){
      delete fTukey;
      fTukey=GetTukeyWindow(aInSize, fTukeyFraction);
    }
    for(unsigned int i=0; i<aInSize; i++) aInData[i]*=fTukey[i];
  }

  // requested resampling
  if(fWorkingFrequency!=fNativeFrequency){
    if(fVerbosity) cout<<"Sample::Transform: resample input data..."<<endl;
    Resample(aInSize, aInData, aOutData);
  }
  else// make a copy
    for(unsigned int i=0; i<aInSize; i++) aOutData[i]=aInData[i];

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Sample::SetFrequencies(const unsigned int aNativeFrequency,
                            const unsigned int aWorkingFrequency,
                            const double aHighPassFrequency){
////////////////////////////////////////////////////////////////////////////////////
  
  if(aWorkingFrequency>aNativeFrequency){
    cerr<<"Sample::SetFrequencies: upsampling is not supported"<<endl;
    return false;
  }
  if (!IsPowerOfTwo(aWorkingFrequency)){
    cerr<<"Sample::SetFrequencies: the working sampling frequency must be a power of 2"<<endl;
    return false;
  }

  // highpass
  if(aHighPassFrequency<0.0){
    cerr<<"Sample::SetFrequencies: the highpass frequency is not valid"<<endl;
    return false;
  }
  if(aHighPassFrequency>=aWorkingFrequency){
    cerr<<"Sample::SetFrequencies: the highpass frequency is larger than the working sampling frequency"<<endl;
    return false;
  }

  // update filters
  if(aNativeFrequency!=fNativeFrequency){
    if(fVerbosity)
      cout<<"Sample::SetFrequencies: new native sampling frequency ("<<aNativeFrequency<<"Hz)"<<endl;
    fNativeFrequency  = aNativeFrequency;

    if(aWorkingFrequency!=fWorkingFrequency){
      if(fVerbosity)
        cout<<"Sample::SetFrequencies: new working sampling frequency ("<<aWorkingFrequency<<"Hz)"<<endl;
      fWorkingFrequency = aWorkingFrequency;
      
      if(aHighPassFrequency!=fHighPassFrequency){
	if(fVerbosity)
          cout<<"Sample::SetFrequencies: new highpass frequency ("<<aHighPassFrequency<<"Hz)"<<endl;
	fHighPassFrequency = aHighPassFrequency;
      }
    }
    MakeSamplingFilters();
    MakeHighPassFilters();
  }

  else if(aWorkingFrequency!=fWorkingFrequency){
    if(fVerbosity)
      cout<<"Sample::SetFrequencies: new working sampling frequency ("<<aWorkingFrequency<<"Hz)"<<endl;
    fWorkingFrequency = aWorkingFrequency;
    
    if(aHighPassFrequency!=fHighPassFrequency){
      if(fVerbosity)
        cout<<"Sample::SetFrequencies: new highpass frequency ("<<aHighPassFrequency<<"Hz)"<<endl;
      fHighPassFrequency = aHighPassFrequency;
      MakeHighPassFilters();
    }
    MakeSamplingFilters();
  }

  else if(aHighPassFrequency!=fHighPassFrequency){
    if(fVerbosity)
      cout<<"Sample::SetFrequencies: new highpass frequency ("<<aHighPassFrequency<<"Hz)"<<endl;
    fHighPassFrequency = aHighPassFrequency;
    MakeHighPassFilters();
  }
  else; // nothing has changed
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::MakeSamplingFilters(void){
////////////////////////////////////////////////////////////////////////////////////

  // presampling is required
  if(!IsPowerOfTwo(fNativeFrequency)){
    presample=true;
    double powerof2=floor(log(fNativeFrequency)/log(2.0));
    fIntermediateFrequency=(int)pow(2.0,powerof2);
    if(fVerbosity>1)
      cout<<"Sample::MakeSamplingFilters: presampling is required ("<<fIntermediateFrequency<<"Hz)"<<endl;
    fResampleFactor = fIntermediateFrequency / fWorkingFrequency;
  }
  else{
    presample=false;
    fIntermediateFrequency=fNativeFrequency;
    fResampleFactor = fNativeFrequency / fWorkingFrequency;
  }
   
  // locals
  double w1;
  double a1;
  double wc;
  double theta,ar,ai;
  
  // build downsampling filters
  ZPGFilter *zpgFilter[NORDER];

  // Filter parameters
  w1=(double)fWorkingFrequency/2.0/(double)fIntermediateFrequency;// Dimensionless frequencies;
  a1=0.1;// Attenuation factor;
  wc=TMath::Tan(TMath::Pi()*w1)*pow(1.0/TMath::Sqrt(a1) - 1.0, -0.5/((double)(NORDER)));
  
  // An order n Butterworth filter has n poles spaced evenly along a
  // semicircle in the upper complex w-plane.  By pairing up poles
  // symmetric across the imaginary axis, the filter can be decomposed
  // into [n/2] filters of order 2, plus perhaps an additional order 1
  // filter.  The following loop pairs up poles and applies the
  // filters with order 2
  if(fVerbosity>1) cout<<"Sample::MakeSamplingFilters: building butterworth filters"<<endl;
  for(int i=0,j=NORDER-1; i<j; i++,j--){
    theta=TMath::Pi()*(i+0.5)/(double)NORDER;
    ar=wc*TMath::Cos(theta);
    ai=wc*TMath::Sin(theta);
    zpgFilter[i]=new ZPGFilter(0,2);
    zpgFilter[i]->SetGain(-wc*wc, 0.0);
    zpgFilter[i]->SetPole(0, ar, ai);
    zpgFilter[i]->SetPole(1, -ar, ai);
    
    // Transform to the z-plane
    zpgFilter[i]->WToZ();
    
    if(iirFilter[i]!=NULL) delete iirFilter[i];
    iirFilter[i] = new IIRFilter(zpgFilter[i]);
    delete zpgFilter[i];
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::MakeHighPassFilters(void){
////////////////////////////////////////////////////////////////////////////////////

  // locals
  double w1;
  double a1;
  double wc;
  double theta,ar,ai;
  
  // build high-pass filters
  ZPGFilter *zpgFilterHP[NORDERHP];

  // Filter parameters
  w1=fHighPassFrequency/(double)fNativeFrequency;// Dimensionless frequency;
  a1=0.98;// Attenuation factor;
  wc=TMath::Tan(TMath::Pi()*w1)*pow(1.0/sqrt(a1) - 1.0, 0.5/((double)(NORDERHP)));
  
  if(fVerbosity>1) cout<<"Sample::SetHighPassFrequency: building high-pass filters"<<endl;
  for(int i=0,j=NORDERHP-1; i<j; i++,j--){
    theta=TMath::Pi()*(i+0.5)/(double)NORDERHP;
    ar=wc*TMath::Cos(theta);
    ai=wc*TMath::Sin(theta);
    zpgFilterHP[i]=new ZPGFilter(2,2);
    zpgFilterHP[i]->SetGain(1.0, 0.0);
    zpgFilterHP[i]->SetZero(0, 0.0, 0.0);
    zpgFilterHP[i]->SetZero(1, 0.0, 0.0);
    zpgFilterHP[i]->SetPole(0, ar, ai);
    zpgFilterHP[i]->SetPole(1, -ar, ai);
      
    // Transform to the z-plane
    zpgFilterHP[i]->WToZ();
      
    if(iirFilterHP[i]!=NULL) delete iirFilterHP[i];
    iirFilterHP[i] = new IIRFilter(zpgFilterHP[i]);
    delete zpgFilterHP[i];
  }
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::RemoveDC(const unsigned int aSize, double *aData){
////////////////////////////////////////////////////////////////////////////////////
  if(!fRemoveDC) return; // nothing to do

  double dc=0.0;
  for(unsigned int i=0; i<aSize; i++) dc+=aData[i];
  dc/=(double)aSize;
  for(unsigned int i=0; i<aSize; i++) aData[i] -= dc;
 
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::HighPass(const unsigned int aSize, double *aData){
////////////////////////////////////////////////////////////////////////////////////

  // apply filter
  if(fVerbosity) cout<<"Sample::HighPass: apply filters"<<endl;
  for(unsigned int i=0,j=NORDERHP-1; i<j; i++,j--)
    iirFilterHP[i]->Apply(aSize, aData);

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::PreSample(unsigned int &aSize, double *aData){
////////////////////////////////////////////////////////////////////////////////////
      
  // input vector duration
  unsigned int duration = aSize/fNativeFrequency;
  unsigned int newsize  = duration*fIntermediateFrequency;

  double si,ei,sj,ej,newvalue,wsum;
  unsigned int istart=0;
  for (unsigned int i=0; i<newsize; i++){// NEW vector
    si=(double)i/(double)fIntermediateFrequency;// bin start
    ei=(double)(i+1)/(double)fIntermediateFrequency;// bin end
    newvalue=0;
    wsum=0;
    for (unsigned int j=istart; j<aSize; j++){// OLD vector
      sj=(double)j/(double)fNativeFrequency;// bin start
      ej=(double)(j+1)/(double)fNativeFrequency;// bin end
      if(ej<si){ continue; }
      else if(sj<si&&ej>=si){ newvalue+=((ej-si)*aData[j]); wsum+=(ej-si); }
      else if(sj>=si&&ej<ei){ newvalue+=((ej-sj)*aData[j]); wsum+=(ej-sj); }
      else if(sj<ei&&ej>=ei){ newvalue+=((ei-sj)*aData[j]); wsum+=(ei-sj); }
      else{ istart=j-1; break; }
    }
    if(wsum) newvalue/=wsum;
    else{
      newvalue=0.0;
    }

    aData[i] = newvalue;

  }

  // set new size
  aSize=newsize;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Sample::Resample(unsigned int aSize, double *aInData, double *aOutData){
////////////////////////////////////////////////////////////////////////////////////

  // get new size
  unsigned int duration = aSize/fNativeFrequency;
  unsigned int outsize = duration*fWorkingFrequency;

  // presampling required
  if(presample){
    if(fVerbosity) cout<<"Sample::Resample: apply pre-sampling"<<endl;
    PreSample(aSize, aInData);
  }

  // apply filter
  if(fResampleFactor>1){
    if(fVerbosity) cout<<"Sample::Resample: apply anti-aliasing filters"<<endl;
    for(unsigned int ii=0,jj=NORDER-1; ii<jj; ii++,jj--)
      iirFilter[ii]->Apply(aSize, aInData);
  }

  //decimate time series
  if(fVerbosity) cout<<"Sample::Resample: decimate time-series"<<endl;
  for (unsigned int j=0; j<outsize; ++j){
    aOutData[j] = *aInData;
    aInData += fResampleFactor;
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
ZPGFilter::ZPGFilter(const unsigned int aNumZeros, const unsigned int aNumPoles){ 
////////////////////////////////////////////////////////////////////////////////////
 
  // gain
  gain[0]=0;
  gain[1]=0;
    
  // zeros
  nzeros=aNumZeros;
  zeros[0] = new double [nzeros];
  zeros[1] = new double [nzeros];
  
  // poles
  npoles=aNumPoles;
  poles[0] = new double [npoles];
  poles[1] = new double [npoles];

}

////////////////////////////////////////////////////////////////////////////////////
ZPGFilter::~ZPGFilter(void){ 
////////////////////////////////////////////////////////////////////////////////////
  delete zeros[0];
  delete zeros[1];
  delete poles[0];
  delete poles[1];
}

////////////////////////////////////////////////////////////////////////////////////
void ZPGFilter::WToZ(void){ 
////////////////////////////////////////////////////////////////////////////////////

  // locals
  double *g0, *g1, *b0, *b1;

  // Compute the total number of zeros and poles in the w-plane,
  // including those at w=infinity
  unsigned int numPoles=npoles;
  unsigned int numZeros=nzeros;
  unsigned int num = (numZeros>numPoles) ? numZeros : numPoles;
  numZeros=numPoles=num;

  // Compute the revised number of zeros and poles in the z-plane,
  // excluding those at z=infinity (w=-i)
  for(unsigned int i=0; i<npoles; i++)
    if(poles[0][i]==0.0&&poles[1][i]==-1.0) numPoles--;
  for(unsigned int i=0; i<nzeros; i++)
    if(zeros[0][i]==0.0&&zeros[1][i]==-1.0) numZeros--;

  // new gains
  double *newgain[2];
  unsigned int ng = npoles+nzeros;
  newgain[0] = new double [ng];
  newgain[1] = new double [ng];

  // new zeros
  double *newzeros[2];
  newzeros[0] = new double [numZeros];
  newzeros[1] = new double [numZeros];

  // new poles
  double *newpoles[2];
  newpoles[0] = new double [numPoles];
  newpoles[1] = new double [numPoles];

  // Transform existing zeros from w to z, except for those at w=-i,
  // which are mapped to z=infinity.  At the same time, compute the
  // gain correction factors.
  g0=newgain[0];
  g1=newgain[1];
  b0=newzeros[0];
  b1=newzeros[1];
  double zr, zi, ratio, denom;
  unsigned int iz=0, ig=0;
  for(unsigned int i=0; i<nzeros; i++,ig++){
    zr=zeros[0][i];
    zi=zeros[1][i];
    if(zr==0.0){
      if(zi==-1.0){// w=-i is mapped to z=infinity
	g0[ig]=0.0;
	g1[ig]=2.0;
      }
      else{// w=i*y is mapped to z=(1-y)/(1+y)
	b0[iz]=(1.0-zi)/(1.0+zi);
	b1[iz]=0.0;
	g0[ig]=0.0;
	g1[ig]=-(1.0+zi);
	iz++;
      }
    }
    else if(fabs(1.0+zi)>fabs(zr)){
      ratio = -zr/(1.0+zi);
      denom = 1.0+zi - ratio*zr;
      b0[iz] = (1.0-zi + ratio*zr)/denom;
      b1[iz] = (zr - ratio*(1.0-zi))/denom;
      g0[ig] = -zr;
      g1[ig] = -(1.0+zi);
      iz++;
    }
    else{
      ratio = -(1.0+zi)/zr;
      denom = -zr + ratio*(1.0+zi);
      b0[iz] = ((1.0-zi)*ratio + zr)/denom;
      b1[iz] = (zr*ratio - 1.0+zi)/denom;
      g0[ig] = -zr;
      g1[ig] = -(1.0+zi);
      iz++;
    }
  }
  // Transform any remaining zeros at w=infinity to z=-1
  for(; iz<numZeros; iz++){
    b0[iz] = -1.0;
    b1[iz] = 0.0;
  }
  // Replace the old filter zeros with the new ones
  if(nzeros>0){
    delete zeros[0];
    delete zeros[1];
  }
  zeros[0] = b0;
  zeros[1] = b1;
  nzeros=numZeros;

  // Transform existing poles from w to z, except for those at w=-i,
  // which are mapped to z=infinity.  At the same time, compute the
  // gain correction factors
  b0=newpoles[0];
  b1=newpoles[1];
  double pr, pi;
  unsigned int ip=0;
  for(unsigned int i=0; i<npoles; i++,ig++){
    pr=poles[0][i];
    pi=poles[1][i];
    if(pr==0.0){
      if(pi==-1.0){// w=-i is mapped to z=infinity
	g0[ig]=0.0;
	g1[ig]=-0.5;
      }
      else{// w=i*y is mapped to z=(1-y)/(1+y)
	b0[ip]=(1.0-pi)/(1.0+pi);
	b1[ip]=0.0;
	g0[ig]=0.0;
	g1[ig]=1.0/(1.0+pi);
	ip++;
      }
    }
    else if(fabs(1.0+pi)>fabs(pr)){
      ratio = -pr/(1.0+pi);
      denom = 1.0+pi - ratio*pr;
      b0[ip] = (1.0-pi + ratio*pr)/denom;
      b1[ip] = (pr - ratio*(1.0-pi))/denom;
      g0[ig] = ratio/denom;
      g1[ig] = 1.0/denom;
      ip++;
    }
    else{
      ratio = -(1.0+pi)/pr;
      denom = -pr + ratio*(1.0+pi);
      b0[ip] = ((1.0-pi)*ratio + pr)/denom;
      b1[ip] = (pr*ratio - 1.0+pi)/denom;
      g0[ig] = ratio/denom;
      g1[ig] = 1.0/denom;
      ip++;
    }
  }
  // Transform any remaining poles at w=infinity to z=-1
  for(; ip<numPoles; ip++){
    b0[ip] = -1.0;
    b1[ip] = 0.0;
  }
  // Replace the old filter poles with the new ones
  if(npoles>0){
    delete poles[0];
    delete poles[1];
  }
  poles[0] = b0;
  poles[1] = b1;
  npoles=numPoles;

  // FIXME: the gains should be multiplied alternately by small and large correction factors
  // Multiply the small and largest factors together
  unsigned int i=0,j=ng-1;
  for(;i<j;i++,j--){
    double ar=g0[i];
    double ai=g1[i];
    double br=g0[j];
    double bi=g1[j];
    double cr=ar*br-ai*bi;
    double ci=ar*bi+ai*br;
  
    // Multiply the gain by the combined factor
    br=gain[0];
    bi=gain[1];
    gain[0]=br*cr-bi*ci;
    gain[1]=br*ci+bi*cr;
  }
  if(i==j){
    /* Multiply by the remaining odd factor. */
    double cr=g0[i];
    double ci=g1[i];
    double br=gain[0];
    double bi=gain[1];

    gain[0]=br*cr-bi*ci;
    gain[1]=br*ci+bi*cr;
  }

  delete newgain[0];
  delete newgain[1];

  return;
}

////////////////////////////////////////////////////////////////////////////////////
IIRFilter::IIRFilter(ZPGFilter *aZpgFilter){ 
////////////////////////////////////////////////////////////////////////////////////
 
  nzeros=aZpgFilter->nzeros;
  npoles=aZpgFilter->npoles;
  zeros[0]=aZpgFilter->zeros[0];
  zeros[1]=aZpgFilter->zeros[1];
  poles[0]=aZpgFilter->poles[0];
  poles[1]=aZpgFilter->poles[1];
  unsigned int num=0;
  
  numDirect=1;
  for(unsigned int i=0; i<nzeros; i++)
    if(zeros[1][i]==0.0){
      num+=1;
      if(zeros[0][i]==0.0)
	numDirect-=1;
    }
    else if(zeros[1][i]>0.0){
      num+=2;
    }
  
  numRecurs=1; num=0;
  for(unsigned int i=0; i<npoles; i++)
    if(poles[1][i]==0.0){
      num+=1;
      if(poles[0][i]==0.0)
	numRecurs-=1;
    }
    else if(poles[1][i]>0.0){
      num+=2;
    }
  
  num = (npoles>=nzeros) ? nzeros :  npoles;
  numDirect+=num;
  numRecurs+=num;

  directCoef = new double [numDirect];
  recursCoef = new double [numRecurs];
    
  double *direct;// The direct filter coefficients
  double *recurs;// The recursive filter coefficients
  direct=directCoef;
  recurs=recursCoef;

  // Expand the denominator as a polynomial in z
  *recurs=-1.0;
  for(unsigned int i=1; i<numRecurs; i++) recurs[i]=0.0;
  for(unsigned int i=0; i<npoles; i++){
    unsigned int j=numRecurs-1;
    double x=poles[0][i];
    double y=poles[1][i];
    if(y==0.0)
      for(; j>0; j--) recurs[j]-=x*recurs[j-1];
    else if(y>0.0){
      for(; j>1; j--) recurs[j]-=2.0*x*recurs[j-1]-(x*x+y*y)*recurs[j-2];
      recurs[j]-=2.0*x*recurs[j-1];
    }
  }

  // Expand the numerator as a polynomial in z
  for(unsigned int i=0; i<numDirect; i++) direct[i]=0.0;
  direct[num-nzeros]=aZpgFilter->gain[0];
  for(unsigned int i=0; i<nzeros; i++){
    unsigned int j=numDirect-1;
    double x=zeros[0][i];
    double y=zeros[1][i];
    if(y==0.0)
      for(; j>num-nzeros; j--) direct[j]-=x*direct[j-1];
    else if(y>0.0){
      for(; j>num-nzeros+1; j--) direct[j]-=2.0*x*direct[j-1]-(x*x+y*y)*direct[j-2];
      direct[j]-=2.0*x*direct[j-1];
    }
  }

  // Initialize filter history
  if(numDirect>=numRecurs)
    num=numDirect-1;
  else
    num=numRecurs-1;
  history = new double [numRecurs];
  history_init = new double [numRecurs];
  for(unsigned int i=0; i<num; i++) history[i]=0.0;
  for(unsigned int i=0; i<num; i++) history_init[i]=history[i];
 
}

////////////////////////////////////////////////////////////////////////////////////
IIRFilter::~IIRFilter(void){ 
////////////////////////////////////////////////////////////////////////////////////

  // CAUTION: poles and zeroes do not need to be freed 
  // since they are to be freed for the ZPGFilter structure given as input
  delete directCoef;
  delete recursCoef;
  delete history;
  delete history_init;

}

////////////////////////////////////////////////////////////////////////////////////
void IIRFilter::Apply(const unsigned int aSize, double *aData){
////////////////////////////////////////////////////////////////////////////////////
  
  double *temp = new double [numRecurs];
  double datum;
  int i,j,k;
   
  // Compute the auxiliary data series
  for(i=0; (i<(int)numRecurs)&&(i<aSize); i++,aData++){
    datum=*aData;
    for(j=1; j<=i; j++)
      datum+=aData[-j]*recursCoef[j];
    for(k=0; j<(int)numRecurs; j++,k++)
      datum+=history[k]*recursCoef[j];
    *aData=datum;
  }
  
  for(; i<(int)aSize; i++, aData++){
    datum=*aData;
    for(j=1; j<(int)numRecurs; j++)
      datum+=aData[-j]*recursCoef[j];
    *aData=datum;
  }
  aData--;
  
  // Store the last few auxiliary data to the temporary history
  for(k=(int)numRecurs-1; k>=(int)aSize; k--)
    temp[k]=history[k-aSize];
  for(; k>=0; k--)
    temp[k]=aData[-k];
  
  // Compute the output data series
  for(; i>(int)numDirect; i--,aData--){
    datum=*aData*directCoef[0];
    for(j=1; j<(int)numDirect; j++)
      datum+=aData[-j]*directCoef[j];
    *aData=datum;
  }
  for(; i>0; i--,aData--){
    datum=*aData*directCoef[0];
    for(j=1; j<i; j++)
      datum+=aData[-j]*directCoef[j];
    for(k=0; j<(int)numDirect; j++,k++)
      datum+=history[k]*directCoef[j];
    *aData=datum;
  }
  
  // Update the filter history from the temporary history
  for(k=0; k<(int)numRecurs; k++)
    history[k]=temp[k];
  delete temp;
  
  // prepare for reverse
  aData+=aSize;
  
  // Perform the auxilliary piece of the filter
  for(i=0; (i<(int)numRecurs)&&(i<(int)aSize); i++,aData--){
    datum=*aData;
    for(j=1; j<=i; j++)
      datum+=aData[j]*recursCoef[j];
    *aData=datum;
  }
  
  for(; i<(int)aSize; i++,aData--){
    datum=*aData;
    for(j=1; j<(int)numRecurs; j++)
      datum+=aData[j]*recursCoef[j];
    *aData=datum;
  }
  
  aData++;
  
  // Perform the direct piece of the filter
  for(; i>(int)numDirect; i--,aData++){
    datum=*aData*directCoef[0];
    for(j=1; j<(int)numDirect; j++)
      datum+=aData[j]*directCoef[j];
    *aData=datum;
  }
  for(; i>0; i--,aData++){
    datum=*aData*directCoef[0];
    for(j=1; j<i; j++)
      datum+=aData[j]*directCoef[j];
    *aData=datum;
  }
  
  // reset history
  for(int i=0; i<(int)numRecurs; i++) history[i]=history_init[i];
  
  // back to begining
  aData-=aSize;
  
  return;
}
