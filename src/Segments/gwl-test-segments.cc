/**
 * @file 
 * @brief Program to test the Segments class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Segments.h"

using namespace std;

/**
 * @brief Test program.
 */
int main (int argc, char* argv[]){

  //*********************************************************************
  // Segments(void)
  //*********************************************************************
  cout<<"\nSegments(void)"<<endl;
  Segments *S = new Segments();
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=0) return 1;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 1;
  delete S;

  //*********************************************************************
  // Segments(const double aGpsStart, const double aGpsEnd, const int aTag)
  //*********************************************************************
  cout<<"\nSegments(const double aGpsStart, const double aGpsEnd, const int aTag)"<<endl;
  S = new Segments(1300000000.0, 1300000100.0, 10);
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=1) return 2;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 2;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1300000000.0) return 2;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1300000100.0) return 2;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=10) return 2;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=100.0) return 2;
  cout<<"\tGetDuration()"<<endl;
  if(S->GetDuration(0)!=100.0) return 2;

  //*********************************************************************
  // Append(const double aGpsStart, const double aGpsEnd, const int aTag)
  //*********************************************************************
  cout<<"\nAppend(const double aGpsStart, const double aGpsEnd, const int aTag)"<<endl;

  // test bad append
  cout<<"\tAppend()"<<endl;
  if(S->Append(1200000000.0, 1200000100.0, 20)==true) return 3;
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=1) return 3;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 3;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1300000000.0) return 3;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1300000100.0) return 3;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=10) return 3;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=100.0) return 3;

  // test good append
  // 1300000000.0 1300000100.0 10
  // 1300000200.0 1300000300.0 20
  cout<<"\tAppend()"<<endl;
  if(S->Append(1300000200.0, 1300000300.0, 20)==false) return 3;
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=2) return 3;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 3;

  // livetime
  cout<<"\tGetLiveTime() 1"<<endl;
  if(S->GetLiveTime()!=200.0) return 3;
  cout<<"\tGetLiveTime() 2"<<endl;
  if(S->GetLiveTime(1300000100, 1300000200)!=0.0) return 3;
  cout<<"\tGetLiveTime() 3"<<endl;
  if(S->GetLiveTime(1200000100, 1200000200)!=0.0) return 3;
  cout<<"\tGetLiveTime() 4"<<endl;
  if(S->GetLiveTime(1300000000.0, 1300000100.0)!=100.0) return 3;
  cout<<"\tGetLiveTime() 5"<<endl;
  if(S->GetLiveTime(1300000000.0, 1300000800.0)!=200.0) return 3;
  cout<<"\tGetLiveTime() 6"<<endl;
  if(S->GetLiveTime(1300000050.0, 1300000250.0)!=100.0) return 3;
  cout<<"\tGetLiveTimeWithTag() 1"<<endl;
  if(S->GetLiveTimeWithTag(10)!=100.0) return 3;
  cout<<"\tGetLiveTimeWithTag() 2"<<endl;
  if(S->GetLiveTimeWithTag(20)!=100.0) return 3;
  cout<<"\tGetLiveTimeWithTag() 3"<<endl;
  if(S->GetLiveTimeWithTag(30)!=0.0) return 3;

  //*********************************************************************
  // IsInsideSegment(unsigned int &aSegmentIndex, const double aGps)
  //*********************************************************************
  cout<<"\nIsInsideSegment(unsigned int &aSegmentIndex, const double aGps)"<<endl;
  unsigned int seg_index=10;

  cout<<"\tIsInsideSegment() 1"<<endl;
  if(S->IsInsideSegment(seg_index, 1200000100.0)==true) return 4;
  cout<<"\tIsInsideSegment() 2"<<endl;
  if(S->IsInsideSegment(seg_index, 1400000100.0)==true) return 4;
  cout<<"\tIsInsideSegment() 3"<<endl;
  if(S->IsInsideSegment(seg_index, 1300000300.0)==true) return 4;
  cout<<"\tIsInsideSegment() 4"<<endl;
  if(S->IsInsideSegment(seg_index, 1300000200.0)==false) return 4;
  if(seg_index!=1) return 4;
  cout<<"\tIsInsideSegment() 5"<<endl;
  if(S->IsInsideSegment(seg_index, 1300000050.0)==false) return 4;
  if(seg_index!=0) return 4;

  //*********************************************************************
  // Append(Segments *aSeg)
  //*********************************************************************
  cout<<"\nAppend(Segments *aSeg)"<<endl;
  Segments *Stest = new Segments(1300000000.0, 1300000150.0);
  
  // test bad append
  cout<<"\tAppend() 1"<<endl;
  if(S->Append(Stest)==true) return 5;
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=2) return 5;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 5;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=200.0) return 5;
  delete Stest;

  // test good append
  // 1300000000.0 1300000100.0 10
  // 1300000200.0 1300000400.0 20
  // 1300000500.0 1300000600.0 1
  Stest = new Segments(1300000203.0, 1300000400.0);
  Stest->Append(1300000500.0, 1300000600.0);
  cout<<"\tAppend() 2"<<endl;
  if(S->Append(Stest)==false) return 5;
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=3) return 5;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 5;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=400.0) return 5;
  cout<<"\tGetLiveTimeWithTag()"<<endl;
  if(S->GetLiveTimeWithTag(1)!=100.0) return 5;

  //*********************************************************************
  // AddSegment(const double aGpsStart, const double aGpsEnd, const int aTag)
  //*********************************************************************
  cout<<"\nAddSegment(const double aGpsStart, const double aGpsEnd, const int aTag)"<<endl;

  cout<<"\tAddSegment() 1"<<endl;
  if(S->AddSegment(1200000000.0, 1200000100.0)==false) return 5;
  // 1200000000.0 1200000100.0 1
  // 1300000000.0 1300000100.0 10
  // 1300000200.0 1300000400.0 20
  // 1300000500.0 1300000600.0 1
  cout<<"\tAddSegment() 2"<<endl;
  if(S->AddSegment(1299999900.0, 1300000000.0)==false) return 5;
  // 1200000000.0 1200000100.0 1
  // 1299999900.0 1300000100.0 10
  // 1300000200.0 1300000400.0 20
  // 1300000500.0 1300000600.0 1
  cout<<"\tAddSegment() 3"<<endl;
  if(S->AddSegment(1300000200.0, 1300000500.0)==false) return 5;
  // 1200000000.0 1200000100.0 1
  // 1299999900.0 1300000100.0 10
  // 1300000200.0 1300000600.0 20
  cout<<"\tAddSegment() 4"<<endl;
  if(S->AddSegment(1300000600.0, 1300000700.0,10)==false) return 5;
  // 1200000000.0 1200000100.0 1
  // 1299999900.0 1300000100.0 10
  // 1300000200.0 1300000700.0 20
  cout<<"\tAddSegment() 5"<<endl;
  if(S->AddSegment(1300000800.0, 1300000900.0)==false) return 5;
  // 1200000000.0 1200000100.0 1
  // 1299999900.0 1300000100.0 10
  // 1300000200.0 1300000700.0 20
  // 1300000800.0 1300000900.0 1
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=4) return 5;
  cout<<"\tGetStatus()"<<endl;
  if(S->GetStatus()==false) return 5;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=900.0) return 5;
  cout<<"\tGetFirst()"<<endl;
  if(S->GetFirst()!=1200000000.0) return 5;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1200000000.0) return 5;
  if(S->GetStart(1)!=1299999900.0) return 5;
  if(S->GetStart(2)!=1300000200.0) return 5;
  if(S->GetStart(3)!=1300000800.0) return 5;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1200000100.0) return 5;
  if(S->GetEnd(1)!=1300000100.0) return 5;
  if(S->GetEnd(2)!=1300000700.0) return 5;
  if(S->GetEnd(3)!=1300000900.0) return 5;
  cout<<"\tGetLast()"<<endl;
  if(S->GetLast()!=1300000900.0) return 5;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=1) return 5;
  if(S->GetTag(1)!=10) return 5;
  if(S->GetTag(2)!=20) return 5;
  if(S->GetTag(3)!=1) return 5;

  cout<<"\tAddSegment() 7"<<endl;
  if(S->AddSegment(1299999900.0, 1299999800.0)==true) return 5;
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=4) return 5;
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=900.0) return 5;

  //*********************************************************************
  // ApplyPadding(const double aPadStart, const double aPadEnd)
  //*********************************************************************
  cout<<"\nApplyPadding(const double aPadStart, const double aPadEnd)"<<endl;

  cout<<"\tApplyPadding() 1"<<endl;
  if(S->ApplyPadding(-0.5, 0.5)==false) return 6;
  // 1199999999.50000 1200000100.50000 1
  // 1299999899.50000 1300000100.50000 10
  // 1300000199.50000 1300000700.50000 20
  // 1300000799.50000 1300000900.50000 1
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=904.0) return 6;

  cout<<"\tApplyPadding() 2"<<endl;
  if(S->ApplyPadding(-50, 50)==false) return 6;
  // 1199999949.50000 1200000150.50000 1
  // 1299999849.50000 1300000950.50000 20
  cout<<"\tGetLiveTime()"<<endl;
  if(S->GetLiveTime()!=1302.0) return 6;

  //*********************************************************************
  // MakeSecond(void)
  //*********************************************************************
  cout<<"\nMakeSecond(void)"<<endl;

  cout<<"\tMakeSecond() 1"<<endl;
  if(S->MakeSecond()==false) return 7;
  // 1199999949.00000 1200000151.00000 1
  // 1299999849.00000 1300000951.00000 20
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=2) return 7;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1199999949.0) return 7;
  if(S->GetStart(1)!=1299999849.0) return 7;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1200000151.0) return 7;
  if(S->GetEnd(1)!=1300000951.0) return 7;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=1) return 7;
  if(S->GetTag(1)!=20) return 7;
  
  //*********************************************************************
  // TruncateAfter(const double aGps)
  //*********************************************************************
  cout<<"\nTruncateAfter(const double aGps)"<<endl;
  if(S->AddSegment(1300001000.0, 1300001100.0,10)==false) return 8;
  if(S->AddSegment(1300002000.0, 1300002100.0,10)==false) return 8;
  // 1199999949.00000 1200000151.00000 1
  // 1299999849.00000 1300000951.00000 20
  // 1300001000.00000 1300001100.00000 10
  // 1300002000.00000 1300002100.00000 10
  
  cout<<"\tTruncateAfter()"<<endl;
  if(S->TruncateAfter(1300001200)==false) return 8;
  if(S->TruncateAfter(1300001050)==false) return 8;
  // 1199999949.00000 1200000151.00000 1
  // 1299999849.00000 1300000951.00000 20
  // 1300001000.00000 1300001050.00000 10
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=3) return 7;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1199999949.0) return 8;
  if(S->GetStart(1)!=1299999849.0) return 8;
  if(S->GetStart(2)!=1300001000.0) return 8;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1200000151.0) return 8;
  if(S->GetEnd(1)!=1300000951.0) return 8;
  if(S->GetEnd(2)!=1300001050.0) return 8;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=1) return 8;
  if(S->GetTag(1)!=20) return 8;
  if(S->GetTag(2)!=10) return 8;

  //*********************************************************************
  // Reverse(void)
  //*********************************************************************
  cout<<"\nReverse(void)"<<endl;
  if(S->Reverse()==false) return 9;
  // 700000000.00000 1199999949.00000 1
  // 1200000151.00000 1299999849.00000 1
  // 1300000951.00000 1300001000.00000 1
  // 1300001050.00000 2000000000.00000 1
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=4) return 9;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=700000000.0) return 9;
  if(S->GetStart(1)!=1200000151.0) return 9;
  if(S->GetStart(2)!=1300000951.0) return 9;
  if(S->GetStart(3)!=1300001050.0) return 9;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1199999949.0) return 9;
  if(S->GetEnd(1)!=1299999849.0) return 9;
  if(S->GetEnd(2)!=1300001000.0) return 9;
  if(S->GetEnd(3)!=2000000000.0) return 9;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=1) return 9;
  if(S->GetTag(1)!=1) return 9;
  if(S->GetTag(2)!=1) return 9;
  if(S->GetTag(3)!=1) return 9;

  cout<<"\tReverse(void)"<<endl;
  if(S->Reverse()==false) return 9;
  // 1199999949.00000 1200000151.00000 1
  // 1299999849.00000 1300000951.00000 1
  // 1300001000.00000 1300001050.00000 1
  S->Dump(3);
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=3) return 9;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1199999949.0) return 9;
  if(S->GetStart(1)!=1299999849.0) return 9;
  if(S->GetStart(2)!=1300001000.0) return 9;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1200000151.0) return 9;
  if(S->GetEnd(1)!=1300000951.0) return 9;
  if(S->GetEnd(2)!=1300001050.0) return 9;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=1) return 9;
  if(S->GetTag(1)!=1) return 9;
  if(S->GetTag(2)!=1) return 9;

  //*********************************************************************
  // Intersect(const double aGpsStart, const double aGpsEnd, const int aTag)
  //*********************************************************************
  cout<<"\nIntersect(const double aGpsStart, const double aGpsEnd, const int aTag)"<<endl;
  S->Reset();
  if(S->AddSegment(1200000000.0, 1200000100.0, -1)==false) return 10;
  if(S->AddSegment(1200000200.0, 1200000300.0, -1)==false) return 10;
  if(S->AddSegment(1200000400.0, 1200000500.0, -1)==false) return 10;
  // 1200000000.0 1200000100.0 -1
  // 1200000200.0 1200000300.0 -1
  // 1200000400.0 1200000500.0 -1
  if(S->Intersect(1100000000.0, 1200000250.0, -2)==false) return 10;
  // 1200000000.0 1200000100.0 -2
  // 1200000200.0 1200000250.0 -2
  cout<<"\tGetN()"<<endl;
  if(S->GetN()!=2) return 10;
  cout<<"\tGetStart()"<<endl;
  if(S->GetStart(0)!=1200000000.0) return 10;
  if(S->GetStart(1)!=1200000200.0) return 10;
  cout<<"\tGetEnd()"<<endl;
  if(S->GetEnd(0)!=1200000100.0) return 10;
  if(S->GetEnd(1)!=1200000250.0) return 10;
  cout<<"\tGetTag()"<<endl;
  if(S->GetTag(0)!=-2) return 10;
  if(S->GetTag(1)!=-2) return 10;

  delete S;
  return 0;
}

