/**
 * @file 
 * @brief See Segments.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Segments.h"

ClassImp(Segments)

/**
 * @brief minimum acceptable GPS time.
 */
#define SEGMENTS_START (700000000)

/**
 * @brief Maximum acceptable GPS time.
 */
#define SEGMENTS_END (2000000000)

/////////////////////////////////////////////////////////////////////////
Segments::Segments(void){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags(ios::fixed);
  cerr.flags(ios::fixed);

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;
}

/////////////////////////////////////////////////////////////////////////
Segments::Segments(const string aSegmentFile, const int aDefaultTag){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags ( ios::fixed );
  cerr.flags ( ios::fixed );

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;

  // read segment file
  ReadAscii *readseg = new ReadAscii(aSegmentFile, "UNKNOWN");

  // 2 columns
  if(readseg->GetNCol()==2){
    if(!readseg->SetFormat(0,'d')) status=false;
    if(!readseg->SetFormat(1,'d')) status=false;
    status *= readseg->GetCol(seg_start, 0);
    status *= readseg->GetCol(seg_end, 1);
    seg_tag.assign(seg_start.size(), aDefaultTag);
  }

  // 3 or 4 columns
  else if(readseg->GetNCol()>2&&readseg->GetNCol()<5){
    if(!readseg->SetFormat(1,'d')) status=false;
    if(!readseg->SetFormat(2,'d')) status=false;
    status *= readseg->GetCol(seg_start, 1);
    status *= readseg->GetCol(seg_end, 2);
    seg_tag.assign(seg_start.size(), aDefaultTag);
  }
  else if(readseg->GetNCol()==5){
    if(!readseg->SetFormat(1,'d')) status=false;
    if(!readseg->SetFormat(2,'d')) status=false;
    if(!readseg->SetFormat(4,'i')) status=false;
    status *= readseg->GetCol(seg_start, 1);
    status *= readseg->GetCol(seg_end, 2);
    status *= readseg->GetCol(seg_tag, 4);
  }
  else{
    cerr<<"Segments::Segments: number of col = "<<readseg->GetNCol()<<" is not supported"<<endl;
    status=false;
  }
  
  delete readseg;
  
  // check consistency
  if(status) status*=CheckConsistency(0);

  // merge overlapping segments
  if(status) MergeOverlaps(0);

  // set time marks
  if(status) SetMarks();

  // compute livetime
  if(status) ComputeLiveTime();
  
  if(!status) cerr<<"Segments::Segments: initialization failed ("<<aSegmentFile<<")"<<endl;
}

/////////////////////////////////////////////////////////////////////////
Segments::Segments(TTree *aSegmentTree){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags ( ios::fixed );
  cerr.flags ( ios::fixed );

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;

  // add segments
  status = Append(aSegmentTree);
  
  if(!status) cerr<<"Segments::Segments: initialization failed"<<endl;
}

/////////////////////////////////////////////////////////////////////////
Segments::Segments(const string aFilePattern, const string aDirectory, const string aTreeName){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags ( ios::fixed );
  cerr.flags ( ios::fixed );

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;

  // chain segment trees...
  TChain *SegTree;
  if(aDirectory.compare("")) SegTree= new TChain((aDirectory+"/"+aTreeName).c_str());
  else                       SegTree= new TChain(aTreeName.c_str());

  // get list of patterns and load it
  vector <string> patlist = SplitString(aFilePattern,' ');
  for(unsigned int p=0; p<patlist.size(); p++) SegTree->Add(patlist[p].c_str());
  patlist.clear();

  // load first tree, if any
  if(SegTree->GetNtrees()>0) SegTree->LoadTree(0);

  // add segments
  status = Append(SegTree);

  delete SegTree;
 
  if(!status) cerr<<"Segments::Segments: initialization failed"<<endl;
}

/////////////////////////////////////////////////////////////////////////
Segments::Segments(const vector<double> aGpsStarts, const vector<double> aGpsEnds){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags ( ios::fixed );
  cerr.flags ( ios::fixed );

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;

  // segments
  seg_start=aGpsStarts;
  seg_end=aGpsEnds;
  seg_tag.assign(seg_start.size(), 1);

  // check input consistency
  if(status) status*=CheckConsistency(0);

  // merge overlapping segments
  if(status) MergeOverlaps(0);

  // set time marks
  if(status) SetMarks();

  // compute livetime
  if(status) ComputeLiveTime();
  
  if(!status) cerr<<"Segments::Segments: initialization failed"<<endl;
}

/////////////////////////////////////////////////////////////////////////
Segments::Segments(const double aGpsStart, const double aGpsEnd, const int aTag){ 
/////////////////////////////////////////////////////////////////////////

  // printing options
  cout.precision(5);
  cerr.precision(5);
  cout.flags ( ios::fixed );
  cerr.flags ( ios::fixed );

  // clear
  status=true;
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  time_mark.clear();
  n_mark.clear();
  seg_livetime=0;

  // segment
  seg_start.push_back(aGpsStart);
  seg_end.push_back(aGpsEnd);
  seg_tag.push_back(aTag);
  
  // check input consistency
  if(status) status*=CheckConsistency(0);

  // merge overlapping segments
  if(status) MergeOverlaps(0);

  // set time marks
  if(status) SetMarks();

  // compute livetime
  if(status) ComputeLiveTime();
  
  if(!status) cerr<<"Segments::Segments: initialization failed"<<endl;
}

/////////////////////////////////////////////////////////////////////////
Segments::~Segments(void){
/////////////////////////////////////////////////////////////////////////
  Reset();
}

/////////////////////////////////////////////////////////////////////////
void Segments::Reset(void){
/////////////////////////////////////////////////////////////////////////
  seg_start.clear();
  seg_end.clear();
  seg_tag.clear();
  seg_livetime=0;
  time_mark.clear();
  n_mark.clear();
  status=true;
  return;
}

/////////////////////////////////////////////////////////////////////////
int Segments::GetSegmentStartBefore(const double aGps){
/////////////////////////////////////////////////////////////////////////
  if(!status) return -1;

  // get mark
  int smark = GetMark(aGps);

  // before first segment
  if(smark<0) return -1;
  
  // loop over segments
  for(; smark<(int)seg_start.size(); smark++){
    if(aGps<seg_start[smark]) break;
  }
  
  return smark-1;
}

/////////////////////////////////////////////////////////////////////////
unsigned int Segments::GetNWithTag(const int aTag){
/////////////////////////////////////////////////////////////////////////
  if(!status) return 0;
  unsigned int sum=0;
  for(unsigned int s=0; s<seg_tag.size(); s++)
    if(seg_tag[s]==aTag) sum++;  
  return sum;
}

/////////////////////////////////////////////////////////////////////////
double Segments::GetLiveTime(const double aGpsStart, const double aGpsEnd){
/////////////////////////////////////////////////////////////////////////
  if(!status) return 0;

  // get first index
  int s = GetSegmentStartBefore(aGpsStart);

  // before segment list
  if(s<0) return 0.0;

  double restricted_livetime = 0.0;

  // get intersection
  for(; s<(int)seg_start.size(); s++){

    // out of requested gps segment
    if(seg_end[s]<=aGpsStart) continue;
    if(seg_start[s]>=aGpsEnd) break;

    // add some livetime
    if(seg_start[s]>=aGpsStart&&
       seg_end[s]<=aGpsEnd)
      restricted_livetime+=GetDuration(s);// completely inside
    else if(seg_start[s]<aGpsStart&&
            seg_end[s]<=aGpsEnd)
      restricted_livetime+=(seg_end[s]-aGpsStart);// overlapping the start
    else if(seg_start[s]>=aGpsStart&&
            seg_end[s]>aGpsEnd)
      restricted_livetime+=(aGpsEnd-seg_start[s]);// overlapping the end
    else return aGpsEnd-aGpsStart;
  }

  return restricted_livetime;
}

/////////////////////////////////////////////////////////////////////////
double Segments::GetLiveTimeWithTag(const int aTag){
/////////////////////////////////////////////////////////////////////////
  if(!status) return 0.0;
  double live=0.0;
  for(unsigned int s=0; s<seg_start.size(); s++)
    if(seg_tag[s]==aTag) live+=GetDuration(s);
  return live;
}

/////////////////////////////////////////////////////////////////////////
TH1I* Segments::GetHistogram(const double aGpsStart, const double aGpsEnd, const double aOffset){
/////////////////////////////////////////////////////////////////////////
  if(!status) return NULL;

  // get first index
  int s = GetSegmentStartBefore(aGpsStart);

  // before segment list --> empty histogram
  if(s<0) return (new TH1I("h_seg","Segments", 1, aGpsStart+aOffset, aGpsEnd+aOffset));
  
  vector <double> blimits;
  vector <int> bvalue;

  // get segments
  blimits.push_back(aGpsStart);
  for(; s<(int)seg_start.size(); s++){

    // out-of-range
    if(seg_end[s]<=aGpsStart) continue;
    if(seg_start[s]>=aGpsEnd) break;
    
    if(seg_start[s]>aGpsStart){
      bvalue.push_back(0);
      blimits.push_back(seg_start[s]);
    }
 
    if(seg_end[s]<aGpsEnd){
      bvalue.push_back(1);
      blimits.push_back(seg_end[s]);
    }
  }
  if(seg_end[s-1]<aGpsEnd){
    bvalue.push_back(0);
    blimits.push_back(aGpsEnd);
  }

  // make histogram
  int nbins=(int)bvalue.size();
  double *bins = new double [nbins+1];
  for(int b=0; b<=nbins; b++) bins[b]=blimits[b]+aOffset;
  TH1I *h_seg = new TH1I("h_seg","Segments",nbins,bins);
  for(int b=1; b<=nbins; b++) h_seg->SetBinContent(b,bvalue[b-1]);
  delete bins;

  blimits.clear();
  bvalue.clear();
  return h_seg;
}

/////////////////////////////////////////////////////////////////////////
TTree* Segments::GetTree(const double aGpsStart, const double aGpsEnd){
/////////////////////////////////////////////////////////////////////////
  if(!status) return NULL;

  double t_s, t_e;
  int t_t;
  TTree *t_seg = new TTree("segments","segments");
  t_seg->Branch("start",&t_s, "start/D");
  t_seg->Branch("end",  &t_e, "end/D");
  t_seg->Branch("tag",  &t_t, "tag/I");

  // get first index
  int s = GetSegmentStartBefore(aGpsStart);

  // before segment list --> empty ttree
  if(s<0) return t_seg;

  // get segments
  for(; s<(int)seg_start.size(); s++){

    // out-of-range
    if(seg_end[s]<=aGpsStart) continue;
    if(seg_start[s]>=aGpsEnd) break;

    t_s=TMath::Max(seg_start[s], aGpsStart);
    t_e=TMath::Min(seg_end[s], aGpsEnd);
    t_t=seg_tag[s];
    t_seg->Fill();
  }
      
  return t_seg;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::IsInsideSegment(unsigned int &aSegmentIndex, const double aGps){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // get first index
  int s = GetSegmentStartBefore(aGps);

  // before segment list
  if(s<0) return false;

  // search segment 
  for(; s<(int)seg_start.size(); s++){

    // out-of-range
    if(seg_end[s]<=aGps) continue;
    if(seg_start[s]>aGps) break;

    // found it
    aSegmentIndex=(unsigned int)s;
    return true;
  }

  return false;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::IsInsideSegment(const double aGps){
/////////////////////////////////////////////////////////////////////////
  unsigned int not_used;
  return IsInsideSegment(not_used, aGps);
}

/////////////////////////////////////////////////////////////////////////
void Segments::SetTags(const int aTag){
/////////////////////////////////////////////////////////////////////////
  for(unsigned int s=0; s<seg_start.size(); s++)
    seg_tag[s] = aTag;
  return;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Append(const double aGpsStart, const double aGpsEnd, const int aTag){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  
  // add segment at the end
  unsigned int current_size = seg_start.size();
  seg_start.push_back(aGpsStart);
  seg_end.push_back(aGpsEnd);
  seg_tag.push_back(aTag);

  // check consistency
  if(!CheckConsistency(current_size)){
    cerr<<"Segments::Append: the input segment ("<<aGpsStart<<") does not start after the last segment"<<endl;
    seg_start.pop_back();
    seg_end.pop_back();
    seg_tag.pop_back();
    return false;
  }
  
  // format
  MergeOverlaps(current_size);
  SetMarks();
  ComputeLiveTime();
  
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Append(vector <double> aGpsStarts, vector <double> aGpsEnds, vector <double> aTags){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // nothing to do
  if(aGpsStarts.size()==0) return true;

  // check input consistency
  if((aGpsStarts.size()!=aGpsEnds.size()) || (aGpsStarts.size()!=aTags.size())){
    cerr<<"Segments::Append: the input segments are corrupted."<<endl;
    return false;
  }
  
  unsigned int current_size = seg_start.size();

  seg_start.insert(seg_start.end(), aGpsStarts.begin(), aGpsStarts.end());
  seg_end.insert(seg_end.end(), aGpsEnds.begin(), aGpsEnds.end());
  seg_tag.insert(seg_tag.end(), aTags.begin(), aTags.end());
  
  // check consistency
  if(!CheckConsistency(current_size)){
    cerr<<"Segments::Append: the input segment ("<<aGpsStarts[0]<<") does not start after the last segment"<<endl;
    seg_start.erase(seg_start.begin()+current_size, seg_start.end());
    seg_end.erase(seg_end.begin()+current_size, seg_end.end());
    seg_tag.erase(seg_tag.begin()+current_size, seg_tag.end());
    return false;
  }

  // format
  MergeOverlaps(current_size);
  SetMarks();
  ComputeLiveTime();

  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Append(Segments *aSeg){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // check input sanity
  if(aSeg==NULL||!aSeg->GetStatus()){
    cerr<<"Segments::Append: input segments are corrupted"<<endl;
    return false;
  }

  // nothing to do
  if(!aSeg->GetN()) return true;

  unsigned int current_size = seg_start.size();

  seg_start.insert(seg_start.end(), aSeg->seg_start.begin(), aSeg->seg_start.end());
  seg_end.insert(seg_end.end(), aSeg->seg_end.begin(), aSeg->seg_end.end());
  seg_tag.insert(seg_tag.end(), aSeg->seg_tag.begin(), aSeg->seg_tag.end());
  
  // check consistency
  if(!CheckConsistency(current_size)){
    cerr<<"Segments::Append: the input segment ("<<aSeg->seg_start[0]<<") does not start after the last segment"<<endl;
    seg_start.erase(seg_start.begin()+current_size, seg_start.end());
    seg_end.erase(seg_end.begin()+current_size, seg_end.end());
    seg_tag.erase(seg_tag.begin()+current_size, seg_tag.end());
    return false;
  }

  // format
  MergeOverlaps(current_size);
  SetMarks();
  ComputeLiveTime();

  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Append(TTree *aSegmentTree){ 
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // read tree
  double sstart, send;
  int stag=1;
  if((aSegmentTree->SetBranchAddress("start",&sstart)!=0) ||
     (aSegmentTree->SetBranchAddress("end",&send)!=0) ||
     aSegmentTree->SetBranchAddress("tag",&stag)!=0){
    cerr<<"Segments::Append: missing branches"<<endl;
    return false;
  }

  // nothing to do
  if(!aSegmentTree->GetEntries()) return true;

  unsigned int current_size = seg_start.size();

  // get segments
  for(unsigned int s=0; s<(unsigned int)aSegmentTree->GetEntries(); s++){
    aSegmentTree->GetEntry(s);
    seg_start.push_back(sstart);
    seg_end.push_back(send);
    seg_tag.push_back(stag);
  }
  
  // check consistency
  if(!CheckConsistency(current_size)){
    cerr<<"Segments::Append: the input segments do not start after the last segment"<<endl;
    seg_start.erase(seg_start.begin()+current_size, seg_start.end());
    seg_end.erase(seg_end.begin()+current_size, seg_end.end());
    seg_tag.erase(seg_tag.begin()+current_size, seg_tag.end());
    return false;
  }

  // format
  MergeOverlaps(current_size);
  SetMarks();
  ComputeLiveTime();

  return true;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::AddSegment(const double aGpsStart, const double aGpsEnd, const int aTag){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // just append
  if(!GetN() || aGpsStart>=seg_start.back()){
    return Append(aGpsStart, aGpsEnd, aTag);
  }

  // get first index
  int s = GetSegmentStartBefore(aGpsStart);

  // insert before segment list
  seg_start.insert(seg_start.begin()+(unsigned int)(s+1), aGpsStart);
  seg_end.insert(seg_end.begin()+(unsigned int)(s+1), aGpsEnd);
  seg_tag.insert(seg_tag.begin()+(unsigned int)(s+1), aTag);
  

  // check consistency
  if(!CheckConsistency((unsigned int)(s+1))){
    cerr<<"Segments::AddSegment: the input segment ("<<aGpsStart<<") is invalid"<<endl;
    seg_start.erase(seg_start.begin()+(unsigned int)(s+1));
    seg_end.erase(seg_end.begin()+(unsigned int)(s+1));
    seg_tag.erase(seg_tag.begin()+(unsigned int)(s+1));
    return false;
  }

  // format
  MergeOverlaps((unsigned int)(s+1));
  SetMarks();
  ComputeLiveTime();

  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::AddSegments(Segments *aSeg){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // just append
  if(!GetN() || aSeg->GetFirst()>=seg_start.back()){
    return Append(aSeg);
  }

  for(unsigned int s=0; s<aSeg->GetN(); s++){
    if(!AddSegment(aSeg->GetStart(s), aSeg->GetEnd(s), aSeg->GetTag(s))) return false;
  }

  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::ApplyPadding(const double aPadStart, const double aPadEnd){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // apply padding for each segment
  double start_new, end_new;
  for(unsigned int s=0; s<seg_start.size(); s++){
    start_new = seg_start[s]+aPadStart;
    end_new = seg_end[s]+aPadEnd;

    // valid segment
    if(end_new>start_new){
      seg_start[s]=start_new;
      seg_end[s]=end_new;
    }
    // remove segment
    else{
      seg_start.erase(seg_start.begin()+s, seg_start.begin()+s+1);
      seg_end.erase(seg_end.begin()+s, seg_end.begin()+s+1);
      seg_tag.erase(seg_tag.begin()+s, seg_tag.begin()+s+1);
    }
  }

  // format
  status=CheckConsistency(0);
  MergeOverlaps(0);
  SetMarks();
  ComputeLiveTime();
  
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::TruncateAfter(const double aGps){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // get first index
  int s = GetSegmentStartBefore(aGps);

  // before segment list --> remove everything
  if(s<0){
    Reset();
    return true;
  }

  // after last segment --> nothing to do
  else if((unsigned int)s==seg_start.size()-1){
  }

  // truncate segments
  else{
    seg_start.erase(seg_start.begin()+(unsigned int)s+1, seg_start.end());
    seg_end.erase(seg_end.begin()+(unsigned int)s+1, seg_end.end());
    seg_tag.erase(seg_tag.begin()+(unsigned int)s+1, seg_tag.end());
  }

  // truncate last segment?
  if(aGps<seg_end.back()){
    seg_end.back()=aGps;
  }

  // format
  SetMarks();
  ComputeLiveTime();
  
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::MakeSecond(void){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // round starts and ends
  for(unsigned int s=0; s<seg_start.size(); s++){
    seg_start[s] = floor(seg_start[s]);
    seg_end[s] = ceil(seg_end[s]);
  }

  // format
  MergeOverlaps(0);
  SetMarks();
  ComputeLiveTime();
  
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Reverse(void){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;
  
  // no segment
  if(seg_start.size()==0){
    Reset();
    return AddSegment(SEGMENTS_START, SEGMENTS_END, 1);
  }

  double e = seg_start[0];
  double s = seg_end.back();

  // erase the 1st element
  seg_start.erase(seg_start.begin());

  // erase the last element
  seg_end.pop_back();

  // swap vectors
  seg_start.swap(seg_end);

  // add first and last segment
  if(e!=SEGMENTS_START){
    seg_start.insert(seg_start.begin(),SEGMENTS_START);
    seg_end.insert(seg_end.begin(),e);
  }
  if(s!=SEGMENTS_END){
    seg_start.push_back(s);
    seg_end.push_back(SEGMENTS_END);
  }

  // clear all tags
  seg_tag.clear();
  seg_tag.assign(seg_start.size(),1);
  
  // format
  MergeOverlaps(0);
  SetMarks();
  ComputeLiveTime();
  
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Intersect(Segments *aSeg){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;

  // check input sanity
  if(aSeg==NULL||!aSeg->GetStatus()){
    cerr<<"Segments::Intersect: input segments are corrupted"<<endl;
    return false;
  }


  // new vectors
  vector<double> seg_start_tmp;
  vector<double> seg_end_tmp;
  vector<int> seg_tag_tmp;

  int sstart=0;

  // loop over "input" segments
  for(unsigned int s=0; s<aSeg->GetN(); s++){

    // loop over "this" segments
    for(unsigned int ss=sstart; ss<GetN(); ss++){

      // input segment starts after this end --> this segment is removed
      if(seg_end[ss]<=aSeg->GetStart(s)){ 
	sstart=ss+1;
	continue; 
      }

      // input segment is fully inside this segment --> keep input segment
      else if(seg_start[ss]<=aSeg->GetStart(s)&&seg_end[ss]>=aSeg->GetEnd(s)){ 
	sstart=ss; 
	seg_start_tmp.push_back(aSeg->GetStart(s));
        seg_end_tmp.push_back(aSeg->GetEnd(s));
	seg_tag_tmp.push_back(GetTagProduct(seg_tag[ss],aSeg->GetTag(s)));
	break; 
      }

      // input segment overlapping this segment end --> keep overlap
      else if(seg_start[ss]<=aSeg->GetStart(s)&&seg_end[ss]>aSeg->GetStart(s)&&seg_end[ss]<=aSeg->GetEnd(s)){
	sstart=ss+1; 
	seg_start_tmp.push_back(aSeg->GetStart(s));
        seg_end_tmp.push_back(seg_end[ss]);   
	seg_tag_tmp.push_back(GetTagProduct(seg_tag[ss],aSeg->GetTag(s)));
      }

      // this segment is fully inside input segment --> keep this segment
      else if(seg_start[ss]>=aSeg->GetStart(s)&&seg_end[ss]<=aSeg->GetEnd(s)){
	sstart=ss+1; 
	seg_start_tmp.push_back(seg_start[ss]);
        seg_end_tmp.push_back(seg_end[ss]);   
	seg_tag_tmp.push_back(GetTagProduct(seg_tag[ss],aSeg->GetTag(s)));
      }

      // input segment overlapping this segment start --> keep overlap      
      else if(seg_start[ss]>=aSeg->GetStart(s)&&seg_start[ss]<aSeg->GetEnd(s)){
	sstart=ss; 
	seg_start_tmp.push_back(seg_start[ss]);
        seg_end_tmp.push_back(aSeg->GetEnd(s));
	seg_tag_tmp.push_back(GetTagProduct(seg_tag[ss],aSeg->GetTag(s)));
      }
      else{
	break;
      }
    }
  }
  
  // switch to class vectors
  seg_end.clear();   seg_end = seg_end_tmp;
  seg_start.clear(); seg_start = seg_start_tmp;
  seg_tag.clear();   seg_tag = seg_tag_tmp;

  // format
  MergeOverlaps(0);
  SetMarks();
  ComputeLiveTime();
 
  return status;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::Intersect(const double aGpsStart, const double aGpsEnd, const int aTag){
/////////////////////////////////////////////////////////////////////////
  if(!status) return false;
  //if(gps_start>=gps_end) return false;

  Segments *stmp = new Segments(aGpsStart, aGpsEnd, aTag);
  bool res = Intersect(stmp);
  delete stmp;
  return res;
}

/////////////////////////////////////////////////////////////////////////
void Segments::Dump(unsigned int aNcols, const string aTxtFileName){
/////////////////////////////////////////////////////////////////////////
  if(!status) return;

  // open output file
  ofstream txtfile;
  if(aTxtFileName.compare("")){
    txtfile.open(aTxtFileName.c_str(), ofstream::out);
    txtfile.flags ( ios::fixed );
  }
  
  if(aNcols==2){
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_start.size(); s++)
        txtfile<<seg_start[s]<<" "<<seg_end[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_start.size(); s++)
        cout<<seg_start[s]<<" "<<seg_end[s]<<endl;
    }
  }
  else if(aNcols==3){
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_start.size(); s++)
        txtfile<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_start.size(); s++)
        cout<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<endl;
    }
  }
  else if(aNcols==4){
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_start.size(); s++)
        txtfile<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<" "<<seg_end[s]-seg_start[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_start.size(); s++)
        cout<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<" "<<seg_end[s]-seg_start[s]<<endl;
    }
  }
  else if(aNcols==1){
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_start.size(); s++)
        cout<<seg_end[s]-seg_start[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_start.size(); s++)
        txtfile<<seg_end[s]-seg_start[s]<<endl;
    }
  }
  else if(aNcols==0){
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_tag.size(); s++)
        txtfile<<seg_tag[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_tag.size(); s++)
        cout<<seg_tag[s]<<endl;
    }
  }
  else {
    if(txtfile.is_open()){
      for(unsigned int s=0; s<seg_start.size(); s++)
        txtfile<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<" "<<seg_end[s]-seg_start[s]<<" "<<seg_tag[s]<<endl;
    }
    else{
      for(unsigned int s=0; s<seg_start.size(); s++)
        cout<<s<<" "<<seg_start[s]<<" "<<seg_end[s]<<" "<<seg_end[s]-seg_start[s]<<" "<<seg_tag[s]<<endl;
    }
  }
  
  if(txtfile.is_open())
    txtfile.close();

  return;
}

/////////////////////////////////////////////////////////////////////////
bool Segments::CheckConsistency(const unsigned int aStartIndex){
/////////////////////////////////////////////////////////////////////////

  // sizes
  if(seg_start.size()!=seg_end.size()){
    cerr<<"Segments::CheckConsistency: size mismatch (start/end)"<<endl;
    return false;
  }
  if(seg_start.size()!=seg_tag.size()){
    cerr<<"Segments::CheckConsistency: size mismatch (start/tag)"<<endl;
    return false;
  }
  
  // check the segment times
  for(unsigned int s=aStartIndex; s<seg_start.size(); s++){
    
    // too old!
    if(seg_start[s]<SEGMENTS_START){
      cerr<<"Segments::CheckConsistency: segment "<<seg_start[s]<<" "<<seg_end[s]<<" is too old: before Mar 12 2002 20:26:27 UTC"<<endl;
      return false;
    }

    // too far in the future!
    if(seg_end[s]>SEGMENTS_END){
      cerr<<"Segments::CheckConsistency: segment "<<seg_start[s]<<" "<<seg_end[s]<<" ends too far in the future"<<endl;
      return false;
    }

    // negative duration
    if(seg_end[s]<=seg_start[s]){
      cerr<<"Segments::CheckConsistency: segment "<<seg_start[s]<<" "<<seg_end[s]<<" has a negative duration"<<endl;
      return false;
    }

    // time sorted
    if(s&&seg_start[s-1]>seg_start[s]){
      cerr<<"Segments::CheckConsistency: segments should be time sorted: "<<seg_start[s-1]<<" ("<<s-1<<")>"<<seg_start[s]<<"("<<s<<")"<<endl;
      return false;
    }
  }

  return true;
}

/////////////////////////////////////////////////////////////////////////
void Segments::MergeOverlaps(const unsigned int aStartIndex){
/////////////////////////////////////////////////////////////////////////

  // loop over segments
  for(unsigned int s=aStartIndex; s<seg_start.size(); s++){

    // normal situation: (s) is after (s-1) and disjoint
    if((!s) || (seg_start[s] > seg_end[s-1])) continue;

    // merge (s) --> (s-1)
    seg_start[s-1]=TMath::Min(seg_start[s], seg_start[s-1]);
    seg_end[s-1]=TMath::Max(seg_end[s], seg_end[s-1]);
    seg_tag[s-1]=GetTagProduct(seg_tag[s], seg_tag[s-1]);
    seg_start.erase(seg_start.begin()+s);
    seg_end.erase(seg_end.begin()+s);
    seg_tag.erase(seg_tag.begin()+s);

    // do not move
    s--;
  }

  return;
}

/////////////////////////////////////////////////////////////////////////
void Segments::SetMarks(){
/////////////////////////////////////////////////////////////////////////
  
  // initiate
  n_mark.clear();
  time_mark.clear();
  
  // no segments -> no mark
  if(!seg_start.size()) return;

  // init
  n_mark.push_back(0);
  time_mark.push_back((unsigned int)floor(seg_start[0]/100000.0));

  // set marks
  for(unsigned int s=1; s<seg_start.size(); s++){
    if(floor(seg_start[s]/100000.0)>floor(seg_start[s-1]/100000.0)){
      time_mark.push_back((unsigned int)floor(seg_start[s]/100000.0));
      n_mark.push_back(s);
    }
  }

  return;
}

/////////////////////////////////////////////////////////////////////////
int Segments::GetMark(const unsigned int aGps){
/////////////////////////////////////////////////////////////////////////

  int r_mark=-1;
  unsigned int gps_mark = (unsigned int)floor(aGps/100000.0);
  
  // get mark
  for(unsigned int t=0; t<time_mark.size(); t++){
    if(gps_mark>=time_mark[t]){
      r_mark=(int)n_mark[t];
      break;
    }
  }
  
  return r_mark;
}

/////////////////////////////////////////////////////////////////////////
void Segments::ComputeLiveTime(void){
/////////////////////////////////////////////////////////////////////////
  seg_livetime = 0.0;
  for(unsigned int s=0; s<seg_start.size(); s++)
    seg_livetime += (seg_end[s]-seg_start[s]);
  return;
}

