/**
 * @file 
 * @brief Time segment list management.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Segments__
#define __Segments__

#include "CUtils.h"
#include "ReadAscii.h"
#include <TFile.h>
#include <TChain.h>
#include <TH1.h>

using namespace std;

/**
 * @brief Manage time segment lists.
 * @details Every gravitational-wave analysis has to deal with segment lists at some point.
 * This class is designed to manage segment lists in an easy way.
 *
 * A segment is a time interval limited by 2 GPS times: `[GPS_START]` (included) and `[GPS_END]` (excluded).
 * A segment list must respect the rules documented in CheckConsistency()
 *
 * Segments can be individually tagged by an signed integer `[TAG]`.
 *
 * This class offers methods to transform segment lists: new segments, intersection, padding, truncation...
 */
class Segments {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Default constructor of the Segments class.
   * @details An empty segment list is initialized.
   */
  Segments(void);
  
  /**
   * @brief Constructor of the Segments class.
   * @details The original segment list is downloaded from a text file.
   * Several formats are supported to extract the segments and the tags:
   * - 2 columns: `[GPS_START]` `[GPS_END]`. In this case the segment tags are initialized to the default value.
   * - 3 or 4 columns: (second column)=`[GPS_START]`, (third column)=`[GPS_END]`. In this case the segment tags are initialized to the default value.
   * - 5 columns: (second column)=`[GPS_START]`, (third column)=`[GPS_END]`, (fifth column)=`[TAG]`.
   * - any other format: the Segments object is corrupted.
   *
   *
   * @note Input segments can overlap in time (overlaps will be internally merged).
   * @param[in] aSegmentFile Path to the segment text file.
   * @param[in] aDefaultTag Default tag value aaplied to the segments when the number of columns is less than 5.
   */
  Segments(const string aSegmentFile,
           const int aDefaultTag=1);

  /**
   * @brief Constructor of the Segments class.
   * @details The original segment list is downloaded from a TTree.
   * 3 branches must be found: `start/D`, `end/D`, and `tag/I`.
   * @note Input segments can overlap in time (overlaps will be internally merged).
   * @param[in] aSegmentTree Pointer to the segment TTree structure.
   */
  Segments(TTree *aSegmentTree);

  /**
   * @brief Constructor of the Segments class.
   * @details The original segment list is downloaded from a TTree in a list of ROOT files.
   * 3 branches must be found: `start/D`, `end/D`, and `tag/I`.
   * @note Input segments can overlap in time (overlaps will be internally merged).
   * @param[in] aFilePattern Input file pattern.
   * @param[in] aDirectory ROOT sub-directory. Use "" if none.
   * @param[in] aTreeName Segment tree name.
   */
  Segments(const string aFilePattern, const string aDirectory, const string aTreeName);

  /**
   * @brief Constructor of the Segments class.
   * @details The original segment list is downloaded from vectors.
   * @note Input segments can overlap in time (overlaps will be internally merged).
   * @note All segments are given a tag 1.
   * @param[in] aGpsStarts Vector of GPS starts.
   * @param[in] aGpsEnds Vector of GPS ends.
   */
  Segments(const vector<double> aGpsStarts, const vector<double> aGpsEnds);

  /**
   * @brief Constructor of the Segments class.
   * @details The original segment list contains one single segment.
   * @param[in] aGpsStart Segment GPS start.
   * @param[in] aGpsEnd Segment GPS end.
   * @param[in] aTag Segment tag.
   */
  Segments(const double aGpsStart, const double aGpsEnd, const int aTag=1);

  /**
   * @brief Destructor of the Segments class.
   */
  virtual ~Segments(void);  
  /**
     @}
  */

  /**
   * @brief Resets the segment list.
   * @details As a result, the segment list is empty and the status is back to OK.
   */
  void Reset(void);

  /**
   * @brief Returns the status of the Segments object.
   */ 
  inline bool GetStatus(void) const { return status; };

  /**
   * @brief Returns the segment starting just before a given GPS time.
   * @returns The segment index or -1 if no segment starts before the GPS time.
   * @param[in] aGps GPS time of interest
   */ 
  int GetSegmentStartBefore(const double aGps);

  /**
   * @brief Returns the number of time segments in the list.
   */ 
  inline unsigned int GetN(void){ return seg_start.size(); };

  /**
   * @brief Returns the number of time segments in the list with a given tag.
   * @param[in] aTag Tag value of segments to consider.
   */ 
  unsigned int GetNWithTag(const int aTag);

  /**
   * @brief Returns the list of segment starts.
   */ 
  inline vector<double> GetStarts(void){ return seg_start; };

  /**
   * @brief Returns the list of segment ends.
   */ 
  inline vector<double> GetEnds(void){ return seg_end; };

  /**
   * @brief Returns the list of segment tags.
   */ 
  inline vector<int> GetTags(void){ return seg_tag; };

  /**
   * @brief Returns the starting time of a given segment.
   * @param[in] aSegmentIndex Segment index.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
   */ 
  inline double GetStart(const unsigned int aSegmentIndex=0){ return seg_start[aSegmentIndex]; };

  /**
   * @brief Returns the ending time of a given segment.
   * @param[in] aSegmentIndex Segment index.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
   */ 
  inline double GetEnd(const unsigned int aSegmentIndex=0){ return seg_end[aSegmentIndex]; };

  /**
   * @brief Returns the starting time of the first segment.
   * @pre There should be at least one segment in the list or else this function crashes.
   */ 
  inline double GetFirst(){ return GetStart(0); };

  /**
   * @brief Returns the ending time of the last segment.
   * @pre There should be at least one segment in the list or else this function crashes.
   */ 
  inline double GetLast(){ return GetEnd(GetN()-1); };

  /**
   * @brief Returns the duration of a given segment.
   * @param[in] aSegmentIndex Segment index.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
   */ 
  inline double GetDuration(const unsigned int aSegmentIndex=0){ return seg_end[aSegmentIndex]-seg_start[aSegmentIndex]; };

  /**
   * @brief Returns the tag of a given segment.
   * @param[in] aSegmentIndex Segment index.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
   */ 
  inline int GetTag(const unsigned int aSegmentIndex=0){ return seg_tag[aSegmentIndex]; };

  /**
   * @brief Returns the total livetime.
   * @details Every segments in the list are considered.
   */ 
  inline double GetLiveTime(void){ return seg_livetime; };

  /**
   * @brief Returns the livetime between two GPS times.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   */ 
  double GetLiveTime(const double aGpsStart, const double aGpsEnd);

  /**
   * @brief Returns the livetime of one segment.
   * @param[in] aSegmentIndex Segment index.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
   * @sa GetDuration().
   */ 
  inline double GetLiveTime(const unsigned int aSegmentIndex){ return GetDuration(aSegmentIndex); };

  /**
   * @brief Returns the livetime for segments with a given tag.
   * @param[in] aTag Tag value of segments to consider.
   */ 
  double GetLiveTimeWithTag(const int aTag);

  /**
   * @brief Returns an histogram representing the segments.
   * @details Only segments between 'aGpsStart' and 'aGpsEnd' are represented.
   * The GPS time is represented on the X axis:
   * - 1 is used when inside a segment.
   * - 0 is used when outside a segment.
   * @param[in] aGpsStart Starting GPS time of the histogram.
   * @param[in] aGpsEnd Ending GPS time of the histogram.
   * @param[in] aOffset Time offset applied to GPS times.
   * @note The user is in charge of deleting this histogram.
   * @returns A NULL pointer if the Segments object is corrupted.
   */ 
  TH1I* GetHistogram(const double aGpsStart, const double aGpsEnd, const double aOffset=0.0);

  /**
   * @brief Returns an histogram representing the segments.
   * @details The GPS time is represented on the X axis:
   * - 1 is used when inside a segment.
   * - 0 is used when outside a segment.
   * @param[in] aOffset Time offset applied to GPS times.
   * @note The user is in charge of deleting this histogram.
   * @returns A NULL pointer if the Segments object is corrupted.
   */ 
  inline TH1I* GetHisto(const double aOffset=0.0){
    return GetHistogram(seg_start[0],seg_end[seg_start.size()-1],aOffset);
  };

  /**
   * @brief Returns a formatted TTree representing the segments.
   * This tree has 3 branches: "start", "end", and "tag".
   * Only segments between 'aGpsStart' and 'aGpsEnd' are copied in the tree.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   * @note The user is in charge of deleting this TTree.
   * @returns A NULL pointer if the Segments object is corrupted.
    */ 
  TTree* GetTree(const double aGpsStart, const double aGpsEnd);

  /**
   * @brief Returns a formatted TTree representing the segments.
   * @details This tree has 3 branches: "start", "end", and "tag".
   * @note The user is in charge of deleting this TTree.
   * @returns A NULL pointer if the Segments object is corrupted.
    */ 
  TTree* GetTree(void){
    return GetTree(seg_start[0], seg_end[seg_start.size()-1]);
  };

  /**
   * @brief Checks whether a given GPS time is inside a segment of the list.
   * @returns This function returns true if the GPS value is found inside a time segment. It returns false otherwise
   * @param[out] aSegmentIndex Segment index of segment containing the GPS time. This is irrelevant if false is returned.
   * @param[in] aGps GPS time of interest.
   */ 
  bool IsInsideSegment(unsigned int &aSegmentIndex, const double aGps);
      
  /**
   * @brief Checks whether a given GPS time is inside a segment of the list.
   * @returns This function returns true if the GPS value is found inside a time segment. It returns false otherwise
   * @param[in] aGps GPS time of interest.
   */ 
  bool IsInsideSegment(const double aGps);

  /**
   * @brief Tags a given segment.
   * @param[in] aSegmentIndex Segment index.
   * @param[in] aTag New tag value.
   * @pre The segment index must be valid (less than the number of segments returned by GetN()).
  */
  inline void SetTag(const unsigned int aSegmentIndex, const int aTag){
    seg_tag[aSegmentIndex] = aTag;
  };

  /**
   * @brief Tags all segments.
   * @param[in] aTag New tag value.
   */
  void SetTags(const int aTag);

  /**
   * @brief Appends a segment to the list.
   * @details This function appends a time segment at the end of the current Segments object.
   * It only works if the new segment starts after the last segment start (an overlap is possible).
   * @returns true if the segment was correctly added, false otherwise.
   * @param[in] aGpsStart Segment GPS start.
   * @param[in] aGpsEnd Segment GPS end.
   * @param[in] aTag Segment tag.
   */
  bool Append(const double aGpsStart, const double aGpsEnd, const int aTag=1);

  /**
   * @brief Appends segments to the list.
   * @details This function appends a list of segments at the end of the current Segments object.
   * It only works if the first segment starts after the last segment start (an overlap is possible).
   * @returns true if the segments were correctly added, false otherwise.
   * @param[in] aGpsStarts Segment GPS starts.
   * @param[in] aGpsEnds Segment GPS ends.
   * @param[in] aTags Segment tags.
   */
  bool Append(vector <double> aGpsStarts, vector <double> aGpsEnds, vector <double> aTags);

  /**
   * @brief Appends segments to the list.
   * @details This function appends a list of segments at the end of the current Segments object.
   * It only works if the first segment starts after the last segment start (an overlap is possible).
   * @returns true if the segments were correctly added, false otherwise.
   * @param[in] aSeg Segment list to append.
   */
  bool Append(Segments *aSeg);

  /**
   * @brief Appends segments to the list.
   * @details The Segments object is automatically re-organized to remove overlaps.
   * @note Three branches must be found: `start/D`, `end/D`, and `tag/I`.
   * @returns true if the segments were correctly added, false otherwise.
   * @param[in] aSegmentTree Segment tree with 3 branches: "start", "end", and "tag".
   */
  bool Append(TTree *aSegmentTree);
  
  /**
   * @brief Adds a segment to the list.
   * @details The Segments object is automatically re-organized to remove overlaps.
   * @returns true if the segment was correctly added, false otherwise.
   * @param[in] aGpsStart Segment GPS start.
   * @param[in] aGpsEnd Segment GPS end.
   * @param[in] aTag Segment tag.
   */
  bool AddSegment(const double aGpsStart, const double aGpsEnd, const int aTag=1);
  
  /**
   * @brief Adds a segment list to the list.
   * @details The Segments object is automatically re-organized to remove overlaps.
   * @returns true if the segments were correctly added, false otherwise.
   * @param[in] aSeg Segment list to add.
   */
  bool AddSegments(Segments *aSeg);
  
  /**
   * @brief Apply a padding to all segments.
   * @details This function shifts the segment start and end by a given padding value.
   * The Segments object is automatically re-organized to remove overlaps. Some segments are removed if the duration becomes null or negative.
   * @returns true if the padding was correctly added, false otherwise.
   * @param[in] aPadStart Padding value added to the segment starts.
   * @param[in] aPadEnd Padding value added to the segment ends.
   */
  bool ApplyPadding(const double aPadStart, const double aPadEnd);

  /**
   * @brief Removes segments after a given time.
   * @note if the GPS time is inside a segment, the segment is truncated.
   * @param[in] aGps GPS time after which to truncate.
   */
  bool TruncateAfter(const double aGps);

  /**
   * @brief Rounds segment boundaries to integer seconds.
   * @details Segments starts are rounded to the previous integer and segments ends are rounded to the next integer.
   * @note The Segments object is automatically re-organized to remove overlaps.
   * @returns true if the segment was correctly modified, false otherwise.
   */
  bool MakeSecond(void);

  /**
   * @brief Reverses the segment list.
   * @details It means that the gaps between segments are now considered as segments.
   * The first segment starts at :: SEGMENTS_START.
   * The last segment ends at :: SEGMENTS_END.
   * @returns true if the segments were correctly reversed, false otherwise.
   * @note All tags are lost in the reversing process. The new segments are tagged with 1.
   */
  bool Reverse(void);
  
  /**
   * @brief Intersects with a segment list.
   * @details This function intersects the current segment list with a second segment list given in argument.
   * @warning This function is destructive and there is no way to go back to the original segment list.
   * @returns true if the segments were correctly intersected, false otherwise.
   * @note The segment tags are obtained using GetTagProduct() for the overlapping segments.
   * @param[in] aSeg External Segments objects (not modified)
   */
  bool Intersect(Segments *aSeg);

  /**
   * @brief Intersects with a segment.
   * @details  This function intersects the current segment list with a segment given in argument.
   * @warning This function is destructive and there is no way to go back to the original segment list.
   * @returns true if the segments were correctly intersected, false otherwise.
   * @note The segment tags are obtained using GetTagProduct() for the overlapping segments.
   * @param[in] aGpsStart Segment GPS start.
   * @param[in] aGpsEnd Segment GPS end.
   * @param[in] aTag Segment tag.
   */
  bool Intersect(const double aGpsStart, const double aGpsEnd, const int aTag=1);

  /**
   * @brief Dumps the current segment list in the standard output or in a file.
   * Several formats are possible:
   * - if 'aNcols=0' The list of tags is printed
   * - if 'aNcols=1' The list of duration is printed
   * - if 'aNcols=2' The list of segment starts and ends is printed
   * - if 'aNcols=3' The list of segment starts and ends is printed, as well as the segment index
   * - if 'aNcols=4' The list of segment starts and ends is printed, as well as the segment index and duration
   * - if 'aNcols>=5' The list of segment starts and ends is printed, as well as the segment index, duration and tag
   *
   * @param[in] aNcols number of columns in the output.
   * @param[in] aTxtFileName Text file name. Use "" for the standard output.
  */
  void Dump(unsigned int aNcols=2, const string aTxtFileName="");

 private:
  
  bool status;                    ///< Status of the Segments object.
  vector<double> seg_start;       ///< List of segment starts.
  vector<double> seg_end;         ///< List of segment ends.
  vector<int> seg_tag;            ///< List of segment tags.
  double seg_livetime;            ///< Integrated livetime.
  vector <unsigned int> time_mark;///< Time marks.
  vector <unsigned int> n_mark;   ///< Segment index marks.

  /**
   * @brief Computes the product of 2 tags.
   * @details
   * - If one of the 2 tags is strictly negative, the lowest value is returned.
   * - If both tags are positive, the greatest value is returned
   * @param[in] aTag1 First tag.
   * @param[in] aTag2 Second tag.
   */
  inline int GetTagProduct(const int aTag1, const int aTag2){
    if(aTag1<0||aTag2<0) return TMath::Min(aTag1,aTag2);
    return TMath::Max(aTag1,aTag2);
  };

  /**
   * @brief Checks the sanity of the segment list.
   * @details List of checks:
   * - The list of starts, ends and tags must be the same size.
   * - All segments must start after ::SEGMENTS_START
   * - All segments must end after ::SEGMENTS_END
   * - All segments must have a strictly positive duration.
   * - Segments must be sorted in increasing `[GPS_START]`.
   *
   * @param[in] aStartIndex Segment index where to start the checks.
   * @returns false if the segment list is not sane, true otherwise.
   */
  bool CheckConsistency(const unsigned int aStartIndex);

  /**
   * @brief Merges segment overlaps.
   * @details When merging 2 segments, the tag with the highest value is kept.
   * @note This function assumes the segments consistency.
   * @param[in] aStartIndex Segment index where to start the merge.
   */
  void MergeOverlaps(const unsigned int aStartIndex);

  /**
   * @brief Sets time marks for internal lookup.
   * @note This function assumes the segments consistency.
  */
  void SetMarks(void);
  
  /**
   * @brief Returns the time mark for a given GPS time.
   * @returns -1 is returned if the GPS time is before the first segment.
   * @param[in] aGps GPS time of interest.
   */
  int GetMark(const unsigned int aGps);
  
  /**
   * @brief Computes the sgments livetime.
   * @note This function assumes the segments consistency.
  */
  void ComputeLiveTime(void);


  ClassDef(Segments,0)
};

#endif


