//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "ffl.h"

using namespace std;

int main (int argc, char* argv[]){

  // check the argument
  if(argc!=7){
    cerr<<endl;
    cerr<<argv[0]<<":"<<endl;
    cerr<<"This program dumps segments after thresholding input data."<<endl;
    cerr<<endl;
    cerr<<"Usage:"<<endl; 
    cerr<<endl;
    cerr<<"dqchanthr.exe [path to ffl file] [channel name] [threshold value] [threshold type] [GPS start] [GPS stop]"<<endl; 
    cerr<<endl;
    cerr<<"[path to ffl file] = path to a valid ffl file"<<endl; 
    cerr<<"[channel name]     = channel name"<<endl; 
    cerr<<"[threshold value]  = threshold value"<<endl; 
    cerr<<"[threshold type]   = \"abs\": both positive and negative values"<<endl; 
    cerr<<"                   = \"positive\": only positive values"<<endl; 
    cerr<<"                   = \"negative\": only negative values"<<endl; 
    cerr<<"                   = \"equal\": strict equality"<<endl; 
    cerr<<"[GPS start]        = start"<<endl; 
    cerr<<"[GPS stop]         = stop"<<endl; 
    cerr<<endl;
   return 1;
  }

  // ffl
  string fflfilepath=(string)argv[1];
  if(!IsTextFile(fflfilepath)){
    cerr<<"dqchanthr: missing ffl file: "<<fflfilepath<<endl;
    return 1;
  }

  // channel
  string channelname = (string)argv[2];

  // threshold
  double thr=atof(argv[3]);

  // threshold type
  string thrtype=(string)argv[4];
  if(thrtype.compare("abs")&&thrtype.compare("positive")&&thrtype.compare("negative")&&thrtype.compare("equal")){
    cerr<<"dqchanthr: unknown threshold type: "<<thrtype<<endl;
    return 1;
  }

  // GPS
  int start=atoi(argv[5]);
  int stop=atoi(argv[6]);
  if(start<700000000||stop<=start){
    cerr<<"dqchanthr: invalid GPS range: "<<start<<" "<<stop<<endl;
    return 1;
  }
  
  // FFL
  ffl *FFL = new ffl (fflfilepath, "GWOLLUM", 0);
  FFL->LoadFrameFile();
  FFL->ExtractChannels(start);

  // Sampling
  int dummy;
  int sampling = FFL->GetChannelSampling(channelname,dummy);
  if(sampling<=0){
    cerr<<"dqchanthr: cannot extract channel "<<channelname<<endl;
    return 1;
  }
  
  Segments *Seg = new Segments();

  // read data
  int s = start;
  double * data; int datasize;
  bool pos=false;
  double segstart,segstop;
  while(s<stop){
    data = FFL->GetData(datasize,channelname,s,s+1000);
    if(datasize<=0){
      cerr<<"dqchanthr: cannot extract channel "<<channelname<<" at "<<s<<"-"<<s+1000<<endl;
      if(data!=NULL) delete data;
      // FIXME: need to deal with gaps!
    }
    else{// apply threshold
      
      // ABS      
      if(!thrtype.compare("abs")){
	for(int i=0; i<datasize; i++){
	  if(!pos&&fabs(data[i])>=thr){
	    pos=true;
	    segstart=(double)s+(double)i/sampling;
	  }
	  else if(pos&&fabs(data[i])<thr){
	    pos=false;
	    segstop=(double)s+(double)i/sampling;
	    Seg->AddSegment(segstart,segstop);
	  }
	  else; 
	}
      }
      
      // POSITIVE
      else if(!thrtype.compare("positive")){
	for(int i=0; i<datasize; i++){
	  if(!pos&&data[i]>=thr){
	    pos=true;
	    segstart=(double)s+(double)i/sampling;
	  }
	  else if(pos&&data[i]<thr){
	    pos=false;
	    segstop=(double)s+(double)i/sampling;
	    Seg->AddSegment(segstart,segstop);
	  }
	  else;
	}
      }
      
      // NEGATIVE
      else if(!thrtype.compare("negative")){
	for(int i=0; i<datasize; i++){
	  if(!pos&&data[i]<=thr){
	    pos=true;
	    segstart=(double)s+(double)i/sampling;
	  }
	  else if(pos&&data[i]>thr){
	    pos=false;
	    segstop=(double)s+(double)i/sampling;
	    Seg->AddSegment(segstart,segstop);
	  }
	  else;
	}
      }
        
      // EQUAL
      else{
	for(int i=0; i<datasize; i++){
	  if(!pos&&data[i]==thr){
	    pos=true;
	    segstart=(double)s+(double)i/sampling;
	  }
	  else if(pos&&data[i]!=thr){
	    pos=false;
	    segstop=(double)s+(double)i/sampling;
	    Seg->AddSegment(segstart,segstop);
	  }
	  else;
	}
      }
    }
        
    s+=1000;
  }
  
  if(pos) Seg->AddSegment(segstart,stop);// last segment
  
  Seg->Dump();
  
  // exit
  return 0;
}

