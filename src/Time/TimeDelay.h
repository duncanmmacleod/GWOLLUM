/**
 * @file 
 * @brief See Measure of time delay.
 * @author Jolien Creighton, and David Chin, and Steven
 */
#ifndef _TIMEDELAY_H
#define _TIMEDELAY_H

#include "Date.h"

#ifdef __cplusplus
extern "C"
{
#endif
  
  /**
   * @brief Undocumented.
   */
  double ArrivalTimeDiff(
                         const double detector1_earthfixed_xyz_metres[3],
                         const double detector2_earthfixed_xyz_metres[3],
                         const double source_right_ascension_radians,
                         const double source_declination_radians,
                         const double gpstime
                         );
  
  /**
   * @brief Computes difference in arrival time of the same signal at detector and at center of Earth-fixed frame.
   */
  double TimeDelayFromEarthCenter(
				  const double detector_earthfixed_xyz_metres[3],
				  double source_right_ascension_radians,
				  double source_declination_radians,
				  const double gpstime
				  );
  
#ifdef __cplusplus
}
#endif

#endif /* !defined _TIMEDELAY_H */
