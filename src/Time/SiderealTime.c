/*
 * Copyright (C) 2007  Jolien Creighton, and Kipp Cannon
 */

#include "Date.h"

/** Returns the Greenwich Sidereal Time IN RADIANS corresponding to a
 * specified GPS time.  Aparent sidereal time is computed by providing the
 * equation of equinoxes in units of seconds.  For mean sidereal time, set
 * this parameter to 0.
 *
 * This function returns the sidereal time in radians measured from the
 * Julian epoch (current J2000).  The result is NOT modulo 2 pi.
 *
 * Inspired by the function sidereal_time() in the NOVAS-C library, version
 * 2.0.1, which is dated December 10th, 1999, and carries the following
 * references:
 *
 * Aoki, et al. (1982) Astronomy and Astrophysics 105, 359-361.
 * Kaplan, G. H. "NOVAS: Naval Observatory Vector Astrometry
 *   Subroutines"; USNO internal document dated 20 Oct 1988;
 *   revised 15 Mar 1990.
 *
 * See http://aa.usno.navy.mil/software/novas for more information.
 */
double GreenwichSiderealTime(const double gpstime, double equation_of_equinoxes){
  struct tm utc;
  double julian_day;
  double t_hi, t_lo;
  double t;
  double sidereal_time;

  /*
   * Convert GPS seconds to UTC.  This is where we pick up knowledge
   * of leap seconds which are required for the mapping of atomic
   * time scales to celestial time scales.  We deal only with integer
   * seconds.
   */

  if(!GPSToUTC(&utc, (int)gpstime)) return 0;

  /*
   * And now to Julian day number.  Again, only accurate to integer
   * seconds.
   */

  julian_day = JulianDay(&utc);
  
  /*
   * Convert Julian day number to the number of centuries since the
   * Julian epoch (1 century = 36525.0 days).  Here, we incorporate
   * the fractional part of the seconds.  For precision, we keep
   * track of the most significant and least significant parts of the
   * time separately.  The original code in NOVAS-C determined t_hi
   * and t_lo from Julian days, with t_hi receiving the integer part
   * and t_lo the fractional part.  Because LAL's Julian day routine
   * is accurate to the second, here the hi/lo split is most
   * naturally done at the integer seconds boundary.  Note that the
   * "hi" and "lo" components have the same units and so the split
   * can be done anywhere.
   */
  
  t_hi = (julian_day - EPOCH_J2000_0_JD) / 36525.0;
  t_lo = (gpstime-floor(gpstime))*1e9 / (1e9 * 36525.0 * 86400.0);

  /*
   * Compute sidereal time in sidereal seconds.  (magic)
   */
  
  t = t_hi + t_lo;

  sidereal_time = equation_of_equinoxes + (-6.2e-6 * t + 0.093104) * t * t + 67310.54841;
  sidereal_time += 8640184.812866 * t_lo;
  sidereal_time += 3155760000.0 * t_lo;
  sidereal_time += 8640184.812866 * t_hi;
  sidereal_time += 3155760000.0 * t_hi;
  
  /*
   * Return radians (2 pi radians in 1 sidereal day = 86400 sidereal
   * seconds).
   */
  
  return sidereal_time * M_PI / 43200.0;
}


/**
 * Convenience wrapper, calling GreenwichSiderealTime() with the
 * equation of equinoxes set to 0.
 */
double GreenwichMeanSiderealTime(const double gpstime){
  return GreenwichSiderealTime(gpstime, 0.0);
}
