/**
 * @file 
 * @brief Generic timing routines.
 * @author Karl Wette, Duncan Brown, David Chin, Jolien Creighton, Kipp Cannon, Reinhard Prix, Stephen Fairhurst
 */
#ifndef _DATE_H
#define _DATE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LeapSeconds.h" /* contains the leap second table */

#ifdef  __cplusplus
extern "C"
{
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#define EPOCH_J2000_0_JD 2451545.0         /**< Julian Day of the J2000.0 epoch (2000 JAN 1 12h UTC). */
#define EPOCH_UNIX_GPS 315964800
#define EPOCH_GPS_TAI_UTC 19               /**< Leap seconds (TAI-UTC) on the GPS epoch (1980 JAN 6 0h UTC) */
  
  /**
   * @brief Returns the leap seconds TAI-UTC at a given GPS second.
   */
  int LeapSeconds(int gpssec);
  
  /**
   * @brief Returns the leap seconds TAI-UTC for a given UTC broken down time.
   */  
  int LeapSecondsUTC(const struct tm *utc);
  
  /**
   * @brief Returns a pointer to a tm structure representing the time
   * specified in seconds since the GPS epoch.
   */
  struct tm* GPSToUTC(struct tm *utc, int gpssec);

  /**
   * @brief Returns the GPS seconds for a specified UTC time structure.
   */
  int UTCToGPS( const struct tm *utc );
  
  /**
   * @brief Returns the Julian Day (JD) corresponding to the date given in a broken down time structure.
   */
  double JulianDay(const struct tm *utc);
  
  /**
   * @brief Returns the Greenwich mean or aparent sideral time in radians.
   */
  double GreenwichSiderealTime(const double gpstime, double equation_of_equinoxes);
  
  /**
   * @brief Returns the Greenwich Mean Sidereal Time in RADIANS for a specified GPS time.
   */
  double GreenwichMeanSiderealTime(const double gpstime);
  
#ifdef  __cplusplus
}
#endif

#endif /* _DATE_H */
