/**
 * @file 
 * @brief See TimeDelay.h
 * @author Jolien Creighton, and David Chin, and Steven
 */
#include "Date.h"
#include "TimeDelay.h"


/**
 * @brief Scalar product of two 3-vectors.
 * @param[in] vec1 First vector.
 * @param[in] vec2 Second vector.
 */
static double dotprod(const double vec1[3], const double vec2[3]){
  return vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];
}

double ArrivalTimeDiff(
                       const double detector1_earthfixed_xyz_metres[3],
                       const double detector2_earthfixed_xyz_metres[3],
                       const double source_right_ascension_radians,
                       const double source_declination_radians,
                       const double gpstime
                       ){
  double delta_xyz[3];
  double ehat_src[3];
  const double greenwich_hour_angle = GreenwichMeanSiderealTime(gpstime) - source_right_ascension_radians;
  
  /* compute the unit vector pointing from the geocenter to the
   * source */
  
  ehat_src[0] = cos(source_declination_radians) * cos(greenwich_hour_angle);
  ehat_src[1] = cos(source_declination_radians) * -sin(greenwich_hour_angle);
  ehat_src[2] = sin(source_declination_radians);
  
  /*
   * position of detector 2 with respect to detector 1
   */
  
  delta_xyz[0] = detector2_earthfixed_xyz_metres[0] - detector1_earthfixed_xyz_metres[0];
  delta_xyz[1] = detector2_earthfixed_xyz_metres[1] - detector1_earthfixed_xyz_metres[1];
  delta_xyz[2] = detector2_earthfixed_xyz_metres[2] - detector1_earthfixed_xyz_metres[2];
  
  /*
   * Arrival time at detector 1 - arrival time at detector 2.  This
   * is positive when the wavefront arrives at detector 1 after
   * detector 2 (and so t at detector 1 is greater than t at detector
   * 2).
   */
  
  return dotprod(ehat_src, delta_xyz) / 299792458.0;
}

double TimeDelayFromEarthCenter(
				const double detector_earthfixed_xyz_metres[3],
				double source_right_ascension_radians,
				double source_declination_radians,
				const double gpstime
				){
  static const double earth_center[3] = {0.0, 0.0, 0.0};
  
  /*
   * This is positive when the wavefront arrives at the detector
   * after arriving at the geocentre.
   */
  
  return ArrivalTimeDiff(detector_earthfixed_xyz_metres, earth_center, source_right_ascension_radians, source_declination_radians, gpstime);
}

