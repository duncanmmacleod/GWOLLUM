/*
*  Copyright (C) 2012 Karl Wette
*  Copyright (C) 2007 Bernd Machenschalk, Jolien Creighton, Kipp Cannon
*/
#include "Date.h"

static int delta_tai_utc(int gpssec){
  int leap;
  for ( leap = 1; leap < numleaps; ++leap )
    if ( gpssec == leaps[leap].gpssec )
      return leaps[leap].taiutc - leaps[leap-1].taiutc;

  return 0;
}

/** Returns the leap seconds TAI-UTC at a given GPS second. */
int LeapSeconds(int gpssec /**< [In] Seconds relative to GPS epoch.*/){
  int leap;

  if( gpssec < leaps[0].gpssec ) return 0;

  /* scan leap second table and locate the appropriate interval */
  for ( leap = 1; leap < numleaps; ++leap )
    if ( gpssec < leaps[leap].gpssec ) break;

  return leaps[leap-1].taiutc;
}

/* Returns the leap seconds TAI-UTC for a given UTC broken down time. */
int LeapSecondsUTC( const struct tm *utc){
  double jd;
  int leap;

  jd = JulianDay( utc );
  
  /* scan leap second table and locate the appropriate interval */
  for ( leap = 1; leap < numleaps; ++leap )
    if ( jd < leaps[leap].jd )
      break;
  
  return leaps[leap-1].taiutc;
}

/* Returns a pointer to a tm structure representing the time specified in seconds since the GPS epoch. */
struct tm * GPSToUTC(
		     struct tm *utc, /*< [Out] Pointer to tm struct where result is stored. */
		     int gpssec /*< [In] Seconds since the GPS epoch. */
		     ){
  time_t unixsec;
  int leapsec;
  int delta;
  leapsec = LeapSeconds( gpssec );
  unixsec  = gpssec - leapsec + EPOCH_GPS_TAI_UTC; /* get rid of leap seconds */
  unixsec += EPOCH_UNIX_GPS; /* change to unix epoch */
  memset( utc, 0, sizeof( *utc ) ); /* blank out utc structure */
  utc = gmtime_r( &unixsec, utc ); /* FIXME: use gmtime ?? */
  /* now check to see if we need to add a 60th second to UTC */
  if ( ( delta = delta_tai_utc( gpssec ) ) > 0 )
    utc->tm_sec += 1; /* delta only ever is one, right?? */
  return utc;
}

int UTCToGPS( const struct tm *utc ){
  time_t unixsec;
  int gpssec;
  int leapsec;

  /* compute leap seconds */
  leapsec = LeapSecondsUTC( utc );
  unixsec = utc->tm_sec + utc->tm_min*60 + utc->tm_hour*3600
    + utc->tm_yday*86400 + (utc->tm_year-70)*31536000
    + ((utc->tm_year-69)/4)*86400 - ((utc->tm_year-1)/100)*86400
    + ((utc->tm_year+299)/400)*86400;
  gpssec  = unixsec;
  gpssec -= EPOCH_UNIX_GPS; /* change to gps epoch */
  gpssec += leapsec - EPOCH_GPS_TAI_UTC;
  /* now check to see if this is an additional leap second */
  if ( utc->tm_sec == 60 )
    --gpssec;
  return gpssec;
}

/** Returns the Julian Day (JD) corresponding to the date given in a broken down time structure.*/
double JulianDay(const struct tm *utc /**< [In] UTC time in a broken down time structure. */){
  const int sec_per_day = 60 * 60 * 24; /* seconds in a day */
  int year, month, day, sec;
  double jd;
  
  /* this routine only works for dates after 1900 */
  if ( utc->tm_year <= 0 ) return 0.0;

  year  = utc->tm_year + 1900;
  month = utc->tm_mon + 1;     /* month is in range 1-12 */
  day   = utc->tm_mday;        /* day is in range 1-31 */
  sec   = utc->tm_sec + 60*(utc->tm_min + 60*(utc->tm_hour)); /* seconds since midnight */
  
  jd = 367*year - 7*(year + (month + 9)/12)/4 + 275*month/9 + day + 1721014;
  /* note: Julian days start at noon: subtract half a day */
  jd += (double)sec/(double)sec_per_day - 0.5;
  return jd;
}
