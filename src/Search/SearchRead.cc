//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "SearchRead.h"

ClassImp(SearchRead)

////////////////////////////////////////////////////////////////////////////////////
SearchRead::SearchRead(const string aFilePattern, const int aVerbose): GwollumPlot("SearchRead","FIRE"), Chain("coinc",aFilePattern.c_str(),(bool)aVerbose), InjRea(aFilePattern,aVerbose) {
////////////////////////////////////////////////////////////////////////////////////
  
  status_OK=(bool)TChain::GetEntries();
  verbosity=aVerbose;
  SetGridx(1); SetGridy(1); 
  
  // add cluster trees
  triggers_0 = new ReadTriggers(aFilePattern,"clusters_0",verbosity);
  triggers_1 = new ReadTriggers(aFilePattern,"clusters_1",verbosity);
  status_OK*=triggers_0->GetStatus();
  status_OK*=triggers_1->GetStatus();
  if(status_OK) AddFriend(triggers_0->Ttree,"c0");
  if(status_OK) AddFriend(triggers_1->Ttree,"c1");

  // attach branches (coinc tree)
  if(SetBranchAddress("MorphMatch",&CoMatch)<0){
    cerr<<"SearchRead::SearchRead: missing or corrupted 'MorphMatch' branch"<<endl;
    status_OK=false;
  }
  if(SetBranchAddress("MorphMatchSumw2",&CoMatchSumw2)<0){
    cerr<<"SearchRead::SearchRead: missing or corrupted 'MorphMatchSumw2' branch"<<endl;
    status_OK=false;
  }
  if(SetBranchAddress("MorphMatchShift",&CoMatchShift)<0){
    cerr<<"SearchRead::SearchRead: missing or corrupted 'MorphMatchShift' branch"<<endl;
    status_OK=false;
  }

  // get trigger offsets
  Chain *coinc2param = new Chain("coinc2param",aFilePattern.c_str());
  coinc2param->SetBranchAddress("toffset_0",&toffset_0);
  coinc2param->SetBranchAddress("toffset_1",&toffset_1);
  if(coinc2param->GetMinimum("toffset_0")!=coinc2param->GetMaximum("toffset_0")){
    cerr<<"SearchRead::SearchRead: there should be a single time offset for the input clusters"<<endl;
    status_OK=false;
  }
  if(coinc2param->GetMinimum("toffset_1")!=coinc2param->GetMaximum("toffset_1")){
    cerr<<"SearchRead::SearchRead: there should be a single time offset for the input clusters"<<endl;
    status_OK=false;
  }
  delete coinc2param;
  
  // inj <--> coinc mapping
  coincmap = new Chain("coincmap",aFilePattern.c_str());// FIXME: reuse existing tfiles?
  CoInjIndex = -1; // default if missing tree or branch
  if(coincmap->GetNfiles()){
    coincmap->SetBranchAddress("injindex",&CoInjIndex);
    if(status_OK) AddFriend(coincmap);
  }

  // tag coinc best-matching an injection
  injbestmatch=true;
  tinj = new TTree("tinj","tinj");
  tinj->Branch("injbestmatch",&injbestmatch,"injbestmatch/O");

  if(status_OK&&coincmap->GetNfiles()){
    int bestcoinc=-1, ii, cc;
    double bestmatch=-1;
    for(int c=0; c<GetEntries(); c++){
      GetEntry(c);
      
      if(GetCoincInjectionIndex()<0){// does not match any injection
	injbestmatch=false;
	tinj->Fill();
	continue;
      }
      else{// injection is recovered --> get the best coinc for this injection
	cc=c;
	ii=GetCoincInjectionIndex();
	bestmatch=-1;
	while(1){// find the best match
	  GetEntry(cc);
	  if(GetCoincInjectionIndex()!=ii) break;
	  if(GetCoincMorphMatch()>bestmatch){
	    bestmatch=GetCoincMorphMatch();
	    bestcoinc=cc;
	  }
	  cc++;
	  if(cc>=GetEntries()) break;
	}
	while(1){// fill the tree
	  GetEntry(c);
	  if(GetCoincInjectionIndex()!=ii) break;
	  if(c==bestcoinc) injbestmatch=true;
	  else injbestmatch=false;
	  tinj->Fill();
	  c++;
	  if(c>=GetEntries()) break;
	}
	c--;
      }
    }
    if(tinj->GetEntries()==GetEntries()) AddFriend(tinj);
    else{
      cerr<<"SearchRead::SearchRead: for some reason, the recovered injection could not be tagged: "<<tinj->GetEntries()<<"/"<<GetEntries()<<endl;
    }
  }

  // update style
  if(coincmap->GetNfiles()){ // injection style
    SetLineColor(2);
    SetMarkerStyle(6);
    SetMarkerColor(2);
  }
  else{// non injections
    if(!toffset_0&&!toffset_1){// zero-lag
      SetLineColor(5);
      SetMarkerStyle(6);
      SetMarkerColor(5);
    }
    else{// background
      SetLineColor(0);
      SetMarkerStyle(6);
      SetMarkerColor(0);
    }
  }
  
}

////////////////////////////////////////////////////////////////////////////////////
SearchRead::~SearchRead(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>1) cout<<"SearchRead::~SearchRead"<<endl;
  delete triggers_0;
  delete triggers_1;
  delete coincmap;
  delete tinj;
}

