/**
 * @file 
 * @brief Read triggers in ROOT files.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __ReadTriggers__
#define __ReadTriggers__

#include "ReadTriggerMetaData.h"
#include "Triggers.h"

using namespace std;

/**
 * Read trigger files.
 * This class was designed to read triggers saved in ROOT files and following the <a href="../../Main/convention.html#triggers">GWOLLUM convention</a>. It inherits from the Triggers (reading-mode) and the ReadTriggerMetaData classes. The purpose is to offer a class which logically loads all the trigger information contained in trigger files: the triggers, the segments and the metadata.
 * \author    Florent Robinet
 */
class ReadTriggers: public ReadTriggerMetaData, public Triggers {
  
 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the ReadTriggers class.
   * The trigger files designated by the file pattern 'aPattern' are loaded. The triggers, the segments and the metadata are accessed in the root directory 'aDirectory'. The verbosity level, set by 'aVerbose', follows the <a href="../../Main/convention.html#verbosity">GWOLLUM convention</a> for verbosity. The stream name saved in the metadata is used to construct the Streams inheritance.
   *
   * @param aPattern input file pattern
   * @param aDirectory trigger ROOT directory
   * @param aVerbose verbosity level
   */
  ReadTriggers(const string aPattern, const string aDirectory="",
               const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the ReadTriggers class.
   */
  virtual ~ReadTriggers(void);
  /**
     @}
  */
  
  ClassDef(ReadTriggers,0)  
};

#endif


