/**
 * @file 
 * @brief See TriggerBuffer.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "TriggerBuffer.h"

ClassImp(TriggerBuffer)

////////////////////////////////////////////////////////////////////////////////////
TriggerBuffer::TriggerBuffer(const unsigned int aSize, const string aStreamName,
                             const unsigned int aVerbose):
MakeTriggers(aStreamName, aVerbose) { 
////////////////////////////////////////////////////////////////////////////////////

  // allocate memory for the buffer
  BufferSize = aSize;
  b_time     = new double [BufferSize];
  b_freq     = new double [BufferSize];
  b_tstart   = new double [BufferSize];
  b_tend     = new double [BufferSize];
  b_fstart   = new double [BufferSize];
  b_fend     = new double [BufferSize];
  b_snr      = new double [BufferSize];
  b_q        = new double [BufferSize];
  b_amp      = new double [BufferSize];
  b_ph       = new double [BufferSize];
  b_pos      = 0;

}

////////////////////////////////////////////////////////////////////////////////////
TriggerBuffer::~TriggerBuffer(void){
////////////////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"TriggerBuffer::~TriggerBuffer"<<endl;
  delete b_time;
  delete b_freq;
  delete b_tstart;
  delete b_tend;
  delete b_fstart;
  delete b_fend;
  delete b_q;
  delete b_snr;
  delete b_amp;
  delete b_ph;
}

////////////////////////////////////////////////////////////////////////////////////
bool TriggerBuffer::AddTrigger(const double aTime,   const double aFrequency,
			       const double aSNR,    const double aQ,
			       const double aTstart, const double aTend,
			       const double aFstart, const double aFend,
			       const double aAmplitude, const double aPhase){
////////////////////////////////////////////////////////////////////////////////////
  if(!BufferSize) return Triggers::AddTrigger(aTime,aFrequency,aSNR,aQ,aTstart,aTend,aFstart,aFend,aAmplitude,aPhase);
  
  if(b_pos==BufferSize){
    cerr<<"TriggerBuffer::AddTrigger: the buffer is full ("<<BufferSize<<" triggers)"<<endl;
    return false;
  }

  b_time[b_pos]   = aTime;
  b_freq[b_pos]   = aFrequency;
  b_tstart[b_pos] = aTstart;
  b_tend[b_pos]   = aTend;
  b_fstart[b_pos] = aFstart;
  b_fend[b_pos]   = aFend;
  b_q[b_pos]      = aQ;
  b_snr[b_pos]    = aSNR;
  b_amp[b_pos]    = aAmplitude;
  b_ph[b_pos]     = aPhase;
  b_pos++;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool TriggerBuffer::Flush(void){
////////////////////////////////////////////////////////////////////////////////////

  // flush triggers
  for(unsigned int t=0; t<b_pos; t++){
    if(!MakeTriggers::AddTrigger(b_time[t],
                                 b_freq[t],
                                 b_snr[t],
                                 b_q[t],
                                 b_tstart[t],
                                 b_tend[t],
                                 b_fstart[t],
                                 b_fend[t],
                                 b_amp[t],
                                 b_ph[t])){
      return false;
    }
  }

  // flush trigger segments
  if((s_start.size()>0) && (s_start[0]>=Segments::GetStart(Segments::GetN()-1))){
    vector<double> tmp(s_start.size(), 1);
    if(!Segments::Append(s_start, s_end, tmp)) return false;// append
  }
  else{
    for(unsigned int s=0; s<s_start.size(); s++){
      if(!Segments::AddSegment(s_start[s], s_end[s], 1)) return false; // add
    }
  }

  
  // reset buffer
  Reset();
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool TriggerBuffer::SetSegments(Segments *aSeg){
////////////////////////////////////////////////////////////////////////////////////
  if(aSeg->GetN()==0) return true;
  if(aSeg->GetStart(0)<s_end.back()) return false;

  for(unsigned int s=0; s<aSeg->GetN(); s++){
    s_start.push_back(aSeg->GetStart(s));
    s_end.push_back(aSeg->GetEnd(s));
    s_pos.push_back(b_pos);
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerBuffer::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  b_pos=0;
  s_start.clear();
  s_end.clear();
  s_pos.clear();
  return;
}


////////////////////////////////////////////////////////////////////////////////////
void TriggerBuffer::ResetAfter(const double aTime){
////////////////////////////////////////////////////////////////////////////////////
    
  // rewind and remove segments ending after input time
  for(int s=(int)s_end.size()-1; s>0; s--){
    if(s_end[s]>aTime){
      b_pos=s_pos[s];
      s_start.pop_back();
      s_end.pop_back();
      s_pos.pop_back();
    }
    break;
  }

  return;
}

