/**
 * @file 
 * @brief Access metadata in trigger files.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __ReadTriggerMetaData__
#define __ReadTriggerMetaData__

#include "Segments.h"
#include "Streams.h"

using namespace std;


/**
 * @brief Access metadata in trigger files.
 * @details This class is designed to read triggers metadata stored in TTrees saved in ROOT files.
 * It should be used when you are only interested by the metadata content of trigger files.
 * Only the segment and metadata contents of the files are loaded so this class is much faster than the ReadTriggers class since the triggers are never loaded.
 * This is quite useful when hundreds of files need to be scanned.
 * @author Florent Robinet
 */
class ReadTriggerMetaData: public Segments, public Streams {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the ReadTriggerMetaData class.
   * @details The segment tree and the metadata tree in ROOT files designated by a file pattern are scanned.
   *
   * The metadata parameters (frequency, Q, and SNR) are scanned and the absolute minimum/maximum is found.
   *
   * The segment tree in the input files are also downloaded. These segments can be accessed using the methods of the Segments class from which this class inherits.
   *
   * The stream name in the metadata is used to initiate the Streams object.
   *
   * @warning When scanning the files, the reference process name and stream name are determined from the first file. Then, if, for the next files, the process name or the stream name changes, files are skipped.
   * @note The process version and the username are extracted from the last file.
   *
   * @param[in] aPattern Input file pattern.
   * @param[in] aDirectory ROOT subdirectory. Use "" if none.
   * @param[in] aVerbose Verbosity level.
   */
  ReadTriggerMetaData(const string aPattern, const string aDirectory="", const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the ReadTriggerMetaData class.
   */
  virtual ~ReadTriggerMetaData(void);
  /**
     @}
  */
  
  /**
   * @brief Returns the process name.
   */
  inline string GetProcessName(void){ 
    return Mprocessname;
  };
  
  /**
   * @brief Returns the process version.
   */
  inline string GetProcessVersion(void){ 
    return Mprocessversion;
  };
  
  /**
   * @brief Returns the process user name.
   */
  inline string GetProcessUser(void){ 
    return Mprocessuser;
  };
  
  /**
   * @brief Returns the absolute minimum frequency [Hz].
   */
  inline double GetFrequencyMin(void){ return Mfmin_stat; };

  /**
   * @brief Returns the absolute maximum frequency [Hz].
   */
  inline double GetFrequencyMax(void){ return Mfmax_stat; };

  /**
   * @brief Returns the absolute minimum Q value.
   */
  inline double GetQMin(void){ return Mqmin_stat; };

  /**
   * @brief Returns the absolute maximum Q value.
   */
  inline double GetQMax(void){ return Mqmax_stat; };

  /**
   * @brief Returns the absolute minimum SNR value.
   */
  inline double GetSNRMin(void){ return Msnrmin_stat; };

  /**
   * @brief Returns the absolute maximum SNR value.
   */
  inline double GetSNRMax(void){ return Msnrmax_stat; };

protected:
    unsigned int Verbose;     ///< Verbosity level.

 
 private:
  
  // DATA
  double *Mstart;           ///< Start time.
  double *Mend;             ///< End time.
  string Mprocessname;      ///< Process name.
  string Mprocessversion;   ///< Process version.
  string Mprocessuser;      ///< User name.
  double *Mfmin;            ///< Meta frequency min.
  double *Mfmax;            ///< Meta frequency max.
  double *Mqmin;            ///< Meta Q min.
  double *Mqmax;            ///< Meta Q max.
  double *Msnrmin;          ///< Meta SNR min.
  double *Msnrmax;          ///< Meta SNR max.
  double Mfmin_stat;        ///< Meta frequency min (static).
  double Mfmax_stat;        ///< Meta frequency max (static).
  double Mqmin_stat;        ///< Meta Q min (static).
  double Mqmax_stat;        ///< Meta Q max (static).
  double Msnrmin_stat;      ///< Meta SNR min (static).
  double Msnrmax_stat;      ///< Meta SNR max (static).

  ClassDef(ReadTriggerMetaData,0)  
};

#endif


