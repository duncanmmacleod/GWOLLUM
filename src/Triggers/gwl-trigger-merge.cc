/**
 * @file 
 * @brief Program to print the segments of trigger files.
 * @snippet this gwl-trigger-merge-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */

#include "ReadTriggers.h"
#include "MakeTriggers.h"

using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  // check the argument
  if(argc!=3){
    //! [gwl-trigger-merge-usage]
    cerr<<"This program merges multiple trigger files (ROOT) into one."<<endl;
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [outdir] [ROOT trigger files]"<<endl; 
    cerr<<endl;
    cerr<<"  [outdir]: output directory"<<endl; 
    cerr<<"  [ROOT trigger files]: list of ROOT trigger files (pattern)"<<endl; 
    cerr<<""<<endl;
    //! [gwl-trigger-merge-usage]
    return 1;
  }

  // outdir
  string outdir = (string)argv[1];
  system(("mkdir -p "+outdir).c_str());
  
  // input files
  string infiles = (string)argv[2];

  // extract segments
  ReadTriggerMetaData *meta = new ReadTriggerMetaData(infiles,"",0);
  cout<<"gwl-trigger-merge: merging triggers between "<<meta->Segments::GetStart(0)<<" and "<<meta->Segments::GetEnd(meta->Segments::GetN()-1)<<endl;

  // no livetime --> exit
  double livetime=meta->Segments::GetLiveTime();
  if(livetime<=0){
    cout<<"gwl-trigger-merge: no livetime"<<endl;
    delete meta;
    return 0;
  }

  // get segment tree
  TTree *segtree = meta->Segments::GetTree();
  
  // output file name
  unsigned int start=(unsigned int)(meta->Segments::GetStart(0));
  unsigned int duration=(unsigned int)ceil(meta->Segments::GetEnd(meta->Segments::GetN()-1))-start;
  string outfilename = meta->Streams::GetTriggerFileName(start, duration, "root", meta->GetProcessName(), outdir);
  
  // extract tree
  TChain *merged_Mtree = new TChain("metadata");
  TChain *merged_Ttree = new TChain("triggers");
  vector <string> filelist = SplitString(infiles,' ');
  for(unsigned int f=0; f<filelist.size(); f++){
    merged_Mtree->Add(filelist[f].c_str());
    merged_Ttree->Add(filelist[f].c_str());
  }
  cout<<"gwl-trigger-merge: number of triggers: "<<merged_Ttree->GetEntries()<<endl;


  TFile *f;

  // there is some triggers
  if(merged_Ttree->GetEntries()){

    // merge trigger tree
    merged_Ttree->Merge(outfilename.c_str());
    delete merged_Ttree;

    // add metadata tree
    f = TFile::Open(outfilename.c_str(), "UPDATE");
    f->cd();
    merged_Mtree->Merge(f,0);// NOT SURE ABOUT THE 0 ???

    // add segment tee
    f = TFile::Open(outfilename.c_str(), "UPDATE");
    f->cd();
    segtree->Write();
    f->Close();
  }

  // there is no triggers
  else{
    delete merged_Ttree;
    MakeTriggers *Empty = new MakeTriggers(meta->Streams::GetName(),0);
    Empty->SetProcessName(meta->GetProcessName());
    Empty->Append(meta);
    if(!Empty->Write(outdir,"root","","","RECREATE", false).compare("")){
      cerr<<"gwl-trigger-merge: failed to write triggers"<<endl;
      return 1;
    }
    delete Empty;

    // add meta tree
    f = TFile::Open(outfilename.c_str(), "UPDATE");
    f->cd();
    merged_Mtree->Merge(f,0);// NOT SURE ABOUT THE 0 ???
  }
  cout<<"gwl-trigger-merge: merged file: "<<outfilename<<endl;

  
  // clean
  delete merged_Mtree;
  delete meta;
  delete segtree;
  
  return 0;
}

