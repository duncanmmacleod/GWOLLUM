/**
 * @file 
 * @brief See TriggerSelect.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "TriggerSelect.h"

ClassImp(TriggerSelect)

////////////////////////////////////////////////////////////////////////////////////
TriggerSelect::TriggerSelect(const string aName, const unsigned int aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  name = aName;
  verbose = aVerbose;
  sstart = "";
  
  // default time
  tmin_0   = 700000000.0;
  tmax_0   = 2000000000.0;
  ntbins_0 = 1;
  tbins = new double [0];
 
  // default frequency
  fmin_0   = 1.0;
  fmax_0   = 10000.0;
  nfbins_0 = 1;
  fbins = new double [0];

  // default Q
  qmin_0   = 1.0;
  qmax_0   = 1000.0;
  nqbins_0 = 1;
  qbins = new double [0];
 
  // default SNR
  smin_0   = 1.0;
  smax_0   = 1e20;
  nsbins_0 = 1;
  sbins = new double [0];
 
  // set default selection
  ResetSelection();
}

////////////////////////////////////////////////////////////////////////////////////
TriggerSelect::TriggerSelect(ReadTriggers *aTriggers, const unsigned int aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  name = aTriggers->Streams::GetName();
  verbose = aVerbose;
  sstart = "";

  // default time
  if(aTriggers->Segments::GetN()){
    tmin_0 = aTriggers->GetFirst();
    tmax_0 = aTriggers->GetLast();
  }
  else{
    tmin_0 = 0.0;
    tmax_0 = 1.0;
  }
  ntbins_0 = 1;
  tbins = new double [0];
 
  // default frequency
  fmin_0   = aTriggers->GetFrequencyMin();
  fmax_0   = aTriggers->GetFrequencyMax();
  nfbins_0 = 1;
  fbins = new double [0];

  // default Q
  qmin_0   = aTriggers->GetQMin();
  qmax_0   = 1.01*aTriggers->GetQMax();
  nqbins_0 = 1;
  qbins = new double [0];
 
  // default SNR
  smin_0   = aTriggers->GetSNRMin();
  smax_0   = 1.01*aTriggers->GetSNRMax();
  nsbins_0 = 1;
  sbins = new double [0];
 
  // set default selection
  ResetSelection();
}

////////////////////////////////////////////////////////////////////////////////////
TriggerSelect::~TriggerSelect(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbose>1) cout<<"TriggerSelect::~TriggerSelect"<<endl;
  delete tbins;
  delete fbins;
  delete qbins;
  delete sbins;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::ResetSelection(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbose) cout<<"TriggerSelect::ResetSelection: set default selection"<<endl;

  // time
  tmin   = tmin_0;
  tmax   = tmax_0;
  ntbins = ntbins_0;
  MakeTbins();

  // frequency
  fmin   = fmin_0;
  fmax   = fmax_0;
  nfbins = nfbins_0;
  MakeFbins();

  // Q
  qmin   = qmin_0;
  qmax   = qmax_0;
  nqbins = nqbins_0;
  MakeQbins();

  // SNR
  smin   = smin_0;
  smax   = smax_0;
  nsbins = nsbins_0;
  MakeSbins();

  clustertag = -1;
  usedate = false;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::SetTimeSelection(const unsigned int aResolution,
                                     const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  tmin=aMin;
  tmax=aMax;
  ntbins=aResolution;
  if(verbose>1)
    cout<<"TriggerSelect::SetTimeSelection: "<<tmin<<"-"<<tmax<<", resolution = "<<ntbins<<endl;
  return MakeTbins();
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::SetFrequencySelection(const unsigned int aResolution,
                                          const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  fmin=aMin;
  fmax=aMax;
  nfbins=aResolution;
  if(verbose>1)
    cout<<"TriggerSelect::SetFrequencySelection: "<<fmin<<"-"<<fmax<<", resolution = "<<nfbins<<endl;
  return MakeFbins();
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::SetQSelection(const unsigned int aResolution,
                                  const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  qmin=aMin;
  qmax=aMax;
  nqbins=aResolution;
  if(verbose>1)
    cout<<"TriggerSelect::SetQSelection: "<<qmin<<"-"<<qmax<<", resolution = "<<nqbins<<endl;
  return MakeQbins();
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::SetSnrSelection(const unsigned int aResolution,
                                    const double aMin, const double aMax){
////////////////////////////////////////////////////////////////////////////////////
  smin=aMin;
  smax=aMax;
  nsbins=aResolution;
  if(verbose>1)
    cout<<"TriggerSelect::SetSnrSelection: "<<smin<<"-"<<smax<<", resolution = "<<nsbins<<endl;
  return MakeSbins();
}

////////////////////////////////////////////////////////////////////////////////////
TH1D* TriggerSelect::GetTH1D(const string aParameter){
////////////////////////////////////////////////////////////////////////////////////

  // title
  string title = name;
  if(clustertag>=0) title += " (clusters)";
  else title += " (triggers)";

  TH1D *h;

  // create histogram
  if((!aParameter.compare("Time")) && (tmin<tmax)){
    h = new TH1D((name+"_"+aParameter).c_str(), title.c_str(), ntbins, tbins);
    h->GetXaxis()->SetNoExponent();
    h->GetXaxis()->SetTitle("Time [s]");
  }
  else if((!aParameter.compare("Frequency")) && (fmin<fmax)){
    h = new TH1D((name+"_"+aParameter).c_str(), title.c_str(), nfbins, fbins);
    h->GetXaxis()->SetTitle("Frequency [Hz]");
  }
  else if((!aParameter.compare("Q")) && (qmin<qmax))
    h = new TH1D((name+"_"+aParameter).c_str(), title.c_str(), nqbins, qbins);
  else if((!aParameter.compare("SNR")) && (smin<smax))
    h = new TH1D((name+"_"+aParameter).c_str(), title.c_str(), nsbins, sbins);
  else{
    h = new TH1D((name+"_"+aParameter).c_str(), title.c_str(), 1, 0.0, 1.0);
  }

  // axis
  if(clustertag>=0) title = "number of clusters / bin";
  else title = "number of triggers / bin";
  h->GetYaxis()->SetTitle(title.c_str());
  h->GetXaxis()->SetMoreLogLabels();
  h->GetYaxis()->SetMoreLogLabels();
  h->GetXaxis()->SetNdivisions(4,5,0);
  h->GetXaxis()->SetLabelSize(0.04);
  h->GetYaxis()->SetLabelSize(0.04);
  h->GetXaxis()->SetTitleSize(0.04);
  h->GetYaxis()->SetTitleSize(0.04);
  h->GetXaxis()->SetTitleOffset(1.15);
  h->GetYaxis()->SetTitleOffset(1.3);

  // use a date format
  if(usedate && (!aParameter.compare("Time"))){
    double offset=EPOCH_UNIX_GPS - LeapSeconds(tmin) + EPOCH_GPS_TAI_UTC;
    h->GetXaxis()->SetTimeFormat("");
    h->GetXaxis()->SetTimeDisplay(1);
    h->GetXaxis()->SetTimeOffset(offset,"gmt");
    h->GetXaxis()->SetTitle("Date");
  }

  return h;
}

////////////////////////////////////////////////////////////////////////////////////
TH2D* TriggerSelect::GetTH2D(const string aParameterX, const string aParameterY){
////////////////////////////////////////////////////////////////////////////////////

  // title
  string title = name;
  if(clustertag>=0) title += " (clusters)";
  else title += " (triggers)";

  TH2D *h;

  if((!aParameterX.compare("Time")) && (tmin<tmax)){
    if((!aParameterY.compare("Time")) && (tmin<tmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   ntbins,tbins,
                   ntbins,tbins);
      h->GetYaxis()->SetNoExponent();
      h->GetYaxis()->SetTitle("Time [s]");
    }
    else if((!aParameterY.compare("Frequency")) && (fmin<fmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   ntbins,tbins,
                   nfbins,fbins);
      h->GetYaxis()->SetTitle("Frequency [Hz]");
    }
    else if((!aParameterY.compare("Q")) && (qmin<qmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   ntbins,tbins,
                   nqbins,qbins);
      h->GetYaxis()->SetTitle("Q");
    }
    else if((!aParameterY.compare("SNR")) && (smin<smax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   ntbins,tbins,
                   nsbins,sbins);
      h->GetYaxis()->SetTitle("SNR");
    }
    else
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   1, 0.0, 1.0,
                   1, 0.0, 1.0);
    h->GetXaxis()->SetNoExponent();
    h->GetXaxis()->SetTitle("Time [s]");
  }
  else if((!aParameterX.compare("Frequency")) && (fmin<fmax)){
    if((!aParameterY.compare("Time")) && (tmin<tmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nfbins,fbins,
                   ntbins,tbins);
      h->GetYaxis()->SetNoExponent();
      h->GetYaxis()->SetTitle("Time [s]");
    }
    else if((!aParameterY.compare("Frequency")) && (fmin<fmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nfbins,fbins,
                   nfbins,fbins);
      h->GetYaxis()->SetTitle("Frequency [Hz]");
    }
    else if((!aParameterY.compare("Q")) && (qmin<qmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nfbins,fbins,
                   nqbins,qbins);
      h->GetYaxis()->SetTitle("Q");
    }
    else if((!aParameterY.compare("SNR")) && (smin<smax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nfbins,fbins,
                   nsbins,sbins);
      h->GetYaxis()->SetTitle("SNR");
    }
    else
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   1, 0.0, 1.0,
                   1, 0.0, 1.0);
    h->GetXaxis()->SetTitle("Frequency [Hz]");
  }
  else if((!aParameterX.compare("Q")) && (qmin<qmax)){
    if((!aParameterY.compare("Time")) && (tmin<tmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nqbins,qbins,
                   ntbins,tbins);
      h->GetYaxis()->SetNoExponent();
      h->GetYaxis()->SetTitle("Time [s]");
    }
    else if((!aParameterY.compare("Frequency")) && (fmin<fmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nqbins,qbins,
                   nfbins,fbins);
      h->GetYaxis()->SetTitle("Frequency [Hz]");
    }
    else if((!aParameterY.compare("Q")) && (qmin<qmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nqbins,qbins,
                   nqbins,qbins);
      h->GetYaxis()->SetTitle("Q");
    }
    else if((!aParameterY.compare("SNR")) && (smin<smax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nqbins,qbins,
                   nsbins,sbins);
      h->GetYaxis()->SetTitle("SNR");
    }
    else
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   1, 0.0, 1.0,
                   1, 0.0, 1.0);
      h->GetXaxis()->SetTitle("Q");
  }
  else if((!aParameterX.compare("SNR")) && (smin<smax)){
    if((!aParameterY.compare("Time")) && (tmin<tmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nsbins,sbins,
                   ntbins,tbins);
      h->GetYaxis()->SetNoExponent();
      h->GetYaxis()->SetTitle("Time [s]");
    }
    else if((!aParameterY.compare("Frequency")) && (fmin<fmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nsbins,sbins,
                   nfbins,fbins);
      h->GetYaxis()->SetTitle("Frequency [Hz]");
    }
    else if((!aParameterY.compare("Q")) && (qmin<qmax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nsbins,sbins,
                   nqbins,qbins);
      h->GetYaxis()->SetTitle("Q");
    }
    else if((!aParameterY.compare("SNR")) && (smin<smax)){
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   nsbins,sbins,
                   nsbins,sbins);
      h->GetYaxis()->SetTitle("SNR");
    }
    else
      h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                   1, 0.0, 1.0,
                   1, 0.0, 1.0);
    h->GetXaxis()->SetTitle("SNR");
  }
  else
    h = new TH2D((aParameterY+aParameterX).c_str(), title.c_str(),
                 1, 0.0, 1.0,
                 1, 0.0, 1.0);

  // axis
  h->GetXaxis()->SetMoreLogLabels();
  h->GetYaxis()->SetMoreLogLabels();
  h->GetXaxis()->SetNdivisions(4,5,0);
  h->GetXaxis()->SetLabelSize(0.04);
  h->GetYaxis()->SetLabelSize(0.04);
  h->GetXaxis()->SetTitleSize(0.04);
  h->GetYaxis()->SetTitleSize(0.04);
  h->GetXaxis()->SetTitleOffset(1.15);
  h->GetYaxis()->SetTitleOffset(1.3);

  // use a date format
  if(usedate && (!aParameterX.compare("Time"))){
    double offset=EPOCH_UNIX_GPS - LeapSeconds(tmin) + EPOCH_GPS_TAI_UTC;
    h->GetXaxis()->SetTimeFormat("");
    h->GetXaxis()->SetTimeDisplay(1);
    h->GetXaxis()->SetTimeOffset(offset,"gmt");
    h->GetXaxis()->SetTitle("Date");
  }

  // use a date format
  if(usedate && (!aParameterY.compare("Time"))){
    double offset=EPOCH_UNIX_GPS - LeapSeconds(tmin) + EPOCH_GPS_TAI_UTC;
    h->GetYaxis()->SetTimeFormat("");
    h->GetYaxis()->SetTimeDisplay(1);
    h->GetYaxis()->SetTimeOffset(offset,"gmt");
    h->GetYaxis()->SetTitle("Date");
  }

  return h;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::PrintSelection(void){
////////////////////////////////////////////////////////////////////////////////////
  cout<<"TriggerSelect::PrintSelection:"<<endl;
  cout<<"             Time:      "<<tmin<<" --> "<<tmax<<" ("<<ntbins<<" bins)"<<endl;
  cout<<"             Frequency: "<<fmin<<" --> "<<fmax<<" ("<<nfbins<<" bins)"<<endl;
  cout<<"             Q:         "<<qmin<<" --> "<<qmax<<" ("<<nqbins<<" bins)"<<endl;
  cout<<"             SNR:       "<<smin<<" --> "<<smax<<" ("<<nsbins<<" bins)"<<endl;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::MakeTbins(void){
////////////////////////////////////////////////////////////////////////////////////
  delete tbins;
  tbins = new double [ntbins+1];
  for(unsigned int b=0; b<ntbins+1; b++) 
    tbins[b] = tmin + (double)b*(tmax-tmin)/(double)ntbins;

  // make time stamp
  if(usedate){
    struct tm utcmin;
    char buffer [80];
    GPSToUTC(&utcmin, (int)tmin);
    strftime(buffer, 80, "%Y-%b-%d %H:%M:%S UTC", &utcmin);
    sstart=buffer;
  }
  else{
    stringstream tmpstream;
    tmpstream.flags(ios::fixed);
    tmpstream<<setprecision(3)<<tmin;
    sstart=tmpstream.str();
    tmpstream.clear(); tmpstream.str("");
  }
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::MakeFbins(void){
////////////////////////////////////////////////////////////////////////////////////
  delete fbins;
  fbins = new double [nfbins+1];
  if(fmin>0){
    for(unsigned int b=0; b<nfbins+1; b++) 
      fbins[b] = fmin*pow(10.0, (double)b*log10(fmax/fmin)/(double)nfbins);
  }
  else{
    for(unsigned int b=0; b<nfbins+1; b++) 
      fbins[b] = fmin + (double)b*(fmax-fmin)/(double)nfbins;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::MakeQbins(void){
////////////////////////////////////////////////////////////////////////////////////
  delete qbins;
  qbins = new double [nqbins+1];
  if(qmin>0){
    for(unsigned int b=0; b<nqbins+1; b++) 
      qbins[b] = qmin*pow(10.0, (double)b*log10(qmax/qmin)/(double)nqbins);
  }
  else{
    for(unsigned int b=0; b<nqbins+1; b++) 
      qbins[b] = qmin + (double)b*(qmax-qmin)/(double)nqbins;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerSelect::MakeSbins(void){
////////////////////////////////////////////////////////////////////////////////////
  delete sbins;
  sbins = new double [nsbins+1];
  if(smin>0){
    for(unsigned int b=0; b<nsbins+1; b++) 
      sbins[b] = smin*pow(10.0, (double)b*log10(smax/smin)/(double)nsbins);
  }
  else{
    for(unsigned int b=0; b<nsbins+1; b++) 
      sbins[b] = smin + (double)b*(smax-smin)/(double)nsbins;
  }
  return;
}

