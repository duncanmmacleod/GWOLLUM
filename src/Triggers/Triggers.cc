/**
 * @file 
 * @brief See Triggers.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Triggers.h"

ClassImp(Triggers)

////////////////////////////////////////////////////////////////////////////////////
Triggers::Triggers(const unsigned int aVerbose): Verbose(aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  // default clustering
  SetClusterizeDt(0.1);// 100ms time clustering
  SetClusterizeSizeMin(0);// no minimal cluster size
  SetClusterizeSnrThr(0.0);// no SNR threshold
  
  // random id
  srand(time(NULL));
  randid = rand();
  ostringstream tmpstream;
  tmpstream<<randid;
  srandid = tmpstream.str();
  
  // init trigger trees
  readmode = false; // write mode
  Ttree_write = new TTree(("triggers_"+srandid).c_str(), "triggers");
  Ttree_write->Branch("time",     &Ttime,  "time/D");
  Ttree_write->Branch("frequency",&Tfreq,  "frequency/D");
  Ttree_write->Branch("q",        &Tq,     "q/D");
  Ttree_write->Branch("snr",      &Tsnr,   "snr/D");
  Ttree_write->Branch("tstart",   &Ttstart,"tstart/D");
  Ttree_write->Branch("tend",     &Ttend,  "tend/D");
  Ttree_write->Branch("fstart",   &Tfstart,"fstart/D");
  Ttree_write->Branch("fend",     &Tfend,  "fend/D");
  Ttree_write->Branch("amplitude",&Tamp,   "amplitude/D");
  Ttree_write->Branch("phase",    &Tph,    "phase/D");
  Ttree_write->Branch("tstart_us",&Ttstart_us,"tstart_us/I");
  Ttree=Ttree_write;

  Reset();
}

////////////////////////////////////////////////////////////////////////////////////
Triggers::~Triggers(void){
////////////////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"Triggers::~Triggers"<<endl;
  Reset();
  delete Ttree_write;
}

////////////////////////////////////////////////////////////////////////////////////
void Triggers::Reset(void){ 
////////////////////////////////////////////////////////////////////////////////////
  ResetClusters();
  Ttree_write->Reset();
  if(readmode){
    readmode = false; // back to write mode
    Ttree=Ttree_write;
    delete Ttree_read;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Triggers::AddTrigger(const double aTime, const double aFrequency,
			  const double aSNR, const double aQ,
			  const double aTstart, const double aTend,
			  const double aFstart, const double aFend,
			  const double aAmplitude, const double aPhase){
////////////////////////////////////////////////////////////////////////////////////
  if(readmode){
    cerr<<"Triggers::AddTrigger: this function cannot be called in a read mode"<<endl;
    return false;
  }

  // save trigger parameters
  // limit the time resolution to 1 ms
  Ttime=floor(aTime*1000000.0)/1000000.0;
  Tfreq=aFrequency;
  Tsnr=aSNR;
  Tq=aQ;
  Tamp=aAmplitude;
  Tph=aPhase;
  // enforce Tstart <= Tmin <= Tend
  Ttstart=TMath::Min(floor(aTstart*1000000.0)/1000000.0, Ttime); 
  Ttend=TMath::Max(ceil(aTend*1000000.0)/1000000.0, Ttime); 
  Tfstart=TMath::Min(aFstart,Tfreq);
  Tfend=TMath::Max(aFend,Tfreq);
  Ttstart_us=(int)floor((aTstart-(int)aTstart)*1000000+0.5);

  // fill tree
  if(Ttree->Fill()<=0) return false;

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Triggers::SortTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  if(readmode){
    cerr<<"Triggers::SortTriggers: this function cannot be called in a read mode"<<endl;
    return false;
  }
  if(Verbose) cout<<"Triggers::SortTriggers: sorting triggers"<<endl;

  // test number of triggers
  Long64_t n_entries = Ttree_write->GetEntries();
  if(n_entries<2) return true;
  if(n_entries>INT_MAX){
    cerr<<"Triggers::SortTriggers: too many triggers to sort ("<<INT_MAX<<")"<<endl;
    return false;
  }

  // build new index
  if(Ttree_write->BuildIndex("tstart","tstart_us")!=(int)n_entries){
    cerr<<"Triggers::SortTriggers: the TTree index build failed"<<endl;
    return false;
  }
  TTreeIndex *index = (TTreeIndex*)Ttree_write->GetTreeIndex();
  
  // create and fill sorted tree
  TTree *sortedtree = Ttree_write->CloneTree(0);
  for(Long64_t t=0; t<n_entries; t++){
    Ttree_write->GetEntry(index->GetIndex()[t]);
    sortedtree->Fill();
  }
  delete index;
  delete Ttree_write;
  
  // re-address trees
  sortedtree->SetBranchAddress("time",     &Ttime);
  sortedtree->SetBranchAddress("frequency",&Tfreq);
  sortedtree->SetBranchAddress("q",        &Tq);
  sortedtree->SetBranchAddress("snr",      &Tsnr);
  sortedtree->SetBranchAddress("tstart",   &Ttstart);
  sortedtree->SetBranchAddress("tend",     &Ttend);
  sortedtree->SetBranchAddress("fstart",   &Tfstart);
  sortedtree->SetBranchAddress("fend",     &Tfend);
  sortedtree->SetBranchAddress("amplitude",&Tamp);
  sortedtree->SetBranchAddress("phase",    &Tph);
  sortedtree->SetBranchAddress("tstart_us",&Ttstart_us);

  Ttree_write=sortedtree;
  Ttree=Ttree_write;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Triggers::SetTriggerBranchStatus(const string aBname, const bool aStatus){ 
////////////////////////////////////////////////////////////////////////////////////
  UInt_t found;
  Ttree->SetBranchStatus(aBname.c_str(), aStatus, &found);
  return (bool)found;
}

////////////////////////////////////////////////////////////////////////////////////
void Triggers::ResetClusters(void){
////////////////////////////////////////////////////////////////////////////////////
  Ctime.clear();
  Ctstart.clear();
  Ctend.clear(); 
  Cfreq.clear(); 
  Cfstart.clear(); 
  Cfend.clear(); 
  Csnr.clear(); 
  Camp.clear(); 
  Cq.clear(); 
  Cph.clear(); 
  Cfirstentry.clear(); 
  Csize.clear(); 
  Ctag.clear();
  cl_meandur  = 0.0;
  cl_maxdur   = 0.0;
  cl_totdur   = 0.0;
}

////////////////////////////////////////////////////////////////////////////////////
bool Triggers::Clusterize(const int aTag){ 
////////////////////////////////////////////////////////////////////////////////////

  // some printing
  if(Verbose) cout<<"Triggers::Clusterize: cluster "<<Ttree->GetEntries()<<" triggers..."<<endl;
  if(Verbose>1){
    cout<<"\t> delta_t       = "<<clusterize_delta_t<<endl;
    cout<<"\t> size min      = "<<clusterize_sizemin<<endl;
    cout<<"\t> SNR threshold = "<<clusterize_snr_thr<<endl;
  }

  // reset any previous clustering
  ResetClusters();

  // loop over time sorted triggers
  double cl_duration;
  for(Long64_t t=0; t<Ttree->GetEntries(); t++){
    Ttree->GetEntry(t);
    
    // this is the same cluster...
    if((t>0) && (Ttstart-Ctend.back()<=clusterize_delta_t)){
      Csize.back()++; // one more tile

      // update cluster parameters
      if(Ttend > Ctend.back()) Ctend.back()=Ttend;
      if(Ttstart < Ctstart.back()) Ctstart.back()=Ttstart;
      if(Tfend > Cfend.back()) Cfend.back()=Tfend;
      if(Tfstart < Cfstart.back()) Cfstart.back()=Tfstart;
      if(Tsnr>Csnr.back()){
        Csnr.back() = Tsnr;
        Camp.back() = Tamp;
        Cph.back() = Tph;
        Ctime.back() = Ttime;
        Cfreq.back() = Tfreq;
        Cq.back() = Tq;
      }
    }
    //... or start a new cluster
    else{

      // previous cluster is finished
      if(t>0){

        // check cluster selection (SNR and size)
        if((Csnr.back()<clusterize_snr_thr) || (Csize.back()<clusterize_sizemin)){
          Ctend.pop_back();
          Ctstart.pop_back();
          Cfend.pop_back();
          Cfstart.pop_back();
          Csize.pop_back();
          Csnr.pop_back();
          Camp.pop_back();
          Ctime.pop_back();
          Cfreq.pop_back();
          Cq.pop_back();
          Cph.pop_back();
          Cfirstentry.pop_back();
          Ctag.pop_back();
        }

        // cluster statistics
        else{
          cl_duration = Ctend.back()-Ctstart.back();
          cl_totdur+=cl_duration;
          if(cl_duration>cl_maxdur) cl_maxdur=cl_duration;
        }
      }
      
      // initiate cluster
      Ctend.push_back(Ttend);
      Ctstart.push_back(Ttstart);
      Cfend.push_back(Tfend);
      Cfstart.push_back(Tfstart);
      Csize.push_back(1);
      Csnr.push_back(Tsnr);
      Camp.push_back(Tamp);
      Ctime.push_back(Ttime);
      Cfreq.push_back(Tfreq);
      Cq.push_back(Tq);
      Cph.push_back(Tph);
      Cfirstentry.push_back(t);
      Ctag.push_back(aTag);
    }
  }

  // save last cluster, if any
  if(Ctend.size()>0){
    
    // check cluster selection (SNR and size)
    if((Csnr.back()<clusterize_snr_thr) || (Csize.back()<clusterize_sizemin)){
      Ctend.pop_back();
      Ctstart.pop_back();
      Cfend.pop_back();
      Cfstart.pop_back();
      Csize.pop_back();
      Csnr.pop_back();
      Camp.pop_back();
      Ctime.pop_back();
      Cfreq.pop_back();
      Cq.pop_back();
      Cph.pop_back();
      Cfirstentry.pop_back();
      Ctag.pop_back();
    }
    
    // cluster statistics
    else{
      cl_duration = Ctend.back()-Ctstart.back();
      cl_totdur+=cl_duration;
      if(cl_duration>cl_maxdur) cl_maxdur=cl_duration;
    }
  }

  // more statistics
  if(Ctend.size()>0) cl_meandur=cl_totdur/(double)(Ctend.size());

  // number of clusters
  if(Verbose>1) cout<<"Triggers::Clusterize: "<<Ctend.size()<<" clusters were found"<<endl;
  if(Verbose>2){
    cout<<"Triggers::Clusterize: cluster mean duration = "<<cl_meandur<<" s"<<endl;
    cout<<"Triggers::Clusterize: cluster max duration = "<<cl_maxdur<<" s"<<endl;
    cout<<"Triggers::Clusterize: cluster total duration = "<<cl_totdur<<" s"<<endl;
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
int Triggers::GetClusterIndex(const double aTime){ 
////////////////////////////////////////////////////////////////////////////////////

  // loop over clusters
  for(unsigned int c=0; c<GetClusterN(); c++){
    if(aTime<Ctstart[c]) break;
    if(aTime>=Ctend[c]) continue;
    
    return c;
  }
  return -1; // not found
}

////////////////////////////////////////////////////////////////////////////////////
bool Triggers::ReadTriggerFiles(const string aPattern, const string aDirectory){ 
////////////////////////////////////////////////////////////////////////////////////
  
  // reset triggers
  Reset();

  // switch to read-mode
  readmode = true;

  if(Verbose) cout<<"Triggers::ReadTriggerFiles: loading triggers in \""<<aPattern<<"\""<<endl;

  // trigger tree
  string trigger_tree = "triggers";
  if(aDirectory.compare("")) trigger_tree=aDirectory+"/"+trigger_tree;
  Ttree_read = new TChain(trigger_tree.c_str());

  // get list of patterns and load it
  vector <string> patlist = SplitString(aPattern,' ');
  for(unsigned int p=0; p<patlist.size(); p++) Ttree_read->Add(patlist[p].c_str());
  Ttree_read->SetName(("triggers_"+srandid).c_str());
  patlist.clear();
  
  // get tree offsets
  Toffsets=Ttree_read->GetTreeOffset();

  // load first tree if any
  if(Ttree_read->GetNtrees()>0) Ttree_read->LoadTree(0);
  
  // attach branches
  if(Ttree_read->SetBranchAddress("time", &Ttime)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'time' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("frequency", &Tfreq)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'frequency' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("q", &Tq)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'q' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("snr", &Tsnr)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'snr' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("tstart", &Ttstart)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'tstart' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("tend", &Ttend)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'tend' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("fstart", &Tfstart)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'fstart' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("fend", &Tfend)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'fend' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("amplitude", &Tamp)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'amplitude' branch"<<endl;
    Reset();
    return false;
  }
  if(Ttree_read->SetBranchAddress("phase", &Tph)!=0){
    cerr<<"Triggers::ReadTriggerFiles: missing or corrupted 'phase' branch"<<endl;
    Reset();
    return false;
  }

  Ttree=Ttree_read;

  return true;
}

