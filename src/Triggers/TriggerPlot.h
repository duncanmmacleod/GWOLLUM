/**
 * @file 
 * @brief Plot triggers.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __TriggerPlot__
#define __TriggerPlot__

#include "GwollumPlot.h"
#include "ReadTriggers.h"
#include "TriggerSelect.h"
#include "Date.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"

using namespace std;

/**
 * Plot triggers.
 * This class was designed to produce and display a 'collection' of plots representing a set of triggers. A collection is composed of several plot types:
 * - "rate": trigger rate as a function of time
 * - "frequency": frequency distribution
 * - "snr": SNR distribution
 * - "freqtime": frequency versus time
 * - "snrtime": SNR vs time
 * - "snrfreq": SNR vs frequency
 *
 * A collection is defined by a 'selection' which is a set of criteria used to select the triggers to be plotted. This way, several collections can be defined and plotted in the same time.
 * \author Florent Robinet
 */
class TriggerPlot: public GwollumPlot, public ReadTriggers {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the TriggerPlot class.
   * @details A set of plot collections is initialized.
   * @param[in] aNcoll Number of collections.
   * @param[in] aPattern Input trigger file pattern.
   * @param[in] aDirectory Trigger ROOT directory.
   * @param[in] aStyleName GWOLLUM style name.
   * @param[in] aVerbose Verbosity level.
   */
  TriggerPlot(const unsigned int aNcoll, const string aPattern, const string aDirectory="",
              const string aStyleName="GWOLLUM", const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the TriggerPlot class.
   */
  virtual ~TriggerPlot(void);
  /**
     @}
  */

  /**
   * @brief Builds collection plots.
   * @details Once the selection is ready, use this function to build the plots for a given selection.
   * @param[in] aCollIndex Collection index. Use a negative value to make all collections.
   * @param[in] aInSeg Input segments (optional). This object is used to make selecton time boxes. It is not used to select triggers!!!
   */
  void MakeCollection(const int aCollIndex=-1, Segments *aInSeg=NULL);

  /**
   * @brief Prints an external object.
   * @param[in] aObj Object to plot.
   */
  inline void PrintPlot(TObject *aObj){
    GwollumPlot::ResetLegend(); GwollumPlot::Wpad->Clear(); GwollumPlot::Draw(aObj);
  };

  /**
   * @brief Prints a given plot type from a given collection.
   * @param[in] aPlotType Plot type: "freqtime", "snrtime", "snrfreq", "rate", "frequency", or "snr".
   * @param[in] aCollIndex Collection index.
   * @param[in] aFirst If set to false, the plot is printed over the existing canvas.
   */
  inline void PrintPlot(const string aPlotType,
                        const unsigned int aCollIndex,
                        const bool aFirst=true){
    vector <unsigned int> ind(1, aCollIndex);
    return PrintPlot(aPlotType, ind, aFirst);
  };
  
  /**
   * @brief Prints a given plot type from all collections.
   * @param[in] aPlotType Plot type: "freqtime", "snrtime", "snrfreq", "rate", "frequency", or "snr".
   */
  inline void PrintPlot(const string aPlotType){
    vector <unsigned int> ind;
    for(unsigned int i=0; i<Ncoll; i++) ind.push_back(i);
    return PrintPlot(aPlotType, ind, true);
    
  };
  
  /**
   * @brief Prints a given plot type from a few collections.
   * If aFirst is set to false, the plots are printed on the existing print.
   * @param[in] aPlotType Plot type: "freqtime", "snrtime", "snrfreq", "rate", "frequency", or "snr".
   * @param[in] aCollIndex Collection indices.
   * @param[in] aFirst If set to false, the plots are printed over the existing canvas. 
   */
  void PrintPlot(const string aPlotType,
                 const vector<unsigned int> aCollIndex,
                 const bool aFirst);

  /**
   * @brief Returns the selection of a given collection.
   * @param[in] aCollIndex Collection index.
   * @pre The collection index must be valid.
   */
  inline TriggerSelect* GetCollectionSelection(const unsigned int aCollIndex){
    return Collection[aCollIndex];
  };

  /**
   * @brief Sets selection segments for all collections.
   * @details Collection triggers/clusters can be selected using a segment list.
   * If the trigger/cluster peak time is not in the selection segment list, it is rejected.
   * Specific blue strips are plotted to indicate the selection segments.
   *
   * When calling this function with a pointer to NULL or an invalid segment list, no selection is performed.
   *
   * @warning The Segments object is NOT copied, only the pointer is.
   * As a result, do not modify the segment list while using this class.
   *
   * @param[in] SelectionSegments Pointer to a valid segment list.
   */
  void SetSelectionSegments(Segments *SelectionSegments);

  /**
   * @brief Sets a legend to a collection.
   * @param[in] aCollIndex Collection index.
   * @param[in] aLegend Legend label.
   * @pre The collection index must be valid.
   */
  inline void SetCollectionLegend(const unsigned int aCollIndex,
                                  const string aLegend){
    legend[aCollIndex]=aLegend;
  };

  /**
   * @brief Assigns a color to a collection.
   * @param[in] aCollIndex Collection index.
   * @param[in] aColorIndex Color index (ROOT conventions).
   * @pre The collection index must be valid.
   */
  inline void SetCollectionColor(const unsigned int aCollIndex,
                                 const unsigned int aColorIndex){
    Hfreqtime[aCollIndex]->SetMarkerColor(aColorIndex);
  };

  /**
   * @brief Assigns a marker style to a collection.
   * @param[in] aCollIndex Collection index.
   * @param[in] aMarkerStyleIndex Marker style index (ROOT conventions).
   * @param[in] aMarkerSize Marker size (ROOT conventions).
   * @pre The collection index must be valid.
   */
  inline void SetCollectionMarker(const unsigned int aCollIndex,
                                  const unsigned int aMarkerStyleIndex,
                                  const double aMarkerSize=1.0){
    Hfreqtime[aCollIndex]->SetMarkerStyle(aMarkerStyleIndex);
    Hfreqtime[aCollIndex]->SetMarkerSize(aMarkerSize);
  };

  /**
   * @brief Returns the number of triggers/clusters in a given collection.
   * @param[in] aCollIndex Collection index.
   * @pre The collection index must be valid.
   */
  inline unsigned int GetCollectionN(const unsigned int aCollIndex){
    return (unsigned int)Gfreqtime[aCollIndex]->GetN();
  };

  /**
   * @brief Returns a TH1D copy from a given collection.
   * @param[in] aPlotType Plot type: "rate", "frequency", or "snr".
   * @param[in] aCollIndex Collection index.
   * @pre The collection index must be valid.
   */
  TH1D* GetTH1D(const string aPlotType, const unsigned int aCollIndex);

  /**
   * @brief Returns the number of collections.
   */
  inline unsigned int GetN(void){ return Ncoll; };
  
  /**
   * @brief Returns the Y max of the plot in the canvas.
   * @param[in] aPlotType Plot type: "freqtime", "snrtime", "snrfreq", "rate", "frequency", or "snr".
   * @param[in] aCollIndex Collection index.
   * @pre The collection index must be valid.
   */
  inline double GetYmax(const string aPlotType, const int aCollIndex){
    if(!aPlotType.compare("snrtime"))
      return Hsnrtime[aCollIndex]->GetYaxis()->GetXmax();
    else if(!aPlotType.compare("snrfreq"))
      return Hsnrfreq[aCollIndex]->GetYaxis()->GetXmax();
    else if(!aPlotType.compare("rate"))
      return Htime[aCollIndex]->GetYaxis()->GetXmax();
    else if(!aPlotType.compare("frequency"))
      return Hfreq[aCollIndex]->GetYaxis()->GetXmax();
    else if(!aPlotType.compare("snr"))
      return Hsnr[aCollIndex]->GetYaxis()->GetXmax();
    else
      return Hfreqtime[aCollIndex]->GetYaxis()->GetXmax();
  };
  
  /**
   * @brief Returns the Y min of the plot in the canvas.
   * @param[in] aPlotType Plot type: "freqtime", "snrtime", "snrfreq", "rate", "frequency", or "snr".
   * @param[in] aCollIndex Collection index.
   * @pre The collection index must be valid.
   */
  inline double GetYmin(const string aPlotType, const int aCollIndex){
    if(!aPlotType.compare("snrtime"))
      return Hsnrtime[aCollIndex]->GetYaxis()->GetXmin();
    else if(!aPlotType.compare("snrfreq"))
      return Hsnrfreq[aCollIndex]->GetYaxis()->GetXmin();
    else if(!aPlotType.compare("rate"))
      return Htime[aCollIndex]->GetYaxis()->GetXmin();
    else if(!aPlotType.compare("frequency"))
      return Hfreq[aCollIndex]->GetYaxis()->GetXmin();
    else if(!aPlotType.compare("snr"))
      return Hsnr[aCollIndex]->GetYaxis()->GetXmin();
    else
      return Hfreqtime[aCollIndex]->GetYaxis()->GetXmin();
  };
  
  /**
   * @brief Plots the loudest event with a star marker.
   * @param[in] aFlag Set to true to plot a star.
   */
  inline void PlotLoudestEvent(const bool aFlag=true){ plotstar=aFlag; };
  

 private:
  
  // PLOTS
  unsigned int Ncoll;        ///< Number of plot collections.
  TH2D   **Hfreqtime;        ///< Freq vs. time container.
  TGraph **Gfreqtime;        ///< Freq vs. time.
  TGraph *Gfreqtimeloud;     ///< Freq vs. time (loudest event).
  TGraph *Gfreqtimeloud2;    ///< Freq vs. time (loudest event).
  TH2D   **Hsnrtime;         ///< SNR vs. time container.
  TGraph **Gsnrtime;         ///< SNR vs. time.
  TH2D   **Hsnrfreq;         ///< SNR vs. freq container.
  TGraph **Gsnrfreq;         ///< SNR vs. freq.
  TH1D   **Htime;            ///< Time (rate).
  TH1D   **Hfreq;            ///< Frequency.
  TH1D   **Hsnr;             ///< SNR.
 
   // STYLE
  string *legend;            ///< Collection legend.
  bool plotstar;             ///< Flag to plot the loudest event.

  // SELECTION
  TriggerSelect **Collection;///< Collection selection.
  Segments *selseg;          ///< Selection segments for all collections.
  
  // BOX
  TPave *SegBox;             ///< Time box.
  TPave **OnBox;             ///< ON segments.
  TPave **SelBox;            ///< Selected segments.
  unsigned int nonbox;       ///< Number of on-boxes.
  unsigned int nselbox;      ///< Number of selected-boxes.

  /**
   * @brief Resets a collection.
   * @details All the plots in a collection are reset.
   * If the style has changed, the new style is applied.
   * @param[in] aCollIndex Collection index.
   */
  void ResetCollection(const unsigned int aCollIndex);

  /**
   * @brief Resets collection parameters to default values.
   * @details
   * - Reset trigger selection (TriggerSelect::ResetSelection()).
   * - Plot range: use the trigger min/max.
   * - Use GPS to measure time
   * - Use triggers
   * 
   * @param[in] aCollIndex Collection index.
   */
  void ResetParameters(const unsigned int aCollIndex);


  ClassDef(TriggerPlot,0)  
};

#endif


