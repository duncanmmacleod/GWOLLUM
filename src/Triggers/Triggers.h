/**
 * @file 
 * @brief Manage a trigger set.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Triggers__
#define __Triggers__

#include <TROOT.h>
#include <TTreeIndex.h>
#include "TChain.h"
#include "Segments.h"

using namespace std;

/**
 * @brief Manage a trigger set.
 * @details This class is designed to manage a set of triggers.
 * A trigger is described as a set of parameters and these parameters are stored in a ROOT TTree.
 *
 * The trigger set can be defined according to two modes and some methods are mode-specific:
 * - the 'read-mode': the trigger set is loaded from ROOT files
 * - the 'write-mode': the trigger set is built from scratch and triggers are saved in memory.
 *
 * This class also offers the possibility to cluster the set of triggers with an algorithm called Clusterize. For the description of the clustering algorithms, see Clusterize().
 * @author Florent Robinet
 */
class Triggers {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Triggers class.
   * @details The trigger and cluster structures are initialized.
   * By default, the write-mode is activated.
   * To Read triggers, call ReadTriggerFiles().
   *
   * The clustering (see Clusterize()) parameters are given default values:
   * - Clustering time window  = 0.1 s (SetClusterizeDt())
   * - Clustering SNR threshold = 0.0 (SetClusterizeSnrThr())
   * - Cluster minimum size = 0.0 (SetClusterizeSizeMin())
   *
   * @param[in] aVerbose Verbosity level.
   */
  Triggers(const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the Triggers class.
   */
  virtual ~Triggers(void);
  /**
     @}
  */

  /**
   * @brief Resets the Triggers object.
   * @details All triggers and clusters are flushed out.
   * The object is back to write mode.
   */
  void Reset(void);
  
  /**
   * @brief Adds a trigger to the Triggers object (only available in write-mode).
   * @note Time parameters are saved with a resolution of 1 us.
   * @param[in] aTime Trigger GPS time [s].
   * @param[in] aFrequency Trigger frequency [Hz].
   * @param[in] aSNR Trigger Signal-to-noise ratio.
   * @param[in] aQ Trigger Quality factor.
   * @param[in] aTstart Trigger GPS start time [s].
   * @param[in] aTend Trigger GPS end time [s].
   * @param[in] aFstart Trigger frequency start [Hz].
   * @param[in] aFend Trigger frequency end [Hz].
   * @param[in] aAmplitude Trigger amplitude.
   * @param[in] aPhase Trigger phase [rad].
   */
  bool AddTrigger(const double aTime, const double aFrequency,
		  const double aSNR, const double aQ,
		  const double aTstart, const double aTend,
		  const double aFstart, const double aFend,
		  const double aAmplitude, const double aPhase);

  /**
   * @brief Sort triggers by increasing values of tstart (only available in write-mode).
   * @brief The TTree::BuildIndex() is used with "tstart" as a major index and "tstart_us" as a minor index.
   * @warning this function only work if the number of tree entries is smaller than INT_MAX.
   * @returns true is the sorting is successful, false otherwise.
   */
  bool SortTriggers(void);

  /**
   * @brief Sets a new clustering \f$\delta t\f$ parameter.
   * @sa Clusterize()
   * @param[in] aDt New \f$\delta t\f$ value [s].
   */
  inline void SetClusterizeDt(const double aDt){ clusterize_delta_t=aDt; };

  /**
   * @brief Gets the clustering \f$\delta t\f$ parameter.
   * @sa Clusterize()
   */
  inline double GetClusterizeDt(void){ return clusterize_delta_t; };

  /**
   * @brief Sets the cluster minimal size: \f$M_{min}\f$.
   * @sa Clusterize()
   * @param[in] aSizeMin New cluster minimal size: \f$M_{min}\f$.
   */
  inline void SetClusterizeSizeMin(const unsigned int aSizeMin){ clusterize_sizemin=aSizeMin; };

  /**
   * @brief Gets the cluster minimal size: \f$M_{min}\f$.
   * @sa Clusterize()
   */
  inline unsigned int GetClusterizeSizeMin(void){ return clusterize_sizemin; };

  /**
   * @brief Sets the SNR threshold for clustering: \f$\rho_{min}\f$. 
   * @sa Clusterize()
   * @param[in] aSnrThr SNr threshold: \f$\rho_{min}\f$.
   */
  inline void SetClusterizeSnrThr(const double aSnrThr){ clusterize_snr_thr=aSnrThr; };

  /**
   * @brief Gets the SNR threshold for clustering. 
   * @sa Clusterize()
   */
  inline double GetClusterizeSnrThr(void){ return clusterize_snr_thr; };

  /**
   * @brief Sets a new cluster tag value.
   * @sa Clusterize()
   * @param[in] aClusterIndex Cluster index.
   * @param[in] aTag New cluster tag value.
   * @pre The cluster index must be valid (in-range).
   */
  inline void SetClusterTag(const unsigned int aClusterIndex, const int aTag){ Ctag[aClusterIndex]=aTag; };

  /**
   * @brief Sets a new tag value for all clusters.
   * @param aTag New cluster tag value.
   */
  inline void SetClusterTag(const int aTag){
    for(unsigned int c=0; c<Ctstart.size(); c++) Ctag[c]=aTag;
  };
  
  /**
   * @brief Modifies the branch status of the trigger tree.
   * @details This function allows to change the status of the trigger tree branches.
   * This function is particularly useful to speed up processes looping over triggers.
   * @sa <a href="http://root.cern.ch/root/htmldoc/TTree.html#TTree:SetBranchStatus">TTree::SetBranchStatus( )</a>.
   * @warning The user MUST re-activate the branch before using any other class functions.
   * @param[in] aBname Branch name.
   * @param[in] aStatus New branch status.
   */
  bool SetTriggerBranchStatus(const string aBname, const bool aStatus=true);

  /**
   * @brief Resets the list of clusters.
   * @details All clusters in memory are deleted.
   */
  void ResetClusters(void);
  
  /**
   * @brief Clusters the triggers.
   * @details Triggers are clustered with an algorithm called "clusterize".
   * @warning The triggers are assumed to be sorted by starting time ("tstart"). If this is not the case, call SortTriggers() first.
   *
   * The clusterize algorithm considers 2 consecutive triggers.
   * If the time distance between the end of the first trigger and the start of the second trigger is smaller than (or equal to) \f$\delta t\f$, the 2 triggers are clustered together.
   * This also true if the first trigger ends after the start of the second trigger.
   * This operation is iterated over the list of triggers.
   * 
   * A cluster is given parameters:
   * - A size given by the number of triggers in the cluster.
   * - A start time associated to the smallest trigger start time.
   * - An end time associated to the largest trigger end time.
   * - A start frequency associated to the smallest trigger start frequency.
   * - An end frequency associated to the largest trigger end frequency.
   * - A time corresponding to the trigger with the largest SNR in the cluster.
   * - A frequency corresponding to the trigger with the largest SNR in the cluster.
   * - A SNR corresponding to the trigger with the largest SNR in the cluster.
   * - A quality factor corresponding to the trigger with the largest SNR in the cluster.
   * - An amplitude corresponding to the trigger with the largest SNR in the cluster.
   * - An phase corresponding to the trigger with the largest SNR in the cluster.
   *
   * A cluster is saved only if all the following conditions are met:
   * - The cluster size must be larger than (or equal to) \f$M_{min}\f$.
   * - The cluster SNR must be larger than (or equal to) \f$\rho_{min}\f$.
   *
   * The clustering parameters \f$\delta t\f$, \f$M_{min}\f$, and \f$\rho_{min}\f$ are set with SetClusterizeDt(),SetClusterizeSizeMin(), and SetClusterizeSnrThr() respectively.
   *
   * Resulting clusters are tagged. By default, this tag is set 1 for all clusters.
   *
   * @note If the triggers were previously clustered, the memory is cleaned first (see ResetClusters()).
   * @param[in] aTag Default tag assigned to all clusters.
   */
  bool Clusterize(const int aTag=1);

  /**
   * @brief Returns the file path containing a given trigger (read-mode).
   * @warning This function can only be called in read-mode.
   * @returns "" if the trigger cannot be found.
   * @param[in] aTriggerIndex Trigger index.
   */
  string GetTriggerFile(Long64_t aTriggerIndex);

  /**
   * @brief Returns the current number of triggers in memory.
   * @returns -1 in case of errors.
   */
  inline Long64_t GetTriggerN(void){ return Ttree->GetEntries(); };

  /**
   * @brief Returns the time of a given trigger [s].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerTime(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Ttime;
    return 0.0;
  };

  /**
   * @brief Returns the frequency of a given trigger [Hz].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerFrequency(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tfreq;
    return 0.0;
  };

  /**
   * @brief Returns the quality factor of a given trigger.
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerQ(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tq;
    return 0.0;
  };

  /**
   * @brief Returns the signal-to-noise ratio of a given trigger.
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerSnr(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tsnr;
    return 0.0;
  };

  /**
   * @brief Returns the start time of a given trigger [s].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerTimeStart(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Ttstart;
    return 0.0;
  };

  /**
   * @brief Returns the end time of a given trigger [s].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerTimeEnd(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Ttend;
    return 0.0;
  };

  /**
   * @brief Returns the start frequency of a given trigger [Hz].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerFrequencyStart(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tfstart;
    return 0.0;
  };

  /**
   * @brief Returns the end frequency of a given trigger [Hz].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerFrequencyEnd(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tfend;
    return 0.0;
  };

  /**
   * @brief Returns the amplitude of a given trigger.
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerAmplitude(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tamp;
    return 0.0;
  };

  /**
   * @brief Returns the phase of a given trigger [rad].
   * @returns -99.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerPhase(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tph;
    return -99.0;
  };

   /**
   * @brief Returns the duration of a given trigger [s].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerDuration(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Ttend-Ttstart;
    return 0.0;
  };

   /**
   * @brief Returns the bandwidth of a given trigger [Hz].
   * @returns 0.0 is returned if this function fails.
   * @param[in] aTriggerIndex Trigger index.
   */
  inline double GetTriggerBandwidth(const Long64_t aTriggerIndex){
    if(Ttree->GetEntry(aTriggerIndex)>0) return Tfend-Tfstart;
    return 0.0;
  };

  /**
   * @brief Returns the current number of clusters in memory.
   */
  inline unsigned int GetClusterN(void){ return Ctstart.size(); };

  /**
   * @brief Returns the time of a given cluster [s].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterTime(const unsigned int aClusterIndex){
    return Ctime[aClusterIndex];
  };

  /**
   * @brief Returns the frequency of a given cluster [Hz].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterFrequency(const unsigned int aClusterIndex){
    return Cfreq[aClusterIndex];
  };

  /**
   * @brief Returns the quality factor of a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterQ(const unsigned int aClusterIndex){
    return Cq[aClusterIndex];
  };

  /**
   * @brief Returns the signal-to-noise ratio of a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterSnr(const unsigned int aClusterIndex){
    return Csnr[aClusterIndex];
  };

  /**
   * @brief Returns the start time of a given cluster [s].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterTimeStart(const unsigned int aClusterIndex){
    return Ctstart[aClusterIndex];
  };

  /**
   * @brief Returns the end time of a given cluster [s].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterTimeEnd(const unsigned int aClusterIndex){
    return Ctend[aClusterIndex];
  };

  /**
   * @brief Returns the start frequency of a given cluster [Hz].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterFrequencyStart(const unsigned int aClusterIndex){
    return Cfstart[aClusterIndex];
  };

  /**
   * @brief Returns the end frequency of a given cluster [Hz].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterFrequencyEnd(const unsigned int aClusterIndex){
    return Cfend[aClusterIndex];
  };

  /**
   * @brief Returns the amplitude of a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterAmplitude(const unsigned int aClusterIndex){
    return Camp[aClusterIndex];
  };

  /**
   * @brief Returns the phase of a given cluster [rad].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterPhase(const unsigned int aClusterIndex){
    return Cph[aClusterIndex];
  };

   /**
   * @brief Returns the duration of a given cluster [s].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterDuration(const unsigned int aClusterIndex){
    return Ctend[aClusterIndex]-Ctstart[aClusterIndex];
  };

   /**
   * @brief Returns the bandwidth of a given cluster [Hz].
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline double GetClusterBandwidth(const unsigned int aClusterIndex){
    return Cfend[aClusterIndex]-Cfstart[aClusterIndex];
  };

   /**
   * @brief Returns the size (number of triggers) a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline unsigned int GetClusterSize(const unsigned int aClusterIndex){
    return Csize[aClusterIndex];
  };

   /**
   * @brief Returns the tag of a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline int GetClusterTag(const unsigned int aClusterIndex){
    return Ctag[aClusterIndex];
  };

   /**
   * @brief Returns the index of the first trigger in a given cluster.
   * @param[in] aClusterIndex Cluster index.
   * @pre The cluster index should be valid (in-range).
   */
  inline Long64_t GetClusterFirstTrigger(const unsigned int aClusterIndex){
    return Cfirstentry[aClusterIndex];
  };

  /**
   * @brief Returns the mean duration of clusters [s].
   */
  inline double GetClusterMeanDuration(void){ return cl_meandur; };
  
  /**
   * @brief Returns the maximal duration of clusters [s].
   */
  inline double GetClusterMaxDuration(void){ return cl_maxdur; };
  
  /**
   * @brief Returns the total duration of clusters [s].
   */
  inline double GetClusterTotalDuration(void){ return cl_totdur; };

  /**
   * @brief Returns the index of cluster overlapping a given GPS time.
   * @returns -1 is returned if no cluster is found.
   * @param[in] aTime GPS time to test.
   */
  int GetClusterIndex(const double aTime);

  /**
   * @brief Reads a set of trigger files.
   * @details When this function is called, the Triggers object is switched to a read-mode.
   * The trigger tree points to the list of triggers saved in the input files.
   * Some (write-mode) functions are therefore de-activated.
   * The read-mode can be removed with the ResetTriggers() function.
   * @param[in] aPattern Trigger file pattern.
   * @param[in] aDirectory ROOT directory where to find the 'triggers' tree.
   * @returns true if a trigger tree was found in all input ROOT files, false otherwise.
   */
  bool ReadTriggerFiles(const string aPattern, const string aDirectory="");

 protected:

  // GENERAL
  int Verbose;         ///< verbosity level
  int randid;          ///< Random integer to identify the class object.
  string srandid;      ///< Random integer (string) to identify the class object.

  // TRIGGERS
  TTree *Ttree;        ///< Pointer to active Ttree.
  TChain *Ttree_read;  ///< Trigger tree read-mode.
  TTree *Ttree_write;  ///< Trigger tree write-mode.
  double Ttime;        ///< Trigger GPS time.
  double Tfreq;        ///< Trigger frequency [Hz].
  double Tq;           ///< Trigger Q.
  double Tsnr;         ///< Trigger SNR.
  double Ttstart;      ///< Trigger GPS starting time.
  double Ttend;        ///< Trigger GPS ending time.
  double Tfstart;      ///< Trigger starting frequency [Hz].
  double Tfend;        ///< Trigger ending frequency [Hz].
  double Tamp;         ///< Trigger amplitude.
  double Tph;          ///< Trigger phase [rad].
  int Ttstart_us;      ///< Trigger starting time (us).
  Long64_t *Toffsets;  ///< Tree offsets in the chain (read-mode) - DO NOT DELETE

 private:

  // CLUSTERS
  vector<double> Ctime;        ///< Cluster GPS time.
  vector<double> Ctstart;      ///< Cluster GPS time start.
  vector<double> Ctend;        ///< Cluster GPS time end.
  vector<double> Cfreq;        ///< Cluster frequency.
  vector<double> Cfstart;      ///< Cluster frequency start.
  vector<double> Cfend;        ///< Cluster frequency end.
  vector<double> Csnr;         ///< Cluster SNR.
  vector<double> Camp;         ///< Cluster Amplitude.
  vector<double> Cq;           ///< Cluster Q.
  vector<double> Cph;          ///< Cluster phase.
  vector<Long64_t> Cfirstentry;///< Cluster first trigger entry.
  vector<unsigned int> Csize;  ///< Cluster size.
  vector <int> Ctag;           ///< Cluster tag.
  double cl_meandur;           ///< Cluster mean duration.
  double cl_maxdur;            ///< Cluster max duration.
  double cl_totdur;            ///< Cluster total duration.
  
  // GENERAL
  bool readmode;           ///< Read/write mode: true=read, false=write.

  // CLUSTERIZE
  double clusterize_delta_t;      ///< Time clustering parameter \f$\delta t\f$ [s].
  unsigned int clusterize_sizemin;///< Minimum cluster size \f$M_{min}\f$.
  double clusterize_snr_thr;      ///< Minimum cluster SNR \f$\rho_{min}\f$.

  ClassDef(Triggers,0)  
};

#endif


