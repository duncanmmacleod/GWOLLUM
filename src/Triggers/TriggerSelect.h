/**
 * @file 
 * @brief Select trigger parameters.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __TriggerSelect__
#define __TriggerSelect__

#include "ReadTriggers.h"
#include <TH1.h>
#include <TH2.h>

using namespace std;

/**
 * @brief Select trigger parameters.
 * @details This class is designed to operate a selection over trigger parameters: time, frequency, Q and SNR.
 * For each parameter, the user can select a minimum, a maximum and a resolution.
 * The resolution is the number of bins to cover the parameter range.
 * The time is linearly binned while the other parameters are logarithmically binned.
 * Once a selection is defined, pre-binned histograms can be extracted.
 * @author Florent Robinet
 */
class TriggerSelect{
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the TriggerSelect class. 
   * @details Single-bin selections.
   * @param[in] aName Selection name.
   * @param[in] aVerbose Verbosity level.
   */
  TriggerSelect(const string aName, const unsigned int aVerbose=0);

  /**
   * @brief Constructor of the TriggerSelect class.
   * @details The default selection is defined using the metadata (min/max) of the input ReadTriggers object.
   * For each parameter the resolution is set to 1 bin.
   * The selection is given the input Streams name.
   * @param[in] aTriggers Input triggers.
   * @param[in] aVerbose Verbosity level.
   */
  TriggerSelect(ReadTriggers *aTriggers, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the TriggerSelect class.
   */
  virtual ~TriggerSelect(void);
  /**
     @}
  */

  /**
   * @brief Resets the selection back to default.
   */
  void ResetSelection(void);

  /**
   * @brief Use a date format for time axes.
   */
  inline void UseDateTime(void){ usedate = true; };
  
  /**
   * @brief Use a GPS format for time axes.
   */
  inline void UseGpsTime(void){ usedate = false; };
  
  /**
   * @brief Use cluster tags.
   * @warning Only positive tags can be considered.
   * @param[in] aTag Cluster tag.
   */
  inline void UseClusterTag(unsigned int aTag){ clustertag=(int)aTag; };
  
  /**
   * @brief Use triggers.
   */
  inline void UseTriggers(void){ clustertag=-1; };
  
  /**
   * @brief Returns the cluster tag used for the selection.
   * @returns -1 if triggers are used.
   */
  inline int GetClusterTag(void){ return clustertag; };

  /**
   * @brief Returns the time stamp for this selection.
   * @details The minimum time for this selection is returned.
   * @returns A date is returned if UseDateTime() was called, a GPS time is returned otherwise.
   */
  inline string GetTimeStamp(void){ return sstart; };

  /**
   * @brief Sets the time selection.
   * @param[in] aResolution Resolution (number of bins).
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  void SetTimeSelection(const unsigned int aResolution,
                        const double aMin, const double aMax);

  /**
   * @brief Sets the time resolution.
   * @param[in] aResolution Resolution (number of bins).
   */
  inline void SetTimeResolution(const unsigned int aResolution){
    SetTimeSelection(aResolution, tmin, tmax);
  };

  /**
   * @brief Sets the minimal time value.
   * @param[in] aMin Minimal value.
   */
  inline void SetTimeMin(const double aMin){
    SetTimeSelection(ntbins, aMin, tmax);
  };

  /**
   * @brief Sets the maximal time value.
   * @param[in] aMax Maximal value.
   */
  inline void SetTimeMax(const double aMax){
    SetTimeSelection(ntbins, tmin, aMax);
  };

  /**
   * @brief Sets the time range.
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  inline void SetTimeRange(const double aMin, const double aMax){
    SetTimeSelection(ntbins, aMin, aMax);
  };

  /**
   * @brief Sets the frequency selection.
   * @param[in] aResolution Resolution (number of bins).
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  void SetFrequencySelection(const unsigned int aResolution,
                             const double aMin, const double aMax);

  /**
   * @brief Sets the frequency resolution.
   * @param[in] aResolution Resolution (number of bins).
   */
  inline void SetFrequencyResolution(const unsigned int aResolution){
    SetFrequencySelection(aResolution, fmin, fmax);
  };

  /**
   * @brief Sets the minimal frequency value.
   * @param[in] aMin Minimal value.
   */
  inline void SetFrequencyMin(const double aMin){
    SetFrequencySelection(nfbins, aMin, fmax);
  };

  /**
   * @brief Sets the maximal frequency value.
   * @param[in] aMax Maximal value.
   */
  inline void SetFrequencyMax(const double aMax){
    SetFrequencySelection(nfbins, fmin, aMax);
  };

  /**
   * @brief Sets the frequency range.
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  inline void SetFrequencyRange(const double aMin, const double aMax){
    SetFrequencySelection(nfbins, aMin, aMax);
  };

  /**
   * @brief Sets the Q selection.
   * @param[in] aResolution Resolution (number of bins).
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  void SetQSelection(const unsigned int aResolution,
                     const double aMin, const double aMax);

  /**
   * @brief Sets the Q resolution.
   * @param[in] aResolution Resolution (number of bins).
   */
  inline void SetQResolution(const unsigned int aResolution){
    SetQSelection(aResolution, qmin, qmax);
  };

  /**
   * @brief Sets the minimal Q value.
   * @param[in] aMin Minimal value.
   */
  inline void SetQMin(const double aMin){
    SetQSelection(nqbins, aMin, qmax);
  };

  /**
   * @brief Sets the maximal Q value.
   * @param[in] aMax Maximal value.
   */
  inline void SetQMax(const double aMax){
    SetQSelection(nqbins, qmin, aMax);
  };

  /**
   * @brief Sets the Q range.
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  inline void SetQRange(const double aMin, const double aMax){
    SetQSelection(nqbins, aMin, aMax);
  };

  /**
   * @brief Sets the SNR selection.
   * @param[in] aResolution Resolution (number of bins).
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  void SetSnrSelection(const unsigned int aResolution,
                       const double aMin, const double aMax);

  /**
   * @brief Sets the SNR resolution.
   * @param[in] aResolution Resolution (number of bins).
   */
  inline void SetSnrResolution(const unsigned int aResolution){
    SetSnrSelection(aResolution, smin, smax);
  };

  /**
   * @brief Sets the minimal SNR value.
   * @param[in] aMin Minimal value.
   */
  inline void SetSnrMin(const double aMin){
    SetSnrSelection(nsbins, aMin, smax);
  };

  /**
   * @brief Sets the maximal SNR value.
   * @param[in] aMax Maximal value.
   */
  inline void SetSnrMax(const double aMax){
    SetSnrSelection(nsbins, smin, aMax);
  };

  /**
   * @brief Sets the SNR range.
   * @param[in] aMin Minimal value.
   * @param[in] aMax Maximal value.
   */
  inline void SetSnrRange(const double aMin, const double aMax){
    SetSnrSelection(nsbins, aMin, aMax);
  };

  /**
   * @brief Returns the minimal time value.
   */
  inline double GetTimeMin(void){ return tmin; };

  /**
   * @brief  Returns the maximal time value.
   */
  inline double GetTimeMax(void){ return tmax; };

  /**
   * @brief  Returns the time range value.
   */
  inline double GetTimeRange(void){ return tmax-tmin; };

  /**
   * @brief  Returns the time resolution value.
   */
  inline unsigned int GetTimeResolution(void){ return ntbins; };

  /**
   * @brief Returns the minimal frequency value.
   */
  inline double GetFrequencyMin(void){ return fmin; };

  /**
   * @brief Returns the maximal frequency value.
   */
  inline double GetFrequencyMax(void){ return fmax; };
 
  /**
   * @brief Returns the frequency range value.
   */
  inline double GetFrequencyRange(void){ return fmax-fmin; };

  /**
   * @brief Returns the frequency resolution value.
   */
  inline unsigned int GetFrequencyResolution(void){ return nfbins; };

  /**
   * @brief Returns the minimal Q value.
   */
  inline double GetQMin(void){ return qmin; };

  /**
   * @brief Returns the maximal Q value.
   */
  inline double GetQMax(void){ return qmax; };

  /**
   * @brief Returns the Q range value.
   */
  inline double GetQRange(void){ return qmax-qmin; };

  /**
   * @brief Returns the Q resolution value.
   */
  inline unsigned int GetQResolution(void){ return nqbins; };

  /**
   * @brief Returns the minimal SNR value.
   */
  inline double GetSnrMin(void){ return smin; };

  /**
   * @brief Returns the maximal SNR value.
   */
  inline double GetSnrMax(void){ return smax; };

  /**
   * @brief Returns the SNR range value.
   */
  inline double GetSnrRange(void){ return smax-smin; };

  /**
   * @brief Returns the SNR resolution value.
   */
  inline unsigned int GetSnrResolution(void){ return nsbins; };

  /**
   * @brief Returns an empty 1-D histogram binned in a given parameter.
   * @details
   * - \"Time\",
   * - \"Frequency\",
   * - \"Q\",
   * - \"SNR\".
   *
   * @note The user must delete the returned histogram.
   * @param[in] aParameter Parameter name.
   */
  TH1D* GetTH1D(const string aParameter);

  /**
   * @brief Returns an empty 2-D histogram binned in a given combination of parameters.
   * @details
   * - \"Time\",
   * - \"Frequency\",
   * - \"Q\",
   * - \"SNR\".
   *
   * @note The user must delete the returned histogram.
   * @param[in] aParameterX Parameter name for X axis.
   * @param[in] aParameterY Parameter name for Y axis.
   */
  TH2D* GetTH2D(const string aParameterX, const string aParameterY);

  /**
   * @brief Prints the current selection in the standard output.
   */ 
  void PrintSelection(void);

 private:

  unsigned int verbose;    ///< Verbosity level.
  string name;             ///< Name.
  int clustertag;          ///< cluster tag: -1 for triggers.
  bool usedate;            ///< Flag to use a date format.
  string sstart;           ///< Start time string. See MakeTbins().
  
  // TIME
  double tmin;             ///< Time min (current).
  double tmin_0;           ///< Time min (default).
  double tmax;             ///< Time max (current).
  double tmax_0;           ///< Time max (default).
  unsigned int ntbins;     ///< Number of time bins (current).
  unsigned int ntbins_0;   ///< Number of time bins (default).
  double *tbins;           ///< Time bins.

  // FREQUENCY
  double fmin;             ///< Frequency min (current).
  double fmin_0;           ///< Frequency min (default).
  double fmax;             ///< Frequency max (current).
  double fmax_0;           ///< Frequency max (default).
  unsigned int nfbins;     ///< Number of frequency bins (current).
  unsigned int nfbins_0;   ///< Number of frequency bins (default).
  double *fbins;           ///< Frequency bins.

  // Q
  double qmin;             ///< Q min (current).
  double qmin_0;           ///< Q min (default).
  double qmax;             ///< Q max (current).
  double qmax_0;           ///< Q max (default).
  unsigned int nqbins;     ///< Number of Q bins (current).
  unsigned int nqbins_0;   ///< Number of Q bins (default).
  double *qbins;           ///< Q bins.

  // SNR
  double smin;             ///< SNR min (current).
  double smin_0;           ///< SNR min (default).
  double smax;             ///< SNR max (current).
  double smax_0;           ///< SNR max (default).
  unsigned int nsbins;     ///< Number of SNR bins (current).
  unsigned int nsbins_0;   ///< Number of SNR bins (default).
  double *sbins;           ///< SNR bins.

  /**
   * @brief Makes time binning (linear).
   */
  void MakeTbins(void);

  /**
   * @brief Makes frequency binning (log).
   */
  void MakeFbins(void);

  /**
   * @brief Makes Q binning (log).
   */
  void MakeQbins(void);

  /**
   * @brief Makes SNR binning (log).
   */
  void MakeSbins(void);


  ClassDef(TriggerSelect,0)  
};

#endif


