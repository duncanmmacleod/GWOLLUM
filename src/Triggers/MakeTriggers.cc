/**
 * @file 
 * @brief See MakeTriggers.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "MakeTriggers.h"

ClassImp(MakeTriggers)

////////////////////////////////////////////////////////////////////////////////////
MakeTriggers::MakeTriggers(const string aStreamName, const unsigned int aVerbose): 
Triggers(aVerbose),
  Segments(),
  Streams(aStreamName, aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  // metadata tree
  Mtree = new TTree("metadata", "metadata");
  Mtree->Branch("Mprocessname", &Mprocessname);
  Mtree->Branch("Mstreamname", &(Streams::Name));
  Mtree->Branch("Mprocessversion", &Mprocessversion);
  Mtree->Branch("Mprocessuser", &Mprocessuser);
  Mtree->Branch("start", &Mstart, "start/D");
  Mtree->Branch("end", &Mend, "end/D");
  Mtree->Branch("Mfmin", &Mfmin, "Mfmin/D");
  Mtree->Branch("Mfmax", &Mfmax, "Mfmax/D");
  Mtree->Branch("Mqmin", &Mqmin, "Mqmin/D");
  Mtree->Branch("Mqmax", &Mqmax, "Mqmax/D");
  Mtree->Branch("Msnrmin", &Msnrmin, "Msnrmin/D");
  Mtree->Branch("Msnrmax", &Msnrmax, "Msnrmax/D");
  
  // default metadata
  Mprocessname="PROCESS";
  Mprocessversion="0";
  char *login = getlogin();
  if(login==NULL) Mprocessuser = "unknown";
  else            Mprocessuser = (string)login;
  Mdvar = new double [0];// user-defined
  Mivar = new int    [0];// user-defined
  Muvar = new unsigned int[0];// user-defined
  Msvar = new string [0];// user-defined
  
  // create the hdf5 memory datatype
  h5_type = new H5::CompType(sizeof(h5_triggers));
  h5_type->insertMember("time", HOFFSET(h5_triggers, h5_time), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("frequency", HOFFSET(h5_triggers, h5_freq), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("tstart", HOFFSET(h5_triggers, h5_tstart), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("tend", HOFFSET(h5_triggers, h5_tend), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("fstart", HOFFSET(h5_triggers, h5_fstart), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("fend", HOFFSET(h5_triggers, h5_fend), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("snr", HOFFSET(h5_triggers, h5_snr), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("q", HOFFSET(h5_triggers, h5_q), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("amplitude", HOFFSET(h5_triggers, h5_amp), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("phase", HOFFSET(h5_triggers, h5_ph), H5::PredType::NATIVE_DOUBLE);
  h5_type_s = new H5::CompType(sizeof(h5_segments));
  h5_type_s->insertMember("start", HOFFSET(h5_segments, h5_start), H5::PredType::NATIVE_DOUBLE);
  h5_type_s->insertMember("end", HOFFSET(h5_segments, h5_end), H5::PredType::NATIVE_DOUBLE);
  
}

////////////////////////////////////////////////////////////////////////////////////
MakeTriggers::~MakeTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"MakeTriggers::~MakeTriggers"<<endl;
  delete Mtree;
  metaname.clear();
  metatype.clear();
  delete Mdvar;
  delete Mivar;
  delete Muvar;
  delete [] Msvar;
  delete h5_type;
  delete h5_type_s;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  if(Verbose) cout<<"MakeTriggers::Reset: reset all"<<endl;
  Triggers::Reset();
  Segments::Reset();
  Mtree->Reset();
}

////////////////////////////////////////////////////////////////////////////////////
string MakeTriggers::Write(const string aOutDir,
			   const string aFileFormat,
			   const string aFileName,
			   const string aSubDir,
			   const string aWriteMode,
			   const bool aWriteMeta){
////////////////////////////////////////////////////////////////////////////////////

  // no trigger, no segment
  if((Ttree->GetEntries()==0)&&(GetN()==0))
    return "";

  // check output directory
  if(!IsDirectory(aOutDir)){
    cerr<<"MakeTriggers::Write: output directory does not exist: "<<aOutDir<<endl;
    return "";
  }

  // file formats
  vector <string> fileformat;
  if(aFileFormat.find("xml")!=string::npos)  fileformat.push_back("xml");
  if(aFileFormat.find("hdf5")!=string::npos) fileformat.push_back("h5");
  if(aFileFormat.find("txt")!=string::npos)  fileformat.push_back("txt");
  if(aFileFormat.find("root")!=string::npos) fileformat.push_back("root");
  if(!fileformat.size())                     fileformat.push_back("root");

  // no segment -> create one from triggers
  if(GetN()==0){
    // get time of first and last trigger to create default segment
    double st = Ttree->GetMinimum("tstart");
    double en = Ttree->GetMaximum("time");
    if(!Segments::AddSegment((unsigned int)floor(st), (unsigned int)ceil(en))){
      return "";
    }
  }

  // get segment tree
  TTree *Stree=Segments::GetTree();

  // start - end
  Mstart=Segments::GetStart(0);
  Mend=Segments::GetEnd(Segments::GetN()-1);
  
  // set internal metadata
  if(Ttree->GetEntries()){
    Mfmin=Ttree->GetMinimum("fstart");
    Mfmax=Ttree->GetMaximum("fend");
    Mqmin=Ttree->GetMinimum("q");
    Mqmax=Ttree->GetMaximum("q");
    Msnrmin=Ttree->GetMinimum("snr");
    Msnrmax=Ttree->GetMaximum("snr");
  }
  else{
    Mfmin   = -1.0;
    Mfmax   = -1.0;
    Mqmin   = -1.0;
    Mqmax   = -1.0;
    Msnrmin = -1.0;
    Msnrmax = -1.0;
  }
  
  // fill metadata tree (single entry)
  Mtree->Reset();
  Mtree->Fill();
  
  // output file name
  ostringstream outfilestream;
  string outfile;
  string wm;
  
  // loop over output formats
  for(unsigned int f=0; f<fileformat.size(); f++){

    // output file name
    if(aFileName.compare(""))
      outfilestream<<aOutDir<<"/"<<aFileName<<"."<<fileformat[f];
    else
      outfile = Streams::GetTriggerFileName((unsigned int)Mstart, (unsigned int)(ceil(Mend)-floor(Mstart)), fileformat[f], Mprocessname, aOutDir);

    // save root trees
    if(!fileformat[f].compare("root")){// ROOT format

      // sub-directory
      wm="RECREATE";
      if(aSubDir.compare("")) wm=aWriteMode;// updates are only possible with sub-directories
      TFile outTFile(outfile.c_str(), wm.c_str());
      outTFile.cd();
      if(aSubDir.compare("")){
	outTFile.mkdir(aSubDir.c_str());
	outTFile.cd(aSubDir.c_str());
      }

      // write trigger tree
      if(Ttree->Write("triggers")<0) return "";

      // write segment tree
      if(Stree->Write("segments")<0) return "";

      // write metadata tree
      if(aWriteMeta)
        if(Mtree->Write("metadata")<0) return "";

      outTFile.Close();
    }

    // save h5
    else if(!fileformat[f].compare("h5"))
      WriteHDF5(outfile);

    // save xml
    else if(!fileformat[f].compare("xml"))
      WriteXML(outfile);

    // save txt
    else
      WriteTXT(outfile);

    if(Verbose>1){
      cout<<"MakeTriggers::Write: "<<Ttree->GetEntries()<<" triggers"<<endl;
      cout<<"                     "<<Stree->GetEntries()<<" segments"<<endl;
      cout<<"                     in "<<outfile<<endl;
    }
    outfilestream.clear();
    outfilestream.str("");
  }
  
  delete Stree;
  fileformat.clear();
  Reset();

  return outfile;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::InitMetaData(vector<string> aMetaName, vector<string> aMetaType){
////////////////////////////////////////////////////////////////////////////////////
 
  if(Verbose) cout<<"MakeTriggers::InitMetaData: init user-defined metadata"<<endl;

  unsigned int sizemin = TMath::Min(aMetaName.size(), aMetaType.size());

  // get user mdata
  metaname.clear();
  metatype.clear();
  metaname.assign(aMetaName.begin(), aMetaName.begin()+sizemin);
  metatype.assign(aMetaType.begin(), aMetaType.begin()+sizemin);

  // allocate memory
  delete Mdvar;
  delete Mivar;
  delete Muvar;
  delete [] Msvar;
  Mdvar = new double [sizemin];
  Mivar = new int [sizemin];
  Muvar = new unsigned int [sizemin];
  Msvar = new string [sizemin];

  if(Verbose>1) cout<<"MakeTriggers::InitMetaData: metadata size = "<<sizemin<<endl;
  for(unsigned int m=0; m<sizemin; m++){
    Mdvar[m]=-1.0;
    Mivar[m]=-1;
    Muvar[m]=0;
    Msvar[m]="";

    // attach branches
    if(!metatype[m].compare("d"))
      Mtree->Branch(metaname[m].c_str(), &Mdvar[m], (metaname[m]+"/D").c_str());
    else if(!metatype[m].compare("i"))
      Mtree->Branch(metaname[m].c_str(), &Mivar[m], (metaname[m]+"/I").c_str());
    else if(!metatype[m].compare("u"))
      Mtree->Branch(metaname[m].c_str(), &Muvar[m], (metaname[m]+"/i").c_str());
    else if(!metatype[m].compare("s"))
      Mtree->Branch(metaname[m].c_str(), &Msvar[m]);
    else{
      metaname.erase(metaname.begin()+m);
      metatype.erase(metatype.begin()+m);
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::SetMetaData(const string aMetaName, const double aMetaValue){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int m=0; m<metaname.size(); m++){
    if(metaname[m].compare(aMetaName)) continue;
    if(!metatype[m].compare("d")){
      Mdvar[m]=aMetaValue;
      break;
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::SetMetaData(const string aMetaName, const int aMetaValue){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int m=0; m<metaname.size(); m++){
    if(metaname[m].compare(aMetaName)) continue;
    if(!metatype[m].compare("i")){
      Mivar[m]=aMetaValue;
      break;
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::SetMetaData(const string aMetaName, const unsigned int aMetaValue){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int m=0; m<metaname.size(); m++){
    if(metaname[m].compare(aMetaName)) continue;
    if(!metatype[m].compare("u")){
      Muvar[m]=aMetaValue;
      break;
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::SetMetaData(const string aMetaName, const string aMetaValue){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int m=0; m<metaname.size(); m++){
    if(metaname[m].compare(aMetaName)) continue;
    if(!metatype[m].compare("s")){
      Msvar[m]=aMetaValue;
      break;
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::WriteHDF5(const string aFileName){ 
////////////////////////////////////////////////////////////////////////////////////
 
  // create dataset and data space for segments
  h5_segments *h5_s; hsize_t dimsf_s[1];
  dimsf_s[0] = Segments::GetN();
  h5_s = new h5_segments [Segments::GetN()];
  H5::DataSpace dataspace_s(1, dimsf_s);
  for(unsigned int s=0; s<Segments::GetN(); s++){
    h5_s[s].h5_start=Segments::GetStart(s);
    h5_s[s].h5_end=Segments::GetEnd(s);
  }

  // create dataset and data space for triggers
  h5_triggers *h5_t; hsize_t dimsf[1];
  if(GetClusterN()){// use clusters
    dimsf[0] = GetClusterN();
    h5_t = new h5_triggers [GetClusterN()];
  }
  else{// use triggers
    dimsf[0] = Ttree->GetEntries();
    h5_t = new h5_triggers [Ttree->GetEntries()];
  }
  H5::DataSpace dataspace(1, dimsf);
 
  // save clusters
  if(GetClusterN()){
    for(unsigned int c=0; c<GetClusterN(); c++){
      h5_t[c].h5_time=GetClusterTime(c);
      h5_t[c].h5_freq=GetClusterFrequency(c);
      h5_t[c].h5_snr=GetClusterSnr(c);
      h5_t[c].h5_q=GetClusterQ(c);
      h5_t[c].h5_amp=GetClusterAmplitude(c);
      h5_t[c].h5_ph=GetClusterPhase(c);
      h5_t[c].h5_tstart=GetClusterTimeStart(c);
      h5_t[c].h5_tend=GetClusterTimeEnd(c);
      h5_t[c].h5_fstart=GetClusterFrequencyStart(c);
      h5_t[c].h5_fend=GetClusterFrequencyEnd(c);
    }
  }
  // save triggers
  else{
    for(Long64_t t=0; t<Ttree->GetEntries(); t++){
      Ttree->GetEntry(t);
      h5_t[t].h5_time=GetTriggerTime(t);
      h5_t[t].h5_freq=GetTriggerFrequency(t);
      h5_t[t].h5_snr=GetTriggerSnr(t);
      h5_t[t].h5_q=GetTriggerQ(t);
      h5_t[t].h5_amp=GetTriggerAmplitude(t);
      h5_t[t].h5_ph=GetTriggerPhase(t);
      h5_t[t].h5_tstart=GetTriggerTimeStart(t);
      h5_t[t].h5_tend=GetTriggerTimeEnd(t);
      h5_t[t].h5_fstart=GetTriggerFrequencyStart(t);
      h5_t[t].h5_fend=GetTriggerFrequencyEnd(t);
    }
  }
 
  // create the output file
  H5::H5File* h5_file = new H5::H5File(aFileName.c_str(), H5F_ACC_TRUNC);

  // Create the data space for the attributes
  H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
  
  // File attributes 
  H5::Attribute attribute;

  H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
  attribute = h5_file->createAttribute("Mprocessname", strdatatype, attr_dataspace);
  attribute.write(strdatatype, Mprocessname);
  attribute = h5_file->createAttribute("Mstreamname", strdatatype, attr_dataspace);
  attribute.write(strdatatype, Streams::Name);
  attribute = h5_file->createAttribute("Mprocessversion", strdatatype, attr_dataspace);
  attribute.write(strdatatype, Mprocessversion);
  attribute = h5_file->createAttribute("Mprocessuser", strdatatype, attr_dataspace);
  attribute.write(strdatatype, Mprocessuser);

  // loop over metadata
  for(unsigned int m=0; m<metaname.size(); m++){
    //attribute = h5_file->createAttribute(Musername[m], H5::PredType::STD_I32BE, attr_dataspace);
    if(!metatype[m].compare("d")){
      attribute = h5_file->createAttribute(metaname[m], H5::PredType::NATIVE_DOUBLE, attr_dataspace);
      attribute.write( H5::PredType::NATIVE_DOUBLE, &(Mdvar[m]));
    }
    else if(!metatype[m].compare("s")){
      H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
      attribute = h5_file->createAttribute(metaname[m], strdatatype, attr_dataspace);
      attribute.write(strdatatype, Msvar[m]);
    }
    else if(!metatype[m].compare("u")){
      attribute = h5_file->createAttribute(metaname[m], H5::PredType::NATIVE_UINT, attr_dataspace);
      attribute.write(H5::PredType::NATIVE_UINT, &(Muvar[m]));
    }
    else{
      attribute = h5_file->createAttribute(metaname[m], H5::PredType::NATIVE_INT, attr_dataspace);
      attribute.write( H5::PredType::NATIVE_INT, &(Mivar[m]));
    }
  }

  // create the dataset
  H5::DataSet* dataset;
  dataset = new H5::DataSet(h5_file->createDataSet("triggers", *h5_type, dataspace));

  // Write the data to the dataset
  dataset->write(h5_t, *h5_type);
  delete dataset;

  dataset = new H5::DataSet(h5_file->createDataSet("segments", *h5_type_s, dataspace_s));
  dataset->write(h5_s, *h5_type_s);
  delete dataset;

  // cleaning
  delete h5_file;
  delete h5_t;
  delete h5_s;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::WriteTXT(const string aFileName){ 
////////////////////////////////////////////////////////////////////////////////////
  
  // open txt file stream        
  ofstream txtfile(aFileName.c_str());

  // write file header
  txtfile.precision ( 5 );
  txtfile<<"# central time [s]"<<endl;
  txtfile<<"# central frequency [Hz]"<<endl;
  txtfile<<"# snr []"<<endl;
  txtfile<<"# q []"<<endl;
  txtfile<<"# amplitude [Hz^-1/2]"<<endl;
  txtfile<<"# phase [rad]"<<endl;
  txtfile<<"# starting time [s]"<<endl;
  txtfile<<"# ending time [s]"<<endl;
  txtfile<<"# starting frequency [Hz]"<<endl;
  txtfile<<"# ending frequency [Hz]"<<endl;
  
  // print clusters
  if(GetClusterN()){
    for(unsigned int c=0; c<GetClusterN(); c++){
      txtfile<<fixed<<GetClusterTime(c)<<"\t"
             <<GetClusterFrequency(c)<<"\t"
             <<GetClusterSnr(c)<<"\t"
             <<GetClusterQ(c)<<"\t"
             <<scientific<<GetClusterAmplitude(c)<<"\t"
             <<fixed<<GetClusterPhase(c)<<"\t"
             <<GetClusterTimeStart(c)<<"\t"
             <<GetClusterTimeEnd(c)<<"\t"
             <<GetClusterFrequencyStart(c)<<"\t"
             <<GetClusterFrequencyEnd(c)<<endl;
    }
  }
  // print triggers
  else{
    for(Long64_t t=0; t<Ttree->GetEntries(); t++){
      Ttree->GetEntry(t);
      txtfile<<fixed<<GetTriggerTime(t)<<"\t"
             <<GetTriggerFrequency(t)<<"\t"
             <<GetTriggerSnr(t)<<"\t"
             <<GetTriggerQ(t)<<"\t"
             <<scientific<<GetTriggerAmplitude(t)<<"\t"
             <<fixed<<GetTriggerPhase(t)<<"\t"
             <<GetTriggerTimeStart(t)<<"\t"
             <<GetTriggerTimeEnd(t)<<"\t"
             <<GetTriggerFrequencyStart(t)<<"\t"
             <<GetTriggerFrequencyEnd(t)<<endl;
    }
  }

  txtfile.close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::WriteXML(const string aFileName){ 
////////////////////////////////////////////////////////////////////////////////////

  // open txt file stream        
  ofstream xmlfile(aFileName.c_str());
  xmlfile.precision ( 5 );

  // THIS FUNCTION IS VERY UGLY
  // BUT XML IS AWFUL !!!!
  // THIS OUGHT TO DISAPPEAR
  
  string ifo=Streams::GetDetectorPrefix();

  //print header
  xmlfile<<"<?xml version='1.0' encoding='utf-8'?>"<<endl;
  xmlfile<<"<LIGO_LW>"<<endl;

  // print process table
  xmlfile<<"\t<Table Name=\"process:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:comment\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:node\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:domain\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:unix_procid\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:start_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"process:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:is_online\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:ifos\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:jobid\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:username\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:program\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:end_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:version\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process:cvs_repository\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"process:cvs_entry_time\"/>"<<endl;
  xmlfile<<"\t\t<Stream Delimiter=\",\" Type=\"Local\" Name=\"process:table\">"<<endl;
  xmlfile<<"\t\t\t,,,,,\"process:process_id:0\",,\""<<ifo<<"\",,\""<<Mprocessuser<<"\",\""<<Mprocessname<<"\",,\""<<Mprocessversion<<"\",,"<<endl;
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;

  // print process_params table
  xmlfile<<"\t<Table Name=\"process_params:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"process_params:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process_params:program\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process_params:type\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process_params:value\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"process_params:param\"/>"<<endl;
  xmlfile<<"\t\t<Stream Delimiter=\",\" Type=\"Local\" Name=\"process_params:table\">"<<endl;
  
  for(unsigned int m=0; m<metaname.size(); m++){
    if(!metatype[m].compare("s")) xmlfile<<"\t\t\t\"process:process_id:0\",\""<<Mprocessname<<"\",\"lstring\",\""<<metaname[m]<<"\",\""<<Msvar[m]<<"\"";
    else if(!metatype[m].compare("i")) xmlfile<<"\t\t\t\"process:process_id:0\",\""<<Mprocessname<<"\",\"int_8s\",\""<<metaname[m]<<"\",\""<<Mivar[m]<<"\"";
    else if(!metatype[m].compare("u")) xmlfile<<"\t\t\t\"process:process_id:0\",\""<<Mprocessname<<"\",\"uint_8s\",\""<<metaname[m]<<"\",\""<<Muvar[m]<<"\"";
    else xmlfile<<"\t\t\t\"process:process_id:0\",\""<<Mprocessname<<"\",\"real_8\",\""<<metaname[m]<<"\",\""<<Mdvar[m]<<"\"";
    if(m!=metaname.size()-1) xmlfile<<","<<endl;
    else xmlfile<<endl;
  }
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;

  // print sngl_burst header
  xmlfile<<"\t<Table Name=\"sngl_burst:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"sngl_burst:ifo\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"sngl_burst:peak_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"sngl_burst:peak_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"sngl_burst:start_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"sngl_burst:start_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:duration\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"sngl_burst:search\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"sngl_burst:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"sngl_burst:event_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:peak_frequency\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:central_freq\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:bandwidth\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"sngl_burst:channel\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:amplitude\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:snr\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_4\" Name=\"sngl_burst:confidence\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_8\" Name=\"sngl_burst:chisq\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_8\" Name=\"sngl_burst:chisq_dof\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"sngl_burst:param_one_name\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"real_8\" Name=\"sngl_burst:param_one_value\"/>"<<endl;
  xmlfile<<"\t\t<Stream Name=\"sngl_burst:table\" Type=\"Local\" Delimiter=\",\">"<<endl;

  // print clusters
  if(GetClusterN()){
    for(unsigned int c=0; c<GetClusterN()-1; c++){
       xmlfile<<"\t\t\t\""<<ifo<<"\","
              <<fixed<<(int)floor(GetClusterTime(c))<<","
              <<(int)floor((GetClusterTime(c)-floor(GetClusterTime(c)))*1e9)<<","
              <<(int)floor(GetClusterTimeStart(c))<<","
              <<(int)floor((GetClusterTimeStart(c)-floor(GetClusterTimeStart(c)))*1e9)<<","
              <<GetClusterDuration(c)<<",\""
              <<Mprocessname<<"\",\"process:process_id:0\",\"sngl_burst:event_id:"
              <<c<<"\","
              <<GetClusterFrequency(c)<<","
              <<(GetClusterFrequencyEnd(c)+GetClusterFrequencyStart(c))/2.0<<","
              <<GetClusterBandwidth(c)<<",\""
              <<Streams::GetNameSuffix()<<"\","
              <<scientific<<GetClusterAmplitude(c)<<fixed<<","
              <<GetClusterSnr(c)<<",0,0,0,\"phase\","
              <<GetClusterPhase(c)<<","<<endl;
    }
    xmlfile<<"\t\t\t\""<<ifo<<"\","
           <<fixed<<(int)floor(GetClusterTime(GetClusterN()-1))<<","
           <<(int)floor((GetClusterTime(GetClusterN()-1)-floor(GetClusterTime(GetClusterN()-1)))*1e9)<<","
           <<(int)floor(GetClusterTimeStart(GetClusterN()-1))<<","
           <<(int)floor((GetClusterTimeStart(GetClusterN()-1)-floor(GetClusterTimeStart(GetClusterN()-1)))*1e9)<<","
           <<GetClusterDuration(GetClusterN()-1)<<",\""
           <<Mprocessname<<"\",\"process:process_id:0\",\"sngl_burst:event_id:"
           <<GetClusterN()-1<<"\","
           <<GetClusterFrequency(GetClusterN()-1)<<","
           <<(GetClusterFrequencyEnd(GetClusterN()-1)+GetClusterFrequencyStart(GetClusterN()-1))/2.0<<","
           <<GetClusterBandwidth(GetClusterN()-1)<<",\""
           <<Streams::GetNameSuffix()<<"\","
           <<scientific<<GetClusterAmplitude(GetClusterN()-1)<<fixed<<","
           <<GetClusterSnr(GetClusterN()-1)<<",0,0,0,\"phase\","
           <<GetClusterPhase(GetClusterN()-1)<<endl;
  }

  // print triggers
  else{
    for(Long64_t t=0; t<Ttree->GetEntries()-1; t++){
      Ttree->GetEntry(t);
      xmlfile<<"\t\t\t\""<<ifo<<"\","
             <<fixed<<(int)floor(GetTriggerTime(t))<<","
             <<(int)floor((GetTriggerTime(t)-floor(GetTriggerTime(t)))*1e9)<<","
             <<(int)floor(GetTriggerTimeStart(t))<<","
             <<(int)floor((GetTriggerTimeStart(t)-floor(GetTriggerTimeStart(t)))*1e9)<<","
             <<GetTriggerDuration(t)<<",\""
             <<Mprocessname<<"\",\"process:process_id:0\",\"sngl_burst:event_id:"
             <<t<<"\","
             <<GetTriggerFrequency(t)<<","
             <<(GetTriggerTimeEnd(t)+GetTriggerFrequencyStart(t))/2.0<<","
             <<GetTriggerBandwidth(t)<<",\""
             <<Streams::GetNameSuffix()<<"\","
             <<scientific<<GetTriggerAmplitude(t)<<fixed<<","
             <<GetTriggerSnr(t)<<",0,0,0,\"phase\","
             <<GetTriggerPhase(t)<<","<<endl;
    }
    if(Ttree->GetEntries()){
      xmlfile<<"\t\t\t\""<<ifo<<"\","
             <<fixed<<(int)floor(GetTriggerTime(Ttree->GetEntries()-1))<<","
             <<(int)floor((GetTriggerTime(Ttree->GetEntries()-1)-floor(GetTriggerTime(Ttree->GetEntries()-1)))*1e9)<<","
             <<(int)floor(GetTriggerTimeStart(Ttree->GetEntries()-1))<<","
             <<(int)floor((GetTriggerTimeStart(Ttree->GetEntries()-1)-floor(GetTriggerTimeStart(Ttree->GetEntries()-1)))*1e9)<<","
             <<GetTriggerDuration(Ttree->GetEntries()-1)<<",\""
             <<Mprocessname<<"\",\"process:process_id:0\",\"sngl_burst:event_id:"
             <<Ttree->GetEntries()-1<<"\","
             <<GetTriggerFrequency(Ttree->GetEntries()-1)<<","
             <<(GetTriggerTimeEnd(Ttree->GetEntries()-1)+GetTriggerFrequencyStart(Ttree->GetEntries()-1))/2.0<<","
             <<GetTriggerBandwidth(Ttree->GetEntries()-1)<<",\""
             <<Streams::GetNameSuffix()<<"\","
             <<scientific<<GetTriggerAmplitude(Ttree->GetEntries()-1)<<fixed<<","
             <<GetTriggerSnr(Ttree->GetEntries()-1)<<",0,0,0,\"phase\","
             <<GetTriggerPhase(Ttree->GetEntries()-1)<<endl;
    }
  }
  
  // close sngl_burst table
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;
  
  // print segment_definer table
  xmlfile<<"\t<Table Name=\"segment_definer:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment_definer:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment_definer:segment_def_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"segment_definer:ifos\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"segment_definer:name\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"segment_definer:comment\"/>"<<endl;
  xmlfile<<"\t\t<Stream Delimiter=\",\" Type=\"Local\" Name=\"segment_definer:table\">"<<endl;
  xmlfile<<"\t\t\t\"process:process_id:0\",\"segment_definer:segment_def_id:0\",\""<<ifo<<"\",\""<<Mprocessname<<"-segments\",\""<<Mprocessname<<" processed segments\""<<endl;
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;
  
  // print segment_summary table
  xmlfile<<"\t<Table Name=\"segment_summary:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment_summary:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment_summary:start_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment_summary:start_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment_summary:end_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment_summary:end_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment_summary:segment_def_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"lstring\" Name=\"segment_summary:comment\"/>"<<endl;
  xmlfile<<"\t\t<Stream Delimiter=\",\" Type=\"Local\" Name=\"segment_summary:table\">"<<endl;
  if(Segments::GetN())
    xmlfile<<"\t\t\t\"process:process_id:0\","<<fixed<<(int)floor(Segments::GetStart(0))<<","
           <<(int)floor((Segments::GetStart(0)-floor(Segments::GetStart(0)))*1e9)
           <<","<<(int)floor(Segments::GetEnd(Segments::GetN()-1))<<","
           <<(int)floor((Segments::GetEnd(Segments::GetN()-1)-floor(Segments::GetEnd(Segments::GetN()-1)))*1e9)<<",\"segment_summary:segment_def_id:0\",\""
           <<Mprocessname<<" processed time interval\""<<endl;
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;

  // print segments header
  xmlfile<<"\t<Table Name=\"segment:table\">"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment:process_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment:segment_id\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment:start_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment:start_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment:end_time\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"int_4s\" Name=\"segment:end_time_ns\"/>"<<endl;
  xmlfile<<"\t\t<Column Type=\"ilwd:char\" Name=\"segment:segment_def_id\"/>"<<endl;
  xmlfile<<"\t\t<Stream Name=\"segment:table\" Type=\"Local\" Delimiter=\",\">"<<endl;
     
  for(unsigned int s=0; s<Segments::GetN()-1; s++)
    xmlfile<<"\t\t\t\"process:process_id:0\",\"segment:segment_id:"<<s<<"\","<<fixed
           <<(int)floor(Segments::GetStart(s))<<","
           <<(int)floor((Segments::GetStart(s)-floor(Segments::GetStart(s)))*1e9)<<","
           <<(int)floor(Segments::GetEnd(s))<<","
           <<(int)floor((Segments::GetEnd(s)-floor(Segments::GetEnd(s)))*1e9)<<",\"segment_definer:segment_def_id:0\","<<endl;
  if(Segments::GetN())
    xmlfile<<"\t\t\t\"process:process_id:0\",\"segment:segment_id:"<<Segments::GetN()-1<<"\","<<fixed
           <<(int)floor(Segments::GetStart(Segments::GetN()-1))<<","
           <<(int)floor((Segments::GetStart(Segments::GetN()-1)-floor(Segments::GetStart(Segments::GetN()-1)))*1e9)<<","
           <<(int)floor(Segments::GetEnd(Segments::GetN()-1))<<","
           <<(int)floor((Segments::GetEnd(Segments::GetN()-1)-floor(Segments::GetEnd(Segments::GetN()-1)))*1e9)<<",\"segment_definer:segment_def_id:0\""<<endl;

  // close segments table
  xmlfile<<"\t\t</Stream>"<<endl;
  xmlfile<<"\t</Table>"<<endl;
  
  xmlfile<<"</LIGO_LW>"<<endl;
    
  
  xmlfile.close();

  return;
}
