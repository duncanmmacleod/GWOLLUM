/**
 * @file 
 * @brief Create and manage a buffer for triggers.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">fl
orent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __TriggerBuffer__
#define __TriggerBuffer__

#include "MakeTriggers.h"

using namespace std;

/**
 * @brief Create and manage a buffer for triggers.
 * @details First the size of the buffer is defined in the constructor TriggerBuffer().
 * Then triggers can be added with the following sequence:
 * - SetSegments() save the time segments for which the triggers will be added.
 * - Call AddTrigger() to add triggers.
 * - Call Flush() to transfer the triggers in the final Triggers structure.
 * 
 * At any time it is possible to remove the last triggers added in the buffer with ResetAfter().
 * @author Florent Robinet
 */
class TriggerBuffer: public MakeTriggers {

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the TriggerBuffer class.
   * @details The size of the buffer is defined.
   * This size determines the maximum number of triggers which can be saved in the buffer.
   * The memory for the buffer is then allocated.
   * @param[in] aSize Buffer size.
   * @param[in] aStreamName Stream name.
   * @param[in] aVerbose Verbosity level.
   */
  TriggerBuffer(const unsigned int aSize, const string aStreamName,
                const int unsigned aVerbose=0);
  
  /**
   * @brief Destructor of the TriggerBuffer class.
   */
  virtual ~TriggerBuffer(void);
  /**
     @}
  */

  /**
   * @brief Returns the size of the buffer.
   */
  inline unsigned int GetSize(void){ return BufferSize; };

  /**
   * @brief Adds a trigger to the buffer.
   * @details This function overloads Triggers::AddTrigger() to fill the buffer instead the final Triggers structure.
   * @note If the buffer size is 0, Triggers::AddTrigger() is called directly.
   * @returns false if the buffer is full.
   * @param[in] aTime Trigger GPS time [s].
   * @param[in] aFrequency Trigger frequency [Hz].
   * @param[in] aSNR Trigger Signal-to-noise ratio.
   * @param[in] aQ Trigger Quality factor.
   * @param[in] aTstart Trigger GPS start time [s].
   * @param[in] aTend Trigger GPS end time [s].
   * @param[in] aFstart Trigger frequency start [Hz].
   * @param[in] aFend Trigger frequency end [Hz].
   * @param[in] aAmplitude Trigger amplitude.
   * @param[in] aPhase Trigger phase [rad].
   */
  bool AddTrigger(const double aTime,   const double aFrequency,
		  const double aSNR,    const double aQ,
		  const double aTstart, const double aTend,
		  const double aFstart, const double aFend,
		  const double aAmplitude, const double aPhase);
 
  /**
   * @brief Flushes the trigger buffer to the Triggers structure.
   * @details Moreover the current segments are added to the Triggers structure.
   * The buffer is reset after the flush.
   */
  bool Flush(void);

  /**
   * @brief Sets new segments.
   * @details This function should be called before adding triggers in the buffer.
   * It defines the time segments corresponding to the triggers.
   * @warning It is the user responsibility to guarantee that the triggers and the segments are compatible.
   * @warning If this function is called several times in a row, segments should be consecutive with no time overlap.
   * @param[in] aSeg Segments.
   */
  bool SetSegments(Segments *aSeg);

  /**
   * @brief Resets the buffer.
   */
  void Reset(void);

  /**
   * @brief Removes triggers (and segments) from the buffer when they start after a given time.
   * @warning Entire segments added with SetSegments() are removed.
   * @param[in] aTime Time after which segments are removed.
   */
  void ResetAfter(const double aTime);

  /**
   * @brief Returns the number of triggers currently in buffer.
   * @note The number of triggers in the Triggers structure is returned if the buffer is of size 0.
   */
  inline Long64_t GetTriggerN(void){
    if(BufferSize) return (Long64_t)b_pos;
    return MakeTriggers::GetTriggerN();
  };
  
 private:
  
  // TRIGGERS
  unsigned int BufferSize;///< Buffer size.
  unsigned int b_pos;     ///< Current buffer index.
  double *b_time;         ///< Trigger peak time.
  double *b_freq;         ///< Trigger peak frequency.
  double *b_tstart;       ///< Trigger time start.
  double *b_tend;         ///< Trigger time end.
  double *b_fstart;       ///< Trigger frequency start.
  double *b_fend;         ///< Trigger frequency end.
  double *b_q;            ///< Trigger Q.
  double *b_snr;          ///< Trigger SNR.
  double *b_amp;          ///< Trigger amplitude.
  double *b_ph;           ///< Trigger phase.

  // SEGMENTS
  vector<double> s_start; ///< List of starts.
  vector<double> s_end;   ///< List of ends.
  vector<int> s_pos;      ///< Buffer index for that segment.

  ClassDef(TriggerBuffer,0)  
};


#endif


