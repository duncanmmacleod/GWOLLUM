/**
 * @file 
 * @brief See InjGen.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjGen.h"

ClassImp(InjGen)


/////////////////////////////////////////////////////////////////////////
InjGen::InjGen(Segments *aInSegments, const string aInjType, const unsigned int aVerbose):
InjTre(aInjType, aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // parameters
  InSegments=aInSegments;
  InjSpacing=100;

  rand = new TRandom3(0);

  // Set default modes
  SetAmplitudeDistribution();
  SetAmplitudeRange();
  SetSigmaDistribution();
  SetSigmaRange();
  SetFrequencyDistribution();
  SetFrequencyRange();
  SetEccentricityDistribution();
  SetEccentricityRange();
  SetPolarizationDistribution();
  SetPolarizationRange();
}

/////////////////////////////////////////////////////////////////////////
InjGen::~InjGen(void){
/////////////////////////////////////////////////////////////////////////
  delete rand;
}

/////////////////////////////////////////////////////////////////////////
unsigned int InjGen::GenerateInjections(void){
/////////////////////////////////////////////////////////////////////////

  // no livetime: no injections
  if(!InSegments->GetLiveTime()) return 0;

  // init timing
  double gps = InSegments->GetFirst()+rand->Uniform(InjSpacing);
  unsigned int current_seg=0;
  unsigned int n_inj = 0;
  
  // loop over injections
  while(gps<InSegments->GetLast()){

    // injection inside segment -> save it
    if(InSegments->IsInsideSegment(current_seg, gps)){
      
      if(!GetInjectionType().compare("Gauss"))
	AddGauss(rand->Uniform(0,2*TMath::Pi()), // ra
		 TMath::ASin(rand->Uniform(-1,1)), //dec
		 gps,
		 GetEccentricity(),
		 GetPolarization(),
		 GetAmplitude(),
		 GetSigma()
		 );

      if(!GetInjectionType().compare("SineGauss"))
	AddSineGauss(rand->Uniform(0,2*TMath::Pi()), // ra
		     TMath::ASin(rand->Uniform(-1,1)), //dec
		     gps,
		     GetEccentricity(),
		     GetPolarization(),
		     GetAmplitude(),
		     GetSigma(),
		     GetFrequency()
		     );

      // one more injection
      n_inj++;
    }
    
    // injection outside segment -> move to next segment
    else{
      if(current_seg+1<InSegments->GetN()){
	gps=InSegments->GetStart(current_seg+1)+rand->Uniform(InjSpacing);
	continue;
      }
      else break;
    }

    // increment GPS
    gps+=InjSpacing;
  }
  
  return n_inj;
}

/////////////////////////////////////////////////////////////////////////
bool InjGen::SetAmplitudeDistribution(const string aDistribution){
/////////////////////////////////////////////////////////////////////////
  if(!aDistribution.compare("UNIFORM"))  ampDistribution=0;
  else if(!aDistribution.compare("LOG")) ampDistribution=1;
  else{
    cerr<<"InjGen::SetAmplitudeDistribution: the amplitude distribution "<<aDistribution<<" is not supported"<<endl;
    return false;
  }
  return true;
}

/////////////////////////////////////////////////////////////////////////
bool InjGen::SetSigmaDistribution(const string aDistribution){
/////////////////////////////////////////////////////////////////////////
  if(!aDistribution.compare("UNIFORM"))  sigmaDistribution=0;
  else if(!aDistribution.compare("LOG")) sigmaDistribution=1;
  else{
    cerr<<"InjGen::SetSigmaDistribution: the sigma distribution "<<aDistribution<<" is not supported"<<endl;
    return false;
  }
  return true;
}

/////////////////////////////////////////////////////////////////////////
bool InjGen::SetFrequencyDistribution(const string aDistribution){
/////////////////////////////////////////////////////////////////////////
  if(!aDistribution.compare("UNIFORM"))  f0Distribution=0;
  else if(!aDistribution.compare("LOG")) f0Distribution=1;
  else{
    cerr<<"InjGen::SetFrequencyDistribution: the frequency distribution "<<aDistribution<<" is not supported"<<endl;
    return false;
  }
  return true;
}

/////////////////////////////////////////////////////////////////////////
bool InjGen::SetEccentricityDistribution(const string aDistribution){
/////////////////////////////////////////////////////////////////////////
  if(!aDistribution.compare("UNIFORM"))  eccDistribution=0;
  else if(!aDistribution.compare("LOG")) eccDistribution=1;
  else{
    cerr<<"InjGen::SetEccentricityDistribution: the eccentricity distribution "<<aDistribution<<" is not supported"<<endl;
    return false;
  }
  return true;
}

/////////////////////////////////////////////////////////////////////////
bool InjGen::SetPolarizationDistribution(const string aDistribution){
/////////////////////////////////////////////////////////////////////////
  if(!aDistribution.compare("UNIFORM"))  psiDistribution=0;
  else if(!aDistribution.compare("LOG")) psiDistribution=1;
  else{
    cerr<<"InjGen::SetPolarizationDistribution: the polarization distribution "<<aDistribution<<" is not supported"<<endl;
    return false;
  }
  return true;
}

/////////////////////////////////////////////////////////////////////////
double InjGen::GetAmplitude(void){
/////////////////////////////////////////////////////////////////////////
  if(ampDistribution==0)      return rand->Uniform(ampMin, ampMax);
  else if(ampDistribution==1) return ampMin*pow(ampMax/ampMin, rand->Uniform());
  else                        return 0.0;
  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjGen::GetSigma(void){
/////////////////////////////////////////////////////////////////////////
  if(sigmaDistribution==0)      return rand->Uniform(sigmaMin, sigmaMax);
  else if(sigmaDistribution==1) return sigmaMin*pow(sigmaMax/sigmaMin, rand->Uniform());
  else                  return 0.0;
  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjGen::GetFrequency(void){
/////////////////////////////////////////////////////////////////////////
  if(f0Distribution==0)       return rand->Uniform(f0Min, f0Max);
  else if(f0Distribution==1)  return f0Min*pow(f0Max/f0Min, rand->Uniform());
  else                        return 0.0;
  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjGen::GetEccentricity(void){
/////////////////////////////////////////////////////////////////////////
  if(eccDistribution==0)       return rand->Uniform(eccMin, eccMax);
  else if(eccDistribution==1)  return eccMin*pow(eccMax/eccMin, rand->Uniform());
  else                         return 0.0;
  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjGen::GetPolarization(void){
/////////////////////////////////////////////////////////////////////////
  if(psiDistribution==0)       return rand->Uniform(psiMin, psiMax);
  else if(psiDistribution==1)  return psiMin*pow(psiMax/psiMin, rand->Uniform());
  else                         return 0.0;
  return 0.0;
}



