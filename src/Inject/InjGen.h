/**
 * @file 
 * @brief Generate a set of injections.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjGen__
#define __InjGen__

#include <TMath.h>
#include <TRandom3.h>
#include "Segments.h"
#include "InjTre.h"

using namespace std;

/**
 * @brief Generate a set of injections.
 * @details Several injection waveforms are supported (Gauss, SineGauss...).
 * This class is used to draw injection parameters and to save them in a InjTre structure.
 * @author Florent Robinet
 */
class InjGen: public InjTre{
  
public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjGen class.
   * @details Injections are performed over a set of Segments.
   * The injection type must be provided:
   * - "Gauss" for Gaussian injections
   * - "SineGauss"  for sine-Gaussian injections
   *
   * @param[in] aInSegments Input segments.
   * @param[in] aInjType Injection type.
   * @param[in] aVerbose Verbosity level.
   */
  InjGen(Segments *aInSegments, const string aInjType, const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the InjGen class.
   */
  virtual ~InjGen();
  /**
     @}
  */
  
  
  /**
   * @brief Generates a set of injection parameters.
   * @returns The number of injections.
   */
  unsigned int GenerateInjections(void);
  
  /**
   * @brief Sets the time distance between 2 consecutive injections.
   * @param[in] aSpacing Time distance between 2 consecutive injections [s].
   */
  inline void SetInjectionSpacing(const double aSpacing){ InjSpacing=aSpacing; };
  
  /**
   * @brief Sets the injection amplitude distribution.
   * @details Amplitudes can either be logarithmically distributed ("LOG") or uniformly distributed ("UNIFORM").
   * @param[in] aDistribution Distribution type.
   */
  bool SetAmplitudeDistribution(const string aDistribution="LOG");
  
  /**
   * @brief Sets the injection amplitude range.
   * @param[in] aMin Minimum amplitude.
   * @param[in] aMax Maximum amplitude.
   */
  inline void SetAmplitudeRange(const double aMin=1e-22, const double aMax=5e-19){
    ampMin=aMin;
    ampMax=aMax;
  };

  /**
   * @brief Sets the injection sigma distribution.
   * @details Sigmas can either be logarithmically distributed ("LOG") or uniformly distributed ("UNIFORM").
   * @param[in] aDistribution Distribution type.
   */
  bool SetSigmaDistribution(const string aDistribution="UNIFORM");

  /**
   * @brief Sets the injection sigma range.
   * @param[in] aMin Minimum sigma [s].
   * @param[in] aMax Maximum sigma [s].
   */
  inline void SetSigmaRange(const double aMin=0.01, const double aMax=1.0){
    sigmaMin=aMin;
    sigmaMax=aMax;
  };

  /**
   * @brief Sets the injection frequency distribution.
   * @details Frequencies can either be logarithmically distributed ("LOG") or uniformly distributed ("UNIFORM").
   * @param[in] aDistribution Distribution type.
   */
  bool SetFrequencyDistribution(const string aDistribution="LOG");

  /**
   * @brief Sets the injection frequency range.
   * @param[in] aMin Minimum frequency [Hz].
   * @param[in] aMax Maximum frequency [Hz].
   */
  inline void SetFrequencyRange(const double aMin=30.0, const double aMax=2048.0){
    f0Min=aMin;
    f0Max=aMax;
  };

  /**
   * @brief Sets the injection eccentricity distribution.
   * @details Eccentricities can either be logarithmically distributed ("LOG") or uniformly distributed ("UNIFORM").
   * @param[in] aDistribution Distribution type.
   */
  bool SetEccentricityDistribution(const string aDistribution="UNIFORM");

  /**
   * @brief Sets the injection eccentricity range.
   * @param[in] aMin Minimum eccentricity.
   * @param[in] aMax Maximum eccentricity.
   */
  inline void SetEccentricityRange(const double aMin=0.0, const double aMax=1.0){
    eccMin=aMin;
    eccMax=aMax;
  };

  /**
   * @brief Sets the injection polarization distribution.
   * @details Polarizations can either be logarithmically distributed ("LOG") or uniformly distributed ("UNIFORM").
   * @param[in] aDistribution Distribution type.
   */
  bool SetPolarizationDistribution(const string aDistribution="UNIFORM");

  /**
   * @brief Sets the injection polarization range.
   * @param[in] aMin Minimum polarization [rad].
   * @param[in] aMax Maximum polarization [rad].
   */
  inline void SetPolarizationRange(const double aMin=0.0, const double aMax=2.0*TMath::Pi()){
    psiMin=aMin;
    psiMax=aMax;
  };
  
 private:
  
  TRandom3 *rand;                 ///< Random generator.
  Segments *InSegments;          ///< Input segments - DO NOT DELETE.
  double InjSpacing;             ///< Injection spacing [s].
  
  // PARAMETERS
  double ampMin;                 ///< amplitude min
  double ampMax;                 ///< amplitude max
  unsigned int ampDistribution;  ///< 0=UNIFORM, 1=LOG/10
  double sigmaMin;               ///< sigma min
  double sigmaMax;               ///< sigma max
  unsigned int sigmaDistribution;///< 0=UNIFORM, 1=LOG/10
  double f0Min;                  ///< frequency min
  double f0Max;                  ///< frequency max
  unsigned int f0Distribution;   ///< 0=UNIFORM, 1=LOG/10
  double eccMin;                 ///< eccentricity min
  double eccMax;                 ///< eccentricity max
  unsigned int eccDistribution;  ///< 0=UNIFORM, 1=LOG/10
  double psiMin;                 ///< psi min
  double psiMax;                 ///< psi max
  unsigned int psiDistribution;  ///< 0=UNIFORM, 1=LOG/10
  
  /**
   * @brief Draws an amplitude parameter.
   */
  double GetAmplitude(void);
  
  /**
   * @brief Draws a sigma parameter.
   */
  double GetSigma(void);

  /**
   * @brief Draws a frequency parameter.
   */
  double GetFrequency(void);

  /**
   * @brief Draws a eccentricity parameter.
   */
  double GetEccentricity(void);

  /**
   * @brief Draws a polarization parameter.
   */
  double GetPolarization(void);
  
  
  ClassDef(InjGen,0)
};

#endif


