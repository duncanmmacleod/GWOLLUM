/**
 * @file 
 * @brief Inject signals in a data stream.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjEct__
#define __InjEct__

#include "Streams.h"
#include "InjRea.h"
#include "GwollumPlot.h"

using namespace std;

/**
 * @brief Inject signals in a data stream.
 * @details This class injects signal waveforms in a data vector (Inject()) using injection files produced with InjGen.
 * The waveform, \f$h_{+}\f$ and \f$h_{times}\f$, is projected on the detector (\f$h_{det}\f$) and is added to the data.
 * @author Florent Robinet
 */
class InjEct: public InjRea, public GwollumPlot {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjEct class.
   * @details Injections listed in the ROOT files designated by a file pattern are loaded with InjRea.
   *
   * @param[in] aStream Input data stream in which to inject.
   * @param[in] aPattern Injection file pattern.
   * @param[in] aVerbose Verbosity level.
   */
  InjEct(Streams* aStream, const string aPattern, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjEct class.
   */
  virtual ~InjEct();
  /**
     @}
  */

  /**
   * @brief Updates the injection sampling frequency.
   * @details Use this function whenever the native sampling frequency of the input stream is changed.
   * Indeed, the size of injection containers must be updated.
   */
  void UpdateNativeSamplingFrequency(void);
 
  /**
   * @brief Injects in a data vector.
   * @details All the injection overlapping the data segments are injected in the data vector:
   * - The injection waveform is calculated (supported: GenerateSineGauss()).
   * - The injection waveform is projected in the detector: Project().
   * - the waveform is added to the data.
   *
   * @warning Call UpdateNativeSamplingFrequency() first to make sure the injection sampling frequency matches the data sampling frequency.
   * @param[in] aDataSize Input vector size.
   * @param[in,out] aData Pointer to input data vector (time-domain).
   * @param[in] aTimeStart GPS time of first sample.
   * @todo Implement the Gaussian injections.
   */
  bool Inject(const unsigned int aDataSize, double *aData, const double aTimeStart);
 
  /**
   * @brief Plots a given injection waveforms.
   * @details Three waveforms are plotted:
   * - h+ (geocentric time)
   * - hx (geocentric time)
   * - hdet projected in the detector (local time)
   *
   * @param[in] aInjIndex Injection index.
   * 
   */
  bool PlotInjection(const Long64_t aInjIndex);

 private:

  Streams *InStream;     ///< Input stream (DO NOT DELETE).

  // INJECTION
  unsigned int sampling; ///< Injection sampling frequency.
  double *window;        ///< Tukey window.
  TGraph *hplus;         ///< Injection waveform \f$h_{+}\f$.
  TGraph *hcross;        ///< Injection waveform \f$h_{times}\f$.
  TGraph *hdet;          ///< Injection waveform projected on the detector \f$h_{det}\f$.
    
  /**
   * @brief Generates a SineGauss waveform for the current injection: \f$h_{+}\f$ and \f$h_{times}\f$.
   */
  void GenerateSineGauss(void);
  
  /**
   * @brief Project waveform onto the detector: \f$h_{det}\f$.
   */
  void Project(void);

  ClassDef(InjEct,0)
};

#endif


