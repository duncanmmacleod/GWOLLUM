/**
 * @file 
 * @brief See InjTre.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjTre.h"

ClassImp(InjTre)


/////////////////////////////////////////////////////////////////////////
InjTre::InjTre(const string aInjType, const unsigned int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  Verbose=aVerbose;

  // random id
  srand(time(NULL));
  randid = rand();
  ostringstream tmpstream;
  tmpstream<<randid;
  srandid = tmpstream.str();

  // default values
  inj_ra = 0.0;
  inj_dec = 0.0;
  inj_time = 0.0;
  inj_ecc = 0.0;
  inj_psi = 0.0;
  inj_amp = 0.0;
  inj_sigma = 0.0;
  inj_f0 = 0.0;

  // injection TTree
  InjTree = new TTree((aInjType+"_"+srandid).c_str(), aInjType.c_str());
  InjTree->Branch("ra",           &inj_ra,    "ra/D");
  InjTree->Branch("dec",          &inj_dec,   "dec/D");
  InjTree->Branch("time",         &inj_time,  "time/D");
  InjTree->Branch("eccentricity", &inj_ecc,   "eccentricity/D");
  InjTree->Branch("polarization", &inj_psi,   "polarization/D");
  InjTree->Branch("amplitude",    &inj_amp,   "amplitude/D");
  InjTree->Branch("sigma",        &inj_sigma, "sigma/D");

  // waveform specific
  if(!aInjType.compare("SineGauss"))
    InjTree->Branch("frequency",  &inj_f0,    "frequency/D");

}

/////////////////////////////////////////////////////////////////////////
InjTre::~InjTre(void){
/////////////////////////////////////////////////////////////////////////
  delete InjTree;
}

/////////////////////////////////////////////////////////////////////////
unsigned int InjTre::AddGauss(const double aRa, 
                              const double aDec, 
                              const double aTime, 
                              const double aEccentricity, 
                              const double aPolarization, 
                              const double aAmplitude, 
                              const double aSigma){
/////////////////////////////////////////////////////////////////////////
  if(((string)InjTree->GetTitle()).compare("Gauss")){
    cerr<<"InjTre::AddGauss: the InjTre object was not constructed for Gauss-type injections"<<endl;
    return 0;
  }
  inj_ra=aRa;
  inj_dec=aDec;
  inj_time=aTime;
  inj_ecc=aEccentricity;
  inj_psi=aPolarization;
  inj_amp=aAmplitude;
  inj_sigma=aSigma;
  InjTree->Fill();
  return (unsigned int)InjTree->GetEntries();
}

/////////////////////////////////////////////////////////////////////////
unsigned int InjTre::AddSineGauss(const double aRa, 
                                  const double aDec, 
                                  const double aTime, 
                                  const double aEccentricity, 
                                  const double aPolarization, 
                                  const double aAmplitude, 
                                  const double aSigma, 
                                  const double aFrequency){
/////////////////////////////////////////////////////////////////////////
  if(((string)InjTree->GetTitle()).compare("SineGauss")){
    cerr<<"InjTre::AddGauss: the InjTre object was not constructed for SineGauss-type injections"<<endl;
    return 0;
  }

  inj_ra=aRa;
  inj_dec=aDec;
  inj_time=aTime;
  inj_ecc=aEccentricity;
  inj_psi=aPolarization;
  inj_amp=aAmplitude;
  inj_sigma=aSigma;
  inj_f0=aFrequency;
  InjTree->Fill();
  return (unsigned int)InjTree->GetEntries();
}

/////////////////////////////////////////////////////////////////////////
bool InjTre::Write(const string aRootFileName){
/////////////////////////////////////////////////////////////////////////
  TFile finj(aRootFileName.c_str(),"recreate");
  if(finj.IsZombie()) return false;
  finj.cd();
  InjTree->Write(InjTree->GetTitle());
  finj.Close();
  return true;
}







