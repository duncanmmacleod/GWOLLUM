/**
 * @file 
 * @brief See InjEct.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjEct.h"

ClassImp(InjEct)

/////////////////////////////////////////////////////////////////////////
InjEct::InjEct(Streams* aStream, const string aPattern, const unsigned int aVerbose):
InjRea(aPattern, aVerbose), GwollumPlot("INJ"){ 
/////////////////////////////////////////////////////////////////////////

  // for plotting
  SetGridx();  SetGridy();
  
  InStream = aStream;
  
  // dummy containers
  sampling = 0;
  window = new double [0];
  hplus  = new TGraph(0); hplus->SetName("hplus");
  hcross = new TGraph(0); hcross->SetName("hcross");
  hdet   = new TGraph(0); hdet->SetName("hdet");

  // real containers
  UpdateNativeSamplingFrequency();
}

/////////////////////////////////////////////////////////////////////////
InjEct::~InjEct(void){
/////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"InjEct::~InjEct"<<endl;
  delete hplus;
  delete hcross;
  delete hdet;
  delete window;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::UpdateNativeSamplingFrequency(void){
/////////////////////////////////////////////////////////////////////////

  // nothing to do
  if(InStream->GetNativeFrequency()==sampling) return;

  delete window;
  delete hplus;
  delete hcross;
  delete hdet;

  sampling=InStream->GetNativeFrequency();
  
  // injection size = 10 sigma ???
  unsigned int injsize = (unsigned int)TMath::Max(0.0, ceil(10*GetInjectionSigmaMax()*sampling));

  // tukey window 10%
  window=GetTukeyWindow(injsize, 0.1);
  
  // injection waveform (+)
  hplus  = new TGraph(injsize); hplus->SetName("hplus");
  hplus->GetXaxis()->SetNoExponent();
  hplus->SetTitle("hplus");
  hplus->SetMarkerColor(2);
  hplus->SetLineColor(2);
  
  // injection waveform (x)
  hcross = new TGraph(injsize); hcross->SetName("hcross");
  hcross->SetTitle("hcross");
  hcross->SetMarkerColor(4);
  hcross->SetLineColor(4);

  // injection waveform projected in the detector (antenna factors)
  hdet   = new TGraph(injsize); hdet->SetName("hdet");
  hdet->SetTitle("hdet");
  hdet->SetMarkerColor(5);
  hdet->SetLineColor(5);

  return;
}

/////////////////////////////////////////////////////////////////////////
bool InjEct::Inject(const unsigned int aDataSize, double *aData, const double aTimeStart){
/////////////////////////////////////////////////////////////////////////
  if(InStream->GetNativeFrequency()!=sampling){
    cerr<<"InjEct::Inject: the native sampling frequency has changed --> cannot inject"<<endl;
    return false;
  }

  // block size
  double start = aTimeStart - (double)hplus->GetN()/2.0/(double)sampling;
  double stop  = aTimeStart + (double)aDataSize/(double)sampling + (double)hplus->GetN()/2.0/(double)sampling;

  // injection index range
  Long64_t injindex_start = GetFirstInjectionIndex(start, stop);
  Long64_t injindex_stop  = GetLastInjectionIndex(start, stop);

  // nothing to inject  
  if(injindex_start<0) return true;
  if(injindex_stop<0)  return true;

  double tlocal;// local gps time
  int dataindex;

  for(Long64_t i=injindex_start; i<=injindex_stop; i++){

    // load injection
    if(LoadInjection(i)<=0) return false;

    // compute hplus and hcross
    if(!GetInjectionType().compare("SineGauss")) GenerateSineGauss();
    else return false;

    // project onto the detector --> hdet
    Project();

    // add injection waveform
    for(unsigned int t=0; t<(unsigned int)hdet->GetN(); t++){

      // absolute local time
      tlocal = hdet->GetX()[t] + GetInjectionTime();

      // check again range
      if(tlocal<aTimeStart) continue;

      // data index
      dataindex=(tlocal-aTimeStart)*sampling;
      if(dataindex<0) continue; //before data vector
      if((unsigned int)dataindex>=aDataSize) break; //after data vector
      
      aData[dataindex]+=hdet->GetY()[t];
    }
  }

  return true;  
}

/////////////////////////////////////////////////////////////////////////
bool InjEct::PlotInjection(const Long64_t aInjIndex){
/////////////////////////////////////////////////////////////////////////

  if(LoadInjection(aInjIndex)<=0) return false;

  // compute hplus and hcross
  if(!GetInjectionType().compare("SineGauss")) GenerateSineGauss();
  else return false;

  // project onto the detector --> hdet
  Project();

  // plot
  Draw(hplus, "APL");
  Draw(hcross, "PLsame");
  Draw(hdet, "PLsame");
  return true;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::GenerateSineGauss(void){
/////////////////////////////////////////////////////////////////////////
  // This function was taken from lalsimulation/src/LALSimBurst.c

  // "peak" amplitudes of plus and cross
  double h0plus  = GetSineGaussh0plus();
  double h0cross = GetSineGaussh0cross();

  // parameters
  double sigma  = 2.0 * GetInjectionSigma()*GetInjectionSigma();
  double factor = 2.0 * TMath::Pi() * GetInjectionFrequency();
  
  // make waveforms centered on 0
  double t;
  for(unsigned int i=0; i<(unsigned int)hplus->GetN(); i++){
    t = ((double)i-(double)hplus->GetN()/2.0)/(double)sampling;
    hplus->SetPoint(i,t, h0plus * exp(-t*t/sigma) * cos(factor*t));
    hcross->SetPoint(i,t, h0cross * exp(-t*t/sigma) * sin(factor*t));
  }
  
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::Project(void){
/////////////////////////////////////////////////////////////////////////

  double tgeo; // geocentric time
  double tlocal; // local time
  double fplus, fcross;// antenna factors
 
  // fill hdet
  for(unsigned int i=0; i<(unsigned int)hplus->GetN(); i++){

    // geocentric time
    tgeo = GetInjectionTime() + ((double)i-(double)hplus->GetN()/2.0)/(double)sampling;
    
    // local time
    tlocal=InStream->GetLocalTime(GetInjectionRa(), GetInjectionDec(), tgeo);

    // antenna factors
    InStream->GetDetectorAMResponse(fplus, fcross,
                                    GetInjectionRa(), GetInjectionDec(), GetInjectionPolarization(),
                                    GreenwichMeanSiderealTime(tlocal));
    
    // populate hdet
    hdet->SetPoint(i, tlocal-GetInjectionTime(),
                   fplus * hplus->GetY()[i] + fcross * hcross->GetY()[i]);
    
  }

  return;
}

