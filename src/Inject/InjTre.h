/**
 * @file 
 * @brief Manage injection TTrees.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjTre__
#define __InjTre__


#include "CUtils.h"
#include <TFile.h>
#include <TTree.h>

using namespace std;

/**
 * @brief Manage injection TTrees.
 * @author Florent Robinet
 */
class InjTre {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjTre class.
   * @details The injection type must be provided:
   * - "Gauss" for Gaussian injections
   * - "SineGauss" for sine-Gaussian injections
   *
   * @param[in] aInjType Injection type.
   * @param[in] aVerbose Verbosity level.
   */
  InjTre(const string aInjType, const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the InjTre class.
   */
  virtual ~InjTre();
  /**
     @}
  */

  /**
   * @brief Adds a Gauss-type injection.
   * @returns The current number of injections in TTree, 0 if this function fails.
   * @param[in] aRa Injection right ascension [rad].
   * @param[in] aDec Injection declination [rad].
   * @param[in] aTime Injection GPS time.
   * @param[in] aEccentricity Injection eccentricity.
   * @param[in] aPolarization Injection polarization angle [rad].
   * @param[in] aAmplitude Injection amplitude.
   * @param[in] aSigma Injection sigma [s].
   */
  unsigned int AddGauss(const double aRa, 
                        const double aDec, 
                        const double aTime, 
                        const double aEccentricity, 
                        const double aPolarization, 
                        const double aAmplitude, 
                        const double aSigma);

  /**
   * @brief Adds a SineGauss-type injection.
   * @returns The current number of injections in TTree, 0 if this function fails.
   * @param[in] aRa Injection right ascension [rad].
   * @param[in] aDec Injection declination [rad].
   * @param[in] aTime Injection GPS time.
   * @param[in] aEccentricity Injection eccentricity.
   * @param[in] aPolarization Injection polarization angle [rad].
   * @param[in] aAmplitude Injection amplitude.
   * @param[in] aSigma Injection sigma [s].
   * @param[in] aFrequency Injection frequency [Hz].
   */
  unsigned int AddSineGauss(const double aRa, 
                            const double aDec, 
                            const double aTime, 
                            const double aEccentricity, 
                            const double aPolarization, 
                            const double aAmplitude, 
                            const double aSigma, 
                            const double aFrequency);

  /**
   * @brief Returns the current number of injections.
   */
  inline unsigned int GetNInjections(void){ return (unsigned int)InjTree->GetEntries(); };

  /**
   * @brief Returns the injection type.
   */
  inline string GetInjectionType(void){ return (string)InjTree->GetTitle(); };

  /**
   * @brief Resets the injection TTree.
   */
  inline void Reset(void){ InjTree->Reset(); };

  /**
   * @brief Writes injections to disk.
   * @param[in] aRootFileName Output file name.
   */  
  bool Write(const string aRootFileName="./myinjections.root");
  

 protected:
  
  unsigned int Verbose; ///< verbosity level
  int randid;           ///< Random integer to identify the class object.
  string srandid;       ///< Random integer (string) to identify the class object.

 private:  

  TTree *InjTree;       ///< Injection tree.
  double inj_ra,        ///< Injection right ascension.
    inj_dec,            ///< Injection declination.
    inj_psi,            ///< Injection polarization angle.
    inj_time,           ///< Injection time.
    inj_amp,            ///< Injection amplitude.
    inj_f0,             ///< Injection frequency.
    inj_sigma,          ///< Injection sigma.
    inj_ecc;            ///< Injection eccentricity.

  ClassDef(InjTre,0)
};

#endif


