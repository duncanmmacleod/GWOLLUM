/**
 * @file 
 * @brief Read a set of injections.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjRea__
#define __InjRea__

#include <TChain.h>
#include <TH1.h>
#include <TMath.h>
#include "CUtils.h"

using namespace std;

/**
 * @brief Read a set of injections.
 * @details Injection parameters, generated with the InjGen class, are loaded.
 * @author Florent Robinet
 */
class InjRea {

  friend class InjRec;///< used to clone InjTree

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjRea class.
   * @details Injections in ROOT files designated by a file pattern are loaded.
   * Each injection is tagged to 'true' by default.
   * Injection files must be generated with the InjGen class.
   * @param aPattern Injection file pattern.
   * @param aVerbose Verbosity level.
   */
  InjRea(const string aPattern, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjRea class.
   */
  virtual ~InjRea();
  /**
     @}
  */
  
  /**
   * @brief Loads a given injection.
   * @details This function overloads the GetEntry() function of the TTree class.
   * @returns The function returns the number of bytes read from the input buffer. If entry does not exist the function returns 0. If an I/O error occurs, the function returns -1.
   * @param[in] aInjIndex Injection index.
   */
  inline int LoadInjection(const Long64_t aInjIndex){
    return InjTree->GetEntry(aInjIndex);
  };

  /**
   * @brief Returns the SineGauss \f$h_{+rss}\f$ peak amplitude of current injection.
   */
  double GetSineGaussh0plus(void);

  /**
   * @brief Returns the SineGauss \f$h_{\times rss}\f$ peak amplitude of current injection.
   */
  double GetSineGaussh0cross(void);

  /**
   * @brief Returns the injection time.
   */
  inline double GetInjectionTime(void){ return inj_time; };

  /**
   * @brief Returns the injection right-ascension.
   */
  inline double GetInjectionRa(void){ return inj_ra; };

  /**
   * @brief Returns the injection declination.
   */
  inline double GetInjectionDec(void){ return inj_dec; };

  /**
   * @brief Returns the injection eccentricity.
   */
  inline double GetInjectionEccentricity(void){ return inj_ecc; };

  /**
   * @brief Returns the injection polarization.
   */
  inline double GetInjectionPolarization(void){ return inj_psi; };

  /**
   * @brief Returns the injection amplitude.
   */
  inline double GetInjectionAmplitude(void){ return inj_amp; };

  /**
   * @brief Returns the minimum injection amplitude.
   */
  inline double GetInjectionAmplitudeMin(void){ return inj_ampmin; };

  /**
   * @brief Returns the maximum injection amplitude.
   */
  inline double GetInjectionAmplitudeMax(void){ return inj_ampmax; };
  
  /**
   * @brief Returns the injection sigma.
   */
  inline double GetInjectionSigma(void){ return inj_sigma; };

  /**
   * @brief Returns the minimum injection sigma.
   */
  inline double GetInjectionSigmaMin(void){ return inj_sigmamin; };

  /**
   * @brief Returns the maximum injection sigma.
   */
  inline double GetInjectionSigmaMax(void){ return inj_sigmamax; };
  
  /**
   * @brief Returns the injection frequency.
   */
  inline double GetInjectionFrequency(void){ return inj_f0; };

  /**
   * @brief Returns the minimum injection frequency.
   */
  inline double GetInjectionFrequencyMin(void){ return inj_f0min; };

  /**
   * @brief Returns the maximum injection frequency.
   */
  inline double GetInjectionFrequencyMax(void){ return inj_f0max; };

  /**
   * @brief Returns the injection tag.
   */
  inline bool GetInjectionTag(void){ return inj_tag[InjTree->GetReadEntry()]; };

  /**
   * @brief Sets a new tag value to the current injection.
   * @param[in] aTag New tag value.
   */
  inline void SetInjectionTag(const bool aTag){ inj_tag[InjTree->GetReadEntry()]=aTag; };

  /**
   * @brief Returns the first injection index included in a given time range.
   * @returns -1 if no injection is found in this time range.
   * @warning This function only works if the input injections are time-sorted!!!
   * @param[in] aTimeStart GPS range start.
   * @param[in] aTimeEnd GPS range stop.
   */
  Long64_t GetFirstInjectionIndex(const double aTimeStart, const double aTimeEnd);
  
  /**
   * @brief Returns the last injection index included in a given time range.
   * @returns -1 if no injection is found in this time range.
   * @param[in] aTimeStart GPS range start.
   * @param[in] aTimeEnd GPS range stop.
   */
  Long64_t GetLastInjectionIndex(const double aTimeStart, const double aTimeEnd);
    
  /**
   * @brief Returns the number of injections.
   */
  inline Long64_t GetN(void){ return InjTree->GetEntries(); };
  
  /**
   * @brief Returns the injection type.
   */
  inline string GetInjectionType(void){ return (string)InjTree->GetName(); };
 
  /**
   * @brief Returns the 1D distribution of an injection parameter.
   * @note The returned histograms must be deleted by the user.
   * @warning Only injections tagged to true are considered.
   * @param[in] aParamName Parameter name. Currently supported: "amplitude" and "f0".
   * @param[in] aNbins Number of bins (must be >0).
   * @param[in] aBinType Binning type. Currently supported: "UNIFORM" and "LOG".
   */
  TH1D* GetInjectionParamDist(const string aParamName, const unsigned int aNbins=1, const string aBinType="UNIFORM");

  /**
   * @brief Returns the input file pattern provided in the constructor.
   */
  inline string GetInputFilePattern(void){ return pattern; };

protected:
  
  unsigned int Verbose; ///< Verbosity level.
 
 private:
  
  string pattern;       ///< Input file pattern.

  TChain *InjTree;      ///< Injection tree.
  double inj_ra;        ///< Injection right ascension.
  double inj_dec;       ///< Injection declination.
  double inj_psi;       ///< Injection polarization angle.
  double inj_time;      ///< Injection time.
  double inj_amp;       ///< Injection amplitude.
  double inj_f0;        ///< Injection frequency.
  double inj_sigma;     ///< Injection sigma.
  double inj_ecc;       ///< Injection eccentricity.

  double inj_timemin;   ///< Time min.
  double inj_timemax;   ///< Time max.
  double inj_sigmamin;  ///< Sigma min.
  double inj_sigmamax;  ///< Sigma max.
  double inj_ampmin;    ///< Amplitude min.
  double inj_ampmax;    ///< Amplitude max.
  double inj_f0min;     ///< Frequency min.
  double inj_f0max;     ///< Frequency max.
 
  bool *inj_tag;        ///< Injection tags.
 
  ClassDef(InjRea,0)
};

#endif


