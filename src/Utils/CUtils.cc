/**
 * @file 
 * @brief See CUtils.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "CUtils.h"

/**
 * @brief Maps the length of a window and the offset within the window to the "y"
 * co-ordinate.
 * @details 
 * Input:
 * size > 0,
 * 0 <= i < size
 *
 * Output:
 * size < 2 --> return 0.0
 * i == 0 --> return -1.0
 * i == (size - 1) / 2 --> return 0.0
 * i == size - 1 --> return +1.0
 *
 * e.g., size = 5 (odd), then i == 2 --> return 0.0
 * if size = 6 (even), then i == 2.5 --> return 0.0
 *
 * (in the latter case, obviously i can't be a non-integer, but that's the
 * value it would have to be for this function to return 0.0)
 * @param[in] aSize Window size.
 * @param[in] aI Window size.
 */
static double Y(int aSize, const int aI){
	aSize -= 1;
	return aSize > 0 ? (2*aI-aSize) / (double)aSize : 0;
}


//////////////////////////////////////////////////////////////////////////////
double medianbiasfactor(const unsigned int nn){
//////////////////////////////////////////////////////////////////////////////

  const unsigned int nmax = 1000;
  double ans = 1.0;
  int n = ((int)nn - 1)/2;
  
  if(nn >= nmax) return LN2;// asymptotic

  for (int i = 1; i <= n; ++i){
    ans -= 1.0/(double)(2*i);
    ans += 1.0/(double)(2*i + 1);
  }

  return ans;
}
//////////////////////////////////////////////////////////////////////////////
vector<string> SplitString(const string stringtodivide, const char separator){  
//////////////////////////////////////////////////////////////////////////////

  vector<string> elements;
  vector<string> elements_with_tabs;
  stringstream ss(stringtodivide);
  string item;
  
  while(getline(ss, item, separator)) 
    if(item.size()) elements.push_back(item);

  // special case of space (a tab = a space)
  if(separator==' '){
    for(unsigned int i=0; i<elements.size(); i++){
      stringstream ss_tab(elements[i]);
      while(getline(ss_tab, item, '\t')) 
	if(item.size()) elements_with_tabs.push_back(item);
    }
  }
  else return elements;

  return elements_with_tabs;
}

//////////////////////////////////////////////////////////////////////////////
string StringToUpper(string stringtoconvert){
//////////////////////////////////////////////////////////////////////////////
  string uppers="";
  for (string::iterator p = stringtoconvert.begin(); stringtoconvert.end() != p; ++p)
    uppers += toupper(*p);
  
  return uppers;
}

//////////////////////////////////////////////////////////////////////////////
string ReplaceAll(string str, const string& from, const string& to){
//////////////////////////////////////////////////////////////////////////////
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
  }
  return str;
}

//////////////////////////////////////////////////////////////////////////////
bool IsDirectory(const char* dirname){
//////////////////////////////////////////////////////////////////////////////
  DIR* dp=NULL; dp = opendir(dirname);
  if(dp == NULL) return false;
  closedir(dp);
  return true;
}

//////////////////////////////////////////////////////////////////////////////
bool ListDirectories(vector <string> &subdir, const string maindir){
//////////////////////////////////////////////////////////////////////////////
  subdir.clear();
  DIR *dir;
  struct dirent *entry;

  if((dir = opendir(maindir.c_str())) == NULL){
    cerr<<"ListDirectories(): failed opening directory "<<maindir<<endl;
    return false;
  }
  else{
    if(closedir(dir) < 0){
      cerr<<"ListDirectories(): failed closing directory "<<maindir<<endl;
      return false;
    }
    else{
      dir = opendir(maindir.c_str());
      entry = readdir(dir);
      while (entry != NULL){
	if(IsDirectory(maindir+"/"+(string)entry->d_name)){
	  if(((string)(entry->d_name)).compare(".")&&((string)(entry->d_name)).compare(".."))
	    subdir.push_back((string)(entry->d_name));
	}
	entry = readdir(dir);
      }
      closedir(dir);
    }
  }
  
  return true;
}

//////////////////////////////////////////////////////////////////////////////
bool IsTextFile(const char* filename){
//////////////////////////////////////////////////////////////////////////////

  FILE* fp=NULL; fp = fopen(filename,"r");
  if(fp == NULL) return false;
  fclose(fp);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
bool IsBinaryFile(const char* filename){
//////////////////////////////////////////////////////////////////////////////
  FILE* fp=NULL; fp = fopen(filename,"rb");
  if(fp == NULL) return false;
  fclose(fp);
  return true;
}

//////////////////////////////////////////////////////////////////////////////
vector<string> Glob(const char* pattern){
//////////////////////////////////////////////////////////////////////////////

  glob_t globbuf;

  globbuf.gl_offs = 2;
  glob(pattern, GLOB_TILDE|GLOB_BRACE|GLOB_MARK, NULL, &globbuf);

  vector<string> results;
  for(unsigned i=0; i<globbuf.gl_pathc; i++) {
    results.push_back(globbuf.gl_pathv[i]);
  }
  
  globfree(&globbuf);
  return results;
}

////////////////////////////////////////////////////////////////////////////////////
double* GetTukeyWindow(const unsigned int aSize, const double aAlpha){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int transition = round(aAlpha*aSize);
  double *Window = new double [aSize];
  unsigned int i;
  
  /* 1.0 and flat in the middle, cos^2 transition at each end, zero
   * at end points, 0.0 <= beta <= 1.0 sets what fraction of the
   * window is transition (0 --> rectangle window, 1 --> Hann window)
   */
  for(i=0; i<(transition+1)/2; i++)
    Window[i] = Window[aSize-1-i] = pow(cos(PIOVERTWO * Y((int)transition, (int)i)), 2);
  for(; i<(aSize + 1)/2; i++)
    Window[i] = Window[aSize-1-i] = 1.0;
  
  return Window;
}

//////////////////////////////////////////////////////////////////////////////
template <typename Word>
std::ostream& write_word( std::ostream& outs, Word value, unsigned size = sizeof( Word )){
//////////////////////////////////////////////////////////////////////////////
  for (; size; --size, value >>= 8)
    outs.put( static_cast <char> (value & 0xFF) );
  return outs;
}

//////////////////////////////////////////////////////////////////////////////
void MakeStereoSoundFile(const string aFileName, const unsigned int aN, const unsigned int aSamplingRate,
			 double *aAmplitudeLeft, double *aAmplitudeRight,
			 const double aScale, const int aOffset){
//////////////////////////////////////////////////////////////////////////////

  ofstream outf(aFileName.c_str(), ios::binary);

  // Write the file headers
  outf<<"RIFF----WAVEfmt ";     // (chunk size to be filled in later)
  write_word(outf,     16, 4);  // no extension data
  write_word(outf,      1, 2);  // PCM - integer samples
  write_word(outf,      2, 2);  // two channels (stereo file)
  write_word(outf, aSamplingRate, 4);  // samples per second (Hz)
  write_word(outf, aSamplingRate*16*2/8, 4);  // (Sample Rate * BitsPerSample * Channels) / 8
  write_word(outf,      4, 2);  // data block size (size of two integer samples, one for each channel, in bytes)
  write_word(outf,     16, 2);  // number of bits per sample (use a multiple of 8)

  // Write the data chunk header
  size_t data_chunk_pos = outf.tellp();
  outf<<"data----";  // (chunk size to be filled in later)
  
  // generate sound wave
  for(unsigned int n=0; n<aN; n++){
    write_word(outf, (int)(aAmplitudeLeft[n+aOffset]*aScale), 2); // left
    write_word(outf, (int)(aAmplitudeRight[n+aOffset]*aScale), 2); // right
  }
  
  // (We'll need the final file size to fix the chunk sizes above)
  size_t file_length = outf.tellp();

  // Fix the data chunk header to contain the data size
  outf.seekp(data_chunk_pos + 4);
  write_word(outf, file_length - data_chunk_pos + 8);

  // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8) bytes
  outf.seekp(0 + 4);
  write_word(outf, file_length-8, 4);

  outf.close();
  return;
}
