/**
 * @file 
 * @brief See GwollumPlot.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "GwollumPlot.h"

ClassImp(GwollumPlot)

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::GwollumPlot(const string aName, const string aStyleName){
////////////////////////////////////////////////////////////////////////////////////
  
  // random id
  srand(time(NULL));
  randid = rand();
  ostringstream tmpstream;
  tmpstream<<randid;
  srandid = tmpstream.str();

  // create style object
  Wstyle = new TStyle((aName+"_style_"+srandid).c_str(),(aName+"_style").c_str());

  // create working canvas and pad
  Wcan = new TCanvas((aName+"_can_"+srandid).c_str(),(aName+"_can").c_str(),0,0,750,500);
  Wcan->Draw();
  Wpad = new TPad((aName+"_pad_"+srandid).c_str(),(aName+"_pad").c_str(),0,0,1,1);
  Wpad->Draw();
  Wpad->cd();

  // set style
  if(!aStyleName.compare("GWOLLUM")) SetGwollumStyle();
  else if(!aStyleName.compare("FIRE")) SetFireStyle();
  else if(!aStyleName.compare("PINK")) SetPinkStyle();
  else if(!aStyleName.compare("STANDARD")) SetStandardStyle();
  else{
    cerr<<"GwollumPlot::GwollumPlot: "+aStyleName+" style is not supported"<<endl;
    cerr<<"GwollumPlot::GwollumPlot: use GWOLLUM style by default"<<endl;
    SetGwollumStyle();
  }
   
  // legends
  Wleg = new TLegend(0.8, 0.1, 0.98, 0.9);
  Wleg->SetTextColor(Wstyle->GetTextColor());// this is needed. bug in root?

  // additional text
  Wtext = new TText();
  Wtext->SetNDC(true);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::~GwollumPlot(void){
////////////////////////////////////////////////////////////////////////////////////
  delete Wtext;
  delete Wpad;
  delete Wcan;
  delete Wleg;
  delete Wstyle;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddText(const string aText,
                          const double aX, const double aY, const double aSize,
                          const int aPadIndex){
////////////////////////////////////////////////////////////////////////////////////
  Wpad->cd(aPadIndex); 
  Wtext->SetText(aX, aY, aText.c_str());
  Wtext->SetTextSize(aSize);
  Wtext->Draw("same");
  gPad->Modified(); 
  gPad->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::UpdateText(const string aText){
////////////////////////////////////////////////////////////////////////////////////
  Wtext->SetText(Wtext->GetX(), Wtext->GetY(), aText.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendEntry(const TObject *aObj, const string aLabel, const string aStyle){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->AddEntry(aObj,aLabel.c_str(),aStyle.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendHeader(const string aLabel){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->SetHeader(aLabel.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::DrawLegend(void){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  Wpad->SetPad(0,0,0.92,1);
  Wpad->Modified(); gPad->Update();
  Wleg->Draw("same");
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const double aScaleFactor){
////////////////////////////////////////////////////////////////////////////////////
  if(aScaleFactor<=0) return;
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  unsigned int newWidth=(unsigned int)ceil(aScaleFactor*wdth);
  unsigned int newHeight=(unsigned int)ceil(aScaleFactor*hght);
  ResizePlot(newWidth, newHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const unsigned int aNewWidth, const unsigned int aNewHeight){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  ResizePlot(aNewWidth, aNewHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetGwollumStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="GWOLLUM";
  Wstyle->SetFillColor(1);
  Wstyle->SetCanvasColor(1);
  Wstyle->SetPadColor(1);
  Wstyle->SetPadRightMargin(0.16);
  Wstyle->SetMarkerColor(7);
  Wstyle->SetLineColor(7);
  Wstyle->SetFuncColor(7);
  Wstyle->SetTitleBorderSize(0);

  // Frame
  Wstyle->SetFrameFillColor(1);
  Wstyle->SetFrameLineColor(0);
  Wstyle->SetLegendFillColor(1);
  Wstyle->SetStatColor(1);
  Wstyle->SetStatTextColor(0);

  // axis
  Wstyle->SetPalette(51);
  Wstyle->SetNumberContours(104);
  Wstyle->SetAxisColor(0,"XYZ");
  Wstyle->SetLabelColor(0,"XYZ");
  Wstyle->SetTitleColor(0,"XYZ");
  Wstyle->SetGridColor(0);

  // histos
  Wstyle->SetOptStat(0);
  Wstyle->SetTitleTextColor(0);
  Wstyle->SetTitleFillColor(1);
  Wstyle->SetHistLineColor(7);
  Wstyle->SetHistLineWidth(2);
  //Wstyle->SetHistFillColor(0);

  // text
  Wstyle->SetTextColor(0);

  Wstyle->cd();
  Wcan->UseCurrentStyle();
  Wcan->Modified(); Wcan->Update();
  Wpad->UseCurrentStyle();
  Wpad->Modified(); Wpad->Update();
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetPinkStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="PINK";
  Wstyle->SetFillColor(1);
  Wstyle->SetCanvasColor(1);
  Wstyle->SetPadColor(1);
  Wstyle->SetPadRightMargin(0.16);
  Wstyle->SetMarkerColor(6);
  Wstyle->SetLineColor(6);
  Wstyle->SetFuncColor(6);
  Wstyle->SetTitleBorderSize(0);
 
  // Frame
  Wstyle->SetFrameFillColor(1);
  Wstyle->SetFrameLineColor(0);
  Wstyle->SetLegendFillColor(1);
  Wstyle->SetStatColor(1);
  Wstyle->SetStatTextColor(0);

  // axis
  Wstyle->SetPalette(81);
  Wstyle->SetNumberContours(104);
  Wstyle->SetAxisColor(0,"XYZ");
  Wstyle->SetAxisColor(1,"Z");
  Wstyle->SetLabelColor(0,"XYZ");
  Wstyle->SetTitleColor(0,"XYZ");
  Wstyle->SetGridColor(0);

  // histos
  Wstyle->SetOptStat(0);
  Wstyle->SetTitleTextColor(0);
  Wstyle->SetTitleFillColor(1);
  Wstyle->SetHistLineColor(6);
  Wstyle->SetHistLineWidth(2);

  // text
  Wstyle->SetTextColor(0);
  
  Wstyle->cd();
  Wcan->UseCurrentStyle();
  Wcan->Modified(); Wcan->Update();
  Wpad->UseCurrentStyle();
  Wpad->Modified(); Wpad->Update();
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetFireStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="FIRE";
  Wstyle->SetFillColor(1);
  Wstyle->SetCanvasColor(1);
  Wstyle->SetPadColor(1);
  Wstyle->SetPadRightMargin(0.16);
  Wstyle->SetMarkerColor(5);
  Wstyle->SetLineColor(5);
  Wstyle->SetFuncColor(5);
  Wstyle->SetTitleBorderSize(0);
 
  // Frame
  Wstyle->SetFrameFillColor(1);
  Wstyle->SetFrameLineColor(0);
  Wstyle->SetLegendFillColor(1);
  Wstyle->SetStatColor(1);
  Wstyle->SetStatTextColor(0);

  // axis
  Wstyle->SetPalette(53);
  Wstyle->SetNumberContours(104);
  Wstyle->SetAxisColor(0,"XYZ");
  Wstyle->SetAxisColor(1,"Z");
  Wstyle->SetLabelColor(0,"XYZ");
  Wstyle->SetTitleColor(0,"XYZ");
  Wstyle->SetGridColor(0);

  // histos
  Wstyle->SetOptStat(0);
  Wstyle->SetTitleTextColor(0);
  Wstyle->SetTitleFillColor(1);
  Wstyle->SetHistLineColor(5);
  Wstyle->SetHistLineWidth(2);

  // text
  Wstyle->SetTextColor(0);
  
  Wstyle->cd();
  Wcan->UseCurrentStyle();
  Wcan->Modified(); Wcan->Update();
  Wpad->UseCurrentStyle();
  Wpad->Modified(); Wpad->Update();
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetStandardStyle(void){
////////////////////////////////////////////////////////////////////////////////////
  
  // General
  stylename="STANDARD";
  Wstyle->SetFillColor(0);
  Wstyle->SetCanvasColor(0);
  Wstyle->SetPadColor(0);
  Wstyle->SetPadRightMargin(0.16);
  Wstyle->SetMarkerColor(kBlue+2);
  Wstyle->SetLineColor(kBlue+2);
  Wstyle->SetFuncColor(kBlue+2);
  Wstyle->SetTitleBorderSize(0);
 
  // Frame
  Wstyle->SetFrameFillColor(0);
  Wstyle->SetFrameLineColor(1);
  Wstyle->SetLegendFillColor(0);
  Wstyle->SetStatColor(0);
  Wstyle->SetStatTextColor(1);

  // axis
  Wstyle->SetNumberContours(104);
  Wstyle->SetPalette(55);
  Wstyle->SetAxisColor(1,"XYZ");
  Wstyle->SetLabelColor(1,"XYZ");
  Wstyle->SetTitleColor(1,"XYZ");
  Wstyle->SetGridColor(1);

  // histos
  Wstyle->SetOptStat(0);
  Wstyle->SetTitleTextColor(1);
  Wstyle->SetTitleFillColor(0);
  Wstyle->SetHistLineColor(kBlue+2);
  Wstyle->SetHistLineWidth(2);
  
  // text
  Wstyle->SetTextColor(1);

  Wstyle->cd();
  Wcan->UseCurrentStyle();
  Wcan->Modified(); Wcan->Update();
  Wpad->UseCurrentStyle();
  Wpad->Modified(); Wpad->Update();
}


/*
bool GwollumPlot::PrintLogo(Float_t v_scale, Float_t skew){

  if(gPad==NULL) return false;
  if(logopad!=NULL){ delete logopad; logopad=NULL; }
  if(logo!=NULL){ delete logo; logo=NULL; }

  // logo
  logo = TImage::Open("$GWOLLUM_DOC/Pics/gwollum_logo_min_trans.gif");
  if(!logo){
    logo=NULL;
    return false;
  }

  logo->SetConstRatio(kFALSE);
  UInt_t h_ = logo->GetHeight();
  UInt_t w_ = logo->GetWidth();
  
  Float_t r = w_/h_;
  Wpad->Update();
  Float_t rpad = Double_t(Wpad->VtoAbsPixel(0) - Wpad->VtoAbsPixel(1))/(Wpad->UtoAbsPixel(1) - Wpad->UtoAbsPixel(0));
  r *= rpad;
  
  Float_t d = 0.055;
  // absolute coordinates
  Float_t x1L = 0.01;
  Float_t y1T = 0.99;
  Float_t x1R = x1L + d*r/skew;
  Float_t y1B = y1T - d*v_scale*skew;
    
  logopad = new TPad("logopad", "logopad", x1L, y1B, x1R, y1T );
  logopad->SetRightMargin(0);
  logopad->SetBottomMargin(0);
  logopad->SetLeftMargin(0);
  logopad->SetTopMargin(0);
    
  Int_t xSizeInPixel = logopad->UtoAbsPixel(1) - logopad->UtoAbsPixel(0);
  Int_t ySizeInPixel = logopad->VtoAbsPixel(0) - logopad->VtoAbsPixel(1);
  if (xSizeInPixel<=25 || ySizeInPixel<=25) {
    delete logopad; logopad=NULL;
    return false; // ROOT doesn't draw smaller than this
  }

  logopad->Draw();
  logopad->cd();
  logo->Draw();


  return true;
} 
*/
