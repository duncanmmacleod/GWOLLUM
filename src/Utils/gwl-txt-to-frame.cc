/**
 * @file 
 * @brief Program to generate frame files from a time series saved in a text file.
 * @snippet this gwl-txt-to-frame-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadAscii.h"
#include <FrameL.h>

using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){
  
  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  if(argc<3){
    //! [gwl-txt-to-frame-usage]
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<"gwl-txt-to-frame [input text file] [channel name] [sampling rate] [time start]"<<endl; 
    cerr<<endl;
    cerr<<"  [input text file]: txt file with columns of data"<<endl; 
    cerr<<"  [channel name]   : name for the output channel"<<endl; 
    cerr<<"  [sampling rate]  : data sampling rate in Hz"<<endl; 
    cerr<<"  [time start]     : GPS time for the first sample"<<endl; 
    cerr<<""<<endl; 
    cerr<<"The text file must contain 1 or 2 columns."<<endl; 
    cerr<<""<<endl; 
    cerr<<"** 2 columns:"<<endl; 
    cerr<<"- first column = GPS time"<<endl; 
    cerr<<"- 2nd column   = channel value"<<endl; 
    cerr<<""<<endl; 
    cerr<<"** 1 column:"<<endl;
    cerr<<"- [sampling rate] and [time start] MUST be provided."<<endl; 
    cerr<<"- column data = channel value"<<endl; 
    cerr<<endl;
    //! [gwl-txt-to-frame-usage]
    return 1;
  }

  // get txt file
  string txtfilename=(string)argv[1];
  if(!IsTextFile(txtfilename)) return 1;
  cout<<"Input file name = "<<txtfilename<<endl;

  // get channel name
  string channelname=(string)argv[2];
  cout<<"Channel name = "<<channelname<<endl;

  // get sampling rate
  unsigned int sampling=0;
  if(argc>3){
    sampling = atoi(argv[3]);
    cout<<"Sampling = "<<sampling<<endl;
  }
  
  // get time start
  double tstart=0.0;
  if(argc>4){
    tstart = atof(argv[4]);
    cout<<"Data start = "<<tstart<<endl;
  }
  
  // load text file
  cout<<"Loading data..."<<endl;
  ReadAscii *RA = new ReadAscii(txtfilename);
  cout<<"... OK"<<endl;
  
  // extract columns
  vector <double> time_vect, val_vect;
  if(RA->GetNCol()==1){
    if(argc<=4){
      cerr<<"gwl-txt-to-frame: the sampling rate and a time start must be provided"<<endl;
      return 1;
    }
    if(!RA->SetFormat(0,'d')) return 2;
    if(!RA->GetCol(val_vect,0)) return 2;

    for(unsigned int i=0; i<RA->GetNRow(); i++) time_vect.push_back(tstart+(double)i/(double)sampling);

  }
  else if(RA->GetNCol()>1){
    if(!RA->SetFormat(0,'d')) return 2;
    if(!RA->SetFormat(1,'d')) return 2;
    if(!RA->GetCol(val_vect,1)) return 2;
    if(!RA->GetCol(time_vect,0));
    for(unsigned int i=0; i<time_vect.size(); i++) time_vect[i]+=tstart;
  }
  else return 2;
  delete RA;

  // sanity checks
  if(time_vect.size()<2) return 3;

  // channel sampling rate
  sampling=(unsigned int)round(1.0/(time_vect[1] - time_vect[0]));

  // printing
  cout<<"Sampling rate = "<<sampling<<" Hz"<<endl;
  cout<<"N samples = "<<time_vect.size()<<endl;
  cout<<"Duration = "<<(double)time_vect.size()/(double)sampling<<" s"<<endl;

  // create new frame
  FrameH *outframe = FrameNew((char*)"unknown");
  outframe->dt = 10.0;// frames of 10 s
  
  // create channel (32 bits)
  FrProcData *channel = FrProcDataNew(outframe,(char*)(channelname.c_str()),sampling, time_vect.size(), -32);
    
  // output file
  FrFile *outfile = FrFileONewM((char*)"outfile", 9, (char*)"gwl-txttoframe", 1024);
  if(outfile == NULL) return 4;

  unsigned int n = (unsigned int)ceil((double)time_vect.size()/(outframe->dt*sampling));
  n*=(unsigned int)(outframe->dt*sampling);
  for(unsigned int l=0; l<n; l++){

    if(l>=time_vect.size())
      channel->data->dataF[l%((int)outframe->dt*sampling)] = 0.0;

    
    // write frame to output file
    if(l&&!(l%((int)outframe->dt*sampling))){
      if(FrameWrite(outframe, outfile) != FR_OK) return 5;
    }

    // set frame time
    if(!(l%((int)outframe->dt*sampling))) outframe->GTimeS = (int)time_vect[l];

    // set channel value
    channel->data->dataF[l%((int)outframe->dt*sampling)] = val_vect[l];

  }
  if(FrameWrite(outframe, outfile) != FR_OK) return 5;

  FrFileOEnd(outfile);

  FrameFree(outframe);

  cout<<"gwl-txt-to-frame: DONE!"<<endl;
  return 0;
}

