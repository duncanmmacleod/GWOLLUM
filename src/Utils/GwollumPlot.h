/**
 * @file 
 * @brief Interface to draw and print ROOT objects.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __GwollumPlot__
#define __GwollumPlot__

#include "CUtils.h"
#include <TStyle.h>
#include <TLine.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TText.h>

using namespace std;

/**
 * @brief Interface to draw and print ROOT objects.
 * @details The GwollumPlot class provides a single interface to draw and print ROOT objects like histograms, graphs and so on.
 * It also includes different pre-defined plotting styles.
 * This class mainly includes wrappers for ROOT drawing functions.
 * Supported styles:
 * - "GWOLLUM" (default): This is the full GWOLLUM style. Backgrounds are black and deep-blue sea markings are used.
 * - "PINK": Backgrounds are black and pink markings are used.
 * - "FIRE": This style is convenient for black and white printing.
 * - "STANDARD": This is the standard ROOT style.
 *
 * @author Florent Robinet
 */
class GwollumPlot{

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the GwollumPlot class.
   * @details A unique name must be provided with 'aName'.
   * You can also choose a style between:
   * - "GWOLLUM" (default): This is the full GWOLLUM style. Backgrounds are black and deep-blue sea markings are used.
   * - "PINK": Backgrounds are black and pink markings are used.
   * - "FIRE": This style is convenient for black and white printing.
   * - "STANDARD": This is the standard ROOT style.
   *
   * @param[in] aName Name to identify the canvas.
   * @param[in] aStyleName Style name.
   */
  GwollumPlot(const string aName, const string aStyleName="GWOLLUM");

  /**
   * @brief Destructor of the GwollumPlot class.
   */
  virtual ~GwollumPlot(void);

  /**
     @}
  */

  /**
   * @brief Adds text in the plot.
   * @details The position of the plot should be given in the pad coordinates (0-1).
   * @param[in] aText Text to add.
   * @param[in] aX X coordinate where to start the text.
   * @param[in] aY Y coordinate where to start the text.
   * @param[in] aSize Text size.
   * @param[in] aPadIndex pad index when using subpads (see DivideCanvas())
   */
  void AddText(const string aText,
               const double aX, const double aY, const double aSize,
               const int aPadIndex=0);

  /**
   * @brief Updates text in the plot.
   * @sa AddText().
   * @param[in] aText Updated text.
   */
  void UpdateText(const string aText);

  /**
   * @brief Adds a legend entry.
   * @param[in] aObj Pointer to ROOT object to which the legend entry applies.
   * @param[in] aLabel Label of the legend entry.
   * @param[in] aStyle Legend style "L" for line, "P" for point and "F" for fill.
   */
  void AddLegendEntry(const TObject *aObj, const string aLabel, const string aStyle="LPF");
  
  /**
   * @brief Adds a legend header.
   * @param[in] aLabel Label of the legend header.
   */
  void AddLegendHeader(const string aLabel);

  /**
   * @brief Draws current legend box.
   */
  void DrawLegend(void);

  /**
   * @brief Resets and removes current legend box.
   */
  inline void ResetLegend(void){
    Wleg->Clear();
    Wpad->SetPad(0,0,1,1);
    Wpad->Modified(1);
    Wpad->Update();
  };
  
  /**
   * @brief Draws ROOT object.
   * @param[in] aObj Pointer to ROOT object to be drawn.
   * @param[in] aOptions Drawing options (ROOT options).
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void Draw(TObject *aObj, const string aOptions="", const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    aObj->Draw(aOptions.c_str());
    gPad->Modified();
    gPad->Update();
  };   

  /**
   * @brief UnDraws a ROOT object.
   * @param[in] aObj Pointer to ROOT object to be remove.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void UnDraw(TObject *aObj, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->GetListOfPrimitives()->Remove(aObj);
    gPad->Modified();
    gPad->Update();
  };
  
  /**
   * @brief Clear pad.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void Clear(const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->Clear();
    gPad->Modified();
    gPad->Update();
  };
  
  /**
   * @brief Prints current canvas in a file.
   * @param[in] aFileName File name to save the current canvas.
   */
  inline void Print(const string aFileName){
    Wcan->cd();
    Wcan->Print(aFileName.c_str());
  }; 

  /**
   * @brief Prints current canvas in a file with a rescaling factor.
   * @details Typically this function is useful when one wants to produce thumbnails.
   * The size of the working canvas is not changed, only the size of the printed object is modified.
   * The width and height are rescaled by 'rescale'.
   * @param[in] aFileName File name to save the current canvas.
   * @param[in] aScaleFactor Scaling factor.
   */
  void Print(const string aFileName, const double aScaleFactor);
  
  /**
   * @brief Prints current canvas in a file with a new size.
   * @details The size of the working canvas is not changed, only the size of the printed object is modified.
   * @param[in] aFileName File name to save the current canvas.
   * @param[in] aNewWidth New canvas horizontal size.
   * @param[in] aNewHeight New canvas vertical size.
   */
  void Print(const string aFileName,
             const unsigned int aNewWidth, const unsigned int aNewHeight);

  /**
   * @brief Set Lin/Log scale for X.
   * @details
   * - aValue = 0 X scale is linear.
   * - aValue = 1 X scale is logarithmic (base 10).
   * - aValue > 1 reserved for possible support of base e or otherSet logarithmic scale for x axis.
   * @param[in] aValue Value.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void SetLogx(const int aValue, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->SetLogx(aValue);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Set Lin/Log scale for Y.
   * @details
   * - aValue = 0 Y scale is linear.
   * - aValue = 1 Y scale is logarithmic (base 10).
   * - aValue > 1 reserved for possible support of base e or otherSet logarithmic scale for y axis.
   * @param[in] aValue Value.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void SetLogy(const int aValue, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->SetLogy(aValue);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Set Lin/Log scale for Z.
   * @details
   * - aValue = 0 Z scale will be linear.
   * - aValue = 1 Z scale will be logarithmic (base 10).
   * - aValue > 1 reserved for possible support of base e or otherSet logarithmic scale for z axis.
   * @param[in] aValue Value.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void SetLogz(const int aValue, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->SetLogz(aValue);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Set/Unset grid for X.
   * @param[in] aValue Value.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void SetGridx(const int aValue=1, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->SetGridx(aValue);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Set/Unset grid for Y.
   * @param[in] aValue Value
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void SetGridy(const int aValue=1, const int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->SetGridy(aValue);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Redraw the frame axis.
   * @param[in] option Options.
   * @param[in] aPadIndex Pad index when using subpads (see DivideCanvas()).
   */
  inline void RedrawAxis(Option_t* option="", int aPadIndex=0){
    Wpad->cd(aPadIndex);
    gPad->RedrawAxis(option);
    gPad->Modified();
    gPad->Update();
  }; 

  /**
   * @brief Resizes the global canvas to new dimensions.
   * @details Set Width and Height of canvas to aWidth and aHeight respectively.
   * If aWidth and/or aHeight are greater than the current canvas window a scroll bar is automatically generated.
   * Use this function to zoom in a canvas and navigate via the scroll bars.
   * @param[in] aWidth New canvas width.
   * @param[in] aHeight New canvas height.
   */
  inline void ResizePlot(const unsigned int aWidth, const unsigned int aHeight){
    Wcan->SetWindowSize(aWidth + (aWidth- Wcan->GetWw()), aHeight + (aHeight - Wcan->GetWh()));
    Wcan->SetCanvasSize(aWidth,aHeight);
    Wcan->Modified(); Wcan->Update();
  }; 
  
  /**
   * @brief Returns the canvas width.
   */
  inline int GetWidth(void){ return Wcan->GetWw(); }; 
  
  /**
   * @brief Returns the canvas height.
   */
  inline int GetHeight(void){ return Wcan->GetWh(); }; 
  
  /**
   * @brief Divides current canvas into sub-pads.
   * @param[in] aNpads Number of pads.
   */
  inline void DivideCanvas(const unsigned int aNpads){ Wpad->DivideSquare((int)aNpads); };

  /**
   * @brief Divides current canvas into sub-pads.
   * @param[in] aNx Number of columns.
   * @param[in] aNy Number of rows.
   */
  inline void DivideCanvas(const unsigned int aNx, const unsigned int aNy){ Wpad->Divide((int)aNx, (int)aNy); };

  /**
   * @brief Returns current style name.
   */
  inline string GetCurrentStyle(void){ return stylename; };
    
  /**
   * @brief Returns color number i in current palette.
   */
  inline unsigned int GetColorPalette(const unsigned int i){ return (unsigned int)Wstyle->GetColorPalette((int)i); };
    
  /**
   * @brief Returns the number of colors in current palette.
   */
  inline unsigned int GetNumberOfColors(void){ return (unsigned int)Wstyle->GetNumberOfColors(); };
    

 protected:
  
  int randid;                      ///< Random integer id.
  string srandid;                  ///< Random string id.

  // STYLE
  TStyle *Wstyle;                  ///< Working style.
  string stylename;                ///< Style name.

  // CANVAS/PAD
  TCanvas *Wcan;                   ///< Working canvas.
  TPad    *Wpad;                   ///< Working pad.

  // ELEMENTS
  TLegend *Wleg;                   ///< Plot legends.
  TText   *Wtext;                  ///< Additional text.

 private:

  /**
   * @brief Sets GWOLLUM style.
   */
  void SetGwollumStyle(void);

  /**
   * @brief Sets PINK style.
   */
  void SetPinkStyle(void);

  /**
   * @brief Sets FIRE style.
   */
  void SetFireStyle(void);

  /**
   * @brief Sets STANDARD style.
   */
  void SetStandardStyle(void);
  
  ClassDef(GwollumPlot,0)
};

#endif


