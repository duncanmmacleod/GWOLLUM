/**
 * @file 
 * @brief Program to generate a ROOT TTree structure in a ROOT file using parameters saved in a text file.
 * @snippet this gwl-txt-to-tree-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "CUtils.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    GwlPrintVersion();
    return 0;
  }

  // check the output directory
  if(argc!=4){
    //! [gwl-txt-to-tree-usage]
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [input text file] [tree name] [column headers]"<<endl; 
    cerr<<endl;
    cerr<<"  [input text file]: txt file with columns of data"<<endl; 
    cerr<<"  [tree name]      : name for output tree"<<endl; 
    cerr<<"  [column headers] : list of columns to read"<<endl; 
    cerr<<"                     name/type"<<endl; 
    cerr<<"                     I for integer"<<endl; 
    cerr<<"                     D for double"<<endl; 
    cerr<<"                     C for character string"<<endl; 
    cerr<<"                     ex: gps/D:frequency/D:SNR/D"<<endl; 
    cerr<<"                     *All* columns must be listed"<<endl; 
    cerr<<endl;
    //! [gwl-txt-to-tree-usage]
    return 1;
  }

  // get txt file
  string txtfilename=(string)argv[1];
  if(!IsTextFile(txtfilename)) return 1;

  // get txt file
  string treename=(string)argv[2];

  // get txt file
  string colhead=(string)argv[3];

  // output TTree
  cout<<argv[0]<<" create TTree..."<<endl;
  TTree *tree = new TTree(treename.c_str(),treename.c_str());

  // load txt file
  cout<<argv[0]<<" load text file..."<<endl;
  Long64_t nlines = tree->ReadFile(txtfilename.c_str(),colhead.c_str());

  // output file
  cout<<argv[0]<<" save TTree ("<<nlines<<" entries)..."<<endl;
  TFile outfile((txtfilename+".root").c_str(),"RECREATE");
  outfile.cd();
  tree->Write();
  outfile.Close();
  
  delete tree;

  return 0;
}

