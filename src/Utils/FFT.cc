/**
 * @file 
 * @brief See FFT.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "FFT.h"

////////////////////////////////////////////////////////////////////////////////////
fft::fft(const unsigned int aSize_t, const string aPlan, const string aType){
////////////////////////////////////////////////////////////////////////////////////
 
  // parameters
  fSize_t = aSize_t;
  fPlan = aPlan;
  if(aType.compare("r2c")) fType=true;
  else fType=false;

  // time-domain size
  if(fSize_t<2) fSize_t = 2;
  fSize_t = fSize_t/2*2;// must be even

  // frequency-domain size
  if(fType) fSize_f = fSize_t;
  else      fSize_f = fSize_t/2+1;// +1 = DC
    
  // allocate memory for time-domain vectors
  xc_t = NULL;
  xr_t = NULL;
  if(fType){
    xc_t = new fftw_complex [fSize_t];
    for(unsigned int i=0; i<fSize_t; i++){
      xc_t[i][0]=0.0;
      xc_t[i][1]=0.0;
    }
  }
  else{
    xr_t = new double [fSize_t];
    for(unsigned int i=0; i<fSize_t; i++) xr_t[i]=0.0;
  }

  // allocate memory for frequency-domain vector
  x_f = new fftw_complex [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++){
    x_f[i][0]=0.0;
    x_f[i][1]=0.0;
  }

  // FFT plans
  if(fType){ // c2c
    if(!fPlan.compare("FFTW_MEASURE")){
      FFT_Forward  = fftw_plan_dft_1d((int)fSize_t, xc_t, x_f, FFTW_FORWARD, FFTW_MEASURE);
      FFT_Backward = fftw_plan_dft_1d((int)fSize_t, x_f, xc_t, FFTW_BACKWARD, FFTW_MEASURE);
    }
    else if(!fPlan.compare("FFTW_PATIENT")){
      FFT_Forward  = fftw_plan_dft_1d((int)fSize_t, xc_t, x_f, FFTW_FORWARD, FFTW_PATIENT);
      FFT_Backward = fftw_plan_dft_1d((int)fSize_t, x_f, xc_t, FFTW_BACKWARD, FFTW_PATIENT);
    }
    else{
      FFT_Forward  = fftw_plan_dft_1d((int)fSize_t, xc_t, x_f, FFTW_FORWARD, FFTW_ESTIMATE);
      FFT_Backward = fftw_plan_dft_1d((int)fSize_t, x_f, xc_t, FFTW_BACKWARD, FFTW_ESTIMATE);
    }
  }
  else{ // r2c
    if(!fPlan.compare("FFTW_MEASURE")){
      FFT_Forward  = fftw_plan_dft_r2c_1d((int)fSize_t, xr_t, x_f, FFTW_MEASURE);
      FFT_Backward = fftw_plan_dft_c2r_1d((int)fSize_t, x_f, xr_t, FFTW_MEASURE);
    }
    else if(!fPlan.compare("FFTW_PATIENT")){
      FFT_Forward  = fftw_plan_dft_r2c_1d((int)fSize_t, xr_t, x_f, FFTW_PATIENT);
      FFT_Backward = fftw_plan_dft_c2r_1d((int)fSize_t, x_f, xr_t, FFTW_PATIENT);
    }
    else{
      FFT_Forward  = fftw_plan_dft_r2c_1d((int)fSize_t, xr_t, x_f, FFTW_ESTIMATE);
      FFT_Backward = fftw_plan_dft_c2r_1d((int)fSize_t, x_f, xr_t, FFTW_ESTIMATE);
    }
  }
   
}

////////////////////////////////////////////////////////////////////////////////////
fft::~fft(void){
////////////////////////////////////////////////////////////////////////////////////
  fftw_destroy_plan(FFT_Forward);
  fftw_destroy_plan(FFT_Backward);
  if(xr_t!=NULL) delete xr_t;
  if(xc_t!=NULL) delete xc_t;
  delete x_f;
}
 
////////////////////////////////////////////////////////////////////////////////////
bool fft::Forward(double *aRe_t, double *aIm_t){
////////////////////////////////////////////////////////////////////////////////////
  if(xc_t==NULL) return false;
  if(aRe_t==NULL || aIm_t==NULL) return false;
  
  // fill data in
  for(unsigned int i=0; i<fSize_t; i++){
    xc_t[i][0]=aRe_t[i];
    xc_t[i][1]=aIm_t[i];
  }

  // fft-forward
  Forward();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool fft::Forward(double *aRe_t){
////////////////////////////////////////////////////////////////////////////////////
  if(aRe_t==NULL) return false;
 
  // fill data in
  if(fType){
    for(unsigned int i=0; i<fSize_t; i++){
      xc_t[i][0]=aRe_t[i];
      xc_t[i][1]=0.0;
    }
  }
  else{
    for(unsigned int i=0; i<fSize_t; i++) xr_t[i]=aRe_t[i];
  }
  
  // fft-forward
  Forward();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool fft::Backward(double *aRe_f, double *aIm_f){
////////////////////////////////////////////////////////////////////////////////////
  if(aRe_f==NULL || aIm_f==NULL) return false;
  
  // fill data in
  for(unsigned int i=0; i<fSize_f; i++){
    x_f[i][0]=aRe_f[i];
    x_f[i][1]=aIm_f[i];
  }

  // fft-backward
  Backward();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool fft::Backward(double *aRe_f){
////////////////////////////////////////////////////////////////////////////////////
  if(aRe_f==NULL) return  false;
 
  // fill data in
  for(unsigned int i=0; i<fSize_f; i++){
    x_f[i][0]=aRe_f[i];
    x_f[i][1]=0.0;
  }

  // fft-backward
  Backward();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetNorm2_f(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++) DataOut[i]=GetNorm2_f(i) / norm;  
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetNorm2_t(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_t];
  for(unsigned int i=0; i<fSize_t; i++) DataOut[i]=GetNorm2_t(i) / norm;  
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetNorm_f(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++) DataOut[i]=GetNorm_f(i) / norm;
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetNorm_t(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_t];
  for(unsigned int i=0; i<fSize_t; i++) DataOut[i]=GetNorm_t(i) / norm;
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetRe_f(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++) DataOut[i]=GetRe_f(i) / norm;
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetRe_t(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_t];
  for(unsigned int i=0; i<fSize_t; i++) DataOut[i]=GetRe_t(i) / norm;
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetIm_f(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++) DataOut[i]=GetIm_f(i) / norm;
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetIm_t(const double aNorm){
////////////////////////////////////////////////////////////////////////////////////
  double norm = aNorm;
  if(norm==0.0) norm = 1.0;
  double *DataOut = new double [fSize_t];
  for(unsigned int i=0; i<fSize_t; i++) DataOut[i]=GetIm_t(i) / norm;
  return DataOut;
}


////////////////////////////////////////////////////////////////////////////////////
double* fft::GetPhase_f(void){
////////////////////////////////////////////////////////////////////////////////////
  double *DataOut = new double [fSize_f];
  for(unsigned int i=0; i<fSize_f; i++) DataOut[i]=GetPhase_f(i);
  return DataOut;
}

////////////////////////////////////////////////////////////////////////////////////
double* fft::GetPhase_t(void){
////////////////////////////////////////////////////////////////////////////////////
  double *DataOut = new double [fSize_t];
  for(unsigned int i=0; i<fSize_t; i++) DataOut[i]=GetPhase_t(i);
  return DataOut;
}



