The GWOLLUM package contains a set of software tools designed to perform gravitational-wave analyses.

For a full documentation, see [the GWOLLUM doxygen page](https://virgo.docs.ligo.org/virgoapp/GWOLLUM/).